/**
 * @format
 */
import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import service from './Service';
import PushNotification from 'react-native-push-notification';

AppRegistry.registerComponent(appName, () => App);
AppRegistry.registerHeadlessTask(AppRegistry.getAppKeys(),service);
PushNotification.createChannel({channelId:"ligal",
    channelName:"Ligal_Platform"
},(created)=>{
    
});
