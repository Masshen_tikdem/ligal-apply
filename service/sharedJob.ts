import * as ShopModel from '../model/local/shop';
import AsyncStorage from "@react-native-async-storage/async-storage";
import * as AgentModel from '../model/local/agent';
import * as ArticleModel from '../model/local/article';
import * as BuyModel from '../model/local/buy';
import * as ClientModel from '../model/local/client';
import * as DeviseModel from '../model/local/devise';
import * as FamilleModel from '../model/local/family';
import * as RightModel from '../model/local/right';
import * as StockModel from '../model/local/stock';
import * as SocietyWebModel from '../model/web/society';
import * as ArticleWebModel from '../model/web/article';
import * as BuyWebModel from '../model/web/buy';
import * as FamilleWebModel from '../model/web/famille';
import * as StockWebModel from '../model/web/stock';
import * as UserWebModel from '../model/web/users';
import * as DeviseWebModel from '../model/web/devise';
import * as CustomerWebModel from '../model/web/customer';
import * as RightWebModel from '../model/web/right';
import * as NotificationModel from '../model/local/notification';
import * as SubscriptionWebModel from '../model/web/subscription';
import * as SubscriptionModel from '../model/local/subscription';
import { getImageLocal, getRandomString, isEmpty } from '../Manager';
import queue,{Worker} from 'react-native-job-queue';
import {int_article,int_buy,int_category,int_customer,int_devise,int_link,int_right,int_shop,int_stock,int_user,buy as buyUpdate} from './shared';
import { Job } from 'react-native-job-queue/lib/typescript/src/models/Job';


/**
 * HERE, WE USE QUEUE FOR EXECUTE TASKS
 */


export async function devise({item,private_key,id}){
    let status=false;
    try {
        if(private_key==null){
            const web=await DeviseWebModel.store(JSON.stringify(item));
            if(web.status==200){
                await DeviseModel.update({
                    is_updated:0,
                    id:item.id,
                    private_key:web.response.private_key
                })
                status=true;
            }
        }else{
            const web=await DeviseWebModel.update(private_key,JSON.stringify(item));
            if(web.status==200){
                await DeviseModel.update({
                    is_updated:0,
                    id:id
                })
                status=true;
            }
        }
    } catch (error) {
        console.log('ERROR',error);
    }
    return status;
}

export async function customer({item,private_key,id}){
    let status=false;
    try {
        if(private_key==null){
            const web=await CustomerWebModel.store(JSON.stringify(item));
            if(web.status==200){
                await ClientModel.update({
                    id:id,
                    private_key:web.response.private_key,
                    is_updated:0
                })
                status=true;
            }
        }else{
            const web=await CustomerWebModel.update(private_key,JSON.stringify(item));
            if(web.status==200){
                const w=await ClientModel.update({
                    is_updated:0,
                    id:id
                })
                status=true;
            }
        }
    } catch (error) {
        console.log("ERROR CUSTOMER",error);
    }
    return status;
}

export async function category({item,private_key,id}){
    let status=false;
    try {
        if(private_key==null){
            const web=await FamilleWebModel.store(JSON.stringify(item));
            if(web.status==200){
                await FamilleModel.update({
                    id:id,
                    private_key:web.response.private_key,
                    is_updated:0
                })
                status=true;
            }
        }else{
            //update
            const web=await FamilleWebModel.update(private_key,JSON.stringify(item));
            if(web.status==200){
                await FamilleModel.update({
                    id:id,
                    is_updated:0
                })
                status=true;
            }
        }
    } catch (error) {
        
    }
    return status;
}

export async function article({item,private_key,id}){
    let status=false;
    try {
        if(private_key==null){
            const web=await ArticleWebModel.store(JSON.stringify(item));
            if(web.status==200){
                await ArticleModel.update({
                    id:id,
                    is_updated:0,
                    private_key:web.response.private_key,
                });
                status=true;
            }
        }else{
            const web=await ArticleWebModel.update(private_key,JSON.stringify(item));
            if(web.status==200){
                await ArticleModel.update({
                    id:id,
                    is_updated:0,
                });
                status=true;
            }
            console.log('ARTICLE RESPONSE',web);
        }
    } catch (error) {
        console.log('ERROR',error);
    }
    return status;
}

export async function stock({item,private_key,id}){
    let status=false;
    try {
        if(private_key==null){
            const web=await StockWebModel.store(JSON.stringify(item));
            if(web.status==200){
                StockModel.update({
                    id:id,
                    private_key:web.response.private_key
                })
                status=true;
                console.log('NORMAL');   
            }else{
                console.log('NET');   
            }
        }
    } catch (error) {
        console.log('ERROR',error);   
    }
    return status;
}

export function configureJob(){
    try {
        queue.configure({
            onQueueFinish:(executedJobs:Job<Object>[])=>{
                console.log('JOB FINISHED',executedJobs);
            },
        });
        queue.getJobs().then(rep=>{
            console.log('QUEUE LIST',rep.length);
        })
        queue.addWorker(new Worker("service",async(payload)=>{
            return new Promise((resolve,reject) => {
            setTimeout(async() => {
                let rep=false;
                if(payload!=null && payload!=undefined){
                    const{id,item,type,private_key}=payload;
                    switch (type) {
                        case "BUY":
                            rep=await buy({id,item,private_key});
                            rep==true?resolve(1):reject({error:"connection"});
                        break;
                        case "DEVISE":
                            rep=await devise({item,id,private_key});
                            rep==true?resolve(1):reject({error:"connection"});
                        break;
                        case "CUSTOMER":
                            rep=await customer({id,item,private_key});
                            rep==true?resolve(1):reject({error:"connection"});
                        break;
                        case "CATEGORY":
                            rep=await category({id,item,private_key});
                            rep==true?resolve(1):reject({error:"connection"});
                        break;
                        case "ARTICLE":
                            rep=await article({id,item,private_key});
                            rep==true?resolve(1):reject({error:"connection"});
                        break;
                        case "STOCK":
                            rep=await stock({id,item,private_key});
                            rep==true?resolve(1):reject({error:"connection"});
                        break;
                        case "USER":
                            rep=await user({user:item});
                            rep==true?resolve(1):reject({error:"connection"});
                        break;
                        case "LINK":
                            /*rep=await link({id_link:id,private_key,});
                            rep==true?resolve(1):reject({error:"connection"});*/
                        break;
                        default:
                            break;
                    }
                }
            }, payload.delay);});
       }))
    } catch (error) {
        
    }
}

export async function buy({item,private_key,id}){
    let response=false;
    try {
        if(private_key==null){
            const web=await BuyWebModel.store(JSON.stringify(item));
            if(web.status==200){
                const rep=await BuyModel.update({
                    id:id,
                    private_key:web.response.private_key
                });
                if(isEmpty(item.customer) && item.id_customer>0){
                    if(rep!=null){
                        buyUpdate(rep);
                    }  
                }
                response=true;
            }
        }else{
            const web=await BuyWebModel.update(private_key,JSON.stringify(item));
            if(web.status==200){
                response=true;
            }
        }
    } catch (error) {
    }
    return response;
}

export async function user({user}){
    let status=false;
    try {
        const item:int_user={};
            item.first_name=user.first_name;
            item.last_name=user.last_name;
            item.sex=user.sex;
            item.address=user.address;
            item.born_date=null;
            item.password=null;
            
            const web=await UserWebModel.update(user.private_key,JSON.stringify(item));
            let setImage=false;
            if(web.status==200){
                await AgentModel.update({
                    id:user.id,
                    is_updated:0
                });
                status=true;
                if(user.image_updated==1){
                    setImage=true;
                    const uri=getImageLocal(user.photo);
                    const type="image/jpeg";
                    const name=user.photo;
                    const data=new FormData();
                    data.append('photo',{uri,type,name},"ligal.png");
                    const webImage=await UserWebModel.updatePhoto(user.private_key,data);
                    if(webImage.status==200){
                        await AgentModel.update({
                            id:user.id,
                            is_updated:0,
                            image_updated:0
                        });
                        status=true;
                    }
                }

            }
    } catch (error) {
        
    }
    return status;
}

export async function work({}){

}

export async function right({item,private_key,id}){
    try {
        if(private_key==null){
            const web=await RightWebModel.store(JSON.stringify(item));
            if(web.status==200){
                await RightModel.update({
                    id:id,
                    private_key:web.response.private_key,
                    is_updated:0,
                })
            }
        }else{
            const web=await RightWebModel.update(private_key,JSON.stringify(item));
            if(web.status==200){
                await RightModel.update({
                    id:id,
                    is_updated:0
                })
            }
        }
    } catch (error) {
        console.log('ERROR',error);
    }
}

export async function link({id_link,link_private_key,private_key,category_private_keys,user,link_status}){
    let status=false;
    try {
        if(id_link>0 && link_private_key==null){
            //store
            const item:int_link=null;
            item.sub=private_key;
            item.high=category_private_keys;
            item.user=user.private_key;
            const web=await FamilleWebModel.storeLink(JSON.stringify(item));
            if(web.status==200){
                await FamilleModel.updateLink({
                    id:id_link,
                    private_key:web.response.private_key,
                    is_updated:0
                })
                status=true;
            }
        }else{
            //update
            const item:int_link=null;
            item.sub=private_key;
            item.high=category_private_keys;
            item.status=link_status==1?true:false;
            item.user=user.private_key;
            const web=await FamilleWebModel.updateLink(link_private_key,JSON.stringify(item));
            if(web.status==200){
                await FamilleModel.update({
                    id:id_link,
                    is_updated:0
                })
                status=true;
            }
        }
    } catch (error) {
        console.log('ERROR',error);
    }
    return status;
}

export async function startQueue(){
    await queue.start();
    const rep=await queue.getJobs();
    console.log('START',rep.length);
}
