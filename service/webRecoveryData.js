//web model
import * as ArticleWebModel from '../model/web/article';
import * as BuyWebModel from '../model/web/buy';
import * as CategoryWebModel from '../model/web/famille';
import * as CustomerWebModel from '../model/web/customer';
import * as StockWebModel from '../model/web/stock';
import * as DeviseWebModel from '../model/web/devise';
import * as UserWebModel from '../model/web/users';
//import * as WorkWebModel from '../model/web/';
import * as RightWebModel from '../model/web/right';
import * as ShopWebModel from '../model/web/society';

//local model
import * as ArticleLocalModel from '../model/local/article';
import * as BuyLocalModel from '../model/local/buy';
import * as CategoryLocalModel from '../model/local/family';
import * as CustomerLocalModel from '../model/local/client';
import * as StockLocalModel from '../model/local/stock';
import * as DeviseLocalModel from '../model/local/devise';
import * as UserLocalModel from '../model/local/agent';
//import * as WorkWebModel from '../model/web/';
import * as RightLocalModel from '../model/local/right';
import * as ShopLocalModel from '../model/local/shop';
import { downloadImage } from '../Manager';




export async function article(list=[],articles=[],devises=[],familles=[]){
    let response=false;
    try {
        const shop=await ShopLocalModel.get();
        const onRecover=async(value,k)=>{
            let id_famille=0;
            let id_devise=0;
            let id=0;
            const onEvent=async(p,index)=>{
                if(p.private_key==value.private_key){
                    id=p.id;
                }
            }
            const onEventFamille=async(p,index)=>{
                if(p.private_key==value.famille_private_key){
                    id_famille=p.id;
                }
            }
            const onEventDevise=async(p,index)=>{
                if(p.private_key==value.devise_private_key){
                    id_devise=p.id;
                }
            }
            if(value!=null){
                await articles.map(onEvent);
                await devises.map(onEventDevise);
                await familles.map(onEventFamille);
                if(value!=null && value!=undefined){
                    if(id==0){
                        //add new article
                        if(id_devise>0 && id_famille>0){
                            response=await ArticleLocalModel.store({
                                description:value.description,
                                devise:id_devise,
                                famille:id_famille,
                                name:value.name,
                                price:value.price,
                                private_key:value.private_key,
                                quantifiable:value.quantifiable,
                                shop:shop.id,
                                status:value.status,
                                type:value.type,
                                id_devise:id_devise,
                                is_updated:0,
                                image_updated:0,
                                mode:value.mode,
                                stockage:value.stockage
                            })
                        }
                    }else{
                        //update article
                        response=await ArticleLocalModel.update({
                            id:id,
                            description:value.description,
                            devise:id_devise>0?id_devise:null,
                            famille:id_famille>0?id_famille:null,
                            is_updated:0,
                            name:value.name,
                            price:value.price,
                            quantifiable:value.quantifiable,
                            status:value.status,
                            type:value.type,
                            id_devise:id_devise,
                            image_updated:0,
                            mode:value.mode,
                            stockage:value.stockage
                        })
                    }
                }
            }
        }
        await list.map(onRecover); 
    } catch (error) {
        
    }
    return response;
}

export async function category(list=[],familles=[]){
    let response=false;
    try {
        const onRecover=async(value,k)=>{
            let id=0;
            const onEvent=async(p,index)=>{
                if(p.private_key==value.private_key){
                    id=p.id;
                }
            }
            if(value!=null){
                if(value!=null && value!={} && value!=undefined){
                    await familles.map(onEvent);
                    if(id==0){
                        response=await CategoryLocalModel.store({
                            is_updated:0,
                            name:value.name,
                            private_key:value.private_key,
                            status:value.status,
                            type:value.type,
                        });
                    }else{
                        response=await CategoryLocalModel.update({
                            id:id,
                            is_updated:0,
                            name:value.name,
                            type:value.type,
                            status:value.status,
                        });
                    }
                }
            }
        }
        await list.map(onRecover);
    } catch (error) {
        
    }
    return response;
}

export async function link(key){
    let response=false;
    
    try {
        const familles=await CategoryLocalModel.get();
        const web=await CategoryWebModel.showLink(key);
        let value=null;
        let id=0;
        let id_high=0;
        let id_sub=0;
        const onEvent=async(p,index)=>{
            if(p.category_private_keys==value.high_private_key){
                id_high=p.category_id;
            }
            if(p.private_key==value.sub_private_key){
                id_sub=p.id;
            }
            if(p.link_private_key==value.private_key){
                id=p.id_link;
            }
        }
        if(web.status==200){
            value=web.response;
            if(value!=null && value!=undefined){
                await familles.map(onEvent);
                if(id==0){
                    if(id_hight>0 && id_sub>0){
                       response=await CategoryLocalModel.storeLink({
                            high_family:id_high,
                            sub_family:id_sub,
                            is_updated:0,
                            private_key:value.private_key,
                            status:value.status
                        })
                    }
                }else{
                    if(id_high>0 && id_sub>0){
                        response=await CategoryLocalModel.updateLink({
                            high_family:id_high,
                            sub_family:id_sub,
                            is_updated:0,
                            id:id,
                            status:value.status
                        });
                    }
                }
            }
        }
    } catch (error) {
        
    }
    return response;
}


export  async function buy(list=[],agents=[],articles=[],devises=[],buys=[],customers=[]){
    let response=false;
    try {
        const onRecover=async(value,k)=>{
            let id_article=0;
            let id_agent=0;
            let id_devise=0;
            let id_customer=0;
            let id=0;
            const onEventUser=(p,index)=>{
                if(p.private_key==value.agent_private_key){
                    id_agent=p.id;
                }
            }
            const onEventArticle=(p,index)=>{
                if(p.private_key==value.article_private_key){
                    id_article=p.id;
                }
            }
            const onEventDevise=(p,index)=>{
                if(p.private_key==value.devise_private_key){
                    id_devise=p.id;
                }
            }
            const onEventCustomer=(p,index)=>{
                if(p.private_key==value.customer_private_key){
                    id_customer=p.id;
                }
            }
            const onEventBuy=(p,index)=>{
                if(p.private_key==value.private_key){
                    id=p.id;
                }
            }
            if(value!=null){
                if(value!=null && value!=undefined){
                    await articles.map(onEventArticle);
                    await devises.map(onEventDevise);
                    await customers.map(onEventCustomer);
                    await agents.map(onEventUser);
                    await buys.map(onEventBuy);
                    if(id==0 && id_devise>0 && id_agent>0 && id_article>0){
                        response=await BuyLocalModel.store({
                            created_at:value.created_at,
                            id_agent:id_agent,
                            id_article:id_article,
                            id_client:id_customer,
                            id_devise:id_devise,
                            is_alter:value.is_alter,
                            price:value.price,
                            private_key:value.private_key,
                            quantity:value.quantity,
                            reference:value.reference,
                            remise:value.remise,
                            status:value.status,
                            description:value.description,
                            facture:value.bill
                        })
                    }
                }
            }
        }
        await list.map(onRecover);
    } catch (error) {
        
    }
    return response;
}

export async function customer(list=[],customers=[]){
    let response=false;
    try {
        const onRecover=async(value,k)=>{
            let id=0;
            const onEvent=async(p,index)=>{
                if(p.private_key==value.private_key){
                    id=p.id;
                }
            }
            if(value!=null){
                await customers.map(onEvent);
                if(value!=null && value!=undefined){
                    if(id==0){
                        response=await CustomerLocalModel.store({
                            name:value.name,
                            phone:value.phone,
                            private_key:value.private_key
                        })
                    }else{
                        response=await CustomerLocalModel.update({
                            name:value.name,
                            phone:value.phone,
                            id:id,
                        });
                    }
                }
            }
        }
        await list.map(onRecover);
    } catch (error) {
        
    }
    return response;
}

export async function devise(list=[],devises=[]){
    let response=false;
    try {
        const onRecover=async(value,k)=>{
            let id=0;
            const onEvent=async(p,index)=>{
                if(p.private_key==value.private_key){
                    id=p.id;
                }
            }
            if(value!=null){
                await devises.map(onEvent);
                if(value!=null && value!=undefined){
                    if(id==0){
                        response=await DeviseLocalModel.store({
                            name:value.name,
                            private_key:value.private_key,
                            signe:value.signe,
                            status:value.status,
                            type:value.type,
                            is_updated:0
                        });
                    }else{
                        response=await DeviseLocalModel.update({
                            id:id,
                            is_updated:0,
                            name:value.name,
                            signe:value.signe,
                            status:value.status,
                            type:value.type
                        });
                    }
                }
            }
        }  
        await list.map(onRecover);
    } catch (error) {
        
    }
    return response;
}

export async function stock(list=[],articles=[],stocks=[],agents=[],devises=[]){
    let response=false;
    try {
        const onRecover=async(value,k)=>{
            let id_article=0;
            let id=0;
            let id_agent=0;
            let id_devise;
            const onEvent=(p,index)=>{
                if(p.private_key==value.private_key){
                    id=p.id;
                }
            }
            const onEventArticles=(p,index)=>{
                if(p.private_key==value.article_private_key){
                    id_article=p.id;
                }
            }
            const onEventUser=(p,index)=>{
                if(p.private_key==value.agent_private_key){
                    id_agent=p.id;
                }
            }
            const onEventDevise=(p,index)=>{
                if(p.private_key==value.devise_private_key){
                    id_devise=p.id;
                }
            }

            if(value!=null){
                if(value!=null && value!=undefined){
                    await stocks.map(onEvent);
                    await articles.map(onEventArticles);
                    await agents.map(onEventUser);
                    await devises.map(onEventDevise);
                    if(id==0 && id_article>0 && id_agent>0){
                        response=await StockLocalModel.store({
                            created_at:value.created_at,
                            fournisseur:value.fournisseur,
                            id_agent:id_agent,
                            id_article:id_article,
                            private_key:value.private_key,
                            quantity:value.quantity,
                            id_devise:id_devise>0?id_devise:null
                        });
                    }
                }   
            }
        }
        await list.map(onRecover);
    } catch (error) {
        console.log('stock error',error);
    }
    return response;
}

export async function user(key,image_updated=false){
    let response=false;
    try {
        if(key==undefined){
            return false;
        }
        const agents=await UserLocalModel.get();
        const web=await UserWebModel.show(key);
        let id=0;
        let value=null;
        const onEvent=(p,index)=>{
            if(p.private_key==value.private_key){
                id=p.id;
            }
        }
        if(web.status==200){
            value=web.response;
            await agents.map(onEvent);
            if(value!=undefined && value!=null){
                if(id==0){
                    response=await UserLocalModel.store({
                        address:value.address,
                        email:value.email,
                        first_name:value.first_name,
                        is_updated:0,
                        last_name:value.last_name,
                        password:value.password,
                        phone:value.phone,
                        private_key:value.private_key,
                        sexe:value.sexe,
                        status:value.status
                    });
                    if(response==true && image_updated==true){
                        const rep=await UserLocalModel.last();
                        await downloadUserImage(value,rep.id);
                    }
                }else{
                    response=await UserLocalModel.update({
                        address:value.address,
                        email:value.email,
                        first_name:value.first_name,
                        last_name:value.last_name,
                        id:id,
                        password:value.password,
                        is_updated:0,
                        sexe:value.sexe,
                        status:value.status
                    })
                    if(response==true && image_updated==true){
                        await downloadUserImage(value,id);
                    }
                }
            }
        }
    } catch (error) {
        console.log('error on user',error);
    }
    return response;
}

export function work(key){
    let response=false;
    try {
        
    } catch (error) {
        
    }
    return response;
}

export function right(key){
    let response=false;
    try {
        
    } catch (error) {
        
    }
    return response;
}

export function subscription(key){
    let response=false;
    try {
        
    } catch (error) {
        
    }
    return response;
}

async function downloadUserImage(item,id){
    try {
        if(item.photo!=null && item.photo!=undefined){
            const photo=getImage(item.photo);
            const imageName="user"+id+item.private_key+'.ligal';
            const load=await downloadImage(imageName.toLowerCase(),photo,null);
            if(load==true){
                await UserLocalModel.update({
                    id:id,
                    photo:imageName.toLowerCase()
                })
            }
        }
    } catch (error) {
        
    }
}