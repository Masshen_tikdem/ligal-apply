import queue from 'react-native-job-queue';
import * as Shared from './sharedJob';
import * as DeviseModel from '../model/local/devise';

/**
 * HERE, WE RANGE DATA INTO QUEUE
 */

export interface int_devise{
    name?:string,
    signe?:string,
    society?:string,
    type?:boolean,
    user?:string,
    status?:boolean
}
export interface int_customer{
    name?:string,
    society?:string,
    user?:string,
    phone?:string,
}
export interface int_category{
    name?:string,
    society?:string,
    type?:string,
    user?:string,
    status?:boolean
}
export interface int_article{
    devise?:string;
    famille?:string;
    name?:string;
    description?:string;
    price?:number;
    type?:string;
    quantifiable?:boolean;
    mode?:string;
    user?:string;
    stockage?:string;
}
export interface int_stock{
    user?:string,
    article?:string,
    quantity?:string,
    provider?:string,
    created_at?:string,
    devise?:string;
}
export interface int_buy{
    article?:string,
    user?:string,
    devise?:string,
    price?:number,
    is_alter?:boolean;
    created_at?:string,
    quantity?:number,
    remise?:number,
    reference?:string,
    bill?:string,
    description?:string,
    customer?:string,
    id_customer?:number
}
export interface int_user{
    first_name?:string,
    last_name?:string,
    sex?:string,
    email?:string,
    address?:string,
    born_date?:string
    password?:string
    private_key?:string,
    image_updated?:number,
    photo?:string
}
export interface int_right{
    society?:string,
    user?:string,
    code?:string,
    value?:string
}
export interface int_link{
    sub?:string
    high?:string
    status?:boolean
    user?:string
}
export interface int_shop{

}




export async function devise({private_key,name,signe,shop,type,user,id,status}){
    try {
        if(private_key==null){
            const item:int_devise={};
            item.name=name;
            item.signe=signe;
            item.society=shop.private_key;
            item.type=type;
            item.user=user.private_key;
            addJob("service",item,private_key,id,2,0,2000,"DEVISE")
        }else{
            const item:int_devise={};
            item.name=name;
            item.signe=signe;
            item.user=user.private_key;
            item.type=type==1;
            item.status=status==1?true:false;
            addJob("service",item,private_key,id,2,0,2000,"DEVISE")
        }
    } catch (error) {
        console.log('ERROR',error);
    }
}

export async function customer({private_key,name,shop,phone,id,user}){
    try {
        if(private_key==null){
            const item:int_customer={};
            item.name=name;
            item.society=shop.private_key;
            item.user=user.private_key;
            item.phone=phone;
            addJob("service",item,private_key,id,5,0,1000,"CUSTOMER");
        }else{
            const item:int_customer={};
            item.name=name;
            item.phone=phone;
            addJob("service",item,private_key,id,5,0,1000,"CUSTOMER");
        }
    } catch (error) {
        console.log("ERROR CUSTOMER CATCH",error);
    }
}

export async function category({private_key,name,shop,type,user,status,id}){
    try {
        if(private_key==null){
            //store
            const item:int_category={};
            item.name=name;
            item.society=shop.private_key;
            item.type=type;
            item.user=user.private_key;
            item.status=status==1?true:false;
            addJob("service",item,private_key,id,1,0,1500,"CATEGORY")
        }else{
            //update
            const item:int_category={};
            item.name=name;
            item.type=type;
            item.status=status==1?true:false;
            item.user=user.private_key;
            addJob("service",item,private_key,id,1,0,1500,"CATEGORY")
        }
    } catch (error) {
        
    }
}

export async function article({private_key,devise_private_key,famille_private_key,name,description,price,type,quantifiable,mode,user,stockage,id}){
    try {
        if(private_key==null){
            const item:int_article={};
            item.devise=devise_private_key;
            item.famille=famille_private_key;
            item.name=name;
            item.description=description;
            item.price=price;
            item.type=type;
            item.quantifiable=quantifiable;
            item.mode=mode;
            item.user=user.private_key;
            item.stockage=stockage;
            addJob("service",item,private_key,id,3,0,2000,"ARTICLE")
        }else{
            const item:int_article={};
            item.devise=devise_private_key;
            item.famille=famille_private_key;
            item.name=name;
            item.description=description;
            item.price=price;
            item.type=type;
            item.quantifiable=quantifiable;
            item.user=user.private_key;
            addJob("service",item,private_key,id,3,0,1500,"ARTICLE")
        }
    } catch (error) {
        console.log('ERROR',error);
    }
}

export async function stock({private_key,agent_private_key,article_private_key,quantity,fournisseur,created_at,id_devise,id}){
    try {
        if(private_key==null){
            const devises=await DeviseModel.get();
                const getDevice=(value)=>{
                    let rep=null;
                    devises.map(p=>{
                        if(id==value){
                            rep=private_key;
                        }
                    })
                    return rep;
            }
            const item:int_stock={};
            item.user=agent_private_key;
            item.article=article_private_key;
            if(quantity>0){
                item.quantity=quantity;
            }else{
                item.quantity=quantity[0].stock
            }
            item.provider=fournisseur;
            item.created_at=created_at;
            item.devise=getDevice(id_devise);
            addJob("service",item,private_key,id,4,0,2000,"STOCK");
        }
    } catch (error) {
        console.log('ERROR',error);   
    }
}

function addJob(key:string,item:any,private_key:string,id?:number,priority:number=5,attempts:number=0,timeout:number=5000,type?:string){
    queue.addJob("service",{item,private_key,id,type},{priority:0,attempts:0,timeout:timeout},true);
}




export async function buy({private_key,article_private_key,agent_private_key,devise_private_key,price,is_alter,created_at,quantity,remise,reference,bill,description,customer_private_key,id,id_customer}){
    try {
        const item:int_buy={};
        item.article=article_private_key;
        item.user=agent_private_key;
        item.devise=devise_private_key;
        item.price=price;
        item.is_alter=is_alter==1?true:false;
        item.created_at=created_at;
        item.quantity=quantity;
        item.remise=remise;
        item.reference=reference;
        item.bill=bill;
        item.description=description;
        if(customer_private_key!=null){
            item.customer=customer_private_key;
        }
        if(customer!=null){
            item.id_customer=id_customer;
        }
        addJob("service",item,private_key,id,6,0,1000,"BUY");
    } catch (error) {
        console.log('GRAND ERROR',error);
    }
}

export async function user({user}){
    try {
        if(user.is_updated==1){
            const item:int_user={};
            item.first_name=user.first_name;
            item.last_name=user.last_name;
            item.sex=user.sex;
            item.address=user.address;
            item.born_date=null;
            item.password=null;
            item.private_key=user.private_key;
            if(user.image_updated==1){
                item.image_updated=1;
                item.photo=user.photo;
            }
            addJob("service",item,user.private_key,user.id,0,0,2000,"USER");
        }
    } catch (error) {
        
    }
}

export async function work({}){

}


export async function link({id_link,link_private_key,private_key,category_private_keys,user,link_status}){
    try {
        if(id_link>0 && link_private_key==null){
            //store
            const item:int_link={};
            item.sub=private_key;
            item.high=category_private_keys;
            item.user=user.private_key;
            addJob("service",item,private_key,id_link,0,0,1000,"LINK");
        }else{
            //update
            const item:int_link={};
            item.sub=private_key;
            item.high=category_private_keys;
            item.status=link_status==1?true:false;
            item.user=user.private_key;
            addJob("service",item,private_key,id_link,0,0,1000,"LINK");
        }
    } catch (error) {
        console.log('ERROR',error);
    }
}