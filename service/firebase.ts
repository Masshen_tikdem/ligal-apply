import { FirebaseDatabaseTypes } from "@react-native-firebase/database";
import { capitalize } from "../Manager";
import * as WebData from './webData';
import S from '../resources/settings.json';
import database from '@react-native-firebase/database';
import * as ShopModel from '../model/local/shop';
import PushNotification from 'react-native-push-notification';


function updateFirebase(key,item,user){
    try {
        const data=database().ref('users/'+user.private_key+'/'+key);
        data.update(item);
    } catch (error) {
        
    }
}

function showNotification({id,title="",content="",channelId}){
    console.log('ID',id);
    try {
        PushNotification.localNotification({
            message:content,
            id:id,
            title:title,
            //bigText:content,
            smallIcon:"ligal",
            largeIcon:'ligal',
            //largeIconUrl:"../assets/logo/ligal.png",
            //bigPictureUrl:"../assets/logo/ligal.png",
            //bigLargeIcon: "ligal", // (optional) default: undefined
            //bigLargeIconUrl: "https://www.example.tld/bigicon.jpg",
            channelId:"ligal",
            //color:'transparent'
        });
    } catch (error) {
        console.log('error',error);
    }
}

export function treatAlert(snapshot:FirebaseDatabaseTypes.DataSnapshot,user,shop,dispatch){
    try {
        if(snapshot.exists()){
            snapshot.forEach(async p=>{
                const key=p.key;//the key of message
                const type=p.val().type;//the type of action
                const sender=p.val().sender;//the sender of this message
                const value=p.val().value;//the private key of information type
                const message=p.val().message;//the message to show 
                const title=p.val().title;//the title of message
                const view=p.val().view;//if the message is view or not
                const used=p.val().used;//if this message is used or not
                const category=p.val().category;//phone,web or desktop
                const image=p.val().image;//if the info is about image also
                const society=p.val().shop;
                const type_info=capitalize(type).toLowerCase();
                let treat=null;
                let action=null;
                if(view==false && society==shop.private_key && capitalize(category).toLowerCase()=="phone"){
                    switch (type_info) {
                        case S.ENUM.WebAction.category:
                            WebData.category(value).then(treat=>{
                                if(treat==true){
                                    updateFirebase(key,{view:true,used:true},user);
                                    action={type:S.EVENT.onChangeSaleInfo,category:true}
                                    dispatch(action);
                                }
                            });
                        break;

                        case S.ENUM.WebAction.link:
                            WebData.link(value).then(treat=>{
                                if(treat==true){
                                    updateFirebase(key,{view:true,used:true},user)
                                    action={type:S.EVENT.onChangeSaleInfo,category:true}
                                    dispatch(action);
                                }
                            });
                            
                        break;

                        case S.ENUM.WebAction.article:
                            WebData.article(value).then(treat=>{
                                if(treat==true){
                                    updateFirebase(key,{view:true,used:true},user)
                                    action={type:S.EVENT.onChangeSaleInfo,article:true}
                                    dispatch(action);
                                }
                            });
                            
                        break;

                        case S.ENUM.WebAction.stock:
                            WebData.stock(value).then(treat=>{
                                if(treat==true){
                                    updateFirebase(key,{view:true,used:true},user)
                                    action={type:S.EVENT.onChangeSaleInfo,article:true}
                                    dispatch(action);
                                }
                            });
                        break;

                        case S.ENUM.WebAction.devise:
                            await WebData.devise(value).then(treat=>{
                                if(treat==true){
                                    updateFirebase(key,{view:true,used:true},user);
                                    action={type:S.EVENT.onChangeSaleInfo,devise:true}
                                    dispatch(action);
                                }
                            });
                        break;

                        case S.ENUM.WebAction.customer:
                            await WebData.customer(value).then(treat=>{
                                if(treat==true){
                                    updateFirebase(key,{view:true,used:true},user)
                                }
                            });
                        break;

                        case S.ENUM.WebAction.buy:
                            await WebData.buy(value).then(treat=>{
                                if(treat==true){
                                    updateFirebase(key,{view:true,used:true},user);
                                    action={type:S.EVENT.onChangeSaleInfo,article:true}
                                    dispatch(action);
                                }
                            });
                        break;

                        case S.ENUM.WebAction.right:
                            WebData.right(value).then(treat=>{
                                if(treat==false){
                                    updateFirebase(key,{view:true,used:true},user)
                                    this.setRights();
                                }
                            });
                        break;

                        case S.ENUM.WebAction.user:
                            WebData.user(value,true).then(treat=>{
                                if(treat==true){
                                    updateFirebase(key,{view:true,used:true},user);
                                }
                            });
                        break;

                        case S.ENUM.WebAction.subscription:
                            WebData.subscription(value).then(treat=>{
                                if(treat==true){
                                    updateFirebase(key,{view:true,used:true},user);
                                }
                            });
                        break;

                        case S.ENUM.WebAction.work:
                            WebData.work(value).then(treat=>{
                                if(treat==true){
                                    updateFirebase(key,{view:true,used:true},user);
                                }
                            });
                        break;

                        case S.ENUM.WebAction.shop:
                            WebData.shop(value).then(treat=>{
                                if(treat==true){
                                    updateFirebase(key,{view:true,used:true},user);
                                    ShopModel.get().then(p=>{
                                        action={type:S.EVENT.onChangeShop,value:p}
                                        dispatch(action);
                                    })
                                }
                            });
                        break
                    
                        default:
                            break;
                    }
                }
            })
        }
    } catch (error) {
        console.log('FIREBASE ERROR');
    }
}