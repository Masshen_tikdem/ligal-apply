import * as ArticleModel from '../model/local/article';
import * as BuyModel from '../model/local/buy';
import * as StockModel from '../model/local/stock';
import * as CustomerModel from '../model/local/client';
import * as NotificationModel from "../model/local/notification";
import { capitalize, getArticleName, getLink, isEmpty } from '../Manager';
import S from '../resources/settings.json';
import DateTime from 'date-and-time';
import { groupBy } from 'lodash';


export async function checkArticles(){
    try {
        const rep=await ArticleModel.get();
        let content="";
        let title="";
        let list=await NotificationModel.get();
        list=list.filter(p=>p.link.search("article")>=0);
        list=groupBy(list,"link");
        const date=DateTime.format(new Date(),"YYYY-MM-DD HH:mm:ss");
        const onEvent=async(item,index)=>{
            if(item.type!="service" && item.buy>0){
                const stock=getStock(item);
                if(stock<5){
                    content="Le produit ";
                    content+=getArticleName(item.name,item.description)+" ";
                    content+="a un faible approvisionnement";
                    title="faible approvisionnement";
                    let items=list[getLink(item.id,"article")];
                    if(items==undefined){
                        items=[];
                    }
                    let passer=true;
                    const elements=items.filter(p=>capitalize(p.status).toLowerCase()==S.GENERAL.STATUS.CANCELED.toLowerCase());
                    if(elements.length>0){
                        passer=false;
                    }
                    //check date
                    if(passer){
                        items=items.sort((a,b)=>a.created_at-b.created_at);
                        if(items.length>0){
                            const date=DateTime.parse(items[0].created_at,"YYYY-MM-DD HH:mm:ss");
                            const today=new Date();
                            const time=date.getTime()+86400*1000*5;
                            if(time>today.getTime()){
                                passer=false;
                            }
                        }
                    }
                    if(passer==true){
                        NotificationModel.store({
                            content:content,
                            link:getLink(item.id,"article"),
                            id_item:item.id,
                            status:S.GENERAL.STATUS.SEND,
                            created_at:date,
                            table:"article",
                            title:title,
                            type:S.GENERAL.TYPE.STOCK
                        })
                    }
                }
            }
        }
        await Promise.all(rep.map(onEvent));
    } catch (error) {
        console.log('error',error);
    }
}

export async function checkBuys(){
    try {
        const rep=await ArticleModel.get();
        let content="";
        let title="";
        let list=await NotificationModel.get();
        list=list.filter(p=>p.link.search("article")>=0);
        list=groupBy(list,"link");
        const date=DateTime.format(new Date(),"YYYY-MM-DD HH:mm:ss");
        const onEvent=async(item,index)=>{
            const stock=getStock(item);
            const buy=await BuyModel.show(item.id);
            if(buy!=null){
                if(buy.length>0){
                    const last_date_string=buy[0].created_at;
                    const last_date_date=DateTime.parse(last_date_string,"YYYY-MM-dd HH:mm:ss");
                    const created_at=DateTime.addDays(last_date_date,10);
                    const time1=(new Date()).getTime();
                    const time2=created_at.getTime();
                    if(time1>time2){
                        const items=await NotificationModel.show(item.id,"article");
                        content=item.type=="service"?"Le service ":"Le produit ";
                        content+=getArticleName(item.name,item.description)+" ";
                        content="est enregistré en vente depuis très longtemps ! Essayez de suivre son évolution de près.";
                        title="Article n’est plus vendu";
                        let passer=true;
                        for(var x of items){
                            const status=capitalize(x.status).toLowerCase();
                            const type=capitalize(x.type).toLowerCase();
                            const db_date_string=x.created_at;
                            const db_date=DateTime.parse(db_date_string,"YYYY-MM-DD HH:mm:ss");
                            const ref_date=DateTime.addDays(new Date(),10);
                            if((status==capitalize(S.GENERAL.STATUS.CANCELED).toLowerCase() || status==capitalize(S.GENERAL.STATUS.READED).toLowerCase() && capitalize(S.GENERAL.TYPE.BUY).toLowerCase()==type) ){
                                passer=false;
                            }
                        }
                        if(passer==true){
                            NotificationModel.store({
                                content:content,
                                link:getLink(item.id,"article"),
                                id_item:item.id,
                                status:S.GENERAL.STATUS.SEND,
                                created_at:date,
                                table:"article",
                                title:title,
                                type:S.GENERAL.TYPE.BUY
                            })
                        }
                    }else{

                    }
                }else{

                }
            }
        }
        await rep.map(onEvent);
    } catch (error) {
        console.log('error',error);
    }
}

export async function checkCustommer(){
    try {
        const rep=CustomerModel.get();
        const date=DateTime.format(new Date(),"YYYY-MM-DD HH:mm:ss");
        const onEvent=async(item,index)=>{
            if(item.private_key==null){
                const items=await NotificationModel.show(item.id,"customer");
                let passer=true;
                for(var x of items){
                    const status=capitalize(x.status).toLowerCase();
                    const type=capitalize(x.type).toLowerCase();
                    if((status==capitalize(S.GENERAL.STATUS.CANCELED).toLowerCase() || status==capitalize(S.GENERAL.STATUS.READED).toLowerCase() && capitalize(S.GENERAL.TYPE.BUY).toLowerCase()==type) ){
                        passer=false;
                    }
                }
            }
        }
    } catch (error) {
        
    }
}

export async function checkCustommerIdentity(){
    try {
        const rep=CustomerModel.get();
        let content="";
        let title="";
        let list=await NotificationModel.get();
        list=list.filter(p=>p.link.search("customer")>=0);
        list=groupBy(list,"link");
        const date=DateTime.format(new Date(),"YYYY-MM-DD HH:mm:ss");
        const onEvent=async(item,index)=>{
            if(item.private_key==null || isEmpty(item.phone)==true){
                if(item.private_key==null){
                    content="Le client ";
                    content+="/*"+item.name+"*/ ";
                    content+="n’est toujours pas  publié dans le compte de l’entreprise!";
                    title="Information du client non transmise";
                }else{
                    content="Le client ";
                    content+="/*"+item.name+"*/ ";
                    content+="est enregistré sans son numéro de téléphone!";
                    title="Le client est enregistré sans numéro de téléphone";
                }
                let items=list[getLink(item.id,"customer")];
                    if(items==undefined){
                        items=[];
                    }
                let passer=true;
                const elements=items.filter(p=>capitalize(p.status).toLowerCase()==S.GENERAL.STATUS.CANCELED.toLowerCase());
                if(elements.length>0){
                    passer=false;
                }
                if(passer){
                    items=items.sort((a,b)=>a.created_at-b.created_at);
                    if(items.length>0){
                        const date=DateTime.parse(items[0].created_at,"YYYY-MM-DD HH:mm:ss");
                        const today=new Date();
                        const time=date.getTime()+86400*1000*5;
                        if(time>today.getTime()){
                            passer=false;
                        }
                    }
                }
                if(passer==true){
                    NotificationModel.store({
                        content:content,
                        link:getLink(item.id,"customer"),
                        id_item:item.id,
                        status:S.GENERAL.STATUS.SEND,
                        created_at:date,
                        table:"customer",
                        title:title,
                        type:S.GENERAL.TYPE.CUSTOMER
                    })
                }
            }
        }
        ;(await rep).map(onEvent);
    } catch (error) {
        
    }
}

function getStock(item){
    let count=0;
    try {
        const buy=item.buy>0?item.buy:0;
        const stock=item.stock>0?item.stock:0;
        if(buy>=0 && stock>=0){
            count=stock-buy;
        }
    } catch (error) {
        
    }
    return count;
}