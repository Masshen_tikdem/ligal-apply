//web model
import * as ArticleWebModel from '../model/web/article';
import * as BuyWebModel from '../model/web/buy';
import * as CategoryWebModel from '../model/web/famille';
import * as CustomerWebModel from '../model/web/customer';
import * as StockWebModel from '../model/web/stock';
import * as DeviseWebModel from '../model/web/devise';
import * as UserWebModel from '../model/web/users';
//import * as WorkWebModel from '../model/web/';
import * as RightWebModel from '../model/web/right';
import * as ShopWebModel from '../model/web/society';
import * as SubscriptionWebModel from '../model/web/subscription';


//local model
import * as ArticleLocalModel from '../model/local/article';
import * as BuyLocalModel from '../model/local/buy';
import * as CategoryLocalModel from '../model/local/family';
import * as CustomerLocalModel from '../model/local/client';
import * as StockLocalModel from '../model/local/stock';
import * as DeviseLocalModel from '../model/local/devise';
import * as UserLocalModel from '../model/local/agent';
import * as SubscriptionLocalModel from '../model/local/subscription';
//import * as WorkWebModel from '../model/web/';
import * as RightLocalModel from '../model/local/right';
import * as ShopLocalModel from '../model/local/shop';
import { downloadImage, getImage, getRandomString } from '../Manager';




export async function article(key){
    let response=false;
    try {
        const articles=await ArticleLocalModel.get();
        const shop=await ShopLocalModel.get();
        const web=await ArticleWebModel.show(key);
        const devises=await DeviseLocalModel.get();
        const familles=await CategoryLocalModel.get();
        let value=null;
        let id_famille=0;
        let id_devise=0;
        let id=0;
        const onEvent=async(p,index)=>{
            if(p.private_key==value.private_key){
                id=p.id;
            }
        }
        const onEventFamille=async(p,index)=>{
            if(p.private_key==value.famille_private_key){
                id_famille=p.id;
            }
        }
        const onEventDevise=async(p,index)=>{
            if(p.private_key==value.devise_private_key){
                id_devise=p.id;
            }
        }

        if(web.status==200){
            value=web.response;
            await Promise.all(articles.map(onEvent));
            await Promise.all(devises.map(onEventDevise));
            await Promise.all(familles.map(onEventFamille));
            if(value!=null && value!=undefined){
                if(id==0){
                    //add new article
                    if(id_devise>0 && id_famille>0){
                        const rep=await ArticleLocalModel.store({
                            description:value.description,
                            devise:id_devise,
                            famille:id_famille,
                            name:value.name,
                            price:value.price,
                            private_key:value.private_key,
                            quantifiable:value.quantifiable,
                            shop:shop.id,
                            status:value.status,
                            type:value.type,
                            id_devise:value.id_devise,
                            is_updated:0,
                            image_updated:0,
                            mode:value.mode,
                            stockage:value.stockage
                        })
                        response=rep!=null;
                    }
                }else{
                    //update article
                    const rep=await ArticleLocalModel.update({
                        id:id,
                        description:value.description,
                        devise:id_devise>0?id_devise:null,
                        famille:id_famille>0?id_famille:null,
                        is_updated:0,
                        name:value.name,
                        price:value.price,
                        quantifiable:value.quantifiable,
                        status:value.status,
                        type:value.type,
                        id_devise:value.id_devise,
                        is_updated:0,
                        image_updated:0,
                        mode:value.mode,
                        stockage:value.stockage
                    })
                    response=rep!=null;
                }
            }
        } 
    } catch (error) {
        
    }
    return response;
}

export async function category(key){
    let response=false;
    try {
        const familles=await CategoryLocalModel.get();
        const web=await CategoryWebModel.show(key);
        let value=null;
        let id=0;
        const onEvent=async(p,index)=>{
            if(p.private_key==value.private_key){
                id=p.id;
            }
        }
        if(web.status==200){
            value=web.response;
            if(value!=null && value!={} && value!=undefined){
                await Promise.all(familles.map(onEvent));
                if(id==0){
                    const rep=await CategoryLocalModel.store({
                        is_updated:0,
                        name:value.name,
                        private_key:value.private_key,
                        status:value.status,
                        type:value.type,
                    });
                    response=rep!=null;
                }else{
                    const rep=await CategoryLocalModel.update({
                        id:id,
                        is_updated:0,
                        name:value.name,
                        type:value.type,
                        status:value.status,
                    });
                    response=rep!=null;
                }
            }
        }

    } catch (error) {
        
    }
    return response;
}

export async function link(key){
    let response=false;
    
    try {
        const familles=await CategoryLocalModel.get();
        const web=await CategoryWebModel.showLink(key);
        let value=null;
        let id=0;
        let id_high=0;
        let id_sub=0;
        const onEvent=async(p,index)=>{
            if(p.category_private_keys==value.high_private_key){
                id_high=p.category_id;
            }
            if(p.private_key==value.sub_private_key){
                id_sub=p.id;
            }
            if(p.link_private_key==value.private_key){
                id=p.id_link;
            }
        }
        if(web.status==200){
            value=web.response;
            if(value!=null && value!=undefined){
                await familles.map(onEvent);
                if(id==0){
                    if(id_hight>0 && id_sub>0){
                       response=await CategoryLocalModel.storeLink({
                            high_family:id_high,
                            sub_family:id_sub,
                            is_updated:0,
                            private_key:value.private_key,
                            status:value.status
                        })
                    }
                }else{
                    if(id_high>0 && id_sub>0){
                        response=await CategoryLocalModel.updateLink({
                            high_family:id_high,
                            sub_family:id_sub,
                            is_updated:0,
                            id:id,
                            status:value.status
                        });
                    }
                }
            }
        }
    } catch (error) {
        
    }
    return response;
}


export  async function buy(key){
    let response=false;
    try {
        const agents=await UserLocalModel.get(); 
        const articles=await ArticleLocalModel.get();
        const devises=await DeviseLocalModel.get();
        const customers=await CustomerLocalModel.get();
        const buys=await BuyLocalModel.get();
        const web=await BuyWebModel.show(key);
        let id_article=0;
        let id_agent=0;
        let id_devise=0;
        let id_customer=0;
        let id=0;
        let value=null;
        const onEventUser=(p,index)=>{
            if(p.private_key==value.agent_private_key){
                id_agent=p.id;
            }
        }
        const onEventArticle=(p,index)=>{
            if(p.private_key==value.article_private_key){
                id_article=p.id;
            }
        }
        const onEventDevise=(p,index)=>{
            if(p.private_key==value.devise_private_key){
                id_devise=p.id;
            }
        }
        const onEventCustomer=(p,index)=>{
            if(p.private_key==value.customer_private_key){
                id_customer=p.id;
            }
        }
        const onEventBuy=(p,index)=>{
            if(p.private_key==value.private_key){
                id=p.id;
            }
        }
        if(web.status==200){
            console.log('AWA NA KOTI');
            value=web.response;
            if(value!=null && value!=undefined){
                await Promise.all(articles.map(onEventArticle));
                await Promise.all(devises.map(onEventDevise));
                await Promise.all(customers.map(onEventCustomer));
                await Promise.all(agents.map(onEventUser));
                await Promise.all(buys.map(onEventBuy));
                if(id==0 && id_devise>0 && id_agent>0 && id_article>0){
                    const rep=await BuyLocalModel.store({
                        created_at:value.local_created_at,
                        id_agent:id_agent,
                        id_article:id_article,
                        id_client:id_customer,
                        id_devise:id_devise,
                        is_alter:value.is_alter,
                        price:value.price,
                        private_key:value.private_key,
                        quantity:value.quantity,
                        reference:value.reference,
                        remise:value.remise,
                        status:value.status,
                        description:value.description,
                        facture:value.bill
                    });
                    response=rep!=null;
                }else{
                    if(id_customer>0){
                        BuyLocalModel.update({
                            id_client:id_customer
                        })
                    }
                    response=true;
                }
            }
        }
    } catch (error) {
        console.log('ERROR',error);
    }
    return response;
}

export async function customer(key){
    let response=false;
    try {
        const customers=await CustomerLocalModel.get();
        const web=await CustomerWebModel.show(key);
        let value=null;
        let id=0;
        const onEvent=async(p,index)=>{
            if(p.private_key==value.private_key){
                id=p.id;
            }
        }
        if(web.status==200){
            value=web.response;
            await Promise.all(customers.map(onEvent));
            if(value!=null && value!=undefined){
                if(id==0){
                    const rep=await CustomerLocalModel.store({
                        name:value.name,
                        phone:value.phone,
                        private_key:value.private_key
                    })
                    response=rep!=null;
                }else{
                    const rep=await CustomerLocalModel.update({
                        name:value.name,
                        phone:value.phone,
                        id:id,
                    });
                    response=rep!=null;
                }
            }
        }
    } catch (error) {
        
    }
    return response;
}

export async function devise(key){
    let response=false;
    try {
        console.log('KEY',key);
        const devises=await DeviseLocalModel.get();
        const web=await DeviseWebModel.show(key);
        let id=0;
        let value=null;
        const onEvent=async(p,index)=>{
            if(p.private_key==value.private_key){
                id=p.id;
            }
        }
        if(web.status==200){
            value=web.response;
            await Promise.all(devises.map(onEvent));
            if(value!=null && value!=undefined){
                if(id==0){
                    const rep=await DeviseLocalModel.store({
                        name:value.name,
                        private_key:value.private_key,
                        signe:value.signe,
                        status:value.status,
                        type:value.type,
                        is_updated:0
                    });
                    response=rep!=null;
                    /*if(response==true && value.type==1){
                        const items=devises.filter(p=>p.id!=rep.id);
                        await Promise.all(items.map(async(p)=>{
                            await DeviseLocalModel.update({
                                type:0,
                                id:p.id
                            })
                        }))
                    }*/
                }else{
                    const rep=await DeviseLocalModel.update({
                        id:id,
                        is_updated:0,
                        name:value.name,
                        signe:value.signe,
                        status:value.status,
                        type:value.type
                    });
                    response=rep!=null;
                    /*if(response==true && value.type==1){
                        const items=devises.filter(p=>p.id!=rep.id);
                        await Promise.all(items.map(async(p)=>{
                            await DeviseLocalModel.update({
                                type:0,
                                id:p.id
                            })
                        }))
                    }*/
                }
            }
        }  
    } catch (error) {
        
    }
    return response;
}

export async function stock(key){
    let response=false;
    try {
        const articles=await ArticleLocalModel.get();
        const stocks=await StockLocalModel.get();
        const agents=await UserLocalModel.get();
        const devises=await DeviseLocalModel.get();
        const web=await StockWebModel.show(key);
        let id_article=0;
        let id=0;
        let id_agent=0;
        let id_devise=0;
        let value=null;
        const onEvent=(p,index)=>{
            if(p.private_key==value.private_key){
                id=p.id;
            }
        }
        const onEventArticles=(p,index)=>{
            if(p.private_key==value.article_private_key){
                id_article=p.id;
            }
        }
        const onEventUser=(p,index)=>{
            if(p.private_key==value.agent_private_key){
                id_agent=p.id;
            }
        }
        const onEventDevise=(p,index)=>{
            if(p.private_key==value.devise_private_key){
                id_devise=p.id;
            }
        }

        if(web.status==200){
            value=web.response;
            if(value!=null && value!=undefined){
                await Promise.all(stocks.map(onEvent));
                await Promise.all(articles.map(onEventArticles));
                await Promise.all(agents.map(onEventUser));
                await Promise.all(devises.map(onEventDevise));
                if(id==0 && id_article>0 && id_agent>0){
                    const rep=await StockLocalModel.store({
                        created_at:value.created_at,
                        fournisseur:value.fournisseur,
                        id_agent:id_agent,
                        id_article:id_article,
                        private_key:value.private_key,
                        quantity:value.quantity,
                        id_devise:id_devise>0?id_devise:null
                    });
                    response=rep!=null;
                }
            }
        }
    } catch (error) {
        console.log('stock error',error);
    }
    return response;
}

export async function user(key,image_updated=false){
    let response=false;
    try {
        if(key==undefined){
            return false;
        }
        const agents=await UserLocalModel.get();
        const web=await UserWebModel.show(key);
        let id=0;
        let value=null;
        const onEvent=(p,index)=>{
            if(p.private_key==value.private_key){
                id=p.id;
            }
        }
        if(web.status==200){
            value=web.response;
            await Promise.all(agents.map(onEvent));
            if(value!=undefined && value!=null){
                if(id==0){
                    response=await UserLocalModel.store({
                        address:value.address,
                        email:value.email,
                        first_name:value.first_name,
                        is_updated:0,
                        last_name:value.last_name,
                        password:value.password,
                        phone:value.phone,
                        private_key:value.private_key,
                        sexe:value.sexe,
                        status:value.status
                    });
                    if(response==true && image_updated==true){
                        const rep=await UserLocalModel.last();
                        await downloadUserImage(value,rep.id);
                    }
                }else{
                    response=await UserLocalModel.update({
                        address:value.address,
                        email:value.email,
                        first_name:value.first_name,
                        last_name:value.last_name,
                        id:id,
                        password:value.password,
                        is_updated:0,
                        sexe:value.sexe,
                        status:value.status
                    })
                    if(response==true && image_updated==true){
                        await downloadUserImage(value,id);
                    }
                }
            }
        }
    } catch (error) {
        console.log('error on user',error);
    }
    return response;
}

export async function work(key){
    let response=false;
    try {
        
    } catch (error) {
        
    }
    return response;
}

export async function shop(key){
    let response=false;
    try {
        const rep=await ShopWebModel.show(key);
        const shop=await ShopLocalModel.get();
        if(rep.status==200){
            const p=rep.response;
            response=await ShopLocalModel.update({
                address:p.address,
                country:p.country,
                description:p.description,
                name:p.name,
                number:p.number,
                phone:p.phone,
                site:p.site,
                town:p.town,
                id:shop.id
            });
        }
    } catch (error) {
        
    }
    return response;
}

export async function right(key){
    let response=false;
    try {
        let id=0;
        let value=null;
        const onEvent=async(p,index)=>{
            if(p.private_key==value.private_key){
                id=p.id;
                await RightLocalModel.update({
                    id:id,
                    is_updated:0,
                    value:value.value
                });
                if(rep){
                    response=true;
                }
            }
        }
        const rights=await RightLocalModel.get();
        const web=await RightWebModel.show(key);
        if(web.status==200){
            value=web.response;
            await Promise.all(rights.map(onEvent));
            response=true;
        }
    } catch (error) {
       
    }
    return response;
}

export async function subscription(key){
    let response=false;
    try {
        const value=await SubscriptionWebModel.show(key);
        const agents=await UserLocalModel.colleagues();
        const subscriptions=await SubscriptionLocalModel.get();
        let user=0;
        let id=0;
        const onEvent=async(p,i)=>{
            if(p.private_key==value.agent_private_key){
                user=p.id;
            }
        }
        const onEventSubscription=async(p,i)=>{
            if(p.private_key==value.response.private_key){
                id=p.id;
            }
        }
        await Promise.all(agents.map(onEvent));
        if(value.status==200){
            await Promise.all(subscriptions.map(onEventSubscription));
            const rep=value.response;
            if(id==0){
                //store
                response=await SubscriptionLocalModel.store({
                    created_at:rep.created_at,
                    price:rep.price,
                    private_key:rep.private_key,
                    status:rep.status,
                    transaction:rep.transaction,
                    type:rep.type,
                    user:user,
                    validate_at:rep.validate_at,
                    unity:rep.unity
                });
            }else{
                //update
                response=await SubscriptionLocalModel.update({
                    created_at:rep.created_at,
                    price:rep.price,
                    status:rep.status,
                    transaction:rep.transaction,
                    type:rep.type,
                    validate_at:rep.validate_at,
                    unity:rep.unity,
                    id:id
                });

            }
        }
    } catch (error) {
        
    }
    return response;
}

async function downloadUserImage(item,id){
    try {
        if(item.photo!=null && item.photo!=undefined){
            const photo=getImage(item.photo);
            const imageName="user"+id+getRandomString(5)+item.private_key+'.ligal';
            const load=await downloadImage(imageName.toLowerCase(),photo,null);
            if(load==true){
                const rep=await UserLocalModel.update({
                    id:id,
                    photo:imageName.toLowerCase()
                })
            }
        }
    } catch (error) {
        console.log('ERROR DOWNLOAD',error);   
    }
}