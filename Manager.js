import { Platform } from 'react-native';
import RFS  from 'react-native-fs';
import AsyncStorage from "@react-native-async-storage/async-storage";
import NetInfo from '@react-native-community/netinfo';
import S from './resources/settings.json';
import DateTime from 'date-and-time';
import fr from 'date-and-time/locale/fr';

export function getFullName(first_name="",last_name=""){
    let rep="";
    if(first_name!=null && first_name!=undefined){
        rep=capitalize(first_name);
    }
    if(last_name!=null && last_name!=undefined){
        rep+=" "+last_name.toUpperCase();
    }
    return rep;
}


export function capitalize(value){
    let rep="";
    if(value!=undefined && value!=null){
        if(value.length>0){
            rep=value[0].toUpperCase();
        }
        if(value.length>1){
            rep+=value.substr(1).toLowerCase();
        }
    }
    return rep;
}

export function getSex(value){
    let rep=null;
    try {
        if(value==null || value==undefined){
            rep="Genre non mentionné";
        }else if(value.toLowerCase()=='f'){
            rep="Femme";
        }
        else if(value.toLowerCase()=='m'){
            rep="Homme"
        }else{
            rep="Genre non mentionné";
        }
    } catch (error) {
        
    }
    return rep;
}
export function getPrice(price=0,devise="",remise=null){
    let rep="";
    try {
        if(price!=undefined && price!=null && devise!=undefined && devise!=null){
            let item=price;
            if(remise>0){
               item-=(item*remise)/100; 
            }
            const value=item+"";
            const split=value.toString().split('\.');
            let rep1="";
            let select="";
            if(split.length>0){
                select=split[0];
            }
            let len=select.length-1;
            if(len>=0){
                let p=0;
                for (let i = len; i >= 0; i--) {
                    rep1+=value.charAt(i);
                    p++;
                    if(p==3){
                        rep1+=" ";
                        p=0;
                    }
                }
                if(rep1.length>0){
                    len=rep1.length-1;
                    for (let i = len; i>=0; i--) {
                       rep+=rep1.charAt(i); 
                    }
                }
                if(split.length>1){
                    select=split[1];
                    let el=0;
                    try {
                        el=parseFloat(select);
                        if(el>0){
                            rep+="."+select;
                        }
                    } catch (error) {
                        
                    }
                }
            }
            rep+=" "+devise.toUpperCase();
        }
    } catch (error) {
        
    }
    return rep;
}

export function getTotal(list=[],devises=[]){
    let rep="";
    try {
        if(list==null || list==undefined || devises==null || devises==undefined){
            return "0";
        }
        if(list.length>0 && devises.length>0){
            let len=devises.length;
            let k=1;
            for (let i = 0; i < len; i++) {
                let argent=devises[i].signe;
                let partiel=0;
                for (let j = 0; j < list.length; j++) {
                    if(devises[i].id==list[j].id_devise){
                        let norme=list[j].price*list[j].quantity;
                        const remise=list[j].remise
                        if(remise>0){
                            norme-=(norme*remise)/100;
                        }
                        partiel+=norme;
                    }
                }
                if(partiel>0){
                    rep+=getPrice(partiel.toFixed(2))+" "+argent.toUpperCase()+" +";
                    k++;
                }
            }
            if(rep.length>0){
                rep=rep.substring(0,rep.length-1);
            }else{
                rep=0;
            }
        }
    } catch (error) {
        rep="0";       
    }
    return rep;
}

export async function getTotalStock(list=[],devises=[]){
    let rep=[];
    try {
        if(list==null || list==undefined || devises==null || devises==undefined){
            return [{stock:0,devise:0,signe:""}];
        }
        if(list.length>0 && devises.length>0){
            let len=devises.length;
            let k=1;
            for (let i = 0; i < len; i++) {
                let argent=devises[i].signe;
                let partiel=0;
                for (let j = 0; j < list.length; j++) {
                    if(devises[i].id==list[j].id_devise){
                        let norme=list[j].quantity;
                        partiel+=norme;
                    }
                }
                if(partiel>0){
                    rep.push({
                        stock:partiel,
                        devise:devises[i].id,
                        signe:argent
                    })
                    k++;
                }
            }
        }
    } catch (error) {
        rep=[];       
        console.log('error',error)
    }
    return rep;
}

export async function getTotalBuy(list=[],devises=[],useQuantity=false){
    let rep=[];
    try {
        if(list==null || list==undefined || devises==null || devises==undefined){
            return [{stock:0,devise:0,signe:""}];
        }
        if(list.length>0 && devises.length>0){
            let len=devises.length;
            let k=1;
            for (let i = 0; i < len; i++) {
                let argent=devises[i].signe;
                let partiel=0;
                for (let j = 0; j < list.length; j++) {
                    if(devises[i].id==list[j].id_devise){
                        let norme=list[j].price;
                        const qte=list[j].quantity;
                        const remise=list[j].remise;
                        if(remise>0){
                            norme-=(norme*remise)/100;
                        }
                        if(useQuantity==true && qte>0){
                            partiel+=norme*qte;
                        }else{
                            partiel+=norme;
                        }
                    }
                }
                if(partiel>0){
                    rep.push({
                        stock:partiel,
                        devise:devises[i].id,
                        signe:argent
                    })
                    k++;
                }
            }
        }
    } catch (error) {
        rep=[];       
        console.log('error',error)
    }
    return rep;
}

export function getDifference(stocks=[],buys=[]){
    let rep=[];
    try {
        for (let index = 0; index < stocks.length; index++) {
            let value=stocks[index].stock;
            let signe=stocks[index].signe;
            let devise=stocks[index].devise;
            for (let j = 0; j < buys.length; j++) {
                if(devise==buys[j].devise){
                    value-=buys[j].stock;
                }
            }
            rep.push(
                {quantity:value,signe,devise}
            );
        }
    } catch (error) {
        
    }
    return rep;
}


export async function saveImage(path="",uri=null,former=null){
    let passer=false;
    try {
        const link=RFS.DocumentDirectoryPath+"/"+path;
        if(uri!=null){
            if(Platform.OS==='ios'){
                const rep=await RFS.copyAssetsFileIOS(uri,link,50,50,10,50)
                passer=true;
            }else if(Platform.OS==='android'){
                const rep=await RFS.copyFile(uri,link)
                passer=true;
            }
            if(former!=null){
                RFS.unlink(RFS.DocumentDirectoryPath+"/"+former);
            }
        }
    } catch (error) {
    }
    return passer;
}
export async function downloadImage(path="",url=null,former=null){
    let passer=false;
    try {
        const link=RFS.DocumentDirectoryPath+"/"+path;
        const download=await RFS.downloadFile({
            fromUrl:url,
            toFile:link
        })
        await download.promise.then(async rep=>{
            if(rep.statusCode>=200 && rep.statusCode<300){
                passer=true;
                if(former!=null){
                    RFS.unlink(RFS.DocumentDirectoryPath+"/"+former);
                }
            }
        })
    } catch (error) {
        console.log('DOWN ERROR',error);
    }
    return passer;
}
export function getImageLocal(name){
    let rep="p"
    try {
        if(name==null || name==undefined){
            return rep;
        }
        if(Platform.OS==='ios'){
            rep=getFilePath(name);
        }else if(Platform.OS==='android'){
            rep="file://"+getFilePath(name);
        }
        
    } catch (error) {
        
    }
    return rep;
}
export function getFilePath(name){
    let rep="";
    try {
        if(name==null || name==undefined){
            return rep;
        }
        rep=RFS.DocumentDirectoryPath+"/"+name;
        
    } catch (error) {
        
    }
    return rep;
}
export function getImage(value){
    let rep='p';
    try {
        if(value!=null && value!=undefined){
            const url=S.NETWORK_FILE+value;
            rep=url;
        }
    } catch (error) {
        rep='p';
    }
    return rep;
}

export function getRandomString(length) {
    var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var result = '';
    for ( var i = 0; i < length; i++ ) {
        result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    }
    return result;
}
export function getArticleName(name="",description=null){
    let rep="";
    try {
        rep=capitalize(name).toUpperCase();
        if(description!=null && description!=undefined && description!=""){
            if(description.trim().length>0){
                rep+=" ("+description.toLowerCase()+")"
            }
        }
    } catch (error) {
        
    }
    return rep;
}
export function getCategoryName(name="",category=null){
    let rep="";
    try {
        rep=capitalize(name).toUpperCase();
        if(categocategory!=null &&category!=undefined && category!=""){
            if(category.trim().length>0){
                rep+="("+category.toLowerCase()+")"
            }
        }
    } catch (error) {
        
    }
    return rep;
}
export function getTitle(value){
    let rep="?";
    if(value!=null && value!=undefined){
        if(value[0]!=undefined){
            rep=value[0].toUpperCase();
            if(rep=="(" && value.length>1){
                rep=value[1].toUpperCase();
            }
        }
    }
    return rep;
}

export async function UserGetStorage(){
    let rep=null;
    let status=500;
    try {
        const value=await AsyncStorage.getItem('@USER');
        rep=value;
        status=200;
    } catch (error) {
        rep=new Object();
        rep.error=error;
        status=500;
    }
    return {response:rep,status:status};
}

export async function getCodeVerify(){
    let rep=null;
    let status=500;
    try {
        const value=await AsyncStorage.getItem('@CODE');
        rep=value;
        status=200;
    } catch (error) {
        rep=new Object();
        rep.error=error;
        status=500;
    }
    return {response:rep,status:status};
}

export async function UserSetStorage(request){
    let rep=null;
    let status=500;
    try {
        await AsyncStorage.setItem('@USER',request);
        rep=new Object();
        rep.state='OK';
        status=200;
    } catch (error) {
        rep=new Object();
        rep.error=error;
        status=500;
    }
    return {response:rep,status:status};
}

export async function setCodeVerify(request){
    let rep=null;
    let status=500;
    try {
        await AsyncStorage.setItem('@CODE',request);
        rep=new Object();
        rep.state='OK';
        status=200;
    } catch (error) {
        rep=new Object();
        rep.error=error;
        status=500;
    }
    return {response:rep,status:status};
}

export async function removeCodeVerify(){
    try {
        await AsyncStorage.removeItem('@CODE');
    } catch (error) {
    }
    
}
export async function UserRemoveStorage(){
    let rep=null;
    let status=500;
    try {
        const value=await AsyncStorage.removeItem('@USER');
        rep=value;
        status=200;
    } catch (error) {
        rep=new Object();
        rep.error=error;
        status=500;
    }
    return {response:rep,status:status};
}

export function isEmpty(value=""){
    let rep=false;
    if(value===undefined || value===null){
        rep=true;
    }else{
        if(trim(value)==""){
            rep=true;
        }
    }
    return rep;
}

export function isDifference(value="",value2=""){
    let rep=true;
    if(value==undefined || value==null || value2==undefined || value2==null){
        if(value==value2){
            rep=false;
        }
    }else{
        if(value.toLowerCase()==value2.toLowerCase()){
            rep=false;
        }
    }
    return rep;
}

export function isDifferenceStrict(value="",value2=""){
    let rep=true;
    if(value==undefined || value==null || value2==undefined || value2==null){
        rep=false;
    }else{
        if(value.toLowerCase()==value2.toLowerCase()){
            rep=false;
        }
        if(value.trim().length==0 || value2.trim().length==0){
            rep=false;
        }
    }
    return rep;
}


export function write(value=0){
    let rep=false;
    if(value ==7 || value==6 || value==2 || value==3){
        rep=true;
    }
    return rep;
}
export function read(value=0){
    let rep=false;
    if(value ==7 || value==5 || value==1 || value==3){
        rep=true;
    }
    return rep;
}
export function update(value=0){
    let rep=false;
    if(value ==7 || value==6 || value==5 || value==4){
        rep=true;
    }
    return rep;
}

export async function isConnected(){
    let rep=false;
    try {
        const p=await NetInfo.fetch();
        rep=p.isConnected;
    } catch (error) {
        
    }
    return rep;
}

export function getNumColumns(width=0){
    let rep=1;
    try {
        if(width<=320){
            rep=1
        }else if(width>320 && width<=480){
            rep=1;
        }else if(width>480 && width<=640){
            rep=2
        }else if(width>640 && width<=960){
            rep=3;
        }else if(width>960 && width<=1024){
            rep=4;
        }else if(width>1024){
            
        }
    } catch (error) {
        
    }
    return rep;
}

export async function uploadFile(path="",url){
    let rep=false
    const network=S.NETWORK+url;
    try {
        if(path!=null && path!=undefined){
            const exist=await RFS.exists(path);
            if(exist==true){
                const web=await RFS.uploadFiles({
                    files:[{
                        filename:"image.ligal",
                        filepath:path,
                        filetype:"image/jpg",
                        name:"image"
                    }],
                    toUrl:network,
                    method:'POST',
                })
                await web.promise.then(response=>{
                })
                RFS.readFile("",(err,data)=>{
                    
                })
            }else{
            }
        }else{
        }
        
    } catch (error) {
    }
    return rep;
}

export function getLink(id,table=""){
    let rep=null;
    try {
        if(table!=null && table!=undefined && id>0){
            rep=table.toLowerCase()+"::"+id;
        }
    } catch (error) {
        
    }
    return rep;
}

export const removeNoNumber=(string="")=>string.replace(/[^\d]/g,'');
export const removeNoNumberDecimal=(string="")=>string.replace(/[^\d](\.?)/g,'');
export const addGaps = (string = "", gaps,join=" ") => {
        const offsets = [0].concat(gaps).concat([string.length]);
        return offsets.map((end, index) => {
          if (index === 0) return "";
          const start = offsets[index - 1];
          return string.substr(start, end - start);
        }).filter(part => part !== "").join(join);
      };
export const addGapsWithSeparator = (string = "", gaps,join=" ",separator=".") => {
    if(separator==null || separator==undefined){
        return addGaps(string,gaps," ");
    }
    if(separator.length==0 || separator.length>1){
        return addGaps(string,gaps," ");
    }
    const split=string.split(separator);
    if(split.length>2){
        return string;
    }
    const value=split[0];
    let decimal="";
    if(split[1]!=null && split[1]!=undefined){
        if(split[1]!=""){
            decimal=separator+split[1];
        }
    }
    const offsets = [0].concat(gaps).concat([value.length]);
    return offsets.map((end, index) => {
        if (index === 0) return "";
        const start = offsets[index - 1];
        return value.substr(start, end - start)+decimal;
    }).filter(part => part !== "").join(join);
};

export const addGapsWithSeparatorMoney = (string = "", gaps,join=" ",separator=".") => {
    let rep="";
    if(separator==null || separator==undefined){
        return addGaps(string,gaps," ");
    }
    if(separator.length==0 || separator.length>1){
        return addGaps(string,gaps," ");
    }
    const split=string.split(separator);
    if(split.length>2){
        return string;
    }
    const value=split[0];
    let decimal="";
    if(split[1]!=null && split[1]!=undefined){
        if(split[1]!=""){
            decimal=separator+split[1];
        }
    }
    const len=value.length;
    for (var i=0; i <len; i++) {
        if((len-1-i)%3==0){
            rep+=value[i]+" ";
        }else{
            rep+=value[i];
        }	
    }
    if(rep[rep.length-1]==" "){
        rep=rep.substr(0,rep.length-1);	
    }
    return rep;
};
export const limitLength = (string = "", maxLength) => string.substr(0, maxLength);

export function getStock(stockValue,buyValue,mode,type){
    let rep={count:0,text:"0",devise:null};
    try {
        if(type!="service" || (type=="service" && mode=="unity") ){
            const buy=buyValue>0?buyValue:0;
            const stock=stockValue>0?stockValue:0;
            let count=0;
            if(buy>=0 && stock>=0){
                count=stock-buy;
                rep={count:count,text:""+count};
            }
        }else if(type=="service"){
            if(mode=="devise"){
                if(stockValue.length>0){
                    const value=getDifference(stockValue,buyValue);
                    let text="";
                    let size=0;
                    for (let index = 0; index < value.length; index++) {
                        text+= getPrice(value[index].quantity)+" "+capitalize(value[index].signe).toUpperCase()+" +";
                        if(value[index].quantity>0){
                            size++;
                        }
                    }
                    text=text.substring(0,text.length-1);
                    rep={count:size,text:text,devise:value};
                }
            }else{
                const buy=buyValue>0?buyValue:0;
                const stock=stockValue>0?stockValue:0;
                let count=0;
                if(buy>=0 && stock>=0){
                    count=stock-buy;
                }
                if(count<0){
                    count=-count;
                }
                rep={count:count,text:""+count};
            }
        }
    } catch (error) {
        console.log('error stock count',error)
    }
    return rep;
}

export function getDateList(dateValue="",mode){
    let date="";
        try {
            DateTime.locale(fr);
            const value=DateTime.parse(dateValue,"YYYY-MM-DD HH:mm:ss");
            if(DateTime.isSameDay(new Date(),value)){
                switch (mode) {
                    case "day":
                        date=DateTime.format(value,"HH:mm");
                    break;
                    case "month":
                        date="Aujourd'hui à "+DateTime.format(value,"HH:mm");
                    break;
                    case "year":
                        date="Aujourd'hui à "+DateTime.format(value,"HH:mm");
                    break;
                
                    default:
                        date="Aujourd'hui à "+DateTime.format(value,"HH:mm");
                    break;
                }
            }
            else if(DateTime.isSameDay(DateTime.addDays(value,1),new Date())){
                date="Hier à "+DateTime.format(value,"HH:mm");
                switch (mode) {
                    case "day":
                        date=DateTime.format(value,"HH:mm");
                    break;
                    case "month":
                        date="Hier à "+DateTime.format(value,"HH:mm");
                    break;
                    case "year":
                        date="Hier à "+DateTime.format(value,"HH:mm");
                    break;
                
                    default:
                        date="Hier à "+DateTime.format(value,"HH:mm");
                    break;
                }
            }
            else{
                switch (mode) {
                    case "day":
                        date=DateTime.format(value,"HH:mm");
                    break;
                    case "month":
                        date=DateTime.format(value,"DD MMM à HH:mm");
                    break;
                    case "year":
                        date=DateTime.format(value,"DD MMM à HH:mm");
                    break;
                
                    default:
                        date=DateTime.format(value,"ddd,DD MMM YYYY à HH:mm");
                    break;
                }
            }
            
        } catch (error) {
            console.log('err',error)            
        }
    return date;
}

export function trim(value){
    let rep="";
    if(value==null || value==undefined){
        return rep;
    }
    capitalize(value).split(" ").map((item)=>{
        rep+=item;
    })
    return rep;
}
