import React from 'react';
import { Component } from 'react';
import { ScrollView } from 'react-native';
import R from '../resources/styles.json';
import S from '../resources/settings.json';
import EmptyMessage from '../components/EmptyMessage';
import Awaiter from '../components/Waiter';
import * as BuyModel from '../model/local/buy';
import { View } from 'react-native';
import { Avatar, Button, Card, Text } from 'react-native-elements';
import DateTime from 'date-and-time';
import fr from 'date-and-time/locale/fr';
import ModalBill from '../components/ModalBill';
import { capitalize, getArticleName, getFullName, getImageLocal, getPrice, getTitle, isEmpty } from '../Manager';


class App extends Component{

    constructor({props,route}){
        super(props)
        this.state={
            route,
            first_name:"",
            last_name:"",
            photo:null,
            logo:null,
            description:"",
            name:"",
            created_at:"",
            price:0,
            quantity:0,
            isLoading:true,
            exist:false,
            famille:"",
            devise:"",
            isAlter:false,
            remise:0,
            customer:null,
            reference:null,
            facture:null,
            mode:"",
            type:"",
            billList:[],
            bill:null,
            visibleBill:false
        }
    }

    async componentDidMount(){
        try {
            const id=this.state.route.params.id;
            const rep=await BuyModel.showBuy(id);
            if(rep!=null){
                this.setState({
                    first_name:rep.first_name,
                    last_name:rep.last_name,
                    photo:rep.photo,
                    name:rep.name,
                    description:rep.description,
                    logo:rep.logo,
                    famille:rep.famille,
                    exist:true,
                    isLoading:false,
                    price:rep.price,
                    quantity:rep.quantity,
                    devise:rep.signe,
                    isAlter:rep.is_alter==1?true:false,
                    remise:rep.remise,
                    created_at:rep.created_at,
                    customer:rep.customer_name,
                    reference:rep.reference,
                    mode:rep.mode,
                    type:rep.type,
                    facture:rep.bill,
                })
                if(!isEmpty(rep.bill)){
                    const repBill=await BuyModel.getByBill(rep.bill);
                    this.setState({
                        billList:repBill,
                        bill:rep.bill
                    })
                }
            }
        } catch (error) {
            
        }
    }
    getDate=()=>{
        let rep="";
        try {
            DateTime.locale(fr);
            const date=DateTime.parse(this.state.created_at,"YYYY-MM-DD HH:mm:ss");
            if(DateTime.isSameDay(new Date(),date)){
                rep="Aujourd'hui à "+DateTime.format(date,'HH:mm');
            }/*else if(DateTime.isSameDay(DateTime.addDays(date,1),new Date())){
                rep="Hier à "+DateTime.format('HH:mm');
            }*/else{
                rep=DateTime.format(date,"dddd, DD MMMM YYYY à HH:mm");
            }
        } catch (error) {
        }
        return rep;
    }
    calcul=()=>{
        let total=0;
        try {
            const price=this.state.price;
            const qte=this.state.quantity;
            const remise=this.state.remise;
            let cal=price*qte;
            cal-=(cal*remise)/100;
            total=getPrice(cal,this.state.devise)
        } catch (error) {
            
        }
        return total;
    }

    render(){

        return(
            this.state.isLoading==true?(
                <Awaiter color={R.color.colorSecondary} size={100} key="awaiter" />
            ):this.state.exist==false?(
                <EmptyMessage
                    title="Aucune information n'est trouvée"
                    key="df"
                />
            ):(
                <ScrollView>
                    <View style={{alignItems:'center',padding:20}}>
                    <Avatar
                        source={{uri:getImageLocal(this.state.photo)}}
                        rounded size="xlarge"
                        title={getTitle(this.state.last_name)}
                        containerStyle={{backgroundColor:R.color.colorPrimary}}
                        placeholderStyle={{backgroundColor:R.color.colorPrimary}}
                    />
                </View>
                <View style={{padding:10,alignItems:'center'}}>
                    <Text style={{fontFamily:'Poppins-Thin',fontSize:16}}>
                        Nom de l'agent
                    </Text>
                    <Text numberOfLines={1} style={{fontFamily:'Poppins-Medium',fontSize:18}}>
                        {getFullName(this.state.first_name,this.state.last_name)}
                    </Text>
                </View>
                <Card containerStyle={{borderTopEndRadius:20,borderTopStartRadius:20,elevation:10,marginBottom:20}}>
                    <Card.Title>
                        Information sur la vente
                    </Card.Title>
                    <View style={{alignItems:'center',flexDirection:'row'}}>
                        <View style={{padding:10}}>
                            <Avatar
                                source={{uri:getImageLocal(this.state.logo)}}
                                rounded size="large"
                                title={getTitle(this.state.name)}
                                containerStyle={{backgroundColor:R.color.colorPrimary}}
                                placeholderStyle={{backgroundColor:R.color.colorPrimary}}
                            />
                        </View>
                        <View style={{flex:1,padding:10}}>
                            <Text numberOfLines={1} style={{fontFamily:'Poppins-Medium',fontSize:18}}>
                                {getArticleName(this.state.name,this.state.description)}
                            </Text>
                            <Text style={{fontFamily:'Poppins-Thin',paddingLeft:10}}>
                                {" de "}
                            </Text>
                            <Text numberOfLines={1} style={{fontFamily:'Poppins-Black',fontSize:18}}>
                                {capitalize(this.state.famille,this.state.description)}
                            </Text> 
                        </View>
                    </View>

                    {
                    (this.state.mode!=S.ENUM.stockage.devise)?(
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <View style={{flex:2,alignItems:'center',padding:5}}>
                            <Text style={{fontFamily:'Poppins-Black',paddingLeft:10,fontSize:20}}>
                                PU
                            </Text>
                            <Text style={{fontFamily:'Poppins-Light',paddingLeft:10,fontSize:14}}>
                                {getPrice(this.state.price,this.state.devise)}
                            </Text>
                        </View>
                        <View style={{flex:1,alignItems:'center',padding:5}}>
                            <Text style={{fontFamily:'Poppins-Black',paddingLeft:10,fontSize:20}}>
                                Qte
                            </Text>
                            <Text style={{fontFamily:'Poppins-Light',paddingLeft:10,fontSize:14}}>
                                {this.state.quantity}
                            </Text>
                        </View>
                        <View style={{flex:2,alignItems:'center',padding:5}}>
                            <Text style={{fontFamily:'Poppins-Black',paddingLeft:10,fontSize:20}}>
                                Remise
                            </Text>
                            <Text style={{fontFamily:'Poppins-Light',paddingLeft:10,fontSize:14}}>
                                {`${this.state.remise}%`}
                            </Text>
                        </View>
                    </View>):null
                    }
                    <View style={{alignItems:'center',padding:5}}>
                        <Text style={{fontFamily:'Poppins-Black',paddingLeft:10,fontSize:20}}>
                            Total
                        </Text>
                        <Text style={{fontFamily:'Poppins-Light',paddingLeft:10,fontSize:14}}>
                            {this.calcul()}
                        </Text>
                    </View>
                    <View>
                        {this.state.isAlter==true?(
                            <Text style={{fontFamily:'Poppins-Light',color:R.color.colorPrimary,paddingLeft:10,fontSize:16,textAlign:'center'}}>
                                Le prix a été modifié lors de la vente
                            </Text>
                        ):null}
                    </View>
                    <View style={{alignItems:'center',padding:5}}>
                        <Text style={{fontFamily:'Poppins-Black',paddingLeft:10,fontSize:20}}>
                            Date de la vente
                        </Text>
                        <Text numberOfLines={1} style={{fontFamily:'Poppins-Light',paddingLeft:10,fontSize:14}}>
                            {this.getDate()}
                        </Text>
                    </View>
                    {(this.state.customer!=null && this.state.customer!=undefined)?(
                        <View style={{alignItems:'center',padding:5}}>
                            <Text style={{fontFamily:'Poppins-Black',paddingLeft:10,fontSize:20}}>
                                Nom du client
                            </Text>
                            <Text style={{fontFamily:'Poppins-Light',color:R.color.colorPrimary,paddingLeft:10,fontSize:16,textAlign:'center'}}>
                                {capitalize(this.state.customer).toUpperCase()}
                            </Text>
                        </View>
                    ):null}
                    {(this.state.reference!=null && this.state.reference!=undefined)?(
                        <View style={{alignItems:'center',padding:5}}>
                            <Text style={{fontFamily:'Poppins-Black',paddingLeft:10,fontSize:20}}>
                                Référence de la vente
                            </Text>
                            <Text style={{fontFamily:'Poppins-Light',color:R.color.colorPrimary,paddingLeft:10,fontSize:16,textAlign:'center'}}>
                                {capitalize(this.state.reference).toUpperCase()}
                            </Text>
                        </View>
                    ):null}
                    {(this.state.facture!=null && this.state.facture!=undefined && this.state.facture!="")?(
                        <View style={{alignItems:'center',padding:5}}>
                            <Text style={{fontFamily:'Poppins-Black',paddingLeft:10,fontSize:20}}>
                                Numéro de la facture
                            </Text>
                            <View style={{ flexDirection:'row',alignItems:'center' }}>
                                <View style={{ flex:1 }}>
                                    <Text style={{fontFamily:'Poppins-Light',color:R.color.colorPrimary,paddingLeft:10,fontSize:16,textAlign:'center'}}>
                                        {capitalize(this.state.facture).toUpperCase()}
                                    </Text>
                                </View>
                                <View>
                                    <Button
                                        title="Voir"
                                        type='clear'
                                        onPress={()=>this.setState({visibleBill:true})}
                                    />
                                </View>
                            </View>
                        </View>
                    ):null}
                </Card>
                <ModalBill
                    list={this.state.billList}
                    onClose={()=>this.setState({visibleBill:false})}
                    bill={this.state.bill}
                    visible={this.state.visibleBill}
                    key="bill_modal"
                />
                </ScrollView>
            )
        )
    }
}
export default (App);