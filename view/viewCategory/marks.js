import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import EmptyMessage from '../../components/EmptyMessage';
import Awaiter from '../../components/Waiter';
import * as FamilleMOdel from '../../model/local/family';
import R from '../../resources/styles.json';
import CardCategory from '../../components/CardCategory';
import S from '../../resources/settings.json';
import { FlatList } from 'react-native';
import * as Manager from '../../Manager';


const mapStateToProps = (state) => {
    return {
        user:state.user,
        category:state.category
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

class App extends Component{

    constructor({props,route}){
        super(props)
        this.state={
            route,
            list:[],
            isLoading:true,
        }
    }


    onLoad=async()=>{
        try {
            const item=this.state.route.params.item;
            const rep=await FamilleMOdel.marks(item.id);
            this.setState({
                list:rep
            })
            if(this.state.isLoading==true){
                this.setState({isLoading:false})
            }
        } catch (error) {
            console.log('er',error)
        }
    }
    renderItem=({item})=>{
        return(
            <CardCategory
                logo={Manager.getImageLocal(item.logo)}
                onPress={()=>this.openCategory(item)}
                title={Manager.capitalize(item.name).toUpperCase()}
            />
        )
    }
    openCategory=(item)=>{
        try {
            this.props.navigation.navigate('ViewSubCategory',{item:item});
        } catch (error) {
         console.log('err',error)   
        }
    }

    async componentDidMount(){
        try {
            await this.onLoad();
        } catch (error) {
            console.log('err',error)
        }
    }

    async componentDidUpdate(){
        try {
            const cat=this.props.category;
            if(cat.category==true){
                const action={type:S.EVENT.onChangeMarketCategory,category:false}
                this.props.dispatch(action);
                this.onLoad();
            }
        } catch (error) {
            
        }
    }

    render(){

        return(
            this.state.isLoading==true?(
                <Awaiter color={R.color.colorSecondary} size={100} key="awaiter" />
            ):this.state.list.length==0?(
                <EmptyMessage title="Aucune marque dans cette catégorie" />
            ):(
                <FlatList
                    data={this.state.list}
                    renderItem={this.renderItem}
                    key="list-mark"
                />
            )
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(App)