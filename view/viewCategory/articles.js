import React, { Component } from 'react';
import { View } from 'react-native';
import Empty from '../../components/EmptyMessage';
import Awaiter from '../../components/Waiter';
import CardArticle from '../../components/CardArticle';
import R from '../../resources/styles.json';
import { FlatList } from 'react-native';
import { capitalize, getArticleName, getImageLocal, getPrice,getStock, read, update } from '../../Manager';
import { connect } from 'react-redux';
import * as ArticleModel from '../../model/local/article';
import S from '../../resources/settings.json';
import { Alert } from 'react-native';

const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.article,
        right:state.right
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

class App extends Component{

    constructor({props,route}){
        super(props)
        this.state={
            route,
            list:[],
            isLoading:false,
            item:null,
            showRead:false
        }
    }
    async componentDidMount(){
        try {
            if(read(this.props.right.article) || update(this.props.right.article)){
                this.setState({showRead:true})
            }
            const articles=this.state.route.params.articles;
            this.setState({
                list:articles,
                item:this.state.route.params.item
            })
        } catch (error) {
            
        }
    }
    onLoad=async()=>{
        try {
            const rep=await ArticleModel.get(this.state.item.id);
            console
            this.setState({
                list:rep
            })
        } catch (error) {
            
        }
    }
    async componentDidUpdate(){
        try {
            const article=this.props.article.market;
            if(article==true){
                console.log('article',this.props.article)
                const action={type:S.EVENT.onChangeMarketArticle,market:false}
                this.props.dispatch(action);
                await this.onLoad();
            }
        } catch (error) {
            console.log('mess',error)
        }
    }
    renderItem=({item})=>{
        const stock=item.stock>0?item.stock:0;
        const buy=item.buy>0?item.buy:0;
        const quantity=stock-buy;
        return(
            <CardArticle
                familly={capitalize(item.famille)}
                name={getArticleName(item.name,item.description)}
                price={
                    (item.type==S.ENUM.article.service && item.mode==S.ENUM.stockage.devise)?getStock(item.stock,item.buy,item.mode,item.type).text:
                    getPrice(item.price,item.devise)
                }
                showPrice={item.mode!=S.ENUM.stockage.devise}
                quantity={getStock(item.stock,item.buy,item.mode,item.type).text}
                service={item.type==S.ENUM.article.service && item.mode!=S.ENUM.stockage.devise}
                key='card'
                logo={getImageLocal(item.logo)}
                onPress={()=>this.openArticle(item)}
            />
        )
    }

    openArticle=(item)=>{
        try {
            if(this.state.showRead)
                this.props.navigation.navigate('ViewArticle',{item:item})
            else{
                Alert.alert("Vérification","Vous n'avez pas l'accès");
            }
        } catch (error) {  
        }
    }

    render(){

        return(
            <>
                {
                    this.state.list.length==0?(
                        <Empty title="Aucun article dans cette catégorie" />
                    ):(
                        <FlatList
                            data={this.state.list}
                            renderItem={this.renderItem}
                            key='list'
                            numColumns={2}
                        />
                    )
                }
            </>
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(App)