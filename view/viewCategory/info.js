import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { View } from 'react-native';
import { Avatar, Card, Text } from 'react-native-elements';
import { FAB } from 'react-native-paper';
import R from '../../resources/styles.json';
import * as Manager from '../../Manager';
import { connect } from 'react-redux';
import * as ArticleModel from '../../model/local/article';
import * as FamilleModel from '../../model/local/family';
import S from '../../resources/settings.json';

const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.article
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

class App extends Component{

    constructor({props,route}){
        super(props)
        this.state={
            opened:false,
            route,
            famille:{
                name:'',
                id:0,
                type:'',
                category:null,
                logo:null
            },
            articles:[],
            marks:[],
        }
    }

    async componentDidMount(){
        try {
            const item=this.state.route.params.item;
            const articles=this.state.route.params.articles;
            this.setState({
                famille:{
                    name:item.name,
                    id:item.id,
                    type:item.type,
                    category:item.category,
                    logo:item.logo
                },
                articles:articles
            })
            const rep=await FamilleModel.marks(item.id);
            this.setState({marks:rep})
        } catch (error) {
            
        }
    }
    onLoad=async()=>{
        try {
            const item=await FamilleModel.show(this.state.famille.id)
            const rep=await ArticleModel.get(this.state.famille.id);
            this.setState({
                articles:rep,
                famille:{
                    name:item.name,
                    id:item.id,
                    type:item.type,
                    category:item.category,
                    logo:item.logo
                },
            })
        } catch (error) {
            
        }
    }
    async componentDidUpdate(){
        try {
            const article=this.props.article.category;
            if(article==true){
                const action={type:S.EVENT.onChangeMarketArticle,category:false}
                this.props.dispatch(action);
                await this.onLoad()
            }
        } catch (error) {
        }
    }

    render(){

        return(
            <>
            <View style={{flex:1}}>
                <ScrollView>
                    <View style={{alignItems:'center',margin:20}}>
                        <Avatar
                            rounded
                            size='xlarge'
                            icon={{name:'cube',type:'font-awesome'}}
                            containerStyle={{backgroundColor:R.color.colorPrimary,elevation:20}}
                            source={{uri:Manager.getImageLocal(this.state.famille.logo)}}
                            placeholderStyle={{backgroundColor:R.color.colorPrimary}}
                        />
                    </View>
                    <Card
                        containerStyle={{borderRadius:20}}
                    >
                        <View>
                            <Text style={{opacity:0.6,fontSize:18,textAlign:'center',fontFamily:'Poppins-Black'}}>
                                {Manager.capitalize(this.state.famille.name).toUpperCase()}
                            </Text>
                            <View style={{flexDirection:'row',alignItems:'center',padding:10}}>
                                {
                                    this.state.famille.category==null?(
                                    <View style={{flex:1}}>
                                        <Text style={{textAlign:'center',fontSize:16,fontFamily:'Poppins-Light'}}>
                                            {
                                                this.state.marks.length==0?"Aucune sous-catégorie":this.state.marks.length+" sous-catégorie"+(this.state.marks.length>1?"s":"")
                                            }
                                        </Text>
                                    </View>
                                    ):null
                                }
                                <View style={{flex:1,alignItems:'center'}}>
                                    <Text style={{textAlign:'center',fontFamily:'Poppins-Light',fontSize:16}}>
                                        {this.state.articles.length+' article'+(this.state.articles.length>1?'s':'')}
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </Card>
                    {/*<Card>
                        <Card.Title>
                            Aide
                        </Card.Title>
                    </Card>*/}
                </ScrollView>
            </View>
            

            </>
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(App)