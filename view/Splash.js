import { StackActions } from '@react-navigation/native';
import React, { Component } from 'react';
import { View,Image,StatusBar, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { capitalize, UserGetStorage, UserSetStorage } from '../Manager';
import * as ShopModel from '../model/local/shop';
import S from '../resources/settings.json';
import R from '../resources/styles.json';
import * as RightModel from '../model/local/right';
import * as SubscriptionWebModel from '../model/web/subscription';
import * as SubscriptionModel from '../model/local/subscription';
import { groupBy } from 'lodash';
import DateTime from 'date-and-time';
import { Text } from 'react-native-paper';

const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.article,
        shop:state.shop
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

class App extends Component{

    constructor(props){
        super(props)
        this.state={
            loading:false
        }
    }

    getWebSubscription=async({user={},shop={}})=>{
        let rep=[];
        try {
            const item=new Object();
            item.user=user.private_key;
            item.society=shop.private_key;
            const web=await SubscriptionWebModel.get(item);
            if(web.status==200){
                rep=web.response;    
            }
        } catch (error) {
            
        }
        return rep;
    }

    checkSubscription=async({user={},shop={}})=>{
        try {
            this.setState({loading:true});
            let list=await this.getWebSubscription({shop,user});
            list=groupBy(list,"private_key");
            let rep=await SubscriptionModel.getSubscription(user.id,S.ENUM.subscription.SUBSCRIPTION,S.ENUM.subscription.WELCOME);
            if(rep.length>0){
                //calculate subscription
                await Promise.all(rep.map(async p=>{
                    let item=list[p.private_key];
                    if(item!=undefined){
                        item=item[0];
                        await SubscriptionModel.update({
                            validate_at:item.validate_at,
                            id:p.id
                        })
                    }
                }));
                rep=await SubscriptionModel.getSubscription(user.id,S.ENUM.subscription.SUBSCRIPTION,S.ENUM.subscription.WELCOME);
                this.calculateSubscription(rep);
            }else{
                list=await this.getWebSubscription({shop,user});
                if(list.length==0){
                    if(this.createSubscription({shop,user})){
                        rep=await SubscriptionModel.getSubscription(user.id,S.ENUM.subscription.SUBSCRIPTION,S.ENUM.subscription.WELCOME);
                        if(rep.length>0){
                            //calculate subscription
                            this.calculateSubscription(rep);
                        }
                    }
                }else{
                    //save any way
                    const onEvent=async(item,index)=>{
                        const result=await SubscriptionModel.store({
                            created_at:item.created_at,
                            price:item.price,
                            private_key:item.private_key,
                            status:item.status,
                            transaction:item.transaction,
                            type:item.type,
                            user:user.id,
                            validate_at:item.validate_at,
                            unity:item.unity
                        });
                    }
                    await Promise.all(list.map(onEvent));
                }
                this.setState({loading:false});
            }
        } catch (error) {
            console.log('error',error)
        }
    }

    calculateSubscription=(list=[])=>{
        try {
            const currentDate=new Date();
            let validate=null;
            let status=false;
            let period=null;
            list.map((item,index)=>{
                if(item.type==S.ENUM.subscription.SUBSCRIPTION || item.type==S.ENUM.subscription.WELCOME){
                    const date=DateTime.parse(item.validate_at,"YYYY-MM-DD HH:mm:ss");
                    if(date.getTime()>currentDate.getTime()){
                        if(validate!=null){
                            if(validate.getTime()<date.getTime()){
                                validate=date;
                            }
                        }else{
                            validate=date;
                        }
                    }
                }
            });
            if(validate!=null){
                if(validate.getTime()>currentDate.getTime()){
                    status=true;
                    period=DateTime.format(validate,"YYYY-MM-DD HH:mm:ss");
                }
            }
            const action={type:S.EVENT.onChangeSubscription,status:status,date:period}
            this.props.dispatch(action);
    
        } catch (error) {
            console.log('error',error);
        }
    }



    createSubscription=async({shop,user})=>{
        let result=false;
        try {
            console.log("SHOP",shop);
            const item=new Object();
            item.type=S.ENUM.subscription.WELCOME;
            item.user=user.private_key;
            item.agent=[user.private_key];
            item.society=shop.private_key;
            item.price=0;
            item.days=60;
            item.formula=1;
            console.log("ITEM",item);
            const rep=await SubscriptionWebModel.store(item);
            if(rep.status==200){
                const item=rep.response;
                result=await SubscriptionModel.store({
                    created_at:item.created_at,
                    price:item.price,
                    private_key:item.private_key,
                    status:item.status,
                    transaction:item.transaction,
                    type:item.type,
                    user:user.id,
                    validate_at:item.validate_at,
                    unity:item.unity
                });
            }
        } catch (reason) {
            const rep=reason.response;
            console.log('error BOOM',rep);
        }
        return result;
    }

    setRights=async(id)=>{
        try {
            const right=await RightModel.get(id);
                const element=new Object();
                const len=right.length;
                element.type=S.EVENT.OnChangeRight;
                const event=async(item,index)=>{
                    const type=S.ENUM;
                    switch (item.code) {
                        case capitalize(type.CATEGORY).toLowerCase():
                            element.category=item.value;
                        break;
                        case capitalize(type.DEVISE).toLowerCase():
                            element.devise=item.value;
                        break;
                        case capitalize(type.VENTE_PRODUIT).toLowerCase():
                            element.vente_produit=item.value;
                        break;
                        case capitalize(type.JOURNAL_PRODUIT).toLowerCase():
                            element.journal=item.value;
                        break;
                        case capitalize(type.DASHBOARD).toLowerCase():
                            element.dashboard=item.value;
                        break;
                        case capitalize(type.STOCK_IN).toLowerCase():
                            element.stock=item.value;
                        break;
                        case capitalize(type.STOCK_OUT).toLowerCase():
                            element.sales=item.value;
                        break;
                        case capitalize(type.CLIENT).toLowerCase():
                            element.customer=item.value;
                        break;
                        case capitalize(type.PRODUCT).toLowerCase():
                            element.article=item.value;
                        break;
                    }
                }
                await Promise.all(right.map(event));
                await this.props.dispatch(element)
        } catch (error) {
            
        }
    }

    checkAppState=async()=>{
        let status=false;
        try {
            const rep=await ShopModel.get();
            if(rep!=null){
                const action={type:S.EVENT.onChangeShop,value:rep};
                await this.props.dispatch(action);
                status=true;
            }
            const storage=await UserGetStorage();
            const redux=await this.props.user;
            if(redux.value!=null && redux.status==true){
                if(storage==null){
                    UserSetStorage(JSON.stringify({user:redux.value}))
                }
                //rights
                const id=redux.value.id;
                await this.setRights(id);
                if(rep==null){
                    this.props.navigation.dispatch(
                        StackActions.replace('Screen')
                   )   
                }else{
                    if(redux.value.work_status>0){
                        await this.checkSubscription({shop:rep,user:this.props.user.value});
                        this.props.navigation.dispatch(
                            StackActions.replace('Home')
                       )
                    }else{
                        this.props.navigation.dispatch(
                            StackActions.replace('Begin')
                       )
                    }
                }
            }else{
                if(storage.response==null){
                    this.props.navigation.dispatch(
                        StackActions.replace('Screen')
                   )
                }else{
                    const user=JSON.parse(storage.response);
                    const action={type:S.EVENT.onChangeUser,value:user.user,status:true}
                    await this.props.dispatch(action)
                    await this.setRights(user.user.id);
                    if(user.user.work_status>0){
                        await this.checkSubscription({shop:rep,user:this.props.user.value});
                        this.props.navigation.dispatch(
                            StackActions.replace('Home')
                       )
                    }else{
                        this.props.navigation.dispatch(
                            StackActions.replace('Begin')
                       )
                    }
                    
                }
            }
        } catch (error) {
            console.log('error screen',error);
             this.props.navigation.dispatch(
                 StackActions.replace('Screen')
             )
        }
        return status;
    }


    async componentDidMount(){
        try {
          setTimeout(async()=>{
              await ShopModel.get();
              await this.checkAppState();
          },1000)  
        } catch (error) {
            
        }
    }

    render(){
        return(
            <>
            <StatusBar hidden />
            <View style={{flex:1,justifyContent:"center",alignItems:'center'}}>
                <Image
                    source={require('../assets/logo/ligal.png')}
                />
                <View style={{ position:'absolute',right:20,bottom:20 }}>
                    <View style={{ flexDirection:'row' }}>
                        {this.state.loading && (<View style={{ marginRight:10 }}>
                            <ActivityIndicator color={R.color.colorSecondary} />
                        </View>)}
                        <View>
                            <Text>
                                LIGAL PLATFORM
                            </Text>
                        </View>
                    </View>
                </View>
            </View>
            </>
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(App);