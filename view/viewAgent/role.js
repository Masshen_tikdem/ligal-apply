import React from "react";
import { ScrollView, View } from "react-native";
import * as RightModel from '../../model/local/right';
import * as RightWebModel from '../../model/web/right';
import R from '../../resources/styles.json';
import S from '../../resources/settings.json';
import Awaiter from '../../components/Waiter';
import { capitalize, read, update, write } from "../../Manager";
import { Avatar, Text } from "react-native-elements";
import CardRole from '../../components/CardRole';
import { FlatList } from "react-native";
import EmptyMessage from '../../components/EmptyMessage';
import { FAB } from "react-native-paper";
import { ProgressDialog } from "react-native-simple-dialogs";
import ModalRole from '../../components/ModalRole';
import database from '@react-native-firebase/database';
import { Alert } from "react-native";
import { connect } from "react-redux";
import * as Shared from '../../service/shared';

const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.article,
        client:state.client,
        sale:state.sale,
        shop:state.shop
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

class App extends React.Component{
    constructor(props){
        super(props)
        this.state={
            isLoading:true,
            showProgress:false,
            VENTE_PRODUIT:null,
            JOURNAL_PRODUIT:null,
            DASHBOARD:null,
            STOCK_IN:null,
            STOCK_OUT:null,
            CLIENT:null,
            PRODUCT:null,
            CATEGORY:null,
            DEVISE:null,
            rights:[],
            user:null ,
            list:[],
            currentValue:0,
            showModal:false,
            currentId:0,
            currentPrivateKey:null,
            code:null          
        }
        this.options=[
            {label:"Aucun rôle",value:0},
            {label:"Lecture seule",value:1},
            {label:"Enregistement seul",value:2},
            {label:"Enregistrement et lecture",value:3},
            {label:"Modification seule",value:4},
            {label:"Lecture et modification",value:5},
            {label:"Enregistrement et modification",value:6},
            {label:"Tous les droits",value:7},
        ];
    }

    async componentDidMount(){
        try {
            const item=this.props.route.params.item;
            this.setState({user:item});
            const rep=await RightModel.get(item.id)
            if(rep!=null){
                this.setState({rights:rep})
                this.setState({isLoading:false})
            }
        } catch (error) {
            
        }
    }
    getTitle=(value="")=>{
        let rep="";
        try {
            switch (capitalize(value).toLowerCase()) {
                case (capitalize(S.ENUM.CATEGORY).toLowerCase()):
                    rep="Gestion des catégories"
                break;
                case (capitalize(S.ENUM.CLIENT).toLowerCase()):
                    rep="Gestion des clients"
                break;
                case (capitalize(S.ENUM.DASHBOARD).toLowerCase()):
                    rep="Gestion sur des stratégies"
                break;
                case (capitalize(S.ENUM.DEVISE).toLowerCase()):
                    rep="Gestion sur des devises"
                break;
                case (capitalize(S.ENUM.JOURNAL_PRODUIT).toLowerCase()):
                    rep="La comptabilité du jour"
                break;
                case (capitalize(S.ENUM.PRODUCT).toLowerCase()):
                    rep="Gestion des articles"
                break;
                case (capitalize(S.ENUM.STOCK_IN).toLowerCase()):
                    rep="L'approvisionnement"
                break;
                case (capitalize(S.ENUM.STOCK_OUT).toLowerCase()):
                    rep="La comptabilité"
                break;
                case (capitalize(S.ENUM.VENTE_PRODUIT).toLowerCase()):
                    rep="Gestion des ventes"
                break;
            
                default:
                break;
            }
        } catch (error) {
            
        }
        return rep;
    }

    getIcon=(value=0,code="")=>{
        let rep={icon:"user-o",type:"font-awesome"};
        try {
            switch (capitalize(code).toLowerCase()) {
                case (capitalize(S.ENUM.CATEGORY).toLowerCase()):
                    rep={
                        icon:"folderopen",
                        type:"ant-design"
                    }
                break;
                case (capitalize(S.ENUM.CLIENT).toLowerCase()):
                    rep={
                        icon:"customerservice",
                        type:"ant-design"
                    }
                break;
                case (capitalize(S.ENUM.DASHBOARD).toLowerCase()):
                    rep={
                        icon:"setting",
                        type:"ant-design"
                    }
                break;
                case (capitalize(S.ENUM.DEVISE).toLowerCase()):
                    rep={
                        icon:"credit",
                        type:"entypo"
                    }
                break;
                case (capitalize(S.ENUM.JOURNAL_PRODUIT).toLowerCase()):
                    rep={
                        icon:"home",
                        type:"ant-design"
                    }
                break;
                case (capitalize(S.ENUM.PRODUCT).toLowerCase()):
                    rep={
                        icon:"tags",
                        type:"ant-design"
                    }
                break;
                case (capitalize(S.ENUM.STOCK_IN).toLowerCase()):
                    rep={
                        icon:"areachart",
                        type:"ant-design"
                    }
                break;
                case (capitalize(S.ENUM.STOCK_OUT).toLowerCase()):
                    rep={
                        icon:"iconfontdesktop",
                        type:"ant-design"
                    }
                break;
                case (capitalize(S.ENUM.VENTE_PRODUIT).toLowerCase()):
                    rep={
                        icon:"shoppingcart",
                        type:"ant-design"
                    }
                break;
                default:
                    break;
            }
        } catch (error) {
            
        }
        return rep;
    }

    getContent=(value=0,code="")=>{
        let rep="";
        try {
            switch (capitalize(code).toLowerCase()) {
                case (capitalize(S.ENUM.CATEGORY).toLowerCase()):
                    if(write(value) && update(value) && read(value)){
                        rep="Cet agent peut tout gérer dans une catégorie : l’ajout ou la modification des catégories"
                    }else if(write(value) && !update(value)){
                        rep="Uniquement l’ajout";
                    }
                    else if(!write(value) && update(value)){
                        rep="Seule la modification est autorisée."
                    }
                    else if(!write(value) && !update(value) && read(value)){
                        rep="Seulement la lecture des données est autorisée, pas d’ajout ni modification d’un élément catégorie de la boutique"
                    }else{
                        rep="Aucun accès dans la catégories des articles";
                    }
                break;
                case (capitalize(S.ENUM.CLIENT).toLowerCase()):
                    if(write(value) && update(value) && read(value)){
                        rep="Cet agent peut tout gérer dans la liste de clients : l'ajout, la modification, l'exploration"
                    }else if(write(value) && !update(value)){
                        rep="Uniquement l’ajout";
                    }
                    else if(!write(value) && update(value)){
                        rep="Seule la modification est autorisée."
                    }
                    else if(!write(value) && !update(value) && read(value)){
                        rep="Seulement la lecture des données est autorisée, pas d’ajout ni modification d’une donnée de clients"
                    }else{
                        rep="Aucun accès dans la liste de clients";
                    }
                break;
                case (capitalize(S.ENUM.DASHBOARD).toLowerCase()):
                    if(write(value) && update(value) && read(value)){
                        rep="Cet agent peut tout gérer dans la stratégie de la boutique"
                    }else if(write(value) && !update(value)){
                        rep="Uniquement l’ajout";
                    }
                    else if(!write(value) && update(value)){
                        rep="Seule la modification est autorisée."
                    }
                    else if(!write(value) && !update(value) && read(value)){
                        rep="Seulement la lecture des données est autorisée, pas d’ajout ni modification"
                    }else{
                        rep="Aucun accès";
                    }
                break;
                case (capitalize(S.ENUM.DEVISE).toLowerCase()):
                    if(write(value) && update(value) && read(value)){
                        rep="Cet agent peut tout gérer dans une devise : l’ajout ou la modification des monnaies"
                    }else if(write(value) && !update(value)){
                        rep="Uniquement l’ajout";
                    }
                    else if(!write(value) && update(value)){
                        rep="Seule la modification est autorisée."
                    }
                    else if(!write(value) && !update(value) && read(value)){
                        rep="Seulement la lecture des données est autorisée, pas d’ajout ni modification d’une donnée de monnaie"
                    }else{
                        rep="Aucun accès";
                    }
                break;
                case (capitalize(S.ENUM.JOURNAL_PRODUIT).toLowerCase()):
                    if(write(value) && update(value) && read(value)){
                        rep="Cet agent peut tout gérer dans le journal quotidien"
                    }else if(write(value) && !update(value)){
                        rep="Uniquement l’ajout";
                    }
                    else if(!write(value) && update(value)){
                        rep="Seule la modification est autorisée."
                    }
                    else if(!write(value) && !update(value) && read(value)){
                        rep="Seulement la lecture des données est autorisée, pas d’ajout ni modification"
                    }else{
                        rep="Aucun accès dans le journal quotidien";
                    }
                break;
                case (capitalize(S.ENUM.PRODUCT).toLowerCase()):
                    if(write(value) && update(value) && read(value)){
                        rep="Cet agent peut tout gérer concernant les articles : l’ajout ou la modification des articles"
                    }else if(write(value) && !update(value)){
                        rep="Uniquement l’ajout";
                    }
                    else if(!write(value) && update(value)){
                        rep="Seule la modification est autorisée."
                    }
                    else if(!write(value) && !update(value) && read(value)){
                        rep="Seulement la lecture des données est autorisée, pas d’ajout ni modification des articles"
                    }else{
                        rep="Aucun accès sur les articles";
                    }
                break;
                case (capitalize(S.ENUM.STOCK_IN).toLowerCase()):
                    if(write(value) && update(value) && read(value)){
                        rep="Cet agent peut tout gérer sur les approvisionnements"
                    }else if(write(value) && !update(value)){
                        rep="Uniquement l’approvisionnement";
                    }
                    else if(!write(value) && update(value)){
                        rep="Seule la modification est autorisée."
                    }
                    else if(!write(value) && !update(value) && read(value)){
                        rep="Seulement la lecture des données est autorisée, pas d’ajout ni modification"
                    }else{
                        rep="Aucun accès sur les approvisionnements";
                    }
                break;
                case (capitalize(S.ENUM.STOCK_OUT).toLowerCase()):
                    if(write(value) && update(value) && read(value)){
                        rep="Cet agent peut tout gérer sur les informations de vente : retour de vente, lecture approfondie, etc."
                    }else if(write(value) && !update(value)){
                        rep="Uniquement l’enregistrement de la vente";
                    }
                    else if(!write(value) && update(value)){
                        rep="Seule la modification est autorisée."
                    }
                    else if(!write(value) && !update(value) && read(value)){
                        rep="Seulement la lecture des données est autorisée, pas d’ajout ni modification"
                    }else{
                        rep="Aucun accès dans la catégories des articles";
                    }
                break;
                case (capitalize(S.ENUM.VENTE_PRODUIT).toLowerCase()):
                    if(write(value) && update(value) && read(value)){
                        rep="Cet agent peut tout gérer dans une vente : les remises, la modification de prix, etc."
                    }else if(write(value) && !update(value)){
                        rep="Vendre uniquement, sans ajustement de prix ou enregistrement de la remise";
                    }
                    else if(!write(value) && update(value)){
                        rep="Seule la modification est autorisée."
                    }
                    else if(!write(value) && !update(value) && read(value)){
                        rep="Seulement la lecture des données est autorisée, pas d’ajout ni modification"
                    }else{
                        rep="Aucun accès";
                    }
                break;
            
                default:
                    break;
            }
        } catch (error) {
            
        }
        return rep;
    }
    getValue=(value=0)=>{
        let rep="";
        try {
            if(write(value) && update(value) && read(value)){
                rep="Tous les droits possibles"
            }else if(!write(value) && update(value)){
                rep="Uniquement la modification";
            }else if(write(value) && !update(value)){
                rep="Uniquement l'ajout"
            }else if(!write(value) && !update(value) && read(value)){
                rep="Lecture seule"
            }else{
                rep="Aucun rôle"
            }
        } catch (error) {
            
        }
        return rep;
    }

    getList=(code)=>{
        let rep=[];
        console.log('code',S.ENUM.DEVISE);
        try {
            switch (code.toUpperCase()) {
                case S.ENUM.CATEGORY:
                    this.options.map((item,index)=>{
                        if(item.value!=2 && item.value!=4 && item.value!=6){
                            rep.push(item);
                        }
                    })
                break;
                case S.ENUM.CLIENT:
                    this.options.map((item,index)=>{
                        if(item.value!=2 && item.value!=4 && item.value!=6){
                            rep.push(item);
                        }
                    })
                break;
                case S.ENUM.DASHBOARD:
                    //à revoir
                    this.options.map((item,index)=>{
                        if(item.value!=2 && item.value!=4 && item.value!=6){
                            rep.push(item);
                        }
                    })
                break;
                case S.ENUM.DEVISE:
                    this.options.map((item,index)=>{
                        if(item.value!=2 && item.value!=4 && item.value!=6){
                            rep.push(item);
                        }
                    })
                break;
                case S.ENUM.JOURNAL_PRODUIT:
                    this.options.map((item,index)=>{
                        if(item.value==0 || item.value==1 || item.value==7){
                            rep.push(item);
                        }
                    })
                break;
                case S.ENUM.PRODUCT:
                    this.options.map((item,index)=>{
                        if(item.value!=2 && item.value!=4 && item.value!=6){
                            rep.push(item);
                        }
                    })
                break;
                case S.ENUM.STOCK_IN:
                    this.options.map((item,index)=>{
                        if(item.value!=2 && item.value!=4 && item.value!=6){
                            rep.push(item);
                        }
                    })
                break;
                case S.ENUM.STOCK_OUT:
                    this.options.map((item,index)=>{
                        if(item.value==0 || item.value==1 || item.value==5 || item.value==7){
                            rep.push(item);
                        }
                    })
                break;
                case S.ENUM.VENTE_PRODUIT:
                    this.options.map((item,index)=>{
                        if(item.value==0 || item.value==2 || item.value==6 || item.value==7){
                            rep.push(item);
                        }
                    })
                break;
                default:
                    this.options.map((item,index)=>{
                        if(item.value!=2 && item.value!=4 && item.value!=6){
                            rep.push(item);
                        }
                    })
                break;
            }
            
        } catch (error) {
            console.log('res',error)
        }
        return rep;
    }

    onPressItem=(item)=>{
        try {
            this.setState({
                currentId:item.id,
                currentValue:item.value,
                list:this.getList(item.code),
                showModal:true
            })
        } catch (error) {
            
        }
    }
    onUpdate=async()=>{
        try {
            const id=this.state.currentId;
            const user=this.state.user;
            const private_key=this.state.currentPrivateKey;
            const value=this.state.currentValue;
            const shop=this.props.shop.value;
            const agent=this.props.user.value;
            if(id>0 && value>=0){
                this.setState({showProgress:true,showModal:false})
                const rep=await RightModel.update({
                    value:value,
                    id:id,
                    is_updated:1
                });
                if(rep!=null){
                    //const actionService={type:S.EVENT.OnExecuteService,right:true}
                    //await this.props.dispatch(actionService);
                    Shared.right({
                        agent_private_key:rep.agent_private_key,
                        code:rep.code,
                        id:rep.id,
                        private_key:rep.private_key,
                        shop:shop,
                        user:agent,
                        value:rep.value
                    });
                    const rep2=await RightModel.get(user.id);
                    if(rep2!=null){
                        this.setState({rights:rep2})
                    }
                    this.setState({showProgress:false})
                    Alert.alert("Modification","Vous avez modifié le rôle avec succès");
                    this.onCloseModal();
                }else{
                    this.setState({showProgress:false,showModal:true})
                    Alert.alert("Modification","La modification n'a pas réussi");
                }
            }else{
                this.onCloseModal();
            }

        } catch (error) {
            console.log('error',error);
        }
    }
    onCloseModal=()=>{
        this.setState({showModal:false,currentId:0,currentValue:0,currentPrivateKey:null})
    }

    renderItem=({item})=>{
        const symbol=this.getIcon(item.value,item.code);
        console.log("CODE:"+item.code,"VALUE:"+item.value);
        return(
            <CardRole
                title={this.getTitle(item.code)}
                content={this.getContent(item.value,item.code)}
                value={this.getValue(item.value)}
                onPress={()=>this.onPressItem(item)}
                code={item.value}
                type={symbol.type}
                icon={symbol.icon}

            />
        )
    }
    onLoadRole=async()=>{
        try {
            this.setState({showProgress:true});
            const user=this.state.user;
            let list=[];
            const onEvent=async(item)=>{
                await RightModel.store({
                    code:item.code,
                    id_agent:user.id,
                    is_updated:0,
                    private_key:item.private_key,
                    value:item.value
                })
            }
            const web=await RightWebModel.get(user.private_key);
            if(web.status==200){
                list=web.response;
                await list.map(onEvent);
                const rep=await RightModel.get(item.id)
                if(rep!=null){
                    this.setState({rights:rep})
                    this.setState({showProgress:false})
                }
            }
            this.setState({showProgress:false})
        } catch (error) {
            
        }
    }

    render(){
        return(
            <>
            {
            this.state.isLoading==true?(
                <Awaiter color={R.color.colorSecondary} size={100} key="awaiter" />
            ):this.state.rights.length==0?(
                <>
                <EmptyMessage
                    title="Aucun droit identifié"
                    key="emptyMessage"
                />
                <FAB
                    label="Recharger les rôles"
                    style={{ position:'absolute',backgroundColor:R.color.colorPrimary,bottom:0,right:0,margin:20 }}
                    color={R.color.background}
                    icon="account-cog-outline"
                    onPress={this.onLoadRole}
                />
                </>
            ):(
                <View>
                    <FlatList
                        data={this.state.rights}
                        renderItem={this.renderItem}
                        key="list-data"
                    />
                </View>
            )
            }
            <ProgressDialog
                visible={this.state.showProgress}
                animationType='slide'
                activityIndicatorColor={R.color.colorPrimary}
                activityIndicatorSize='large'
                message="Patientez SVP!"
                title="Chargement..."
            />
            <ModalRole
                visible={this.state.showModal}
                list={this.state.list}
                value={this.state.currentValue}
                onClose={()=>this.onCloseModal()}
                onPress={()=>this.onUpdate()}
                onChangeValue={value=>this.setState({currentValue:value})}
            />
            </>
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(App);