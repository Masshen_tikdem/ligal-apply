import React from "react";
import { View } from "react-native";
import CardContribution from '../../components/CardContribution';
import Awaiter from '../../components/Waiter';
import R from '../../resources/styles.json';
import * as AgentModel from '../../model/local/agent';
import { ScrollView } from "react-native";
import { StyleSheet } from "react-native";

const styles=StyleSheet.create({
    flex:{
        flexDirection:"row",
        alignItems:'center'
    }
})

class App extends React.Component{
    constructor(props){
        super(props)
        this.state={
            isLoading:true,
            stock:{title:"",content:"",num:0},
            buy:{title:"",content:"",num:0},
            customer:{title:"",content:"",num:0}
        }
    }
    async componentDidMount(){
        try {
            const item=this.props.route.params.item;
            await this.initStock(item.id);
            await this.initBuy(item.id);
            await this.initCustomer(item.id);
            this.setState({isLoading:false})
        } catch (error) {
            console.log('ddd',error)
        }
    }

    initStock=async(id)=>{
        try {
            const rep=await AgentModel.getStock(id);
            if(rep!=null){
                const sum=rep.sum_quantity;
                this.setState({
                    stock:{
                        title:"Approvisionnement de stock",
                        content:sum>1?"Plusieurs produits enregistrés":"Aucun approvisionnement",
                        num:sum
                    }
                })
            }else{
                this.setState({
                    stock:{
                        title:"Approvisionnement de stock",
                        content:"Aucun approvisionnement",
                        num:0
                    }
                })
            }
        } catch (error) {
            
        }
    }
    initBuy=async(id)=>{
        try {
            const rep=await AgentModel.getBuy(id);
            if(rep!=null){
                this.setState({buy:rep})
                console.log('EEE buy ohhh',rep);
            }else{
                this.setState({
                    buy:{
                        title:"Vente des articles",
                        content:"Aucune vente",
                        num:0
                    }
                })
            }
        } catch (error) {
            
        }
    }
    initCustomer=async(id)=>{
        try {
            const rep=await AgentModel.getCustomer(id);
            if(rep!=null){
                this.setState({buy:rep})
                console.log('EEE cutomer ',rep);
            }else{
                this.setState({
                    customer:{
                        title:"Enregistrement des clients",
                        content:"Aucun client enregistré",
                        num:0
                    }
                })
            }
        } catch (error) {
            
        }
    }

    render(){
        return(
            this.state.isLoading==true?(
                <Awaiter color={R.color.colorSecondary} size={100} key="dd" />
            ):(
                <ScrollView>
                    <View>

                    </View>
                    <ScrollView horizontal>
                    <View style={styles.flex}>
                        <View style={{ flex:1 }}>
                            <CardContribution
                                content={this.state.stock.content}
                                title={this.state.stock.title}
                                num={this.state.stock.num}
                                icon="bus"
                                type="font-awesome"
                            />    
                        </View>
                        <View style={{ flex:1 }}>
                            <CardContribution
                                content={this.state.buy.content}
                                title={this.state.buy.title}
                                num={this.state.buy.num}
                                icon="cart-plus"
                                type='font-awesome'
                            />    
                        </View>
                    </View>
                    </ScrollView>
                </ScrollView>
            )
        )
    }
}
export default (App);