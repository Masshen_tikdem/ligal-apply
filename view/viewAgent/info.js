import React from "react";
import { ScrollView } from "react-native";
import { View } from "react-native";
import { Avatar, Text } from "react-native-elements";
import { capitalize, getFullName, getImageLocal, getTitle } from "../../Manager";
import R from '../../resources/styles.json';

class App extends React.Component{
    constructor(props){
        super(props)
        this.state={
            agent:{
                first_name:"",
                last_name:"",
                phone:"",
                photo:null,
                email:"",
                work_poste:"",
                work_created_at:null,
                work_status:null
            }
        }
    }

    componentDidMount(){
        try {
            const item=this.props.route.params.item;
            console.log('D',item)
            if(item!=null && item!=undefined){
                this.setState({
                    agent:{
                        first_name:item.first_name,
                        last_name:item.last_name,
                        sex:item.sex,
                        photo:item.photo,
                        email:item.email,
                        phone:item.phone,
                        work_poste:item.work_poste,
                        work_created_at:item.work_created_at,
                        work_status:item.work_status
                    }
                })
            }
        } catch (error) {
            
        }    
    }

    

    render(){
        return(
            <View>
                <ScrollView>
                    <View style={{padding:10,alignItems:'center'}}>
                        <Avatar
                            source={{ uri:getImageLocal(this.state.agent.photo) }}
                            rounded size="xlarge"
                            title={getTitle(this.state.agent.last_name)}
                            containerStyle={{backgroundColor:R.color.colorPrimary}}
                            placeholderStyle={{backgroundColor:R.color.colorPrimary}}
                        />
                        <Text numberOfLines={1} style={{fontFamily:"Poppins-Black",fontSize:18,padding:10}}>
                            {getFullName(this.state.agent.first_name,this.state.agent.last_name)}
                        </Text>
                    </View>
                    <View style={{flexDirection:'row',alignItems:'center',padding:10}}>
                        <View style={{flex:1,alignItems:'center'}}>
                            <Text style={{fontFamily:'Poppins-Medium'}}>
                                Poste occupé
                            </Text>
                            <Text numberOfLines={1} style={{fontFamily:'Poppins-Bold'}}>
                                {capitalize(this.state.agent.work_poste).toUpperCase()}
                            </Text>
                        </View>
                        <View style={{flex:1,alignItems:'center'}}>
                            <Text>
                                Etat de l'activité
                            </Text>
                            <Text numberOfLines={1} style={{fontFamily:'Poppins-Bold',color:this.state.agent.work_status==1?R.color.colorPrimaryDark:'red'}}>
                                {this.state.agent.work_status==1?"Toujours admis":"Non en service"}
                            </Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}
export default (App);