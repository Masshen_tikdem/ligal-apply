import React from 'react';
import { Component } from 'react';
import { ScrollView, StatusBar, TextPropTypes } from 'react-native';
import { Image } from 'react-native';
import { View } from 'react-native';
import { Button, Card, CheckBox, Header, Icon, Input, Overlay } from 'react-native-elements';
import { connect } from 'react-redux';
import R from '../resources/styles.json';
import S from '../resources/settings.json';
import { StyleSheet } from 'react-native';
import { Alert } from 'react-native';
import * as ShopModel from '../model/local/shop';
import * as ShopWebModel from '../model/web/society';
import * as DeviseModel from '../model/local/devise';
import * as ClientModel from '../model/local/client';
import * as AgentModel from '../model/local/agent';
import * as RightModel from '../model/local/right';
import DateTime from 'date-and-time';
import { UserSetStorage,capitalize } from '../Manager';
import { ProgressDialog } from 'react-native-simple-dialogs';
import { StackActions } from '@react-navigation/native';


const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.article
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

const styles=StyleSheet.create({
    btn:{
        margin:15,
        padding:10,
        borderRadius:20
    },
    txt:{
        fontFamily:"Poppins-Light",
        fontSize:14
    },
    card:{
        elevation:20,
        borderRadius:20,
        marginBottom:20
    },
    message:{
        fontFamily:'Poppins-Light',padding:10,textAlign:'center'
    },
    title:{
        fontSize:40,fontFamily:"Poppins-Black",padding:20,paddingBottom:0,
        textShadowColor:R.color.colorPrimary,textShadowOffset:{width:10,height:2},textShadowRadius:30
    },
    subTitle:{
        fontSize:20,fontFamily:"Poppins-Thin",padding:10,paddingTop:0,paddingLeft:50,marginTop:-15
    }
})

class App extends Component{

    constructor(props){
        super(props)
        this.state={
            name:"",
            address:"",
            showShop:false,
            showDevise:false,
            devise:"",
            signe:"",
            showProgress:false,
            checked:false
        }
    }
    onQuit=()=>{
        this.props.navigation.dispatch(
            StackActions.replace('Begin')
        )
    }
    onReturn=()=>{
        this.setState({showDevise:false})
        setTimeout(()=>{
            this.setState({showShop:true})
        },200)
    }
    onForward=()=>{
        const name=this.state.name;
        if(this.isEmpty(name)){
            Alert.alert('Vérification',"Ecrivez correctement le nom de votre boutique")
        }else{
            this.setState({showShop:false})
            setTimeout(()=>{
                this.setState({showDevise:true})
            },200)
        }
    }
    onCreateRight=async(id)=>{
        try {
            //allow to create, view and update category
            await RightModel.store({
                code:S.ENUM.CATEGORY,
                value:7,
                is_updated:1,
                id_agent:id,
            })
            //allow to create, view and update shop and agents
            await RightModel.store({
                code:S.ENUM.DASHBOARD,
                value:7,
                is_updated:1,
                id_agent:id,
            })
            //allow to create, view and update customer
            await RightModel.store({
                code:S.ENUM.CLIENT,
                value:7,
                is_updated:1,
                id_agent:id,
            })
            //allow to create, view and update devise
            await RightModel.store({
                code:S.ENUM.DEVISE,
                value:7,
                is_updated:1,
                id_agent:id,
            })
            //allow to create, view journal
            await RightModel.store({
                code:S.ENUM.JOURNAL_PRODUIT,
                value:7,
                is_updated:1,
                id_agent:id,
            })
            //allow to create, view and update article
            await RightModel.store({
                code:S.ENUM.PRODUCT,
                value:7,
                is_updated:1,
                id_agent:id,
            })
            //allow to create, view and update stock
            await RightModel.store({
                code:S.ENUM.STOCK_IN,
                value:7,
                is_updated:1,
                id_agent:id,
            })
            //allow to create, view and update sales
            await RightModel.store({
                code:S.ENUM.STOCK_OUT,
                value:7,
                is_updated:1,
                id_agent:id,
            })
            //allow to create, view and update sale
            await RightModel.store({
                code:S.ENUM.VENTE_PRODUIT,
                value:7,
                is_updated:1,
                id_agent:id,
            })
        } catch (error) {

        }
    }
    onSave=async ()=>{
        try {
            const name=this.state.name;
            const address=this.state.address;
            const devise=this.state.devise;
            const signe=this.state.signe;
            const user=this.props.user.value;
            let rep=null;
            if(!this.state.checked){
                Alert.alert("Vérification","Rassurez-vous d'avoir lu et accepté le terme de contrat de Ligal Platform");
                return;
            }
            if(!this.isEmpty(name) && !this.isEmpty(devise) && !this.isEmpty(signe) && user.id>0){
                this.setState({showProgress:true})
                const data={name,address,type:"SHOP"};
                const repSociety=await ShopWebModel.store(data);
                if(repSociety.status!=200 && repSociety!=201){
                    Alert.alert("Vérification","La création de boutique a échoué");
                    console.log("REP SOCIETY",repSociety.response);
                    this.setState({showProgress:false})
                    return;
                }
                const soc=repSociety.response;
                //save shop
                let rep=await ShopModel.store({
                    name:soc.name,
                    address:soc.address,
                    is_updated:0,
                    private_key:soc.private_key,
                })
                const date=DateTime.format(new Date(),"YYYY-MM-DD HH:mm:ss");
                if(rep){
                    //save devise
                    rep=await DeviseModel.store({
                        name:devise,
                        signe:signe,
                        type:1,
                        is_updated:1,
                        status:1,
                    });
                    //save work
                    await AgentModel.storeWork({
                        created_at:date,
                        is_updated:1,
                        id_agent:user.id,
                        id_shop:rep.id,
                        poste:"Admin",
                        status:1,
                        updated_at:date
                    })

                    await RightModel.get(user.id).then(right=>{
                        this.setRights(user.id).then(rep5=>{
                            AgentModel.show(user.id).then(work=>{
                                const action={type:S.EVENT.onChangeUser,status:true,value:work,work:right}
                                this.props.dispatch(action)
                                UserSetStorage(JSON.stringify({user:work})).then(rep6=>{
                                    //go
                                    const actionShop={type:S.EVENT.onChangeShop,value:rep};
                                    this.props.dispatch(actionShop)
                                    this.props.navigation.dispatch(
                                        StackActions.replace('Home')
                                    )
                                    this.setState({
                                        showProgress:false
                                    })
                                })
                            });
                        });
                    });
                }
                else{
                    Alert.alert("Vérification","La boutique n'est pas sauvegardée correctement");
                }
            }else{
                Alert.alert("Vérification","Remplissez correctement des cases");
            }
        } catch (error) {
        }
    }

    setRights=async(id)=>{
        let rep=false;
        try {
            const right=await RightModel.get(id);
            const element=new Object();
            const len=right.length;
            element.type=S.EVENT.OnChangeRight;
            const event=(item,index)=>{
                const type=S.ENUM;
                switch (item.code) {
                    case capitalize(type.CATEGORY).toLowerCase():
                        element.category=item.value;
                    break;
                    case capitalize(type.DEVISE).toLowerCase():
                        element.devise=item.value;
                    break;
                    case capitalize(type.VENTE_PRODUIT).toLowerCase():
                        element.vente_produit=item.value;
                    break;
                    case capitalize(type.JOURNAL_PRODUIT).toLowerCase():
                        element.journal=item.value;
                    break;
                    case capitalize(type.DASHBOARD).toLowerCase():
                        element.dashboard=item.value;
                    break;
                    case capitalize(type.STOCK_IN).toLowerCase():
                        element.stock=item.value;
                    break;
                    case capitalize(type.STOCK_OUT).toLowerCase():
                        element.sales=item.value;
                    break;
                    case capitalize(type.CLIENT).toLowerCase():
                        element.customer=item.value;
                    break;
                    case capitalize(type.PRODUCT).toLowerCase():
                        element.article=item.value;
                    break;
                }
            }
            await right.map(event);
            this.props.dispatch(element)
            rep=true;
            return rep;
        } catch (error) {
        }
    }

    componentDidMount(){
        setTimeout(()=>{
            this.setState({showShop:true})
        },1000)
    }

    isEmpty=(value="")=>{
        let rep=false;
        if(value==undefined || value==null){
            rep=true;
        }else{
            if(value.trim()==""){
                rep=true
            }
        }
        return rep;
    }





    render(){
        return(
            <>
            <StatusBar hidden animated />
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
            <View style={{alignItems:'center',margin:30,marginTop:50}}>
                <Image
                    source={require('../assets/logo/ligal.png')}
                    />
                </View>
            </View>
            <Overlay
                isVisible={this.state.showShop} fullScreen overlayStyle={{padding:0}}
                animationType="slide"
                key="shop"
            >
                <ScrollView >
                    <View style={{alignItems:'center',margin:30}}>
                        <Image
                        source={require('../assets/logo/ligal.png')}
                        />
                    </View>
                    <Card  containerStyle={[styles.card]}>
                    <ScrollView>
                    <Input
                        value={this.state.name}
                        label="Nom de la boutique"
                        placeholder="Boutique, magasin, société, etc "
                        onChangeText={value=>this.setState({name:value})}
                        maxLength={20}
                        onSubmitEditing={()=>this.inputAddress.focus()}
                        returnKeyType='next'
                    />
                    <Input
                        value={this.state.address}
                        label="Adresse de référence"
                        placeholder="siége de votre boutique, magasin, société, etc."
                        onChangeText={value=>this.setState({address:value})}
                        maxLength={100}
                        multiline
                        ref={x=>this.inputAddress=x}
                        inputContainerStyle={{maxHeight:100}}
                        inputStyle={{textAlignVertical:"top"}}
                    />
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <View style={{flex:1}}>
                            <Button
                                title="Annuler"
                                buttonStyle={[styles.btn]}
                                titleStyle={[styles.txt,{color:R.color.colorPrimary}]}
                                type="outline"
                                onPress={()=>this.onQuit()}
                            />
                        </View>
                        <View style={{flex:1}}>
                            <Button
                                title="Suivant"
                                buttonStyle={[styles.btn]}
                                titleStyle={[styles.txt]}
                                onPress={()=>this.onForward()}
                            />
                        </View>
                    </View>
                    </ScrollView>
                    </Card>
                </ScrollView>
            </Overlay>

            <Overlay
                isVisible={this.state.showDevise} fullScreen overlayStyle={{padding:0}}
                animationType="slide"
                key="register"
            >
                <ScrollView>
                    <View style={{alignItems:'center',margin:30}}>
                        <Image
                        source={require('../assets/logo/ligal.png')}
                        />
                    </View>
                    <Card containerStyle={[styles.card]}>
                    <ScrollView>
                    <Input
                        value={this.state.devise}
                        label="Nom de la monnaie"
                        placeholder="franc congolais, dollars, euro, yen, etc"
                        onChangeText={value=>this.setState({devise:value})}
                        maxLength={20}
                        onSubmitEditing={()=>this.inputSigne.focus()}
                        returnKeyType='next'
                    />
                    <Input
                        value={this.state.signe}
                        label="Symbole de la monnaie"
                        placeholder="$, CDF, F, CFA, £, etc."
                        onChangeText={value=>this.setState({signe:value})}
                        maxLength={5}
                        ref={x=>this.inputSigne=x}
                    />
                    <View>
                        <CheckBox
                            checked={this.state.checked}
                            title="Vous acceptez le terme de contrat"
                            onPress={()=>this.props.navigation.navigate('WebContract')}
                            onIconPress={()=>this.setState({checked:!this.state.checked})}
                            checkedColor={R.color.colorPrimary}
                            uncheckedColor={'red'}
                        />
                    </View>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <View style={{flex:1}}>
                            <Button
                                title="Retourner"
                                buttonStyle={[styles.btn]}
                                titleStyle={[styles.txt,{color:R.color.colorPrimary}]}
                                type="outline"
                                onPress={()=>this.onReturn()}
                            />
                        </View>
                        <View style={{flex:1}}>
                            <Button
                                title="Enregistrer"
                                buttonStyle={[styles.btn]}
                                titleStyle={[styles.txt]}
                                onPress={()=>this.onSave()}
                            />
                        </View>
                    </View>
                    </ScrollView>
                    </Card>
                </ScrollView>
            </Overlay>
            <ProgressDialog
                message="Patientez svp!"
                title="Chargement"
                activityIndicatorColor={R.color.colorSecondary}
                activityIndicatorSize='large'
                animationType="fade"
                visible={this.state.showProgress}
            />
            </>
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(App)