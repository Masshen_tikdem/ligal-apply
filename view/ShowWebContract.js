import React from 'react';
import { Component } from 'react';
import { View } from 'react-native';
import { Avatar, Button, Overlay, Text } from 'react-native-elements';
import { ProgressDialog } from 'react-native-simple-dialogs';
import {WebView} from 'react-native-webview';
import R from '../resources/styles.json';



class App extends Component{

    constructor(props){
        super(props);

        this.state={
            showError:false,
            loading:true
        }
    }


    render(){

        console.log('PROPS',this.props);
        return(
            this.state.showError==false?(
                <>
                <WebView
                    source={{ uri:'https://web.ligalplatform.com/contract' }}
                    renderError={()=>(
                        <View style={{ flex:1,justifyContent:'center',alignItems:'center' }}>
                            <Text>
                                Error
                            </Text>
                        </View>
                    )}
                    onError={(event)=>{
                        console.log('error',event);
                        this.setState({showError:true})
                    }}
                    onLoad={(event)=>{
                        this.setState({loading:false});
                        if(event.nativeEvent.canGoBack==false){
                            this.setState({showError:true})
                        }
                    }}
                />
                <ProgressDialog
                    message="Patientez SVP!"
                    title="Recherche de la page"
                    activityIndicatorColor={R.color.colorSecondary}
                    activityIndicatorSize='large'
                    animationType='fade'
                    visible={this.state.loading}
                />
                </>
            ):(
                <Overlay fullScreen isVisible={this.state.showError} overlayStyle={{ padding:0 }} animationType='fade'>
                    <View style={{ flex:1,alignItems:'center',justifyContent:'center' }}>
                        <View style={{ alignItems:'center' }}>
                            <Avatar
                                source={require('../assets/logo/ligal.png')}
                                size='xlarge' rounded
                            />
                            <View style={{ alignItems:'center' }}>
                                <Button
                                    title="Actualiser"
                                    buttonStyle={{ padding:20 }}
                                    type='clear'
                                    onPress={()=>this.setState({showError:false,loading:true})}
                                />
                            </View>

                        </View>

                            <View style={{ position:'absolute',bottom:30,right:10 }}>
                                <Button
                                    title="Quitter"
                                    buttonStyle={{ padding:20 }}
                                    titleStyle={{ color:'red' }}
                                    type='clear'
                                    onPress={()=>this.props.navigation.goBack()}
                                />
                            </View>
                    </View>
                </Overlay>
            )
        )
    }
}

export default (App);