import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import info from './viewCategory/info';
import marks from './viewCategory/marks';
import articles from './viewCategory/articles';
import { FAB } from 'react-native-paper';
import R from '../resources/styles.json';
import ModalCategory from '../components/ModalCreateCategory';
import ModalArticle from '../components/ModalCreateArticle';
import { useState } from 'react';
import { Component } from 'react';
import Empty from '../components/EmptyMessage';
import Awaiter from '../components/Waiter';
import { ConfirmDialog, ProgressDialog } from 'react-native-simple-dialogs';
import * as ArticleModel from '../model/local/article';
import * as FamilleModel from '../model/local/family';
import { capitalize, downloadImage, getImage, getImageLocal, getRandomString, saveImage, update, write } from '../Manager';
import * as DeviseModel from '../model/local/devise';
import * as ShopModel from '../model/local/shop';
import { Alert } from 'react-native';
import { View } from 'react-native';
import { Avatar, Button, ListItem, Text } from 'react-native-elements';
import { connect } from 'react-redux';
import S from '../resources/settings.json';
import ImagePicker from 'react-native-image-picker';
import { Picker } from '@react-native-picker/picker';
import { CommonActions, StackActions } from '@react-navigation/native';
import CatalogModal from '../components/ModalCatalog';
import * as CatalogWebModel from '../model/web/catalog';
import CardCatalog from '../components/CardCatalog';
import * as Shared from '../service/shared';


const Tab=createMaterialTopTabNavigator();

const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.article,
        right:state.right,
        shop:state.shop
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

class App extends Component{

    constructor({props,route}){
        super(props)
        this.state={
            opened:false,
            showCat:false,
            showArticle:false,
            showProgress:false,
            showMark:false,
            isLoading:true,
            route,
            articles:[],
            devises:[],
            item:null,
            list:[],
            marks:[],
            categories:[],
            articleName:'',
            articleDescription:'',
            articlePrice:0,
            articleFamily:0,
            articleQuantifiable:false,
            articleDevise:0,
            showDevise:false,
            showChangeMak:false,
            catName:"",
            catType:"",
            uri:null,
            btns:[],
            logoArticle:null,
            category:0,
            showCatalog:false,
            catalogs:[],
            catalogLoad:false,
            type:null,
            stock:0,
            mode:null,
            maxLength:5
        }
    }
    onShowCat=()=>{
        try {
            this.setState({
                showCat:true,
                catName:this.state.item.name,
                catType:this.state.item.type
            })
        } catch (error) {
            
        }
    }
    onShowMark=()=>{
        try {
            this.setState({
                showMark:true,
                catName:"",
                catType:""
            })
        } catch (error) {
            
        }
    }
    downloadLogo=async(item)=>{
        try {
            const name=this.state.catName;
            const type=this.state.catType;
            const id=this.state.item.id;
            const logo=getImage(item.logo);
            this.setState({showProgress:true,showCatalog:false})
            const imageName='category'+id+getRandomString(20)+'.ligal';
            downloadImage(imageName.toLowerCase(),logo,item.logo).then(async load=>{
                if(load==true){
                const rep=await FamilleModel.update({
                    id:id,
                    logo:imageName.toLowerCase(),
                    image_updated:1,
                    is_updated:1
                })
                if(rep==true){
                    const action={type:S.EVENT.onChangeMarketArticle,category:true,sale:true}
                    const actionCat={type:S.EVENT.onChangeMarketCategory,market:true,category:true,sale:true}
                    const actionService={type:S.EVENT.OnExecuteService,category:true}
                    await this.props.dispatch(action);
                    await this.props.dispatch(actionCat);
                    this.props.dispatch(actionService);
                    const item=await FamilleModel.show(id);
                    this.setState({
                        item:item
                    })
                }
                this.setState({showProgress:false})
                }else{
                    this.setState({showProgress:false})
                }
            });
            
        } catch (error) {
        }
    }
    renderCatalog=({item})=>{
        return(
            <CardCatalog
                onPress={()=>this.downloadLogo(item)}
                key="card"
                image={getImage(item.logo)}
                name={item.name}
            />
        )
    }
    onGetCatalog=async()=>{
        try {
            const name=this.state.catName;
            const type=this.state.catType;
            const id=this.state.item.id;
            this.setState({catalogLoad:true,showCatalog:true})
            const rep=await CatalogWebModel.get();
            if(rep.status==200){
                this.setState({catalogs:rep.response})
            }
            this.setState({catalogLoad:false})
        } catch (error) {
            
        }
    }
    onShowChangeMark=async()=>{
        try {
            const item=this.state.route.params.item;
            if(this.state.categories.length==0){
                this.setState({showProgress:true})
                const rep=await FamilleModel.get();
                this.setState({categories:rep})
                this.setState({showProgress:false,showChangeMak:true,category:item.category_id})
            }else{
                this.setState({showChangeMak:true,category:item.category_id})
            }
        } catch (error) {
            
        }
    }
    onChangeCategory=async()=>{
        try {
            const item=this.state.route.params.item;
            const category=this.state.category;
            const id=item.id_link;
            if(category>0){
                if(item.category_id==category){
                    this.setState({showChangeMak:false})
                }else{
                    this.setState({
                        showProgress:true,
                        showChangeMak:false
                    })
                    const rep=await FamilleModel.updateLink({
                        high_family:category,
                        id,
                        is_updated:1
                    })
                    if(rep==true){
                        const action={type:S.EVENT.onChangeMarketCategory,market:true,category:true}
                        const action2={type:S.EVENT.onChangeMarketArticle,category:true}
                        const actionService={type:S.EVENT.OnExecuteService,link:true} 
                        await this.props.dispatch(action);
                        await this.props.dispatch(action2);
                        await this.props.dispatch(actionService);
                        this.props.navigation.dispatch(
                            CommonActions.goBack()
                        )
                        this.setState({showProgress:false})
                    }else{
                        this.setState({showProgress:false})
                    }
                }
            }else{
                this.setState({showChangeMak:false})
            } 
        } catch (error) {
        }
    }
    async componentDidMount(){
        try {
            const item=this.state.route.params.item;
            this.setState({type:item.type})
            const list=[];
            if(item.category!=null){
                list.push({
                    icon:'close',
                    label:'Change de catégorie',
                    style:{backgroundColor:'brown'},
                    onPress:()=>this.onShowChangeMark()
                })
            }
            if(item.category==null){
                if(write(this.props.right.category)){
                    list.push({
                        icon:'pencil',
                        label:'Ajouter une sous-catégorie',
                        style:{backgroundColor:R.color.colorPrimary},
                        onPress:()=>this.onShowMark()
                    })
                }
                /*list.push({
                    icon:'close',
                    label:'Devenir une marque',
                    style:{backgroundColor:R.color.colorAccent},
                    onPress:()=>this.onShowMark()
                })*/
            }
            if(update(this.props.right.category)){
                list.push({
                    icon:'pencil',
                    label:'Modifier',
                    style:{backgroundColor:R.color.colorPrimary},
                    onPress:()=>this.onShowCat()
                });
            }
            if(write(this.props.right.article)){
                list.push({
                    icon:'plus',
                    label:item.type=='service'?"Ajouter un service":item.type=="product"?"Ajouter un produit":"Ajouter un article",
                    style:{backgroundColor:R.color.colorSecondary},
                    onPress:()=>this.setState({showArticle:true}),
                    color:R.color.background
                })
            }
            this.setState({
                btns:list
            })
            await this.onLoad();
        } catch (error) {
            
        }
    }
    onLoadArticle=async()=>{
        try {
            const item=this.state.route.params.item;
            this.setState({type:item.type})
            let m=[];
            if(item.category==null){
                m=await FamilleModel.marks(item.id);
            }
            const rep=await ArticleModel.get(item.id);
            this.setState({
                articles:rep,
                marks:m
            })
        } catch (error) {
            
        }
    }
    onLoad=async()=>{
        try {
            const item=this.state.route.params.item;
            this.setState({type:item.type})
            let m=[];
            if(item.category==null){
                m=await FamilleModel.marks(item.id);
            }
            const rep=await ArticleModel.get(item.id);
            const p=await FamilleModel.get();
            const d=await DeviseModel.get();
            if(d.length>0){
                this.setState({
                    articleDevise:d[0].id
                })
            }
            this.setState({
                articles:rep,
                item:item,
                list:p,
                devises:d,
                marks:m
            })
            if(this.state.isLoading==true){
                this.setState({
                    isLoading:false
                })
            }
        } catch (error) {
            
        }
    }
    articleNameUse=()=>{
        let rep=false;
        try {
            const name=this.state.articleName;
            this.state.articles.map((item)=>{
                if(capitalize(item.name).toLowerCase()==capitalize(name).toLowerCase()){
                    rep=true
                    return rep;
                }
            })
        } catch (error) {
            
        }
        return rep;
    }
    invalidPrice=()=>{
        let rep=true;
        try {
            const a=parseFloat(this.state.articlePrice);
            if(a>0){
                rep=false;
            }else{
                if(this.state.type==S.ENUM.article.service){
                    if(this.state.mode==S.ENUM.stockage.devise){
                        rep=false;   
                    }
                    
                }
            }
            
        } catch (error) {
            
        }
        return rep;
    }
    getCategory=()=>{
        this.setState({
            articleFamily:this.state.item.id
        })
        return this.state.item.name;
    }
    isValidateCategoryName=()=>{
        let rep=true;
        try {
            this.state.list.map(item=>{
                if(capitalize(item.name).toLowerCase()==capitalize(this.state.catName).toLowerCase() && item.id!=this.state.item.id){
                    rep=false;
                    return rep;
                }
            })
        } catch (error) {
            
        }
        return rep;
    }

    isValidateMarkName=()=>{
        let rep=true;
        try {
            this.state.marks.map(item=>{
                if(capitalize(item.name).toLowerCase()==capitalize(this.state.catName).toLowerCase()){
                    rep=false;
                    return rep;
                }
            })
        } catch (error) {
            
        }
        return rep;
    }
    renderItemMenu=({item})=>{
        return(
            <View style={{flex:1}}>
                <ListItem onPress={()=>this.setState({articleDevise:item.id,showDevise:false})}>
                    <Avatar
                        title={item.signe}
                        titleStyle={{fontSize:20,fontWeight:'bold',color:R.color.colorPrimary}}
                    />
                    <Text numberOfLines={1}>
                        {item.name}
                    </Text>
                </ListItem>
            </View>
        )
    }
    getDevise=()=>{
        let rep="Monnaie"
        try {
            if(this.state.articleDevise>0){
                this.state.devises.map(item=>{
                    if(item.id==this.state.articleDevise){
                        rep=item.signe;
                        return rep;
                    }
                })
            }
        } catch (error) {
            
        }
        return rep;
    }
    onSaveLogo=()=>{
        ImagePicker.launchImageLibrary({},rep=>{
            if(rep.didCancel){
                
            }else if(rep.error){
               
            }
            else if(rep.customButton){
                
            }else{
                const uri=rep.uri
                this.setState({
                    uri
                })
            }
        })
    }

    onSaveLogoArticle=()=>{
        ImagePicker.launchImageLibrary({},rep=>{
            if(rep.didCancel){
                
            }else if(rep.error){
                
            }
            else if(rep.customButton){
                
            }else{
                const uri=rep.uri
                this.setState({
                    logoArticle:uri
                })
            }
        })
    }
    onUpdateCategory=async()=>{
        try {
            const user=this.props.user;
            const shop=this.props.shop;
            const name=this.state.catName;
            const type=this.state.catType;
            const id=this.state.item.id;
            const uri=this.state.uri;
            let logo=null;
            if(uri!=null){
                logo="category"+id+getRandomString(20)+".ligal";
            }
            if(this.isValidateCategoryName() && name.length>0){
                this.setState({
                    showProgress:true
                })
                if(logo!=null){
                    await saveImage(logo.toLowerCase(),uri,this.state.item.logo)
                }
                const rep=await FamilleModel.update({
                    name:name,
                    id:id,
                    type:this.state.articles.length==0?type:null,
                    logo:logo,
                    is_updated:1,
                })
                if(rep!=null){
                    this.setState({
                        uri:null
                    })
                    const action={type:S.EVENT.onChangeMarketArticle,category:true,sale:true}
                    const actionCat={type:S.EVENT.onChangeMarketCategory,market:true,category:true,sale:true}
                    Shared.category({
                        id:rep.id,
                        name:rep.name,
                        private_key:rep.private_key,
                        shop,
                        status:rep.status,
                        type:rep.type,
                        user
                    })
                    await this.props.dispatch(action);
                    await this.props.dispatch(actionCat);
                    //this.props.dispatch(actionService);
                    const item=await FamilleModel.show(id);
                    this.setState({
                        item:item
                    })
                }
                this.setState({
                    showProgress:false,
                    showCat:false
                })
            }else{
                Alert.alert("Vérification","Impossible de modifier! Veuillez vérifier vos données.")
            }
        } catch (error) {
        }
    }

    onSaveMark=async()=>{
        try {
            const name=this.state.catName;
            const type=this.state.item.type;
            const uri=this.state.uri;
            const id=this.state.item.id;
            let logo=null;
            if(uri!=null){
                logo="category"+getRandomString(20)+".ligal";
            }
            if(this.isValidateMarkName() && name.length>0){
                this.setState({
                    showProgress:true
                })
                if(logo!=null){
                    await saveImage(logo.toLowerCase(),uri,null)
                }
                const rep=await FamilleModel.store({
                    name:name,
                    type:type,
                    logo:logo,
                    is_updated:1,
                    status:1
                })
                if(rep==true){
                    this.setState({
                        uri:null
                    })
                    const l=await FamilleModel.last();
                    if(l!=null){
                        if(capitalize(l.name).toLowerCase()==capitalize(this.state.catName).toLowerCase()){
                            const rep2=await FamilleModel.storeLink({
                                high_family:id,
                                is_updated:1,
                                status:1,
                                sub_family:l.id
                            });
                            if(rep2==true){
                                const action={type:S.EVENT.onChangeMarketArticle,category:true,sale:true}
                                const actionCat={type:S.EVENT.onChangeMarketCategory,market:true,category:true,sale:true}
                                const actionService={type:S.EVENT.OnExecuteService,category:true,link:true}
                                await this.props.dispatch(action);
                                await this.props.dispatch(actionCat);
                                await this.props.dispatch(actionService);
                                const item=await FamilleModel.show(id);
                                this.setState({
                                    item:item,
                                    showMark:false
                                })
                            }
                        }    
                    }
                }
                this.setState({
                    showProgress:false,
                    showCat:false
                })
            }else{
                Alert.alert("Vérification","Impossible de modifier! Veuillez vérifier vos données.")
            }
        } catch (error) {
        }
    }
    onSaveArticle=async ()=>{
        try {
            const user=this.props.user.value;
            const name=this.state.articleName;
            const description=this.state.articleDescription;
            const price=this.state.articlePrice;
            const family=this.state.item.id;
            const type=this.state.articleQuantifiable;
            const devise=this.state.articleDevise;
            const typeCat=this.state.type;
            if(!this.articleNameUse() && this.invalidPrice()==false && name.trim()!="" && devise>0 && family>0 ){
                this.setState({showProgress:true})
                    const uri=this.state.logoArticle;
                    let logo=null;
                    if(uri!=null){
                        const image=getRandomString(20)+".ligal";
                        const l=await saveImage(image.toLowerCase(),uri,null);
                        if(l==true){
                            logo=image.toLowerCase();
                        }
                    }
                    const shop=await ShopModel.get();
                    if(shop==null){
                        Alert.alert('Vérification',"Erreur de l'enregistrement")
                        return;
                    }
                    const rep=await ArticleModel.store({
                        description:description,
                        devise:devise,
                        famille:family,
                        is_updated:1,
                        name:name,
                        price:(this.state.type==S.ENUM.article.service && this.state.mode==S.ENUM.stockage.devise)?0:price,
                        quantifiable:this.state.stock==1?1:type?1:0,
                        shop:shop.id,
                        logo:logo,
                        status:1,
                        type:this.state.type,
                        mode:this.state.type!="service"?null:this.state.mode,
                        stockage:this.state.type!="service"?null:this.state.stock
                    });
                    if(rep!=null){
                        this.setState({
                            articleDescription:'',
                            articleName:'',
                            articlePrice:0,
                            showArticle:false,
                            logoArticle:null,
                            stock:0,
                            mode:null,
                        })
                        const action={type:S.EVENT.onChangeMarketArticle,market:true,category:true,added:true,sale:true}
                        //const actionService={type:S.EVENT.OnExecuteService,article:true}
                        Shared.article({
                            description:rep.description,
                            devise_private_key:rep.devise_private_key,
                            famille_private_key:rep.famille_private_key,
                            id:rep.id,
                            mode:rep.mode,
                            name:rep.name,
                            price:rep.price,
                            private_key:rep.private_key,
                            quantifiable:rep.quantifiable,
                            stockage:rep.stockage,
                            type:rep.type,
                            user
                        });
                        this.props.dispatch(action);
                        //this.props.dispatch(actionService);
                        await this.onLoadArticle();
                        this.setState({showProgress:false})
                        Alert.alert("Enregistrement","Article ajouté avec succès");
                    }else{
                        this.setState({showProgress:false})
                        Alert.alert("Enregistrement","Echec");
                    }
                
            }else{
                Alert.alert("Vérification","Saisissez correctement des données");
            }
        } catch (error) {
            
        }
    }

    render(){
        
        return(
            <>
                {
                    this.state.isLoading==true?(
                        <Awaiter color={R.color.colorSecondary} size={100} />
                    ):(
                        <>
                        <Tab.Navigator
                            tabBarOptions={{
                                scrollEnabled:true
                            }}
                        >
                            <Tab.Screen
                                name="INFO"
                                component={info}
                                options={{title:"Présentation"}}
                                initialParams={{item:this.state.item,articles:this.state.articles}}
                            />
                            {
                                this.state.route.params.item.category==null?
                                (
                                    <Tab.Screen
                                        name="MARK"
                                        component={marks}
                                        options={{title:"Les sous-catégories"}}
                                        initialParams={{item:this.state.item,articles:this.state.articles}}
                                    />
                                ):null
                            }
                            <Tab.Screen
                                name="ARTICLE"
                                component={articles}
                                options={{title:this.state.type=='service'?"Les services":this.state.type=="product"?"Les produits":"Les articles"}}
                                initialParams={{item:this.state.item,articles:this.state.articles}}
                            />
                        </Tab.Navigator>
                        {
                            this.state.btns.length>0?(
                                <FAB.Group
                                    open={this.state.opened}
                                    key="fab"
                                    fabStyle={{backgroundColor:R.color.colorPrimaryDark}}
                                    icon={this.state.opened?'close':'plus'}
                                    theme={{
                                        animation:{scale:5},
                                        colors:{text:R.color.colorPrimaryDark},
                                        dark:true,
                                        roundness:30
                                    }}
                                    actions={this.state.btns}
                                    onStateChange={open=>this.setState({opened:!this.state.opened})}
                                />
                            ):null
                        }
                        <ModalArticle
                            visible={this.state.showArticle}
                            onClose={()=>this.setState({showArticle:false})}
                            logo={this.state.logoArticle==null?"p":this.state.logoArticle}
                            value={this.state.articleName}
                            onChangeText={(value)=>this.setState({articleName:value})}
                            valueDescription={this.state.articleDescription}
                            onChangeDescrption={value=>this.setState({articleDescription:value})}
                            valuePrice={this.state.articlePrice}
                            onChangePrice={value=>this.setState({articlePrice:value})}
                            statusName={this.articleNameUse()}
                            statusPrice={this.invalidPrice()}
                            category={capitalize(this.state.item.name)}
                            onPress={()=>this.onSaveArticle()}
                            onRadioNo={()=>this.setState({articleQuantifiable:false})}
                            onRadioYes={()=>this.setState({articleQuantifiable:true})}
                            type={this.state.articleQuantifiable}
                            onCloseMenu={()=>this.setState({showDevise:false})}
                            onShowMenu={()=>this.setState({showDevise:true})}
                            signe={this.state.articleDevise}
                            stock={this.state.stock}
                            onRadioStockNo={()=>this.setState({stock:0,mode:null})}
                            onRadioStockYes={()=>this.setState({stock:1,mode:S.ENUM.stockage.devise})}
                            devises={this.state.devises}
                            visibleDevise={this.state.showDevise}
                            service={this.state.type==S.ENUM.article.service?true:false}
                            onCamera={()=>this.onSaveLogoArticle()}
                            onChangeDevise={(v,index)=>{this.setState({articleDevise:v});}}
                            mode={this.state.mode}
                            onChangeMode={(mode)=>{
                                this.setState({mode:mode})
                            }}

                        />
                        <ModalCategory
                            visible={this.state.showCat}
                            onClose={()=>this.setState({showCat:false})}
                            logo={this.state.uri!=null?this.state.uri:getImageLocal(this.state.item.logo)}
                            value={this.state.catName}
                            onChangeText={value=>this.setState({catName:value})}
                            checked={this.state.catType}
                            status={!this.isValidateCategoryName()}
                            update
                            onProduct={()=>this.setState({catType:"product"})}
                            onService={()=>this.setState({catType:"service"})}
                            onPress={()=>this.onUpdateCategory()}
                            key="modal-cat"
                            onImage={()=>this.onSaveLogo()}
                            catalog
                            onCatalog={()=>this.onGetCatalog()}
                        />
                        <ModalCategory
                            visible={this.state.showMark}
                            onClose={()=>this.setState({showMark:false})}
                            logo={this.state.uri!=null?this.state.uri:"p"}
                            value={this.state.catName}
                            onChangeText={value=>this.setState({catName:value})}
                            checked={this.state.catType}
                            status={!this.isValidateMarkName()}
                            onProduct={()=>this.setState({catType:"product"})}
                            onService={()=>this.setState({catType:"service"})}
                            onPress={()=>this.onSaveMark()}
                            key="modal-mark"
                            onImage={()=>this.onSaveLogo()}
                            mark
                        />
                        <ProgressDialog
                            message="Patientez svp"
                            title="Chargement"
                            activityIndicatorColor={R.color.colorSecondary}
                            activityIndicatorSize="large"
                            animationType="slide"
                            visible={this.state.showProgress}
                        />
                        <ConfirmDialog
                            positiveButton={{title:"Changer",onPress:()=>this.onChangeCategory()}}
                            negativeButton={{title:"Annuler",onPress:()=>this.setState({showChangeMak:false})}}
                            visible={this.state.showChangeMak}
                            animationType='slide'
                            onTouchOutside={()=>this.setState({showChangeMak:false})}
                        >
                            <View>
                                <Text style={{padding:5,paddingLeft:10,fontFamily:'Poppins-Light',fontSize:16}}>
                                    Nom de la catégorie
                                </Text>
                                <Picker
                                    mode="dropdown"
                                    selectedValue={this.state.category}
                                    onValueChange={(item,index)=>this.setState({category:item})}
                                >
                                    {
                                        this.state.categories.map((item,index)=>{
                                            if(item.category==null){
                                                return(
                                                    <Picker.Item
                                                        value={item.id}
                                                        key={`index${item.id}`} 
                                                        label={capitalize(item.name).toUpperCase()}
                                                    />
                                                )
                                            }
                                        })
                                    }
                                </Picker>
                                <View style={{alignItems:'center',padding:5}}>
                                    <Button
                                        title="Supprimer l'appartenance"
                                        type="clear"
                                        titleStyle={{color:R.color.colorSecondary,fontSize:16,fontFamily:'Poppins-Black'}}
                                    />
                                </View>
                            </View>
                        </ConfirmDialog>
                        <CatalogModal
                            key="cat-key"
                            onClose={()=>this.setState({showCatalog:false})}
                            renderItem={this.renderCatalog}
                            visible={this.state.showCatalog}
                            isLoading={this.state.catalogLoad}
                            list={this.state.catalogs}

                        />
                    </>
                    )
                }
            </>
        )
    }
    
}

export default connect(mapStateToProps,mapDispatchToProps)(App);