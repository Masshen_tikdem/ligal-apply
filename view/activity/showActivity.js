import React,{useEffect,useState} from 'react';
import WebView from 'react-native-webview';
import { useSelector } from 'react-redux';

export default function App(props){

    const user=useSelector(state=>state.user.value);
    const shop=useSelector(state=>state.shop.value);


    return(
        <WebView
            source={{ uri:`https://activity.ligalplatform.com/app/?shop=${shop.private_key}&user=${user.private_key}` }}
        />
    )
}