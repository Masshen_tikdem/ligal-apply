import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import React from 'react';
import { StyleSheet } from 'react-native';
import { View } from 'react-native';
import { Avatar, Card, Icon, Text } from 'react-native-elements';
import { Drawer } from 'react-native-paper';
import R from '../resources/styles.json';
import * as Manager from '../Manager';
import { ImageBackground } from 'react-native';
import { Switch } from 'react-native';

const activeTintColor=R.color.colorSecondary;
const inactiveTintColor="#000"

const styles=StyleSheet.create({
    title:{
        fontFamily:'Poppins-Black',
        fontSize:16
    }
})

const enabled=(value)=>{
    let rep=true;
    if(value==0){
        rep=false;
    }
    return rep;
}

export function DrawerContent({props,user,shop,right,connnected=false,onChangeConnection,subscription}){

    return(
        <View style={{flex:1}}>
            <DrawerContentScrollView {...props}
            >
                <View style={{flex:1,marginBottom:10,marginTop:-10,marginLeft:-5,marginRight:-5}}>
                    <Card containerStyle={{margin:0,elevation:10,padding:0}}>
                        <ImageBackground
                            source={require('../assets/images/1630874.jpg')}
                            style={{flex:1,padding:15}}
                        >
                        <Avatar
                            icon={{name:'person',type:"ionicon"}}
                            rounded
                            source={{uri:Manager.getImageLocal(user.photo)}}
                            size="medium"
                            containerStyle={{backgroundColor:R.color.colorPrimary,margin:10}}
                        />
                        <Text numberOfLines={1} style={{fontFamily:'Poppins-Light',color:R.color.background,fontSize:16}}>
                            {Manager.getFullName(user.first_name,user.last_name)}
                        </Text>
                        <Text numberOfLines={1} style={{fontFamily:'Poppins-Bold',color:R.color.background,fontSize:16}}>
                            {Manager.capitalize(shop.name).toUpperCase()}
                        </Text>
                        </ImageBackground>
                    </Card>
                </View>
                <Drawer.Section >
                    <DrawerItem
                        label="Ventes"
                        onPress={()=>enabled(right.vente_produit)?props.navigation.navigate("Sale"):null}
                        icon={({color,size})=>(
                            <Icon name='shopping-basket' type='font-awesome' iconStyle={{opacity:enabled(right.vente_produit)?1:0.4}} color={color} />
                        )}
                        activeTintColor={props.activeTintColor}
                        inactiveTintColor={props.inactiveTintColor}
                        focused={props.state.index===0}
                        labelStyle={{opacity:enabled(right.vente_produit)?1:0.4}}
                    />
                    <DrawerItem
                        label="Journal"
                        onPress={()=>enabled(right.journal)?props.navigation.navigate("Journal"):null}
                        icon={({color,size})=>(
                            <Icon iconStyle={{opacity:enabled(right.journal)?1:0.4}} name='tags' type='font-awesome' color={color} />
                        )}
                        activeTintColor={props.activeTintColor}
                        inactiveTintColor={props.inactiveTintColor}
                        //focused={props.state.index===1}
                        style={styles.title}
                        labelStyle={{opacity:enabled(right.journal)?1:0.4}}
                    />
                    <DrawerItem
                        label="Informations"
                        onPress={()=>props.navigation.navigate("Info")}
                        icon={({color,size})=>(
                            <Icon  name='book' type='font-awesome' color={color} />
                        )}
                        activeTintColor={props.activeTintColor}
                        inactiveTintColor={props.inactiveTintColor}
                        //focused={props.state.index===2}
                        style={styles.title}
                    />
                    <DrawerItem
                        label="Tableau de bord"
                        onPress={()=>props.navigation.navigate("Dashbord")}
                        icon={({color,size})=>(
                            <Icon iconStyle={{opacity:1}} name='th-large' type='font-awesome' color={color} />
                        )}
                        activeTintColor={props.activeTintColor}
                        inactiveTintColor={props.inactiveTintColor}
                        //focused={props.state.index===3}
                        style={styles.title}
                        labelStyle={{opacity:1}}
                    />
                </Drawer.Section>
                <Drawer.Section title="Administration" >
                    <DrawerItem
                        label="Marché"
                        onPress={()=>((enabled(right.article) || enabled(right.category)) && subscription.status )?props.navigation.navigate("Market"):null}
                        icon={({color,size})=>(
                            <Icon iconStyle={{opacity:(enabled(right.article) || enabled(right.category))?1:0.4}} name='folder-open' type='font-awesome' color={color} />
                        )}
                        activeTintColor={props.activeTintColor}
                        inactiveTintColor={props.inactiveTintColor}
                        //focused={props.state.index===4}
                        style={styles.title}
                        labelStyle={{opacity:((enabled(right.article) || enabled(right.category)) && subscription.status )?1:0.4}}
                    />
                    <DrawerItem
                        label="Devises"
                        onPress={()=>(enabled(right.devise) && subscription.status)?props.navigation.navigate("Devises"):null}
                        icon={({color,size})=>(
                            <Icon iconStyle={{opacity:enabled(right.devise)?1:0.4}} name='money' type='font-awesome' color={color} />
                        )}
                        activeTintColor={props.activeTintColor}
                        inactiveTintColor={props.inactiveTintColor}
                        //focused={props.state.index===5}
                        style={styles.title}
                        labelStyle={{opacity:(enabled(right.devise) && subscription.status)?1:0.4}}
                    />
                    <DrawerItem
                        label="Comptes"
                        onPress={()=>(enabled(right.stock) && subscription.status)?props.navigation.navigate("Transaction"):null}
                        icon={({color,size})=>(
                            <Icon iconStyle={{opacity:enabled(right.stock)?1:0.4}} name='bar-chart' type='font-awesome' color={color} />
                        )}
                        activeTintColor={props.activeTintColor}
                        inactiveTintColor={props.inactiveTintColor}
                        //focused={props.state.index===6}
                        style={styles.title}
                        labelStyle={{opacity:(enabled(right.stock) && subscription.status)?1:0.4}}
                    />
                    <DrawerItem
                        label="Clients"
                        onPress={()=>enabled(right.customer)?props.navigation.navigate("Clients"):null}
                        icon={({color,size})=>(
                            <Icon iconStyle={{opacity:enabled(right.customer)?1:0.4}} name='user-o' type='font-awesome' color={color} />
                        )}
                        activeTintColor={props.activeTintColor}
                        inactiveTintColor={props.inactiveTintColor}
                        //focused={props.state.index===7}
                        style={styles.title}
                        labelStyle={{opacity:enabled(right.customer)?1:0.4}}
                    />
                </Drawer.Section>
                <Drawer.Section title="Préférences"  >
                    <DrawerItem
                        label="Boutique"
                        onPress={()=>enabled(right.vente_produit)?props.navigation.navigate("Settings"):null}
                        icon={({color,size})=>(
                            <Icon iconStyle={{opacity:enabled(right.vente_produit)?1:0.4}} name='gears' type='font-awesome' color={color} />
                        )}
                        activeTintColor={props.activeTintColor}
                        inactiveTintColor={props.inactiveTintColor}
                        focused={props.state.index===1}
                        style={styles.title}
                        labelStyle={{opacity:enabled(right.vente_produit)?1:0.4}}
                    />
                    <DrawerItem
                        label="Profil"
                        onPress={()=>props.navigation.navigate("Profil")}
                        icon={({color,size})=>(
                            <Icon name='person' type='ionicon' color={color} />
                        )}
                        activeTintColor={props.activeTintColor}
                        inactiveTintColor={props.inactiveTintColor}
                        style={styles.title}
                    />
                </Drawer.Section>
            </DrawerContentScrollView>
            <Drawer.Section>
                <View style={{flexDirection:'row',alignItems:'center',padding:10}}>
                    <Switch
                        trackColor={{false:"#ccc",true:R.color.colorSecondary}}
                        //thumbColor={connnected==true?}
                        value={connnected}
                        onValueChange={onChangeConnection}
                    />
                    <Text style={{fontFamily:"Poppins-Light",padding:5}}>
                        Utiliser l'internet
                    </Text>
                </View>
                {
                    subscription.status==false?(
                        <View>
                            <Text style={{ fontFamily:'Poppins-Bold',textAlign:'center',padding:5,color:'red' }}>
                                Votre période d'abonnement a touché à sa fin
                            </Text>
                        </View>
                    ):null
                }
            </Drawer.Section>
        </View>
    )
}