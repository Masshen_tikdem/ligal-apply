import React from 'react';
import { Component } from 'react';
import { View } from 'react-native';
import { Avatar, Button, ButtonGroup, Card, Icon, Input, Text } from 'react-native-elements';
import { connect } from 'react-redux';
import R from '../resources/styles.json';
import S from '../resources/settings.json';
import * as AgentModel from '../model/local/agent';
import { getFullName, getImageLocal, getRandomString, getSex, getTitle, isDifference, isDifferenceStrict, saveImage, UserSetStorage } from '../Manager';
import { StyleSheet } from 'react-native';
import { ScrollView } from 'react-native';
import RadioButton from '../components/RadioButton';
import { ProgressDialog } from 'react-native-simple-dialogs';
import ImagePicker from 'react-native-image-picker';
import * as shared from '../service/shared';


const styles=StyleSheet.create({
    title:{
        fontFamily:"Poppins-Black",
        color:R.color.background,
        fontSize:16
    },
    label:{
        fontFamily:"Poppins-Light",
        fontSize:14,
        fontWeight:"normal"
    },
    value:{
        fontFamily:"Poppins-Medium",
        fontSize:16,
        padding:5
    },
    view:{
        padding:10
    }
})

const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.article
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

class App extends Component{

    constructor(props){
        super(props)
        this.state={
            currentUser:{
                first_name:"",
                last_name:"",
                sex:"",
                address:"",
                phone:"",
                photo:null,
                id:0
            },
            index:0,
            first_name:"",
            last_name:"",
            sex:"",
            address:"",
            showProgress:false
        }
    }

    choiceImage=async()=>{
        ImagePicker.showImagePicker({
            cancelButtonTitle:'Annuler',
            takePhotoButtonTitle:'Prendre une photo',
            chooseFromLibraryButtonTitle:'Choisir dans la galerie',
            chooseWhichLibraryTitle:'Choisir',
            title:'Sélectionner une photo'
        },async response=>{
            try {
                if(response!=null){
                    const uri=response.uri;
                    const type=response.type;
                    const name=response.fileName;
                    const source={uri,type,name};
                    const data=new FormData();
                    data.append('photo',source);
                    if(uri!=null){
                        this.setState({showProgress:true})
                        const name=getRandomString(12)+".ligal";
                        const s= await saveImage(name.toLowerCase(),uri,this.state.currentUser.photo);
                        if(s==true){
                            const rep=await AgentModel.update({
                                photo:name.toLowerCase(),
                                id:this.state.currentUser.id,
                                is_updated:1,
                                image_updated:1
                            })
                            if(rep!=null){
                                shared.user({user:rep})
                                this.onReload();
                            }
                        }else{
                            this.setState({showProgress:false})
                        } 
                    }
                }
                this.setState({uri:response.data})
            } catch (error) {
                
            }
        })
    }

    async componentDidMount(){
        const user=this.props.user.value;
        this.setState({
            currentUser:{
                first_name:user.first_name,
                last_name:user.last_name,
                sex:user.sex,
                address:user.address,
                phone:user.phone,
                photo:user.photo,
                id:user.id
            }
        })
    }
    onChangeIndex=(value)=>{
        try {
            if(value==1){
                this.setState({
                    first_name:this.state.currentUser.first_name,
                    last_name:this.state.currentUser.last_name,
                    sex:this.state.currentUser.sex,
                    address:this.state.currentUser.address,
                })
            }
            this.setState({index:value})
        } catch (error) {
            
        }
    }
    componentProfil=()=><Text style={[styles.title,{color:this.state.index==0?R.color.background:R.color.colorPrimary}]}>Profil</Text>
    componentUpdate=()=><Text style={[styles.title,{color:this.state.index==1?R.color.background:R.color.colorPrimary}]} >Modification</Text>

    viewProfil=()=>(
        <ScrollView>
            <View style={styles.view}>
                <Text numberOfLines={1} style={styles.label}>
                    Nom de l'utilisateur
                </Text>
                <Text numberOfLines={1} style={styles.value}>
                    {getFullName(this.state.currentUser.first_name,this.state.currentUser.last_name)}
                </Text>
            </View>
            <Card.Divider/>
            <View style={styles.view}>
                <Text numberOfLines={1} style={styles.label}>
                    Genre
                </Text>
                <Text numberOfLines={1} style={styles.value}>
                    {getSex(this.state.currentUser.sex)}
                </Text>
            </View>
            <Card.Divider/>
            <View style={styles.view}>
                <Text numberOfLines={1} style={styles.label}>
                    Téléphone
                </Text>
                <Text numberOfLines={1} style={styles.value}>
                    {this.state.currentUser.phone}
                </Text>
            </View>
            <Card.Divider/>
            <Card.Divider/>
            <View style={styles.view}>
                <Text numberOfLines={1} style={styles.label}>
                    Adresse
                </Text>
                <Text numberOfLines={1} style={styles.value}>
                    {(this.state.currentUser.address==null || this.state.currentUser.address==undefined || this.state.currentUser.address=="")?"aucune adresse physique":this.state.currentUser.address}
                </Text>
            </View>
        </ScrollView>
    )
    viewUpdate=()=>(
        <ScrollView>
            <Card containerStyle={{marginBottom:10,elevation:10}}>
            <ScrollView>
            <View style={styles.view}>
                <Input
                    value={this.state.first_name}
                    onChangeText={value=>this.setState({first_name:value})}
                    autoCorrect
                    label="Votre prénom"
                    labelStyle={styles.label}
                    placeholder="Votre prénom ici"
                    maxLength={20}
                />
                <Input
                    value={this.state.last_name}
                    onChangeText={value=>this.setState({last_name:value})}
                    autoCorrect
                    label="Votre nom"
                    labelStyle={styles.label}
                    placeholder="Votre nom ici"
                    maxLength={20}
                />
            </View>
            <Card.Divider/>
            <View style={styles.view}>
                <Text numberOfLines={1} style={styles.label}>
                    Genre
                </Text>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    <View style={{flex:1,margin:5}}>
                        <Card containerStyle={{padding:5,margin:0}}>
                            <RadioButton
                                checked={this.state.sex}
                                color={R.color.colorPrimary}
                                title="Femme"
                                value="f"
                                onPress={()=>this.setState({sex:"f"})} 
                                direction='row'

                            />
                        </Card>
                    </View>
                    <View style={{flex:1,margin:5}}>
                        <Card containerStyle={{padding:5,margin:0}}>
                            <RadioButton
                                checked={this.state.sex}
                                color={R.color.colorPrimary}
                                title="Homme"
                                value="m"
                                onPress={()=>this.setState({sex:"m"})} 
                                direction='row'

                            />
                        </Card>
                    </View>
                </View>
            </View>
            <Card.Divider/>
            <View style={styles.view}>
                <Input
                    value={this.state.address}
                    onChangeText={value=>this.setState({address:value})}
                    autoCorrect
                    label="Votre adresse physique"
                    labelStyle={styles.label}
                    placeholder="là où vous résidez"
                    maxLength={100}
                    multiline
                />
            </View>

            <View>
                <Button
                    title="Modifier"
                    buttonStyle={{backgroundColor:R.color.colorPrimary,maxWidth:300,alignSelf:'center',padding:15,paddingLeft:30,paddingRight:30,borderRadius:20}}
                    onPress={()=>this.onUpdate()}
                />
            </View>
        </ScrollView>
            </Card>
        </ScrollView>
    )
    onReload=async()=>{
        try {
            const item=this.props.user.value;
            const rep=await AgentModel.show(item.id);
            if(rep!=null){
                const action={type:S.EVENT.onChangeUser,value:rep}
                const actionService={type:S.EVENT.OnExecuteService,user:true}
                await this.props.dispatch(action);
                await this.props.dispatch(actionService);
                await UserSetStorage(JSON.stringify({user:rep}))
                const user=this.props.user.value;
                this.setState({
                    currentUser:{
                        first_name:user.first_name,
                        last_name:user.last_name,
                        sex:user.sex,
                        address:user.address,
                        phone:user.phone,
                        photo:user.photo,
                        id:user.id
                    }
                })
                this.setState({showProgress:false,index:0})
            }else{
                this.setState({showProgress:false})
            }
        } catch (error) {
            this.setState({showProgress:false})
        }
    }
    onUpdate=async()=>{
        try {
            let first_name=this.state.first_name;
            let last_name=this.state.last_name;
            let sex=this.state.sex;
            let address=this.state.address;
            this.setState({
                showProgress:true
            })
            const rep=await AgentModel.update({
                first_name:isDifferenceStrict(this.state.currentUser.first_name,first_name)?first_name:null,
                last_name:isDifferenceStrict(this.state.currentUser.last_name,last_name)?last_name:null,
                sexe:isDifferenceStrict(this.state.currentUser.sex,sex)?sex:null,
                address:isDifferenceStrict(this.state.currentUser.address,address)?address:null,
                id:this.state.currentUser.id,
                is_updated:1
            })
            if(rep!=null){
                shared.user({user:rep})
                this.onReload();
            }else{
                this.setState({
                    showProgress:false
                })
            }
        } catch (error) {
            console.log('e',error)
        }
    }

    render(){
        return(
            <View style={{flex:1}}>
                <ButtonGroup
                    buttons={[
                        {element:this.componentProfil},
                        {element:this.componentUpdate}
                    ]}
                    selectedIndex={this.state.index}
                    onPress={value=>this.onChangeIndex(value)}
                    containerBorderRadius={10}
                    textStyle={styles.title}
                    buttonStyle={{borderRadius:20}}
                    buttonContainerStyle={{borderRadius:20}}
                    selectedButtonStyle={{backgroundColor:R.color.colorPrimary}}
                />
                <ScrollView>
                <View style={{alignItems:'center',padding:10}}>
                    <Avatar
                        source={{uri:getImageLocal(this.state.currentUser.photo)}}
                        rounded size="xlarge"
                        title={getTitle(this.state.currentUser.last_name)}
                        containerStyle={{borderWidth:4,borderColor:R.color.background,elevation:10,backgroundColor:R.color.colorPrimary}}
                        placeholderStyle={{backgroundColor:R.color.colorPrimary}}
                    >
                        <Avatar.Accessory
                            Component={()=>(
                                <Icon size={30} name='camera-outline'  type='ionicon' color={R.color.background}  />
                            )}
                            size={40} 
                            onPress={()=>this.choiceImage()}
                            style={{backgroundColor:R.color.colorPrimaryDark,borderColor:R.color.background,borderWidth:1}}
                        />
                    </Avatar>
                </View>
                {
                    this.state.index==0?(
                        <this.viewProfil/>
                    ):(
                        <this.viewUpdate/>
                    )
                }
                </ScrollView>
                <ProgressDialog
                    message="Patientez svp!"
                    title="Chargement"
                    visible={this.state.showProgress}
                    activityIndicatorColor={R.color.colorSecondary}
                    animationType="slide"
                    activityIndicatorSize="large"
                />
            </View>
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(App)