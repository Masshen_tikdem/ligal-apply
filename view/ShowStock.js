import React from 'react';
import { Component } from 'react';
import EmptyMessage from '../components/EmptyMessage';
import Awaiter from '../components/Waiter';
import R from '../resources/styles.json';
import S from '../resources/settings.json';
import * as StockModel from '../model/local/stock';
import { ScrollView } from 'react-native';
import { Avatar, Card, Text } from 'react-native-elements';
import { View } from 'react-native';
import { getFullName,getArticleName,capitalize, getImageLocal, getStock, getTitle, isEmpty } from '../Manager';
import DateTime from "date-and-time";
import fr from 'date-and-time/locale/fr';


class App extends Component{
    constructor(props){
        super(props)
        this.state={
            isLoading:true,
            //agent
            first_name:"",
            last_name:"",
            photo:"",
            //article
            name:"",
            description:"",
            famille:"",
            logo:null,
            devise:"",
            price:0,
            mode:"",
            type:"",
            //stock
            quantity:0,
            created_at:null,
            provider:null,



        }
    }

    async componentDidMount(){
        try {
            const id=this.props.route.params.id;
            const rep=await StockModel.showStock(id);
            if(rep!=null){
                const item=rep;
                this.setState({
                    first_name:item.first_name,
                    last_name:item.last_name,
                    photo:item.photo,
                    quantity:item.quantity,
                    provider:item.fournisseur,
                    created_at:item.created_at,
                    logo:item.logo,
                    name:item.name,
                    description:item.description,
                    famille:item.famille,
                    mode:item.mode,
                    type:item.type

                })
            }
            this.setState({
                isLoading:false
            })
        } catch (error) {
            console.log('stock',error);
        }
    }

    getDate=()=>{
        let rep="";
        try {
            DateTime.locale(fr);
            const date=DateTime.parse(this.state.created_at,"YYYY-MM-DD HH:mm:ss");
            if(DateTime.isSameDay(new Date(),date)){
                rep="Aujourd'hui à "+DateTime.format(date,'HH:mm');
            }else if(DateTime.isSameDay(DateTime.addDays(date,1),new Date())){
                rep="Hier à "+DateTime.format('HH:mm');
            }else{
                rep=DateTime.format(date,"dddd, DD MMMM YYYY à HH:mm");
            }
        } catch (error) {
        }
        return rep;
    }

    getQuantity=()=>{
        let rep=null;
        try {
            if(this.state.type!=S.ENUM.article.service){
                rep=this.state.quantity
            }else{
                if(this.state.mode==S.ENUM.stockage.devise){
                    rep=getStock(this.state.quantity,[],this.state.mode,this.state.type).text;
                }else{
                    rep=null;
                }
            }
        } catch (error) {
            
        }
        return rep;
    }

    render(){

        return(
            this.state.isLoading==true?(
                <Awaiter color={R.color.colorSecondary} size={100} key="awaiter" />
            ):(
                <ScrollView>
                    <Card>
                        <View style={{flexDirection:'row',alignItems:'center'}}>
                            <Avatar
                                source={{uri:getImageLocal(this.state.photo)}}
                                size='large' rounded
                                containerStyle={{backgroundColor:R.color.colorPrimary}}
                                title={getTitle(this.state.last_name)}
                            />
                            <View style={{padding:10}}>
                                <Text style={{fontFamily:'Poppins-Bold',fontSize:16}}>
                                    {getFullName(this.state.first_name,this.state.last_name)}
                                </Text>
                            </View>
                        </View>
                    </Card>
                    <View style={{flexDirection:'row',alignItems:'center',margin:10}}>
                        {
                            (this.getQuantity()!=null && this.getQuantity()!=undefined)?(
                                <View style={{alignItems:'center',padding:10,flex:1}}>
                                    <Text style={{fontFamily:'Poppins-Medium',fontSize:14}}>
                                        {this.state.mode==S.ENUM.stockage.devise?"Montant":"Quantité"}
                                    </Text>
                                    <Text numberOfLines={1} style={{fontFamily:'Poppins-Bold',fontSize:16}}>
                                        {this.getQuantity()}
                                    </Text>
                                </View>
                            ):null
                        }
                        <View style={{flex:1,alignItems:'center'}}>
                            <Text style={{fontFamily:'Poppins-Medium',fontSize:14}}>
                                Fournisseur
                            </Text>
                            <Text numberOfLines={1} style={{fontFamily:'Poppins-Bold',fontSize:16}}>
                                {
                                    (!isEmpty(this.state.provider)?getFullName("",this.state.provider):"Aucun fournisseur")
                                }
                            </Text>
                        </View>
                    </View>
                    <View style={{padding:10,alignItems:'center'}}>
                        <Text style={{fontFamily:'Poppins-Medium',fontSize:14}}>
                            Date d'enregistrement
                        </Text>
                        <Text style={{fontFamily:'Poppins-Bold',fontSize:16}}>
                            {this.getDate()}
                        </Text>
                    </View>
                    <View style={{alignItems:'center',flexDirection:'row'}}>
                        <View style={{padding:10}}>
                            <Avatar
                                source={{uri:getImageLocal(this.state.logo)}}
                                rounded size="large"
                                title={getTitle(this.state.name)}
                                containerStyle={{backgroundColor:R.color.colorPrimary}}
                                placeholderStyle={{backgroundColor:R.color.colorPrimary}}
                            />
                        </View>
                        <View style={{flex:1,padding:10}}>
                            <Text numberOfLines={1} style={{fontFamily:'Poppins-Medium',fontSize:18}}>
                                {getArticleName(this.state.name,this.state.description)}
                            </Text>
                            <Text style={{fontFamily:'Poppins-Thin',paddingLeft:10}}>
                                {" de "}
                            </Text>
                            <Text numberOfLines={1} style={{fontFamily:'Poppins-Black',fontSize:18}}>
                                {capitalize(this.state.famille,this.state.description)}
                            </Text> 
                        </View>
                    </View>
                </ScrollView>
            )
        )
    }
}

export default (App);
