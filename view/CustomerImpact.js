import React from 'react';
import { Component } from 'react';
import EmptyMessage from '../components/EmptyMessage';
import Awaiter from '../components/Waiter';
import * as CustomerModel from '../model/local/client';
import * as BuyModel from '../model/local/buy';
import * as DeviseModel from '../model/local/devise';
import R from '../resources/styles.json';
import { View } from 'react-native';
import { FlatList } from 'react-native';
import { getTotal } from '../Manager';
import { Button, Text } from 'react-native-elements';
import { List } from 'react-native-paper';
import { ScrollView } from 'react-native';


class App extends Component{

    constructor(props){
        super(props);
        this.options=[
            {label:"Liste de clients",
            description:"Retrouvez la liste de tous les clients qui visitent votre boutique",
            action:"ShowCustomerList",color:""},
            {label:"Clients rares",
            description:"Essayez de booster les clients qui s’avèrent rares dans votre boutique depuis les 5 derniers jours",
            action:"ShowCustomerRare",color:""},
            {label:"Promotion des articles",
            description:"Essayez d’attirer l’attention de vos clients à travers des nouveaux stocks de votre boutique",
            action:"ShowCustomerPromotion",color:""},
            /*{label:"Clients d'honneurs",
            description:"Nous avons sélectionné pour vous des clients méritants plus d’attention à cause de leurs nombres de visite chez vous",
            action:"ShowCustomerHonor",color:""},
            {label:"Clients favoris",
            description:"Vous pouvez sélectionner vos clients favoris, qui, leurs comportements seront suivis avec beaucoup d’attentions",
            action:"ShowCustomerFavorite",color:""}*/
        ];
        this.state={
            customers:[],
            buys:[],
            devises:[],
            list:[],
            isLoading:true,
            showProgress:false,
            showBuy:false,//show all buy about a customer
            showGreat:false,//show by quantity buyed,
            showByDevise:false,//show by price buyed
        }
    }

    renderItem=({item})=>{
        return(
            <View style={{ padding:20 }}>
            <Text>
                {item.name}
            </Text>
            <Text>
                {getTotal(item.buys,this.state.devises)}
            </Text>
            </View>
        )
    }

    async componentDidMount(){
        try {
            await this.onLoad();
        } catch (error) {
            console.log('error',error);
        }
    }

    onLoad=async()=>{
        try {
            const buys=await BuyModel.get();
            const customers=await CustomerModel.getCustomers();
            const devises=await DeviseModel.get();
            const list=[];
            this.setState({
                buys,
                devises,
                customers,
                list:customers
            });
            /*const onEventBuy=async(x,index)=>{

            }*/
            const onEvent=async(item,index)=>{
                let id=item.id;
                let quantity=0;
                const prices=[];
                let price="";
                let begin=null;
                let last=null;
                let n=0;
                for(var x of buys){
                    if(x.id_customer==id){
                        if(n==0){
                            begin=x.created_at;
                        }else if(n==buys.length-1){
                            last=x.created_at;
                        }
                        if(x.article_type!="service"){
                            quantity+=x.quantity;
                            console.log('QTE de '+item.name,x.quantity);
                        }
                        prices.push({
                            devise:x.signe,
                            price:x.price,
                            id_devise:x.id_devise,
                            remise:x.remise,
                            quantity:x.quantity
                        })
                        console.log('price index',prices);
                    }
                    n++;
                }
                price=getTotal(prices,devises);
                list.push({
                    id,
                    price,
                    quantity,
                    begin,
                    last,
                    name:item.name,
                })
            }
            if(customers.length>0){
                await customers.map(onEvent);
                //console.log('price',list);
                /*this.setState({
                    list
                })*/
            }

            if(this.state.isLoading==true){
                this.setState({isLoading:false})
            }
        } catch (error) {
            console.log('error',error);
        }
    }


    render(){
        return(
            <ScrollView>
            <List.Section 
                theme={{ 
                    animation:{scale:5},
                    colors:{background:R.color.colorPrimary,primary:R.color.colorPrimary}
                }}
            >
                {
                    this.options.map((item,key)=>(
                        <List.Accordion 
                            titleStyle={{ fontSize:18,fontFamily:'Poppins-Black' }}
                            descriptionStyle={{ fontFamily:"Poppins-Light" }}
                            key={`accordion_${key}`}
                            description={item.description}
                            title={item.label}
                        >
                           <View style={{ padding:20,alignItems:'center' }}>
                                <Button
                                    title="Visitez"
                                    onPress={()=>this.props.navigation.navigate(item.action)}
                                    buttonStyle={{ padding:15,backgroundColor:R.color.colorPrimary,borderRadius:10 }}
                                />
                           </View>
                        </List.Accordion>
                    ))
                }
            </List.Section>
            </ScrollView>
        )
    }
}
export default (App);