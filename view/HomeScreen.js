import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Sale from './Home/Sale';
import { useWindowDimensions } from 'react-native';
import R from '../resources/styles.json';
import S from '../resources/settings.json';
import { Icon } from 'react-native-elements';
import { useDispatch, useSelector } from 'react-redux';
import {DrawerContent} from './DrawerContent';
import { StyleSheet } from 'react-native';
import setting from './Home/setting';
import * as NotificationModel from '../model/local/notification';
import * as SubscriptionWebModel from '../model/web/subscription';
import * as SubscriptionModel from '../model/local/subscription';
import database from '@react-native-firebase/database';
import PushNotification from 'react-native-push-notification';
import BackgroundFetch from 'react-native-background-fetch';
import DateTime from 'date-and-time';
import { capitalize} from '../Manager';
import { checkArticles, checkBuys, checkCustommerIdentity } from '../service/notification';
import * as webService from '../service/firebase';
import * as Shared from '../service/sharedJob';
import { groupBy } from 'lodash';



const styles=StyleSheet.create({
    menuitem:{
        fontFamily:'Poppins-Light',
        fontSize:14
    }
})

const Drawer=createDrawerNavigator();

function HomeScreen(){

    const dispatch=useDispatch();
    const dimension=useWindowDimensions();
    const user=useSelector(state=>state.user.value);
    const shop=useSelector(state=>state.shop.value);
    const subscription=useSelector(state=>state.subscription);
    const internet=useSelector(state=>state.setting.internet);
    const right=useSelector(state=>state.right);


    const getWebSubscription=async()=>{
        let rep=[];
        try {
            const item=new Object();
            item.user=user.private_key;
            item.society=shop.private_key;
            const web=await SubscriptionWebModel.get(item);
            if(web.status==200){
                rep=web.response;    
            }
        } catch (error) {
            
        }
        return rep;
    }

    const createSubscription=async()=>{
        let result=false;
        try {
            const item=new Object();
            item.type=S.ENUM.subscription.WELCOME;
            item.user=user.private_key;
            item.agent=user.private_key;
            item.society=shop.private_key;
            item.price=0;
            item.days=30;
            const rep=await SubscriptionWebModel.store(JSON.stringify(item));
            if(rep.status==200){
                const item=rep.response;
                result=await SubscriptionModel.store({
                    created_at:item.created_at,
                    price:item.price,
                    private_key:item.private_key,
                    status:item.status,
                    transaction:item.transaction,
                    type:item.type,
                    user:user.id,
                    validate_at:item.validate_at,
                    unity:item.unity
                });
            }
        } catch (error) {
            console.log('error',error);
        }
        return result;
    }
    
    const checkSubscription=async()=>{
        try {
            let list=await getWebSubscription();
            list=groupBy(list,"private_key");
            let rep=await SubscriptionModel.getSubscription(user.id,S.ENUM.subscription.SUBSCRIPTION,S.ENUM.subscription.WELCOME);
            if(rep.length>0){
                //calculate subscription
                await Promise.all(rep.map(async p=>{
                    let item=list[p.private_key];
                    if(item!=undefined){
                        item=item[0];
                        await SubscriptionModel.update({
                            validate_at:item.validate_at,
                            id:p.id
                        })
                    }
                }));
                rep=await SubscriptionModel.getSubscription(user.id,S.ENUM.subscription.SUBSCRIPTION,S.ENUM.subscription.WELCOME);
                calculateSubscription(rep);
            }else{
                list=await getWebSubscription();
                if(list.length==0){
                    if(createSubscription()){
                        rep=await SubscriptionModel.getSubscription(user.id,S.ENUM.subscription.SUBSCRIPTION,S.ENUM.subscription.WELCOME);
                        if(rep.length>0){
                            //calculate subscription
                            calculateSubscription(rep);
                        }
                    }
                }else{
                    //save any way
                    const onEvent=async(item,index)=>{
                        const result=await SubscriptionModel.store({
                            created_at:item.created_at,
                            price:item.price,
                            private_key:item.private_key,
                            status:item.status,
                            transaction:item.transaction,
                            type:item.type,
                            user:user.id,
                            validate_at:item.validate_at,
                            unity:item.unity
                        });
                    }
                    await list.map(onEvent);
                }
            }
        } catch (error) {
            console.log('error',error)
        }
    }

    const calculateSubscription=(list=[])=>{
        try {
            const currentDate=new Date();
            let validate=null;
            let status=false;
            let period=null;
            list.map((item,index)=>{
                if(item.type==S.ENUM.subscription.SUBSCRIPTION || item.type==S.ENUM.subscription.WELCOME){
                    const date=DateTime.parse(item.validate_at,"YYYY-MM-DD HH:mm:ss");
                    if(date.getTime()>currentDate.getTime()){
                        if(validate!=null){
                            if(validate.getTime()<date.getTime()){
                                validate=date;
                            }
                        }else{
                            validate=date;
                        }
                    }
                }
            });
            if(validate!=null){
                if(validate.getTime()>currentDate.getTime()){
                    status=true;
                    period=DateTime.format(validate,"YYYY-MM-DD HH:mm:ss");
                }
            }
            const action={type:S.EVENT.onChangeSubscription,status:status,date:period}
            dispatch(action);
    
        } catch (error) {
            console.log('error',error);
        }
    }

    const onSnapshot=()=>{
        try {
            const db=database();
            renderConnection=db.ref('users/'+user.private_key).on('value',snapshot=>{
                webService.treatAlert(snapshot,user,shop,dispatch,showNotification);
            })
        } catch (error) {
            console.log('ERROR CONTAN',error)
        }
    }

    function showNotification({id,title="",content="",channelId}){
        console.log('ID',id);
        try {
            PushNotification.localNotification({
                message:content,
                //id:id,
                title:title,
                //bigText:content,
                smallIcon:"ligal",
                largeIcon:'ligal',
                //largeIconUrl:"../assets/logo/ligal.png",
                //bigPictureUrl:"../assets/logo/ligal.png",
                //bigLargeIcon: "ligal", // (optional) default: undefined
                //bigLargeIconUrl: "https://www.example.tld/bigicon.jpg",
                channelId:"ligal",
                //color:'transparent'
            });
        } catch (error) {
            console.log('error',error);
        }
    }

    const replaceCharacter=(value="")=>{
        let rep="";
        try {
            rep=value.replace("/*","");
            rep=rep.replace("*/","");
        } catch (error) {
            
        }
        return rep;
    }

    const treatNotifications=async()=>{
        try {
            const rep=await NotificationModel.get();
            if(rep!=null){
                const onEvent=async(item,index)=>{
                    const status=capitalize(item.status).toLowerCase();
                    if(status==capitalize(S.GENERAL.STATUS.SEND).toLowerCase()){
                        showNotification({
                            channelId:item.link,
                            id:item.id,
                            content:replaceCharacter(item.content),
                            title:replaceCharacter(item.title),
                        })
                        await NotificationModel.update({
                            id:item.id,
                            status:S.GENERAL.STATUS.SHOWED
                        })
                    }
                }
                await Promise.all(rep.map(onEvent));
            }
        } catch (error) {
            console.log('er',error)
        }
    }

    function addEvent(taskId) {
        //Simulate a possibly long-running asynchronous task with a Promise.
        try {
            return new Promise((resolve, reject) => {
                checkArticles().then(a=>{
                    checkBuys().then(b=>{
                        checkCustommerIdentity().then(c=>{
                            //check subscription
                            checkSubscription().then(d=>{
                                 //show Notifications
                                treatNotifications();
                            });
                        })
                    })
                });
                
                showNotification();
                Shared.startQueue().then(rep=>{
                    console.log('QUESSSS');
                });
                resolve();
              });
        } catch (error) {
            console.log("err",error)
        }
    }

    const  initBackground=async()=>{
        try {
            onSnapshot();
            const onEvent=async(taskId)=>{
                switch (taskId) {
                    case "customer":
                        await Shared.startQueue();
                    break;                
                    default:
                        await addEvent(taskId);
                        break;
                }
                BackgroundFetch.finish(taskId);
            }
            const onTimeout = async (taskId) => {
                BackgroundFetch.finish(taskId);
            }
            let status=await BackgroundFetch.configure({minimumFetchInterval:15},onEvent,onTimeout);
        } catch (error) {
            console.log('ERROR',error);
        }
    }

    const onChangeInternet=()=>{
        try {
            const internet=this.props.setting.internet;
            const action={type:S.EVENT.OnChangeSetting,internet:!internet};
            dispatch(action);
        } catch (error) {
            
        }
    }

    React.useEffect(()=>{
        checkSubscription();
        Shared.configureJob();
        initBackground();
        Shared.startQueue().then();
    },[]);

    return(
        <>
            <Drawer.Navigator
                 
                screenOptions={{
                    headerShown:true,
                    headerStyle:{backgroundColor:R.color.colorPrimary},
                    headerTintColor:R.color.background,
                    headerTitleStyle:{fontFamily:'Poppins-Black'},
                }}
                statusBarAnimation="slide"
                drawerContentOptions={{
                    activeTintColor:R.color.colorPrimary,
                    activeBackgroundColor:R.color.colorPrimary,
                    labelStyle:{fontSize:14},
                    inactiveTintColor:'#000',
                }}
                drawerType={dimension.width>800?'permanent':"front"}
                drawerStyle={{backgroundColor:R.color.background}}
                drawerContent={a=><DrawerContent
                    props={a}
                    right={right}
                    user={user}
                    shop={shop} 
                    connnected={internet}
                    onChangeConnection={()=>onChangeInternet()}
                    subscription={subscription}
                    key="drawerContent_key"
                />}
            >
                <Drawer.Screen name='Sale' component={Sale}
                    options={{
                        title:"Ventes",
                        headerShown:false,
                        drawerIcon:({color})=>(
                            <Icon name='shopping-basket' type='font-awesome' color={color} />
                        )
                    }}
                />
                <Drawer.Screen name='Settings' component={setting} options={{title:"Boutique",
                    drawerIcon:({color})=>(
                         <Icon name='gears' type='font-awesome' color={color} />
                     )}}  />


            </Drawer.Navigator>
                
        </>
    )

}

export {HomeScreen}



/*
    

    MenuComponent=()=>(
        <View style={{marginRight:10}}>
            <Menu
                ref={ref=>this.menuRef=ref}
                button={<Icon color={R.color.background} onPress={()=>this.menuRef.show()} name="ellipsis-v" type="font-awesome" />} >
                <MenuItem
                    onPress={()=>{this.props.navigation.navigate("Profil");this.menuRef.hide()}} 
                    textStyle={[styles.menuitem]}
                >
                    Profil
                </MenuItem>
                <MenuItem textStyle={[styles.menuitem]}>
                    A propos
                </MenuItem>
            </Menu>
        </View>
    )
    
    
    
    


    

    
    
    

    

    

    

    
    onTreatLink=async(service)=>{
        try {
            const connected=await isConnected();
            if(service.link==true && connected){
                const action={type:S.EVENT.OnExecuteService,link:false}
                this.props.dispatch(action);
                const rep=await FamilleModel.get();
                const user=this.props.user.value;
                const onEvent=async(p,index)=>{
                    if(p.id_link>0 && p.link_private_key==null){
                        //store
                        const item=new Object();
                        item.sub=p.private_key;
                        item.high=p.category_private_keys;
                        item.user=user.private_key;
                        const web=await FamilleWebModel.storeLink(JSON.stringify(item));
                        if(web.status==200){
                            await FamilleModel.updateLink({
                                id:p.id_link,
                                private_key:web.response.private_key,
                                is_updated:0
                            })
                            this.onDispatch("LINK",web.response)
                        }
                    }else if(p.link_is_updated==1){
                        //update
                        const item=new Object();
                        item.sub=p.private_key;
                        item.high=p.category_private_keys;
                        item.status=p.link_status==1?true:false;
                        item.user=user.private_key;
                        const web=await FamilleWebModel.updateLink(p.link_private_key,JSON.stringify(item));
                        if(web.status==200){
                            await FamilleModel.update({
                                id:p.id_link,
                                is_updated:0
                            })
                            this.onDispatch("LINK",web.response)
                        }
                    }
                }
                await rep.map(onEvent);
            }
        } catch (error) {
            console.log('error link',error)
        }
    }
    
    


    
}

export default connect(mapStateToProps,mapDispatchToProps) (App);*/