import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import info from './viewArticle/info';
import stock from './viewArticle/stock';
import sale from './viewArticle/sale';
import stat from './viewArticle/stat';
import { FAB } from 'react-native-paper';
import R from '../resources/styles.json';
import DateTime from 'date-and-time';
import fr from 'date-and-time/locale/fr';
import ModalArticle from '../components/ModalCreateArticle';
import { Component } from 'react';
import Awaiter from '../components/Waiter';
import { ProgressDialog } from 'react-native-simple-dialogs';
import * as ArticleModel from '../model/local/article';
import * as FamilleModel from '../model/local/family';
import { capitalize } from '../Manager';
import * as DeviseModel from '../model/local/devise';
import { Alert } from 'react-native';
import { View } from 'react-native';
import { Avatar, ListItem, Text } from 'react-native-elements';
import { connect } from 'react-redux';
import S from '../resources/settings.json';
import ModalStock from '../components/ModalCreateStock';
import * as StockModel from '../model/local/stock';
import * as Manager from '../Manager';
import ImagePicker from 'react-native-image-picker';
import CatalogModal from '../components/ModalCatalog';
import * as CatalogWebModel from '../model/web/catalog';
import CardCatalog from '../components/CardCatalog';
import * as Shared from '../service/shared';




const Tab=createMaterialTopTabNavigator();

const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.article,
        shop:state.shop
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

class App extends Component{

    constructor({props,route}){
        super(props)
        this.state={
            opened:false,
            showCat:false,
            showArticle:false,
            isLoading:true,
            showProgress:false,
            route,
            articles:[],
            devises:[],
            item:null,
            list:[],
            articleName:'',
            articleDescription:'',
            articlePrice:0,
            articleFamily:0,
            articleQuantifiable:true,
            articleDevise:0,
            articleMode:null,
            articleType:"",
            articleStockable:0,
            showDevise:false,
            showStock:false,
            stock:"",
            fournisseur:'',
            article:{
                name:'',
                id:0,
                type:0,
                price:0,
                description:'',
                quantity:0,
                devise:'',
                buy:0,
                stock:0,
                id_devise:0,
                famille:"",
                logo:null,
                mode:null,
                stockable:0,
            },
            logo:null,
            uri:"",
            showCatalog:false,
            catalogs:[],
            catalogLoad:false,
            type:null,
            btns:[],
            showReducer:false,
            stockDevise:0,
            stockExist:true

        }
    }
    downloadLogo=async(item)=>{
        try {
            const name=this.state.catName;
            const type=this.state.catType;
            const id=this.state.item.id;
            const logo=Manager.getImage(item.logo);
            this.setState({showProgress:true,showCatalog:false})
            const imageName='article'+id+Manager.getRandomString(20)+'.ligal';
            Manager.downloadImage(imageName.toLowerCase(),logo,item.logo).then(async load=>{
                if(load==true){
                    const rep=await ArticleModel.update({
                        id:id,
                        logo:imageName.toLowerCase(),
                        is_updated:1,
                        image_updated:1
                    })
                if(rep!=null){
                    Shared.article({
                        description:rep.description,
                        devise_private_key:rep.devise_private_key,
                        famille_private_key:rep.famille_private_key,
                        id:rep.id,
                        mode:rep.mode,
                        name:rep.name,
                        price:rep.price,
                        private_key:rep.private_key,
                        quantifiable:rep.quantifiable,
                        stockage:rep.stockage,
                        type:rep.type,
                        user
                    });
                    const action={type:S.EVENT.onChangeMarketArticle,market:true,sale:true,category:true,added:true}
                    const view={type:S.EVENT.onChangeArticle,info:true,stock:true,sale:true}
                    await this.props.dispatch(view);
                    await this.props.dispatch(action);
                    await this.onLoadArticle();
                }
                this.setState({showProgress:false})
                }else{
                    this.setState({showProgress:false})
                }
            });
            
        } catch (error) {
        }
    }
    renderCatalog=({item})=>{
        return(
            <CardCatalog
                onPress={()=>this.downloadLogo(item)}
                key="card"
                image={Manager.getImage(item.logo)}
                name={item.name}
            />
        )
    }
    onGetCatalog=async()=>{
        try {
            const name=this.state.catName;
            const type=this.state.catType;
            const id=this.state.item.id;
            this.setState({catalogLoad:true,showCatalog:true})
            const rep=await CatalogWebModel.get();
            if(rep.status==200){
                this.setState({catalogs:rep.response})
            }
            this.setState({catalogLoad:false})
        } catch (error) {
            
        }
    }
    onShowArticle=()=>{
        try {
            this.setState({
                showArticle:true,
                articleName:this.state.article.name,
                articleDescription:this.state.article.description,
                articleDevise:this.state.article.id_devise,
                articlePrice:this.state.article.price,
                articleQuantifiable:this.state.article.type==1?true:false,
                articleMode:this.state.article.mode,
                articleType:this.state.article.type,
                articleStockable:this.state.article.stockable
            })
        } catch (error) {
            
        }
    }
    async componentDidMount(){
        try {
            await this.onLoad();
        } catch (error) {
        }
    }
    onLoadArticle=async()=>{
        try {
            const article=this.state.route.params.item;
            const rep=await ArticleModel.get(article.id_famille);
            const item=await ArticleModel.show(this.state.article.id);

            const stock=Manager.getStock(item.stock,[],item.mode,item.type);
            const buy=Manager.getStock(item.buy,[],item.mode,item.type);
            if(stock.count==0 && buy.count==0){
                this.setState({stockExist:false})
            }
            this.setState({
                articles:rep,
                item:item,
                type:item.type,
                article:{
                    name:item.name,
                    id:item.id,
                    devise:item.devise,
                    type:item.type,
                    price:item.price,
                    description:item.description,
                    buy:item.buy,
                    stock:item.stock,
                    quantity:0,
                    logo:item.logo,
                    id_devise:item.id_devise,
                    famille:item.famille,
                    mode:item.mode,
                    stockable:item.stockage
                },
            })
        } catch (error) {
        }
    }
    onLoad=async()=>{
        try {
            const item=this.state.route.params.item;
            const r=await StockModel.get();
            const rep=await ArticleModel.get(item.id_famille);
            const p=await FamilleModel.get();
            const d=await DeviseModel.get();
            const btns=[];
            btns.push(
                {
                    icon:'pencil',
                    label:'Modifier',
                    style:{backgroundColor:R.color.colorPrimary},
                    onPress:()=>this.onShowArticle()
                }
            );
            if(item.type!=S.ENUM.article.service || (item.type==S.ENUM.article.service && item.stockage==1)){
                /*btns.push(
                    {
                        icon:'delete',
                        label:"Reduire le stock",
                        style:{backgroundColor:"red"},
                        onPress:()=>this.setState({showReducer:true}),
                        color:R.color.background
                    }
                )*/
                btns.push(
                    {
                        icon:'plus',
                        label:"Approvisionner",
                        style:{backgroundColor:R.color.colorSecondary},
                        onPress:()=>this.setState({showStock:true}),
                        color:R.color.background
                    }
                )
                
            }
            if(d.length>0){
                this.setState({
                    stockDevise:d[0].id
                })
            }
            const stock=Manager.getStock(item.stock,[],item.mode,item.type);
            const buy=Manager.getStock(item.buy,[],item.mode,item.type);
            if(stock.count==0 && buy.count==0){
                this.setState({stockExist:false})
            }
            this.setState({
                articles:rep,
                item:item,
                list:p,
                devises:d,
                type:item.type,
                btns,
                article:{
                    name:item.name,
                    id:item.id,
                    devise:item.devise,
                    type:item.type,
                    price:item.price,
                    description:item.description,
                    buy:item.buy,
                    stock:item.stock,
                    quantity:0,
                    logo:item.logo,
                    id_devise:item.id_devise,
                    famille:item.famille,
                    mode:item.mode,
                    stockable:item.stockage
                }
            })
            if(d.length>0){
                this.setState({showStock:d[0].id})
            }
            if(this.state.isLoading==true){
                this.setState({
                    isLoading:false
                })
            }
        } catch (error) {
        }
    }
    articleNameUse=()=>{
        let rep=false;
        try {
            const name=this.state.articleName;
            this.state.articles.map((item)=>{
                if(capitalize(item.name).toLowerCase()==capitalize(name).toLowerCase() && item.id!=this.state.article.id){
                    rep=true
                    return rep;
                }
            })
        } catch (error) {
            
        }
        return rep;
    }
    invalidPrice=()=>{
        let rep=true;
        try {
            const a=parseFloat(this.state.articlePrice);
            if(a>0){
                rep=false;
            }else{
                if(this.state.article.type==S.ENUM.article.service){
                    if(this.state.article.mode==S.ENUM.stockage.devise){
                        rep=false;   
                    }
                    
                }
            }
            
        } catch (error) {
            
        }
        return rep;
    }
    getCategory=()=>{
        this.setState({
            articleFamily:this.state.item.id
        })
        return this.state.item.name;
    }
    renderItemMenu=({item})=>{
        return(
            <View style={{flex:1}}>
                <ListItem onPress={()=>this.setState({articleDevise:item.id,showDevise:false})}>
                    <Avatar
                        title={item.signe}
                        titleStyle={{fontSize:20,fontWeight:'bold',color:R.color.colorPrimary}}
                    />
                    <Text numberOfLines={1}>
                        {capitalize(item.signe).toUpperCase()}
                    </Text>
                </ListItem>
            </View>
        )
    }
    getDevise=()=>{
        let rep="Monnaie"
        try {
            if(this.state.articleDevise>0){
                this.state.devises.map(item=>{
                    if(item.id==this.state.articleDevise){
                        rep=capitalize(item.signe).toUpperCase();
                        return rep;
                    }
                })
            }
        } catch (error) {
            
        }
        return rep;
    }
    onStock=async()=>{
        try {
            let stock=this.state.stock.trim();
            const fournisseur=this.state.fournisseur;
            const id=this.state.article.id;
            const user=this.props.user.value;
            const id_devise=this.state.stockDevise;
            if(parseFloat(stock)>0 && id>0){
                this.setState({showProgress:true})
                const date=DateTime.format(new Date(),"YYYY-MM-DD HH:mm:ss");
                const rep=await StockModel.store({
                    created_at:date,
                    fournisseur:fournisseur,
                    id_agent:user.id,
                    id_article:id,
                    quantity:parseInt(stock),
                    id_devise:id_devise
                })
                if(rep!=null){
                    this.setState({
                        showStock:false,
                        stock:"",
                        fournisseur:""
                    })
                }
                const action={type:S.EVENT.onChangeMarketArticle,market:true,sale:true,added:true};//for market and sale
                const view={type:S.EVENT.onChangeArticle,info:true,stock:true,sale:true,stat:true}
                //const actionService={type:S.EVENT.OnExecuteService,stock:true}
                Shared.stock({
                    agent_private_key:rep.agent_private_key,
                    article_private_key:rep.article_private_key,
                    created_at:rep.created_at,
                    fournisseur:rep.fournisseur,
                    id:rep.id,
                    id_devise:rep.id_devise,
                    private_key:rep.private_key,
                    quantity:rep.quantity
                })
                await this.props.dispatch(view);
                await this.props.dispatch(action);
                this.setState({showProgress:false})
                Alert.alert("Information","Stock ajouté")
            }else{
                Alert.alert("Vérification","Impossible de valider cet enregistrement")
            }
        } catch (error) {
        }
    }
    onSaveArticle=async ()=>{
        try {
            const user=this.props.user.value;
            const name=this.state.articleName;
            const description=this.state.articleDescription;
            const price=this.state.articlePrice;
            const family=this.state.item.id;
            const id_devise=this.state.articleDevise;
            let quantifiable=this.state.articleQuantifiable;
            let type=this.state.articleType;
            let mode=this.state.articleMode;
            let stockable=this.state.articleStockable;
            if(this.state.stockExist==true){
                type=null;
                mode=null;
                stockable=null;
                quantifiable=null;
            }else{
                if(quantifiable){
                    quantifiable=1
                }else{
                    quantifiable=0;
                }
            }
            
            const devise=this.state.articleDevise;
            if(!this.articleNameUse() && name.trim()!="" && this.invalidPrice()==false && devise>0 && family>0 ){
                this.setState({showProgress:true})
                const rep=await ArticleModel.update({
                    description:description,
                    devise:devise,
                    is_updated:1,
                    name:name,
                    price:price,
                    quantifiable:quantifiable,
                    id:this.state.article.id,
                    id_devise:id_devise>0?id_devise:null,
                    stockage:stockable
                });
                if(rep!=null){
                    this.setState({
                        articleDescription:'',
                        articleName:'',
                        articlePrice:0,
                        showArticle:false,
                        logo:null
                    })
                    const action={type:S.EVENT.onChangeMarketArticle,market:true,sale:true,category:true,added:true}
                    const view={type:S.EVENT.onChangeArticle,info:true,stock:true,sale:true}
                    await this.props.dispatch(view);
                    await this.props.dispatch(action);
                    Shared.article({
                        description:rep.description,
                        devise_private_key:rep.devise_private_key,
                        famille_private_key:rep.famille_private_key,
                        id:rep.id,
                        mode:rep.mode,
                        name:rep.name,
                        price:rep.price,
                        private_key:rep.private_key,
                        quantifiable:rep.quantifiable,
                        stockage:rep.stockage,
                        type:rep.type,
                        user
                    });
                    await this.onLoadArticle();
                    Alert.alert("Enregistrement","L'article a été modifié avec succès!");
                }else{
                    Alert.alert("Enregistrement","La modification n'a pas réussi");
                }
                this.setState({showProgress:family})
            }else{
                Alert.alert("Vérification","Saisissez correctement des données");
            }
        } catch (error) {
        }
    }
    updateType=(type,mode)=>{
        let rep=false;
        try {
            
        } catch (error) {
            
        }
        return rep;
    }
    updateMode=(type,mode)=>{
        let rep=false;
        try {
            
        } catch (error) {
            
        }
        return rep;
    }
    updatePrice=(type,mode)=>{
        let rep=false;
        try {
            
        } catch (error) {
            
        }
        return rep;
    }
    updateQuantifiable=(type,mode)=>{
        let rep=false;
        try {
            
        } catch (error) {
            
        }
        return rep;
    }
    onSaveLogo=()=>{
        const user=this.props.user.value;
        ImagePicker.launchImageLibrary({},async rep=>{
            if(rep.didCancel){
                
            }else if(rep.error){
            }
            else if(rep.customButton){
            }else{
                let logo=null;
                const l=rep.uri;
                if(l!=null){
                    this.setState({showProgress:true});
                    logo="article"+this.state.article.id+Manager.getRandomString(20).trim()+".ligal";
                    const q=await Manager.saveImage(logo.toLowerCase(),l,this.state.article.logo);
                    if(q==true){
                        const response=await ArticleModel.update({
                            is_updated:1,
                            id:this.state.article.id,
                            image_updated:1,
                            logo:logo
                        });
                        if(response!=null){
                            Shared.article({
                                description:response.description,
                                devise_private_key:response.devise_private_key,
                                famille_private_key:response.famille_private_key,
                                id:response.id,
                                mode:response.mode,
                                name:response.name,
                                price:response.price,
                                private_key:response.private_key,
                                quantifiable:response.quantifiable,
                                stockage:response.stockage,
                                type:response.type,
                                user
                            });
                            const action={type:S.EVENT.onChangeMarketArticle,market:true,sale:true,category:true,added:true}
                            const view={type:S.EVENT.onChangeArticle,info:true,stock:true,sale:true}
                            await this.props.dispatch(view);
                            await this.props.dispatch(action);
                        }
                    }
                    this.setState({showProgress:false});
                }
                this.setState({
                    logo:rep.uri
                })
            }
        })
    }

    render(){
        
        return(
            <>
                {
                    this.state.isLoading==true?(
                        <Awaiter color={R.color.colorSecondary} size={100} />
                    ):(
                        <>
                        <Tab.Navigator
                            tabBarOptions={{
                                scrollEnabled:true
                            }}
                        >
                            <Tab.Screen
                                name="INFO"
                                component={info}
                                options={{title:"Présentation"}}
                                initialParams={{item:this.state.item,articles:this.state.articles}}
                            />
                            {
                                (this.state.type!=S.ENUM.article.service || (this.state.article.type==S.ENUM.article.service && this.state.article.mode==S.ENUM.stockage.devise))?(
                                    <Tab.Screen
                                        name="STOCK"
                                        component={stock}
                                        options={{title:"Les stocks"}}
                                        initialParams={{item:this.state.item,articles:this.state.articles}}
                                    />
                                ):null
                            }
                            <Tab.Screen
                                name="SALE"
                                component={sale}
                                options={{title:"Les ventes"}}
                                initialParams={{item:this.state.item,articles:this.state.articles}}
                            />
                            <Tab.Screen
                                name="STAT"
                                component={stat}
                                options={{title:"Statistique"}}
                                initialParams={{item:this.state.item,articles:this.state.articles}}
                            />
                        </Tab.Navigator>
                        <FAB.Group
                            open={this.state.opened}
                            fabStyle={{backgroundColor:R.color.colorPrimaryDark}}
                            icon={this.state.opened?'close':'plus'}
                            theme={{
                                animation:{scale:2},
                                colors:{text:R.color.colorPrimaryDark,background:'red'},
                                dark:true,
                                roundness:10
                            }}
                            actions={this.state.btns}
                            onStateChange={open=>this.setState({opened:!this.state.opened})}
                        />

                        <ModalStock
                            visible={this.state.showStock}
                            onClose={()=>this.setState({showStock:false})}
                            value={this.state.stock}
                            onChangeText={value=>{
                                let text=Manager.removeNoNumberDecimal(value);
                                //text=Manager.addGapsWithSeparator(text,[4,6,9,12,15]," ",".")
                                this.setState({stock:text})
                                }}
                            fournisseur={this.state.fournisseur}
                            onChangeFournisseur={value=>this.setState({fournisseur:value})}
                            onPress={()=>this.onStock()}
                            status={!parseInt(this.state.stock)>0}
                            type={this.state.article.type}
                            mode={this.state.article.mode}
                            devises={this.state.devises}
                            devise={this.state.stockDevise}
                            onChangeDeviseValue={(value)=>this.setState({stockDevise:value})}

                        />

                        <ModalArticle
                            visible={this.state.showArticle}
                            onClose={()=>this.setState({showArticle:false})}
                            logo={this.state.logo!=null?this.state.logo:Manager.getImageLocal(this.state.article.logo)}
                            value={this.state.articleName}
                            onChangeText={(value)=>this.setState({articleName:value})}
                            valueDescription={this.state.articleDescription}
                            onChangeDescrption={value=>this.setState({articleDescription:value})}
                            valuePrice={this.state.articlePrice.toString()}
                            onChangePrice={value=>{
                                let text=Manager.removeNoNumberDecimal(value);
                                //text=Manager.addGapsWithSeparator(text,[3,6,9,12,15]," ",".");
                                this.setState({articlePrice:text})
                            }}
                            statusName={this.articleNameUse()}
                            statusPrice={this.invalidPrice()}
                            category={capitalize(this.state.article.famille)}
                            onPress={()=>this.onSaveArticle()}
                            onRadioNo={()=>this.setState({articleQuantifiable:false})}
                            onRadioYes={()=>this.setState({articleQuantifiable:true})}
                            type={this.state.articleQuantifiable}
                            onCloseMenu={()=>this.setState({showDevise:false})}
                            onShowMenu={()=>this.setState({showDevise:true})}
                            renderItem={this.renderItemMenu}
                            signe={this.state.articleDevise}
                            devises={this.state.devises}
                            visibleDevise={this.state.showDevise}
                            update
                            mode={this.state.articleMode}
                            onChangeMode={(mode)=>{
                                this.setState({articleMode:mode})
                            }}
                            onChangeDevise={(v,index)=>{this.setState({articleDevise:v});}}
                            onRadioStockNo={()=>this.setState({articleStockable:0})}
                            onRadioStockYes={()=>this.setState({articleStockable:1})}
                            service={(this.state.articleType==S.ENUM.article.service && this.state.stockExist==false)?true:false}
                            key="article-update_key"
                            stock={this.state.articleStockable}
                            onCamera={()=>this.onSaveLogo()}
                            catalog
                            onCatalog={()=>this.onGetCatalog()}
                            viewPrice={(this.state.article.type==S.ENUM.article.service && this.state.article.mode==S.ENUM.stockage.devise)?false:true}
                        />
                        <ProgressDialog
                            message="Patientez svp!"
                            title="Chargement"
                            visible={this.state.showProgress}
                            activityIndicatorColor={R.color.colorSecondary}
                            animationType='fade'
                            activityIndicatorSize='large'
                        />
                        <CatalogModal
                            key="cat-key"
                            onClose={()=>this.setState({showCatalog:false})}
                            renderItem={this.renderCatalog}
                            visible={this.state.showCatalog}
                            isLoading={this.state.catalogLoad}
                            list={this.state.catalogs}

                        />
                    </>
                    )
                }
            </>
        )
    }
    
}

export default connect(mapStateToProps,mapDispatchToProps)(App);