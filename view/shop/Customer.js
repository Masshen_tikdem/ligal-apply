import React from 'react';
import { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { StyleSheet } from 'react-native';
import { ScrollView } from 'react-native';
import { View } from 'react-native';
import { Avatar, Button, Card, Divider, Icon, Text } from 'react-native-elements';
import { connect } from 'react-redux';
import Awaiter from '../../components/Waiter';
import R from '../../resources/styles.json';
import * as CustomerModel from '../../model/local/client';
//import * as SubscriptionModel from '../../model/local/'

const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.article
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

const CardAction=({title,icon="user-o",type="font-awesome",content,onPress})=>{

    return(
        <TouchableOpacity onPress={onPress}>
            <View style={{ flexDirection:'row',alignItems:'center',padding:10 }}>
                <View>
                    <Avatar
                        icon={{ name:icon,type:type,color:R.color.colorPrimaryDark }}
                        size="medium"
                        containerStyle={{ backgroundColor:'transparent' }}
                    />
                </View>
                <View style={{ flex:1 }}>
                    <Text style={{ padding:5,paddingBottom:0,fontFamily:'Poppins-Black',fontSize:16 }}>
                        {title}
                    </Text>
                    <Text numberOfLines={2} style={{ padding:5,fontFamily:'Poppins-Light',fontSize:14,paddingTop:0 }}>
                        {content}
                    </Text>
                    <Divider/>
                </View>
                <View>
                    <Icon
                        name="chevron-forward"
                        type="ionicon"
                        color={R.color.colorPrimaryDark}
                    />
                </View>
            </View>
        </TouchableOpacity>
    )
}

const styles=StyleSheet.create({
    title:{
        fontFamily:"Poppins-Black",
        fontSize:18
    },
    num:{
        fontFamily:'Poppins-Black',
        fontSize:26
    },
    sub:{
        fontFamily:"Poppins-Light",
        fontSize:14
    }
})



class App extends Component{

    constructor(props){
        super(props)
        this.state={
            isLoading:true,
            customers:[],
            messages:null,

        }
    }

    async componentDidMount(){
        try {
            await this.onLoad();
        } catch (error) {
            
        }
    }


    onLoad=async()=>{
        try {
          const rep=await CustomerModel.get();
          if(rep!=null){
              this.setState({customers:rep})
          }
          this.setState({isLoading:false})  
        } catch (error) {
            
        }
    }

    componentScreen=()=>(
        <View>

        </View>
    )

    componentStat=()=>{
        return(
            <View style={{ alignItems:'center',padding:10 }}>
                <Text style={styles.title}>
                    Gestion des clients
                </Text>
                <View style={{ alignItems:'center' }}>
                    <Text style={styles.num}>
                        0 <Text style={styles.sub}>Message</Text>
                    </Text>
                </View>
                <View style={{ alignItems:'center',elevation:15 }}>
                    <Card containerStyle={{ elevation:10,margin:10,marginBottom:10 }}>
                        <View>
                            <Text style={[styles.num,{color:R.color.colorSecondary}]}>
                                {this.state.customers.length} <Text style={styles.sub}>{"client"+(this.state.customers.length>1?"s":"")}</Text>
                            </Text>
                        </View>
                    </Card>
                </View>
            </View>
        )
    }

    componentActions=()=>{
        return(
            <View>
                 <CardAction
                    title="Stratégies"
                    icon="thumbs-up"
                    type="font-awesome"
                    content="Découvrir des stratégies de commerce à travers une communauté des commerçants et responsable des sociétés."
                />
                 <CardAction
                    title="Impact des clients "
                    icon="users"
                    onPress={()=>this.props.navigation.navigate('ShowCustomerImpact')}
                    type="entypo"
                    content="Découvrir le niveau d’impact des clients. Découvrez les articles qui attirent la clientèle."
                />
                 <CardAction
                    title="Explorer"
                    content="Attirer d’autres clients chez vous."
                    icon="search-minus"
                    type="font-awesome"
                />
                <CardAction
                    title="Acheter des messages"
                    content="Envoyez des SMS à tous vos clients, quels que soient leurs numéros de téléphone."
                    icon="envelope-o"
                    type="font-awesome"
                    onPress={()=>this.props.navigation.navigate('ShowSubscription')}
                />
            </View>
        )
    }

    render(){

        return(
            this.state.isLoading==true?(
                <Awaiter
                    color={R.color.colorSecondary}
                    size={100}
                    key="awaiter"
                />
            ):(
                <View>
                    <ScrollView>
                        <this.componentScreen />
                        <this.componentStat />
                        <this.componentActions />
                    </ScrollView>
                </View>
            )
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(App)