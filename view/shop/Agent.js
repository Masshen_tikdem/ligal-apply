import React from 'react';
import { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import * as AgentModel from '../../model/local/agent';
import R from '../../resources/styles.json';
import S from '../../resources/settings.json';
import Awaiter from '../../components/Waiter';
import Empty from '../../components/EmptyMessage';
import { FlatList } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { Avatar, Header, Icon, Overlay, SearchBar, Text } from 'react-native-elements';
import { getFullName, getImage, getImageLocal, getSex, getTitle, isConnected, isEmpty } from '../../Manager';
import { FAB } from 'react-native-paper';
import * as UserWebModel from '../../model/web/users';
import ModalAgent from '../../components/ModalAgent';
import { ProgressDialog } from 'react-native-simple-dialogs';
import { Alert } from 'react-native';
import fr from 'date-and-time/locale/fr';
import DateTime from 'date-and-time';
import * as RightWebModel from '../../model/web/right';
import * as RightModel from '../../model/local/right';

const mapStateToProps = (state) => {
    return {
        user:state.user,
        shop:state.shop,
        right:state.right
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

class App extends Component{

    constructor(props){
        super(props)
        this.state={
            list:[],
            isLoading:true,
            showSearch:false,
            showLoading:false,
            search:"",
            isSearched:false,
            users:[],
            text:"",
            showAgent:false,
            userName:"",
            userTitle:"",
            userPhoto:null,
            userCreated_at:null,
            rightJournal:1,
            rightSale:0,
            rightStock:0,
            rightArticle:0,
            rightCategory:0,
            rightCustomer:1,
            rightDevise:1,
            rightVente:2,
            rightDashboard:0,
            showProgress:false,
            currentUser:null,
            poste:""
        }
    }

    onDefault=()=>{
        this.setState({
            rightJournal:1,
            rightSale:0,
            rightStock:0,
            rightArticle:0,
            rightCategory:0,
            rightCustomer:1,
            rightDevise:1,
            rightVente:2,
            rightDashboard:0,
        })
    }

    async componentDidMount(){
        try {
            const user=this.props.user.value;
            //const rep=await AgentModel.colleagues(user.id);
            const rep=await AgentModel.colleagues();
            this.setState({
                list:rep,
                isLoading:false,
            })
        } catch (error) {
            
        }
    }
    onShowAgent=(item)=>{
        try {
            const name=getFullName(item.first_name,item.last_name);
            const photo=getImage(item.photo)
            const title=getTitle(item.last_name);
            this.setState({
                userPhoto:photo,
                userName:name,
                userTitle:title,
                currentUser:item,
                showAgent:true,
                poste:"",
                userCreated_at:item.created_at
            })
        } catch (error) {
            
        }
    }
    onCloseAgent=()=>{
        this.setState({showAgent:false})
    }
    onAdd=async()=>{
        try {
            const user=this.state.currentUser;
            const shop=this.props.shop.value;
            const poste=this.state.poste;
            const date=DateTime.format(new Date(),"YYYY-MM-DD HH:mm:ss");
            if(user!=null && shop.private_key!=undefined){
                if(isEmpty(poste)){
                    Alert.alert("Vérification","Déterminez le poste de l'agent");
                    return;
                }
                const internet=await isConnected();
                if(internet==true){
                    this.setState({showProgress:true})
                    const item=new Object();
                    item.society=shop.private_key;
                    item.user=user.private_key;
                    item.poste=poste;
                    item.created_at=date;
                    const rep=await UserWebModel.storeWork(JSON.stringify(item));
                    if(rep.status==200){
                        //save agent
                        const work=rep.response;
                        const ag=await AgentModel.store({
                            address:user.address,
                            email:user.email,
                            first_name:user.first_name,
                            is_updated:0,
                            last_name:user.last_name,
                            phone:user.phone,
                            private_key:user.private_key,
                            sexe:user.sex,
                            status:1,
                        });
                        if(ag==true){
                            //save work
                            const last=await AgentModel.last();
                            if(last!=null){
                                const stW=await AgentModel.storeWork({
                                    created_at:date,
                                    id_agent:last.id,
                                    id_shop:shop.id,
                                    is_updated:0,
                                    private_key:work.private_key,
                                    poste:poste,
                                    status:1,
                                    updated_at:date
                                })
                            }
                            //the rights
                            const listRights=[
                                {code:S.ENUM.CATEGORY,value:this.state.rightCategory},
                                {code:S.ENUM.CLIENT,value:this.state.rightCustomer},
                                {code:S.ENUM.DEVISE,value:this.state.rightDevise},
                                {code:S.ENUM.JOURNAL_PRODUIT,value:this.state.rightJournal},
                                {code:S.ENUM.PRODUCT,value:this.state.rightArticle},
                                {code:S.ENUM.STOCK_IN,value:this.state.rightStock},
                                {code:S.ENUM.STOCK_OUT,value:this.state.rightSale},
                                {code:S.ENUM.VENTE_PRODUIT,value:this.state.rightVente},
                                {code:S.ENUM.DASHBOARD,value:this.state.rightDashboard},
                            ];
                            const rightElement=new Object();
                            rightElement.list=listRights;
                            rightElement.society=shop.private_key;
                            rightElement.user=user.private_key;
                            const rr=await RightWebModel.store(JSON.stringify(rightElement))
                            const event=async(item,index)=>{
                                if(item!=null){
                                    const aw=await RightModel.store({
                                            code:item.code,
                                            id_agent:last.id,
                                            is_updated:0,
                                            private_key:item.private_key,
                                            value:item.value
                                    })
                                }
                            }
                            if(rr.status==200){
                                await rr.response.map(event);
                            }
                            const ll=await AgentModel.get();
                            this.setState({
                                list:ll,
                                showAgent:false,
                                showProgress:false,
                                showSearch:false
                            })
                        }else{
                            this.setState({showProgress:false})
                        }
                    }else{
                        this.setState({showProgress:false})
                        Alert.alert("Création de l'agent","Echéc de l'enregistrement. Veuillez réessayer, svp!")
                    }
                }else{
                    Alert.alert("Echéc de connexion","Vous devez vous connecter à l'internet")
                }
            }else{
                
            }
        } catch (error) {
        }
    }
    renderUser=({item})=>{
        if(item.private_key==this.props.user.value.private_key){
            return null;
        }
        return(
            <TouchableOpacity key={`indexUser${item.id}`} onPress={()=>this.onShowAgent(item)} style={{flex:1,margin:10,elevation:10}}>
                <View style={{flexDirection:'row',alignItems:'center',padding:5,elevation:10}}>
                    <Avatar
                        rounded size="large"
                        source={{uri:getImage(item.photo)}}
                        title={getTitle(item.last_name)}
                        containerStyle={{backgroundColor:R.color.colorPrimary}}
                        placeholderStyle={{backgroundColor:R.color.colorPrimary}}
                    />
                    <View style={{flex:1,padding:10}}>
                        <Text style={{padding:5,fontFamily:'Poppins-Bold',fontSize:16}}>
                            {getFullName(item.first_name,item.last_name)}
                        </Text>
                        <View style={{flexDirection:'row',alignItems:'center',paddingLeft:10}}>
                            <View style={{flex:1,alignItems:'flex-start'}}>
                                <Text numberOfLines={1} style={{fontFamily:'Poppins-Light'}}>
                                    {getSex(item.sex)}
                                </Text>
                            </View>
                            <View style={{flex:1,alignItems:'flex-end'}}>
                                <Text numberOfLines={1} style={{fontFamily:'Poppins-Medium',fontSize:14,color:R.color.colorSecondary}}>
                                    {item.work_status==1?"Actif":"Non actif"}
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
    renderItem=({item})=>{

        const user=this.props.user.value;
        return(
            <TouchableOpacity 
                activeOpacity={0.9}
                //onPress={()=>this.props.navigation.navigate('ShowAgent',{id:item.id})}
                style={{flex:1,margin:10,elevation:10}}>
                <View style={{flexDirection:'row',alignItems:'center',padding:5,elevation:10}}>
                    <Avatar
                        rounded size="large"
                        source={{uri:getImageLocal(item.photo)}}
                        title={getTitle(item.last_name)}
                        containerStyle={{backgroundColor:R.color.colorPrimary}}
                        placeholderStyle={{backgroundColor:R.color.colorPrimary}}
                    />
                    <View style={{flex:1,padding:10}}>
                        
                        <View style={{ flexDirection:'row',alignItems:'center' }}>
                            <View style={{ flex:1 }}>
                                <Text style={{padding:5,fontFamily:'Poppins-Bold',fontSize:16}}>
                                    {getFullName(item.first_name,item.last_name)}
                                </Text>
                            </View>
                            {
                                user.private_key==item.private_key?(
                                    <View>
                                        <Text style={{fontFamily:'Poppins-Black',fontSize:12,color:R.color.colorPrimary}}>
                                            (Vous)
                                        </Text>
                                    </View>

                                ):null
                            }
                        </View>
                        <View style={{flexDirection:'row',alignItems:'center',paddingLeft:10}}>
                            <View style={{flex:1,alignItems:'flex-start'}}>
                                <Text numberOfLines={1} style={{fontFamily:'Poppins-Light'}}>
                                    {getSex(item.sex)}
                                </Text>
                            </View>
                            <View style={{flex:1,alignItems:'flex-end'}}>
                                <Text numberOfLines={1} style={{fontFamily:'Poppins-Medium',fontSize:14,color:R.color.colorSecondary}}>
                                    {item.work_status==1?"Actif":"Non actif"}
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
    onShowSearch=()=>{
        this.setState({
            showSearch:true
        })
    }
    onSearch=async()=>{
        try {
            const value=this.state.search;
            if(isEmpty(value)==false){
                const user=this.props.user.value;
                const shop=this.props.shop.value;
                const item=new Object();
                item.user=user.private_key;
                item.society=shop.private_key;
                item.name=value;
                this.setState({
                    showLoading:true,
                    isSearched:true,
                    text:value
                })
                const web=await UserWebModel.search(JSON.stringify(item));
                if(web.status==200){
                    this.setState({users:web.response})
                }
                this.setState({showLoading:false})
            }
        } catch (error) {
            
        }
    }

    componentSearch=()=>(
        <Overlay
            isVisible={this.state.showSearch}
            fullScreen
            overlayStyle={{padding:0}}
            animationType='fade'
        >
            <Header
                backgroundColor={R.color.colorPrimary}
                leftComponent={
                    <Icon name="arrow-back" type='ionicon' color={R.color.background} onPress={()=>this.setState({showSearch:false})} />
                }
                placement="left"
                centerComponent={
                    <Text style={{fontFamily:"Poppins-Light",fontSize:16,padding:5,color:R.color.background}}>
                        Engager un autre agent
                    </Text>
                }
            />
            <SearchBar
                value={this.state.search}
                onChangeText={value=>this.setState({search:value})}
                maxLength={20}
                lightTheme
                onBlur={()=>this.onSearch()}
            />
            {
                this.state.showLoading==true?(
                    <Awaiter color={R.color.colorSecondary} size={100} key='awaiter-show' />
                ):this.state.users.length>0?(
                    <FlatList
                        data={this.state.users}
                        renderItem={this.renderUser}
                        key="list-agent"
                        keyExtractor={item=>item.id}
                    />
                ):this.state.isSearched==true?(
                    <Empty key='empty-1' title={`Aucun utilisateur trouvé avec la description «${this.state.text}»`} />
                ):(
                    <Empty key="empty-2" title="Cherchez un agent existant dans ligal" />
                )
            }
        </Overlay>
    )

    getDate=(value="")=>{
        let rep="";
        try {
            DateTime.locale(fr);
            const date=DateTime.parse(value,"YYYY-MM-DD HH:mm:ss");
            if(DateTime.isSameDay(new Date(),date)){
                rep="Aujourd'hui ";
            }else{
                rep=DateTime.format(date,"dddd, DD MMM YYYY");
            }
        } catch (error) {
            
        }
        return rep;
    }

    render(){

        return(
            this.state.isLoading==true?(
                <Awaiter color={R.color.colorSecondary} size={100} key="awaiter" />
            ):this.state.list.length==0?(
                <Empty  title="Aucun autre agent dans cette boutique" />
            ):(
                <View style={{flex:1}}>
                    <View>

                    </View>
                    <FlatList
                        data={this.state.list}
                        renderItem={this.renderItem}
                        key="list"
                        keyExtractor={item=>item.id}
                    />
                    <this.componentSearch  />
                    <ModalAgent
                        label="Profil de l'agent"
                        name={this.state.userName}
                        onClose={()=>this.onCloseAgent()}
                        onPress={()=>this.onAdd()}
                        photo={this.state.userPhoto}
                        title={this.state.userTitle}
                        visible={this.state.showAgent}
                        key="modal-agent"
                        article={this.state.rightArticle}
                        category={this.state.rightCategory}
                        customer={this.state.rightCustomer}
                        dashboard={this.state.rightDashboard}
                        devise={this.state.rightDevise}
                        journal={this.state.rightJournal}
                        sales={this.state.rightSale}
                        stocks={this.state.rightStock}
                        vente={this.state.rightVente}
                        onJournal={()=>this.setState({rightJournal:this.state.rightJournal==1?0:1})}
                        onVente={()=>this.setState({rightVente:this.state.rightVente==2?0:2})}
                        onCustomer={()=>this.setState({rightCustomer:this.state.rightCustomer==1?0:1})}
                        onStock={()=>this.setState({rightStock:this.state.rightStock==1?0:1})}
                        onSale={()=>this.setState({rightSale:this.state.rightSale==1?0:1})}
                        onArticle={(item,index)=>this.setState({rightArticle:item})}
                        onCategory={(item,index)=>this.setState({rightCategory:item})}
                        onDevise={(item,index)=>this.setState({rightDevise:item})}
                        poste={this.state.poste}
                        onChangePoste={value=>this.setState({poste:value})}
                        date={this.getDate(this.state.userCreated_at)}
                    />
                    {/*<FAB
                        icon="plus"
                        label="Ajouter un agent"
                        style={{position:'absolute',
                        bottom:0,right:0,margin:25,
                        elevation:20,
                        borderWidth:1,
                        backgroundColor:R.color.colorPrimary}}
                        onPress={()=>this.onShowSearch()}
                    />*/}
                    <ProgressDialog
                        message="Patientez svp!"
                        title="Chargement..."
                        visible={this.state.showProgress}
                        activityIndicatorColor={R.color.colorSecondary}
                        animationType="slide"
                        activityIndicatorSize="large"
                    />
                </View>
            )
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(App)