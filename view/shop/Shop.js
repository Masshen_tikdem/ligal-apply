import React from 'react';
import { Component } from 'react';
import { View } from 'react-native';
import { Avatar, Button, ButtonGroup, Card, Icon, Input, Text } from 'react-native-elements';
import { connect } from 'react-redux';
import R from '../../resources/styles.json';
import S from '../../resources/settings.json';
import * as AgentModel from '../../model/local/agent';
import { capitalize, getFullName, getImageLocal, getRandomString, getSex, getTitle, isDifference, isDifferenceStrict, isEmpty, saveImage, UserSetStorage } from '../../Manager';
import { StyleSheet } from 'react-native';
import { ScrollView } from 'react-native';
import RadioButton from '../../components/RadioButton';
import { ProgressDialog } from 'react-native-simple-dialogs';
import ImagePicker from 'react-native-image-picker';
import * as ShopModel from '../../model/local/shop';
import { Picker } from '@react-native-picker/picker';
import {PAYS} from '../../resources/All countries';


const styles=StyleSheet.create({
    title:{
        fontFamily:"Poppins-Black",
        color:R.color.background,
        fontSize:16
    },
    label:{
        fontFamily:"Poppins-Light",
        fontSize:14,
        fontWeight:"normal"
    },
    value:{
        fontFamily:"Poppins-Medium",
        fontSize:16,
        padding:5
    },
    view:{
        padding:10
    }
})

const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.article,
        shop:state.shop
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

class App extends Component{

    constructor(props){
        super(props)
        this.state={
            shop:{
                name:"",
                description:"",
                address:"",
                town:"",
                phone:"",
                country:"",
                site:"",
                id:0,
                logo:null
            },
            index:0,
            name:"",
            description:"",
            address:"",
            town:"",
            phone:"",
            country:null,
            site:"",
            logo:null,
            showProgress:false
        }
    }

    choiceImage=async()=>{
        ImagePicker.showImagePicker({
            cancelButtonTitle:'Annuler',
            takePhotoButtonTitle:'Prendre une photo',
            chooseFromLibraryButtonTitle:'Choisir dans la galerie',
            chooseWhichLibraryTitle:'Choisir',
            title:'Sélectionner une photo'
        },async response=>{
            try {
                if(response!=null){
                    const uri=response.uri;
                    const type=response.type;
                    const name=response.fileName;
                    if(uri!=null){
                        this.setState({showProgress:true})
                        const name=getRandomString(12)+".ligal";
                        const s= await saveImage(name.toLowerCase(),uri,this.state.shop.logo);
                        if(s==true){
                            await ShopModel.update({
                                logo:name.toLowerCase(),
                                id:this.state.shop.id,
                                is_updated:1
                            })
                            this.onReload();
                        }else{
                            this.setState({showProgress:false})
                        } 
                    }
                }
                this.setState({uri:response.data})
            } catch (error) {
                
            }
        })
    }

    async componentDidMount(){
        const shop=this.props.shop.value;
        this.setState({
            shop:{
                name:shop.name,
                description:shop.description,
                site:shop.site,
                address:shop.address,
                phone:shop.phone,
                town:shop.town,
                country:shop.country,
                logo:shop.logo,
                id:shop.id
            }
        })
    }
    onChangeIndex=(value)=>{
        try {
            if(value==1){
                this.setState({
                    name:this.state.shop.name,
                    description:this.state.shop.description,
                    address:this.state.shop.address,
                    town:this.state.shop.town,
                    phone:this.state.shop.phone,
                    country:this.state.shop.country,
                    site:this.state.shop.site,
                })
            }
            this.setState({index:value})
        } catch (error) {
            
        }
    }
    componentProfil=()=><Text style={[styles.title,{color:this.state.index==0?R.color.background:R.color.colorPrimary}]}>Profil</Text>
    componentUpdate=()=><Text style={[styles.title,{color:this.state.index==1?R.color.background:R.color.colorPrimary}]} >Modification</Text>

    viewProfil=()=>(
        <ScrollView>
            <View style={styles.view}>
                <Text numberOfLines={1} style={styles.label}>
                    Nom de la boutique
                </Text>
                <Text numberOfLines={1} style={[styles.value,{opacity:isEmpty(this.state.shop.name)?0.6:1}]}>
                    {capitalize(this.state.shop.name).toUpperCase()}
                </Text>
            </View>
            <Card.Divider/>
            <View style={styles.view}>
                <Text numberOfLines={1} style={styles.label}>
                    Pays
                </Text>
                <Text numberOfLines={1} style={[styles.value,{opacity:isEmpty(this.state.shop.country)?0.6:1}]}>
                    {isEmpty(this.state.shop.country)?"Pays non mentionné":capitalize(this.state.shop.country).toUpperCase()}
                </Text>
            </View>
            <Card.Divider/>
            <View style={styles.view}>
                <Text numberOfLines={1} style={styles.label}>
                    Téléphone
                </Text>
                <Text numberOfLines={1} style={[styles.value,{opacity:isEmpty(this.state.shop.phone)?0.6:1}]}>
                    {isEmpty(this.state.shop.phone)?"Numéro de téléphone vide":this.state.shop.country}
                </Text>
            </View>
            <Card.Divider/>
            <View style={styles.view}>
                <Text numberOfLines={1} style={styles.label}>
                    Site
                </Text>
                <Text numberOfLines={1} style={[styles.value,{opacity:isEmpty(this.state.shop.site)?0.6:1}]}>
                    {isEmpty(this.state.shop.site)?"Site web non mentionné":capitalize(this.state.shop.site).toLowerCase()}
                </Text>
            </View>
            <Card.Divider/>
            <View style={styles.view}>
                <Text numberOfLines={1} style={styles.label}>
                    Adresse
                </Text>
                <Text numberOfLines={1} style={[styles.value,{opacity:isEmpty(this.state.shop.address)?0.6:1}]}>
                    {isEmpty(this.state.shop.address)?"Adresse non mentionné":this.state.shop.address}
                </Text>
            </View>
        </ScrollView>
    )
    viewUpdate=()=>(
        <ScrollView>
            <Card containerStyle={{marginBottom:10,elevation:10,borderTopLeftRadius:20,borderTopRightRadius:20}}>
            <ScrollView>
            <View style={styles.view}>
                <Input
                    value={this.state.name}
                    onChangeText={value=>this.setState({name:value})}
                    autoCorrect
                    label="Nom de la boutique"
                    labelStyle={styles.label}
                    placeholder="Le nom de la boutique ici"
                    maxLength={20}
                />
            </View>
            <Card.Divider/>
            <View style={styles.view}>
                <Text numberOfLines={1} style={styles.label}>
                    Pays
                </Text>
                <Picker
                    mode="dropdown"
                    selectedValue={this.state.country}
                    onValueChange={(item,index)=>this.setState({country:item})}
                >
                    <Picker.Item label="  aucun pays" key={"countrys"} value={null} />
                    {
                        PAYS.map((item,index)=>(
                            <Picker.Item label={item.translations.fr} key={"country"+index} value={capitalize(item.translations.fr).toLowerCase()} />
                        ))
                    }
                </Picker>
            </View>
            <View>
                <Input
                    value={this.state.town}
                    onChangeText={value=>this.setState({town:value})}
                    autoCorrect
                    label="Votre ville"
                    labelStyle={styles.label}
                    placeholder="Ville d'activité"
                    maxLength={20}
                />
            </View>
            <Card.Divider/>
            <View>
                <Input
                    value={this.state.site}
                    onChangeText={value=>this.setState({site:value})}
                    autoCorrect
                    label="Votre site web"
                    labelStyle={styles.label}
                    placeholder="Votre site web ici"
                    maxLength={30}
                />
            </View>
            <View style={styles.view}>
                <Input
                    value={this.state.phone}
                    onChangeText={value=>this.setState({phone:value})}
                    autoCorrect
                    label="Téléphone"
                    labelStyle={styles.label}
                    placeholder="numéro de téléphone ici"
                    maxLength={15}
                    keyboardType="phone-pad"
                />
            </View>
            <Card.Divider/>
            <View style={styles.view}>
                <Input
                    value={this.state.address}
                    onChangeText={value=>this.setState({address:value})}
                    autoCorrect
                    label="Adresse physique"
                    labelStyle={styles.label}
                    placeholder="Siege de la boutique"
                    maxLength={100}
                    multiline
                />
            </View>

            <Card.Divider/>
            <View style={styles.view}>
                <Input
                    value={this.state.description}
                    onChangeText={value=>this.setState({description:value})}
                    autoCorrect
                    label="Description de la boutique"
                    labelStyle={styles.label}
                    placeholder="Message aux clients"
                    maxLength={256}
                    multiline
                />
            </View>

            <View>
                <Button
                    title="Modifier"
                    buttonStyle={{backgroundColor:R.color.colorPrimary,maxWidth:300,alignSelf:'center',padding:15,paddingLeft:30,paddingRight:30,borderRadius:20}}
                    onPress={()=>this.onUpdate()}
                />
            </View>
        </ScrollView>
            </Card>
        </ScrollView>
    )
    onReload=async()=>{
        try {
            const item=this.props.shop.value;
            const rep=await ShopModel.get();
            if(rep!=null){
                const action={type:S.EVENT.onChangeShop,value:rep}
                const actionService={type:S.EVENT.OnExecuteService,shop:true};
                await this.props.dispatch(action);
                await this.props.dispatch(actionService);
                const shop=this.props.shop.value;
                this.setState({
                    shop:{
                        ...this.state.shop,
                        name:shop.name,
                        description:shop.description,
                        address:shop.address,
                        town:shop.town,
                        phone:shop.phone,
                        country:shop.country,
                        site:shop.site,
                        logo:shop.logo
                    }
                })
                this.setState({showProgress:false,index:0})
            }else{
                this.setState({showProgress:false})
            }
        } catch (error) {
            this.setState({showProgress:false})
        }
    }
    onUpdate=async()=>{
        try {
            const name=this.state.name;
            const country=this.state.country;
            const town=this.state.town;
            const address=this.state.address;
            const phone=this.state.phone;
            const site=this.state.site;
            const description=this.state.description;
            this.setState({
                showProgress:true
            })
            const rep=await ShopModel.update({
                id:this.state.shop.id,
                name:isDifference(this.state.shop.name,name)?name:null,
                address:isDifference(this.state.shop.address,address)?address:null,
                country,
                is_updated:1,
                phone:isDifferenceStrict(this.state.shop.phone,phone)?phone:null,
                site:site,
                town:town,
                description:description
            })
            if(rep==true){
                this.onReload();
            }else{
                this.setState({
                    showProgress:false
                })
            }
        } catch (error) {
        }
    }

    render(){
        return(
            <View style={{flex:1}}>
                <ButtonGroup
                    buttons={[
                        {element:this.componentProfil},
                        {element:this.componentUpdate}
                    ]}
                    selectedIndex={this.state.index}
                    onPress={value=>this.onChangeIndex(value)}
                    containerBorderRadius={10}
                    textStyle={styles.title}
                    buttonStyle={{borderRadius:20}}
                    buttonContainerStyle={{borderRadius:20}}
                    selectedButtonStyle={{backgroundColor:R.color.colorPrimary}}

                />
                <ScrollView>
                <View style={{alignItems:'center',padding:10}}>
                    <Avatar
                        source={{uri:getImageLocal(this.state.shop.logo)}}
                        rounded size="xlarge"
                        title={getTitle(this.state.shop.name)}
                        containerStyle={{borderWidth:4,borderColor:R.color.background,elevation:10,backgroundColor:R.color.colorPrimary}}
                        placeholderStyle={{backgroundColor:R.color.colorPrimary}}
                    >
                        <Avatar.Accessory
                            Component={()=>(
                                <Icon size={30} name='camera-outline'  type='ionicon' color={R.color.background}  />
                            )}
                            size={40} 
                            onPress={()=>this.choiceImage()}
                            style={{backgroundColor:R.color.colorPrimaryDark,borderColor:R.color.background,borderWidth:1}}
                        />
                    </Avatar>
                </View>
                {
                    this.state.index==0?(
                        <this.viewProfil/>
                    ):(
                        <this.viewUpdate/>
                    )
                }
                </ScrollView>
                <ProgressDialog
                    message="Patientez svp!"
                    title="Chargement"
                    visible={this.state.showProgress}
                    activityIndicatorColor={R.color.colorSecondary}
                    animationType="slide"
                    activityIndicatorSize="large"
                />
            </View>
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(App)