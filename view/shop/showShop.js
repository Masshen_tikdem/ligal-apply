import React, { useEffect, useState } from 'react';
import { StyleSheet } from 'react-native';
import { View } from 'react-native';
import { Avatar, Button, Card, Icon, Input, Overlay } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import { Appbar, FAB, List, Text } from 'react-native-paper';
import { connect } from 'react-redux';
import {capitalize, getImageLocal, getTitle, isEmpty} from '../../Manager';
import R from '../../resources/styles.json';
import * as ShopModel from '../../model/local/shop';
import * as ShopWebModel from '../../model/web/society';
import {Picker} from '@react-native-picker/picker';
import {PAYS as countries} from '../../resources/All countries';
import Edit from '../../components/EditShop';

const mapStateToProps = (state) => {
    return {
        user:state.user,
        shop:state.shop
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

const styles=StyleSheet.create({
    card:{
        borderRadius:20,
        paddingTop:40
    },
    icon:{
        alignItems:'center',
        position:'absolute',
        elevation:10,
        alignSelf:'center',
        top:-15
    },
    card2:{
        borderRadius:20,
        paddingLeft:60,
        marginLeft:30,
        marginBottom:10
    },
    icon2:{
        alignItems:'center',
        position:'absolute',
        elevation:10,
        alignSelf:'center',
        left:10,
        justifyContent:'center',
        flex:1,
        flexDirection:'row'
    },
    avatar:{
        backgroundColor:R.color.colorPrimary,
        borderWidth:2,borderColor:R.color.background,
    },
    btn:{
        padding:15,backgroundColor:R.color.colorAccent
    },
    title:{
        fontSize:18,
        fontFamily:"Poppins-Bold"
    },
    subTitle:{
        fontSize:14,
        fontFamily:"Poppins-Thin"
    }
})



const Name=({title,logo=null,number})=>{
    return(
        <View style={{ flex:1,padding:30,alignItems:'center' }}>
            <Avatar
                rounded size='xlarge'
                containerStyle={{ backgroundColor:R.color.colorPrimary }}
                source={{ uri:getImageLocal(logo) }}
                icon={{ name:"bank",type:"ant-design" }}
                title={getTitle(title)}
            />
            <View>
                <Text style={{ padding:10,fontSize:24,fontFamily:"Poppins-Black",textAlign:"center" }}>
                    {title}
                </Text>
            </View>
            {
                !isEmpty(number)?(
                <View>
                    <Text style={{ padding:10,fontSize:16, }}>
                        {number}
                    </Text>
                </View>
                ):null
            }

        </View>
    )
}

const Item=({value="",title="",icon="star",type="ionicon"})=>{
    return(
        <View style={{ flexDirection:'row',alignItems:'center',paddingBottom:10 }}>
            <View style={{ padding:5,paddingRight:15 }}>
                <Icon
                    name={icon}
                    type={type}
                    color={R.color.colorSecondary}
                />
            </View>
            <View style={{ flex:1 }}>
                <Text style={styles.title}>
                    {value}
                </Text>
                <Text style={styles.subTitle}>
                    {title}
                </Text>
            </View>
        </View>
    )
}

const TitleCountry=({country="",town=""})=>{
    return(
        <View style={{ flex:1 }}>
            <Card containerStyle={styles.card}>
                <View style={{ flex:1 }}>
                    <Item
                        key="t-country"
                        title="Pays"
                        value={country}
                        icon="enviroment"
                        type="ant-design"
                    />
                    <Item
                        key="t-town"
                        title="Ville"
                        value={town}
                        icon="enviromento"
                        type="ant-design"
                    />
                </View>
            </Card>
            <View style={styles.icon}>
                <Avatar
                    rounded size='large'
                    containerStyle={styles.avatar}
                    icon={{ name:"earth",type:"ant-design" }}
                />
            </View>
        </View>
    )
}

const TitleAddress=({value=""})=>{
    return(
        <View style={{ flex:1 }}>
            <Card containerStyle={styles.card2}>
                <View style={{ flex:1 }}>
                    <Text style={{ fontSize:20,fontFamily:"Poppins-Bold" }}>
                        Adresse de la boutique
                    </Text>
                    <Text style={{ fontFamily:'Poppins-Light',fontSize:16,textAlign:'justify' }}>
                    {value}

                    </Text>
                </View>
            </Card>
            <View style={styles.icon2}>
                <Avatar
                    rounded size='large'
                    containerStyle={styles.avatar}
                    icon={{ name:"home",type:"ant-design" }}
                />
            </View>
        </View>
    )
}

const TitleDescription=({value=""})=>{
    return(
        <View>
            <Text style={{ fontSize:20,fontFamily:"Poppins-Bold",padding:10,paddingLeft:60,paddingRight:5 }}>
                Description de la boutique
            </Text>
        <View style={{ flexDirection:'row',alignItems:'center',marginLeft:20,marginRight:20 }}>
            <View style={{ margin:5 }}>
                <Icon
                    name="paperclip"
                    type="ant-design"
                    color={R.color.colorSecondary}
                    size={50}
                />
            </View>
            <View style={{ flex:1,marginBottom:30  }}>
                <Text style={{ fontFamily:'Poppins-Light',fontSize:16,textAlign:'justify',color:"#000"}}>
                    {value}

                </Text>
            </View>
        </View>
        </View>
    )
}

function App(props){

    const[name,setName]=useState(null);
    const[country,setCountry]=useState(null);
    const[town,setTown]=useState(null);
    const[address,setAddress]=useState(null);
    const[site,setSite]=useState(null);
    const[description,setDescription]=useState(null);
    const[phone,setPhone]=useState(null);
    const[logo,setLogo]=useState(null);
    const[number,setNumber]=useState(null);
    const[visible,setVisible]=useState(false);

    useEffect(()=>{
        const shop=props.shop.value;
        if(shop!=null && shop!=undefined){
            setName(shop.name);
            setCountry(shop.country);
            setTown(shop.town);
            setAddress(shop.address);
            setSite(shop.site);
            setPhone(shop.phone);
            setDescription(shop.description);
            setLogo(shop.logo);
            setNumber(shop.number);
        }
    },[]);

    useEffect(()=>{
        if(visible==true){
            const shop=props.shop.value;
            if(shop!=null && shop!=undefined){
                setName(shop.name);
                setCountry(shop.country);
                setTown(shop.town);
                setAddress(shop.address);
                setSite(shop.site);
                setPhone(shop.phone);
                setDescription(shop.description);
                setLogo(shop.logo);
                setNumber(shop.number);
            }
            setVisible(false);
        }
    },[props.shop]);

    const getCountry=(code)=>{
        let rep="";
        try {
            const index=countries.findIndex(p=>p.callingCodes[0]==code);
            if(index!=-1){
                rep=countries[index].translations.fr;
            }
        } catch (error) {
            
        }
        return rep;
    }


    return(
        <View style={{ flex:1 }}>
            <ScrollView>
                <Name
                    title={isEmpty(name)?"":capitalize(name).toUpperCase()}
                    key="title-name"
                />
                <TitleCountry
                    key="title-card"
                    country={isEmpty(country)?"------":getCountry(country)}
                    town={isEmpty(town)?"------":capitalize(town)}
                />
                <View style={{ padding:5,paddingLeft:30,paddingRight:30 }}>
                    <Item
                        key="t-phone"
                        title="numéro de téléphone"
                        value={isEmpty(phone)?"------":capitalize(phone)}
                        icon="phone"
                        type="ant-design"
                    />
                    <Item
                        key="t-site"
                        title="site web"
                        value={isEmpty(site)?"------":capitalize(site)}
                        icon="earth"
                        type="ant-design"
                    />
                </View>
                <TitleAddress
                    key="title-address"
                    value={isEmpty(address)?"-------------------------------":capitalize(address)}
                />
                <TitleDescription
                    key="title-description"
                    value={isEmpty(description)?"-------------------------------":capitalize(description)}
                />
            </ScrollView>
           {/*<FAB
                icon={()=><Icon name="edit" type="ant-design" color={R.color.background} />}
                onPress={()=>setVisible(true)}
                style={{ position:'absolute',bottom:20,right:20,backgroundColor:R.color.colorPrimary }}
           />*/}
            <Edit
                e_address={address}
                e_country={country}
                e_description={description}
                e_name={name}
                e_number={number}
                e_phone={phone}
                e_site={site}
                e_town={town}
                key="overlay-edit"
                visible={visible}
                props={props}
                onClose={()=>setVisible(false)}
            />
        </View>
    )
}

export default connect(mapStateToProps,mapDispatchToProps)(App);