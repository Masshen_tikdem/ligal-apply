import React from 'react';
import R from '../../resources/styles.json';
import S from '../../resources/settings.json';
import Awaiter from '../../components/Waiter';
import { View } from 'react-native';
import { ScrollView } from 'react-native';
import { Avatar, Button, Card, Icon, Text } from 'react-native-elements';
import { FlatList } from 'react-native';
import { TouchableOpacity } from 'react-native';
import visa from '../../assets/logo/visa.png';
import { Image } from 'react-native';
import ModalSubscription from '../../components/ModalSubscription';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import DateTime from 'date-and-time';
import fr from 'date-and-time/locale/fr';


const styles=StyleSheet.create({
    btn:{
        backgroundColor:R.color.colorPrimary,
        padding:10,
        paddingLeft:15,
        paddingRight:15,

    }
})

const mapStateToProps = (state) => {
    return {
        right:state.right,
        subscription:state.subscription
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}



class App extends React.Component{

    constructor(props){
        super(props);
        DateTime.locale(fr);
        this.state={
            isLoading:true,
            payment:{status:null,finished_at:null,begin_at:null},
            message:"",
            methods:[],
            num:"",
            cvc:"",
            nameCard:"",
            expiry:"",
            showPayment:false,
            methodType:"",
            type:"",
            title:"",
            restDate:"",
            validate_at:null
        }
    }

    async componentDidMount(){
        try {
            this.onLoad()
            this.checkSubscription();
        } catch (error) {
            
        }
    }

    checkSubscription=()=>{
        try {
            const value=this.props.subscription;
            if(value.status==false){
                this.setState({restDate:"Off",validate_at:null})
            }else{
                const date=DateTime.parse(value.validate_at,"YYYY-MM-DD HH:mm:ss");
                const currentDate=new Date();
                const diff=date.getTime()-currentDate.getTime();
                if(diff>0){
                    const days=Math.round(diff/(24*3600*1000));
                    const dateString=DateTime.format(date,"ddd DD MMM YYYY")
                    this.setState({restDate:"JJ -"+days,validate_at:dateString});
                }
            }
        } catch (error) {
            
        }
    }

    onLoad=async()=>{
        try {
            const methods=[];
            methods.push({
                name:"Orange Money",
                cat:"orange",
                icon:null,
                image:"../../assets/logo/orange-money.png",
                id:"orange"
            })
            methods.push({
                name:"M-pesa",
                cat:"orange",
                icon:null,
                image:"../../assets/logo/m-pesa.jpg",
                id:"vodacom"
            })
            methods.push({
                name:"Airtel money",
                cat:"orange",
                icon:null,
                image:"../../assets/logo/airtel-money.png",
                id:"airtel"
            })
            methods.push({
                name:"Africell Money",
                cat:"orange",
                icon:null,
                image:"../../assets/logo/afrimoney.png",
                id:"africell"
            })
            methods.push({
                name:"VISA",
                cat:"orange",
                icon:null,
                image:"../../assets/logo/visa.png",
                id:"visa"
            })
            this.setState({methods:methods})
            this.setState({isLoading:false})
        } catch (error) {
            
        }
    }


    renderMethod=({item})=>{
        return(
            <TouchableOpacity>
            <View style={{ alignItems:'center',flex:1,padding:10 }}>
                <Avatar
                    rounded size="medium"
                    containerStyle={{ backgroundColor:R.color.colorPrimary }}
                    source={
                        item.id=="orange"?require('../../assets/logo/orange-money.png'):
                        item.id=="airtel"?require('../../assets/logo/airtel-money.jpg'):
                        item.id=="vodacom"?require('../../assets/logo/m-pesa.jpg'):
                        item.id=="africell"?require('../../assets/logo/afrimoney.png'):
                        item.id=="visa"?require('../../assets/logo/visa.png'):
                        require('../../assets/logo/airtel-money.jpg')
                    }
                />
                <Text style={{ fontFamily:"Poppins-Bold",fontSize:16,padding:5 }}>
                    {item.name}
                </Text>
            </View>
            </TouchableOpacity>
        )
    }

    renderMethodPayment=({item})=>{
        return(
            <TouchableOpacity style={{ padding:10 }} onPress={()=>this.setState({methodType:item.id})} >
            <View style={{ alignItems:'center',flex:1,padding:10,backgroundColor:item.id==this.state.methodType?R.color.colorSecondary:"transparent" }}>
                <Avatar
                    rounded size="medium"
                    containerStyle={{ backgroundColor:R.color.colorPrimary }}
                    source={
                        item.id=="orange"?require('../../assets/logo/orange-money.png'):
                        item.id=="airtel"?require('../../assets/logo/airtel-money.jpg'):
                        item.id=="vodacom"?require('../../assets/logo/m-pesa.jpg'):
                        item.id=="africell"?require('../../assets/logo/afrimoney.png'):
                        item.id=="visa"?require('../../assets/logo/visa.png'):
                        require('../../assets/logo/airtel-money.jpg')
                    }
                />
                <Text style={{ fontFamily:"Poppins-Bold",fontSize:16,padding:5 }}>
                    {item.name}
                </Text>
            </View>
            </TouchableOpacity>
        )
    }

    componentInfo=()=>(
        <View>
            <View style={{ flexDirection:'row',padding:10,paddingTop:20,alignItems:'center' }}>
                <View style={{ padding:10 }}>
                    <Avatar
                        title={this.state.restDate}
                        size="large"
                        rounded
                        containerStyle={{ backgroundColor:R.color.colorPrimary }}
                        titleStyle={{ fontFamily:'Poppins-Regular',fontWeight:'normal',fontSize:12 }}
                    />
                </View>
                <View style={{ flex:1 }}>
                    <Text style={{ fontFamily:'Poppins-ExtraBold',fontSize:16,padding:5 }}>
                        Abonnement
                    </Text>
                    {
                        this.state.validate_at!=null?(
                            <Text style={{ padding:5,paddingTop:0,fontFamily:"Poppins-Black" }}>
                                <Text style={{ fontFamily:"Poppins-Regular" }}>jusqu'à </Text>{this.state.validate_at}
                            </Text>
                        ):null
                    }
                </View>
                {
                    this.state.payment.status==""?(
                        <View>
                        </View>
                    ):null
                }
            </View>
        </View>
    )
    componentMethod=()=>{
        return(
            <Card containerStyle={{ borderRadius:20,padding:0 }}>
                <View style={{ padding:15,backgroundColor:R.color.colorAccent }}>
                    <Text style={{ textAlign:'center',fontFamily:'Poppins-Bold',fontSize:16 }}>
                        Méthodes de paiement
                    </Text>
                </View>
                <View>
                    <FlatList
                        data={this.state.methods}
                        renderItem={this.renderMethod}
                        key="method"
                        keyExtractor={item=>item.id}
                        horizontal
                    />
                </View>
            </Card>
        )
    }

    componentPayMessage=()=>{
        return(
            <View>
                <View style={{ padding:10,paddingBottom:2 }}>
                    <Text style={{ fontFamily:'Poppins-Bold',fontSize:18,textAlign:'center' }}>
                        Acheter des SMS
                    </Text>
                </View>
            <View style={{ flexDirection:'row',alignItems:'center',padding:10,paddingTop:0 }}>
                <View style={{ padding:10 }}>
                    <Icon
                        name="envelope-o"
                        type="font-awesome"
                    />
                </View>
                <View style={{ flex:1 }}>
                    <Text style={{ fontFamily:'Poppins-Light',fontSize:15,padding:5 }}>
                        Acheter des SMS pour rester en contact avec vos clients, Cliquez sur acheter pour découvrir le prix des SMS et l’acheter.
                    </Text>
                </View>
                <View style={{ alignItems:'center' }}>
                    <Text style={{ fontFamily:'Poppins-Black',fontSize:20 }}>
                        0
                    </Text>
                    <Text>
                        SMS
                    </Text>
                    <Button
                        title="Acheter"
                        buttonStyle={styles.btn}
                        onPress={()=>this.onShowPayment("sms")}
                        disabled
                    />
                </View>
            </View>
            </View>
        )
    }

    componentPayFacture=()=>{
        return(
            <View>
                <View style={{ alignItems:'center',padding:10,paddingBottom:0 }}>
                    <Text style={{ fontFamily:'Poppins-Bold',fontSize:18 }}>
                        Payer l'Abonnement
                    </Text>
                </View>
            <View style={{ flexDirection:'row',alignItems:'center',padding:10,paddingTop:0 }}>
                <View style={{ padding:10 }}>
                    <Icon
                        name="usd"
                        type="font-awesome"
                    />
                </View>
                <View style={{ flex:1 }}>
                    <Text style={{ fontFamily:'Poppins-Light',fontSize:15,padding:5 }}>
                    Payez l’abonnement pour vous, aussi pour d’autres membres. Plusieurs moyens sont possibles afin de vous permettre de continuer avec Ligal-Apply. 
                    </Text>
                </View>
                <View style={{ alignItems:'center' }}>
                    <Button
                        title="Payer"
                        buttonStyle={styles.btn}
                        onPress={()=>this.onShowPayment("subscription")}
                        disabled
                    />
                </View>
            </View>
            </View>
        )
    }

    onShowOffice=()=>{
        try {
            this.props.navigation.navigate('WebDesktop')
        } catch (error) {
            
        }
    }

    componentOffice=()=>{
        return(
            <View>
                <View style={{ padding:10,paddingBottom:0,alignItems:'center' }} >
                    <Text style={{ fontFamily:'Poppins-Bold',fontSize:18 }}>
                        Bureau en ligne
                    </Text>
                </View>
            <View style={{ flexDirection:'row',alignItems:'center',padding:10,paddingTop:2 }}>
                <View style={{ padding:10 }}>
                    <Icon
                        name="home"
                        type="font-awesome"
                    />
                </View>
                <View style={{ flex:1 }}>
                    <Text style={{ fontFamily:'Poppins-Light',fontSize:15,padding:5 }}>
                    Ligal-Apply est aussi en ligne, allez-y pour découvrir plus d’options.
                    </Text>
                </View>
                <View style={{ alignItems:'center' }}>
                    <Button
                        title="Visitez"
                        buttonStyle={styles.btn}
                        //disabled={!(this.props.right.dashboard==7)}
                        onPress={()=>this.onShowOffice()}
                    />
                </View>
            </View>
            </View>
        )
    }

    onShowPayment=(type)=>{
        try {
            if(type=="sms" || type=="subscription"){
                const element=this.state.methods[0];
                this.setState({
                    showPayment:true,
                    type:type,
                    methodType:element.id
                })
                if(type=="sms"){
                    this.setState({title:"Achat des SMS"})
                }else{
                    this.setState({title:"Paiement de l'abonnement"})
                }
            }
        } catch (error) {
            
        }
    }

    onClose=()=>{
        this.setState({
            type:"",
            showPayment:false
        })
    }

    render(){
        return(
            this.state.isLoading==true?(
                <Awaiter
                    color={R.color.colorSecondary}
                    size={100}
                    message={this.state.message}
                    key="awaiter"
                />
            ):(
                <View>
                    <ScrollView>
                        <this.componentInfo />
                        <this.componentMethod />
                        <this.componentPayMessage />
                        <this.componentPayFacture />
                        <this.componentOffice />
                    </ScrollView>
                    <ModalSubscription
                        visible={this.state.showPayment}
                        num={this.state.num}
                        cvc={this.state.cvc}
                        methods={this.state.methods}
                        renderMethod={this.renderMethodPayment}
                        expiry={this.state.expiry}
                        methodSelected={this.state.methodType}
                        key="payment"
                        onClose={()=>this.onClose()}
                        title={this.state.title}
                        type={this.state.type}
                    /> 
                </View>

            )            
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(App);