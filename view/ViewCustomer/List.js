import React, { Component } from 'react';
import { View } from 'react-native';
import R from '../../resources/styles.json';
import S from '../../resources/settings.json';
import EmptyMessage from '../../components/EmptyMessage';
import Awaiter from '../../components/Waiter';
import { FlatList } from 'react-native';
import * as CustomerModel from '../../model/local/client';
import { Avatar, Button, ListItem, Text } from 'react-native-elements';
import { capitalize, getPrice, isEmpty } from '../../Manager';
import SMSComponent from '../../components/SMSComponent';
import CardCustomer from '../../components/CardCustomer.js';
import CardInputSMS from '../../components/CardButtonSMS';
import { connect } from 'react-redux';


const mapStateToProps = (state) => {
    return {
        user:state.user,
        shop:state.shop
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

class App extends Component{

    constructor(props){
        super(props)
        this.state={
            isLoading:true,
            refresh:false,
            list:[],
            visible:false,
            society:"",
            content:"",
            sms:0,
            checkAll:false,
            checkTitle:"Tout",
            selectCustomers:[]
        }
    }
    renderItem=({item,index})=>{
        const buy=item.buy;
        let price="";
        if(buy.length>0){
            buy.map((p,i)=>{
                if(i>0){
                    price+=" + ";
                }
                price+=getPrice(p.stock,p.signe)+"";
            })
        }else{
            price="0";
        }
        return(
            <CardCustomer
                checked={this.state.selectCustomers.findIndex(p=>p.id==item.id)!=-1}
                key={`card_customer_${index}`}
                name={item.name}
                subtitle={price}
                onChecked={()=>this.onCheck(item)}
            />     
        )
    }
    onCheck=(item)=>{
        try {
            let index=this.state.selectCustomers.findIndex(p=>p.id==item.id);
            if(index==-1){
                    const list=this.state.selectCustomers;
                    list.push(item);
                    this.setState({
                        selectCustomers:list
                    })
            }else{
                const list=[];
                this.state.selectCustomers.map(p=>{
                    if(p.id!=item.id){
                        list.push(p);
                    }
                })
                this.setState({
                    selectCustomers:list
                })
            }
        } catch (error) {
            console.log('error',error)
        }
    }
    checkAll=()=>{
        try {
            this.state.list.map((item,index)=>{
                this.onCheck(item)
            })
        } catch (error) {
            
        }
    }
    onLoad=async()=>{
        try {
            const rep=await CustomerModel.getCustomers();
            this.setState({
                list:rep
            })
            if(this.state.isLoading==true){
                this.setState({isLoading:false})
            }
        } catch (error) {
            
        }
    }
    onRefresh=async()=>{
        try {
            this.setState({refresh:true});
            await this.onLoad();
            this.setState({refresh:false})
        } catch (error) {
            
        }
    }

    async componentDidMount(){
        try {
            await this.onLoad();
        } catch (error) {
            
        }
    }

    render(){
        return(
            this.state.isLoading==true?(
                <Awaiter  color={R.color.colorSecondary} size={100} key="awaiter" />
            ):this.state.list.length==0?(
                <EmptyMessage onRefresh={()=>this.onRefresh()} title="Liste vide" key="empty" refresh={this.state.refresh} />
            ):(
                <View style={{ flex:1 }}>
                    <FlatList
                        data={this.state.list}
                        renderItem={this.renderItem}
                        extraData={this.state.selectCustomers}
                        key="list"
                        keyExtractor={item=>item.id}
                    />
                    <CardInputSMS
                        checked={this.state.selectCustomers.length===this.state.list.length}
                        onChecked={()=>{this.state.selectCustomers.length===this.state.list.length?this.setState({selectCustomers:[]}):this.checkAll()}}
                        countSMS={this.state.sms}
                        key='card_input_sms'
                        titleCheck={this.state.selectCustomers.length===this.state.list.length?"Désélectionner":"Tout"}
                        onPress={()=>this.setState({visible:true})}
                        disabled={this.state.selectCustomers.length==0}
                    />
                    <SMSComponent
                        visible={this.state.visible}
                        onClose={()=>this.setState({visible:false})}
                        content={this.state.content}
                        societyName={this.state.society}
                        onChangeContent={value=>this.setState({content:value})}
                    />
                </View>
            )
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(App)