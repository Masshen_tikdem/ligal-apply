import React, { Component } from 'react';
import { View } from 'react-native';
import R from '../../resources/styles.json';
import S from '../../resources/settings.json';
import EmptyMessage from '../../components/EmptyMessage';
import Awaiter from '../../components/Waiter';
import { FlatList } from 'react-native';
import * as CustomerModel from '../../model/local/client';
import * as ArticleModel from '../../model/local/article';
import { Avatar, Button, CheckBox, ListItem, Overlay, Text } from 'react-native-elements';
import { capitalize, getArticleName, getPrice, isEmpty } from '../../Manager';
import { Appbar, FAB } from 'react-native-paper';
import CardInputSMS from '../../components/CardButtonSMS';
import SMSComponent from '../../components/SMSComponent';
import CardCustomer from '../../components/CardCustomer';
import { connect } from 'react-redux';


const mapStateToProps = (state) => {
    return {
        user:state.user,
        shop:state.shop
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}


class App extends Component{

    constructor(props){
        super(props)
        this.state={
            isLoading:true,
            refresh:false,
            list:[],
            customers:[],
            articles:[],
            selects:[],
            visible:false,
            selectCustomers:[],
            sms:0,
            titleCheck:'Tout',
            checked:false,
            visibleSMS:false,
            title:"",
            content:""
        }
    }
    renderItem=({item,index})=>{
        const buy=item.buy;
        let price="";
        if(buy.length>0){
            buy.map((p,i)=>{
                if(i>0){
                    price+=" + ";
                }
                price+=getPrice(p.stock,p.signe)+"";
            })
        }else{
            price="0";
        }
        return(
            <CardCustomer
                name={item.name}
                subtitle={price}
                checked={this.state.selectCustomers.findIndex(p=>p.id==item.id)!=-1}
                onChecked={()=>this.onCheck(item)}
                key={`card_customer_promotion${index}`}
            />
        )
    }

    selectArticle=(item)=>{
        try {
            let index=0;
            this.state.selects.map(p=>{
                if(p.id==item.id){
                    index=p.id;
                }
            })
            if(index<1){
                const list=this.state.selects;
                list.push(item);
                this.setState({
                    selects:list
                })
            }else{
                const list=[];
                this.state.selects.map(p=>{
                    if(p.id!=item.id){
                        list.push(p);
                    }
                })
                this.setState({
                    selects:list
                })
            }
        } catch (error) {
            
        }
    }

    

    renderArticle=({item,index})=>{
        return(
            <View style={{ flexDirection:'row',alignItems:'center' }}>
                <View>
                    <CheckBox
                        checked={this.state.selects.findIndex(p=>p.id===item.id)!==-1?true:false}
                        key={`checked${index}`}
                        onPressIn={()=>this.selectArticle(item)}
                    />
                </View>
                <View style={{ flex:1 }}>
                    <Text numberOfLines={1} style={{ fontFamily:'Poppins-Bold' }}>
                        {getArticleName(item.name,item.description)}
                    </Text>
                    <Text numberOfLines={1} style={{ fontFamily:'Poppins-Light' }}>
                        {capitalize(item.famille).toUpperCase()}
                    </Text>
                </View>
            </View>
        )
    }
    onLoad=async()=>{
        try {
            const rep=await CustomerModel.getCustomers();
            const repA=await ArticleModel.get();
            this.setState({
                articles:repA
            })
            this.setState({
                customers:rep,
            })
            if(this.state.isLoading==true){
                this.setState({isLoading:false})
            }
        } catch (error) {
            
        }
    }
    onRefresh=async()=>{
        try {
            this.setState({refresh:true});
            await this.onLoad();
            this.setState({refresh:false})
        } catch (error) {
            
        }
    }

    onSearch=async()=>{
        try {
            this.setState({isLoading:true,visible:false});
            const list=[];
            const onEvent=(item)=>{
                let passer=false;
                for (const key of item.buys) {
                    console.log('KEY',key);
                    for (const p of this.state.selects) {
                        console.log('PPP',p);
                        if(key.id_article==p.id || key.id_famille==p.id_famille){
                            passer=true;
                        }
                    }
                }
                if(passer){
                    list.push(item);
                }
            }
            await this.state.customers.map(onEvent);
            this.setState({isLoading:false,list,selects:[]});
        } catch (error) {
            
        }
    }

    FabComponent=()=>(
        <FAB
            style={{ position:'absolute',bottom:0,right:0,margin:20,backgroundColor:R.color.colorPrimary }}
            icon="plus"
            onPress={()=>this.setState({visible:true})}
        />
    )
    OverlayComponent=()=>(
        <Overlay overlayStyle={{ padding:0 }} fullScreen visible={this.state.visible} animationType='slide'>
            <View style={{ flex:1 }}>
            <Appbar.Header style={{ backgroundColor:R.color.colorPrimary }}>
                <Appbar.BackAction
                    onPress={()=>this.setState({visible:false})}
                />
                <Appbar.Content
                    title="Choix des articles"
                />
            </Appbar.Header>
            <FlatList
                data={this.state.articles}
                extraData={this.state.selects}
                renderItem={this.renderArticle}
                key="article"
                keyExtractor={item=>item.id}
            />
            <View style={{ alignItems:'center',padding:10 }}>
                <Button
                    title="Cherchez"
                    buttonStyle={{ padding:15,borderRadius:15 }}
                    disabled={this.state.selects.length==0}
                    onPress={this.onSearch}
                />
            </View>
            </View>
        </Overlay>
    )

    async componentDidMount(){
        try {
            await this.onLoad();
        } catch (error) {
            
        }
    }

    onCheck=(item)=>{
        try {
            let index=this.state.selectCustomers.findIndex(p=>p.id==item.id);
            if(index==-1){
                    const list=this.state.selectCustomers;
                    list.push(item);
                    this.setState({
                        selectCustomers:list
                    })
            }else{
                const list=[];
                this.state.selectCustomers.map(p=>{
                    if(p.id!=item.id){
                        list.push(p);
                    }
                })
                this.setState({
                    selectCustomers:list
                })
            }
        } catch (error) {
            console.log('error',error)
        }
    }
    checkAll=()=>{
        try {
            this.state.customers.map((item)=>{
                this.onCheck(item)
            })
        } catch (error) {
            
        }
    }

    render(){
        return(
            this.state.isLoading==true?(
                <Awaiter  color={R.color.colorSecondary} size={100} key="awaiter" />
            ):this.state.list.length==0?(
                <View style={{ flex:1 }}>
                    <EmptyMessage onRefresh={()=>this.onRefresh()} title="La liste de clients est vide" key="empty" refresh={this.state.refresh} />
                    <this.OverlayComponent/>
                    <this.FabComponent/>
                </View>
            ):(
                <View style={{ flex:1 }}>
                    <FlatList
                        data={this.state.list}
                        renderItem={this.renderItem}
                        key="list"
                        keyExtractor={item=>item.id}
                    />
                    <this.OverlayComponent/>
                    <this.FabComponent/>
                    <CardInputSMS
                        titleCheck={this.state.titleCheck}
                        countSMS={this.state.sms}
                        checked={this.state.selectCustomers.length===this.state.customers.length}
                        onChecked={()=>{this.state.selectCustomers.length===this.state.customers.length?this.setState({selectCustomers:[]}):this.checkAll()}}
                        key="card_input_sms_promotion"
                        onPress={()=>this.setState({visibleSMS:true})}
                        disabled={this.state.selectCustomers.length==0}
                        titleCheck={this.state.selectCustomers.length===this.state.customers.length?"Désélectionner":"Tout"}
                    />
                    <SMSComponent
                        visible={this.state.visibleSMS}
                        onClose={()=>this.setState({visibleSMS:false})}
                        key="SMS_component_promotion"
                        societyName={this.state.title}
                        content={this.state.content}
                        onChangeContent={value=>this.setState({content:value})}
                    />
                </View>
            )
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(App)