import React, { Component } from 'react';
import { View } from 'react-native';
import R from '../../resources/styles.json';
import S from '../../resources/settings.json';
import EmptyMessage from '../../components/EmptyMessage';
import Awaiter from '../../components/Waiter';
import { FlatList } from 'react-native';
import * as CustomerModel from '../../model/local/client';
import { Avatar, ListItem, Text } from 'react-native-elements';
import { capitalize, getPrice, isEmpty } from '../../Manager';
import DateTime from 'date-and-time';
import fr from 'date-and-time/locale/fr';
import CardInputSMS from '../../components/CardButtonSMS';
import CardCustomer from '../../components/CardCustomer';
import SMSComponent from '../../components/SMSComponent';
import { connect } from 'react-redux';



const mapStateToProps = (state) => {
    return {
        user:state.user,
        shop:state.shop
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}


class App extends Component{

    constructor(props){
        super(props)
        this.state={
            isLoading:true,
            refresh:false,
            list:[],
            selects:[],
            sms:0,
            selectCustomers:[],
            visible:false,
            title:"",
            content:"",
        }
        DateTime.locale(fr);
    }
    renderItem=({item,index})=>{
        const buy=item.buy;
        let price="";
        if(buy.length>0){
            buy.map((p,i)=>{
                if(i>0){
                    price+=" + ";
                }
                price+=getPrice(p.stock,p.signe)+"";
            })
        }else{
            price="0";
        }
        const days=Math.round(item.difference/86400000);
        return(
            <CardCustomer
                name={item.name}
                subtitle={price}
                checked={this.state.selectCustomers.findIndex(p=>p.id==item.id)!=-1}
                onChecked={()=>this.onCheck(item)}
                key={`index_${index}`}
                last={item.last}
                days={days}
            />
        )
    }
    onLoad=async()=>{
        try {
            const rep=await CustomerModel.getCustomers();
            const date=new Date();
            date.setDate(date.getDate()-5);
            const list=[];
            const onEvent=(item)=>{
                if(item.buy.length>0){
                    const buy=item.buys[0];
                    const created_at=DateTime.parse(buy.created_at,"YYYY-MM-DD HH:mm:ss");
                    if(date.getTime()>=created_at.getTime()){
                        item.difference=(new Date()).getTime()-created_at.getTime();
                        item.last=DateTime.format(created_at,"ddd, DD MMM YY HH:mm");
                        list.push(item);
                    }
                }
            }
            await rep.map(onEvent);
            this.setState({
                list
            })
            if(this.state.isLoading==true){
                this.setState({isLoading:false})
            }
        } catch (error) {
            console.log('error',error)
        }
    }
    onRefresh=async()=>{
        try {
            this.setState({refresh:true});
            await this.onLoad();
            this.setState({refresh:false})
        } catch (error) {
            
        }
    }

    async componentDidMount(){
        try {
            await this.onLoad();
        } catch (error) {
            
        }
    }

    onCheck=(item)=>{
        try {
            let index=this.state.selectCustomers.findIndex(p=>p.id==item.id);
            if(index==-1){
                    const list=this.state.selectCustomers;
                    list.push(item);
                    this.setState({
                        selectCustomers:list
                    })
            }else{
                const list=[];
                this.state.selectCustomers.map(p=>{
                    if(p.id!=item.id){
                        list.push(p);
                    }
                })
                this.setState({
                    selectCustomers:list
                })
            }
        } catch (error) {
            console.log('error',error)
        }
    }
    checkAll=()=>{
        try {
            this.state.list.map((item,index)=>{
                this.onCheck(item)
            })
        } catch (error) {
            
        }
    }

    render(){
        return(
            this.state.isLoading==true?(
                <Awaiter  color={R.color.colorSecondary} size={100} key="awaiter" />
            ):this.state.list.length==0?(
                <EmptyMessage onRefresh={()=>this.onRefresh()} title="Liste vide" key="empty" refresh={this.state.refresh} />
            ):(
                <View style={{ flex:1 }}>
                    <FlatList
                        data={this.state.list}
                        renderItem={this.renderItem}
                        key="list"
                        keyExtractor={item=>item.id}
                    />
                    <CardInputSMS
                        key="card_input_sms_rare"
                        checked={this.state.selectCustomers.length===this.state.list.length}
                        onChecked={()=>{this.state.selectCustomers.length===this.state.list.length?this.setState({selectCustomers:[]}):this.checkAll()}}
                        countSMS={this.state.sms}
                        titleCheck={this.state.selectCustomers.length===this.state.list.length?"Désélectionner":"Tout"}
                        onPress={()=>this.setState({visible:true})}
                        disabled={this.state.selectCustomers.length==0}
                    />
                    <SMSComponent
                        visible={this.state.visible}
                        onClose={()=>this.setState({visible:false})}
                        content={this.state.content}
                        onChangeContent={value=>this.setState({content:value})}
                        societyName={this.state.title}
                        key="SMS_component"
                    />
                </View>
            )
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(App)