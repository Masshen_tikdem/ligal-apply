import React from 'react';
import { Component } from 'react';
import { FlatList } from 'react-native';
import { View } from 'react-native';
import { Avatar, Button, Card, Input, ListItem, Text } from 'react-native-elements';
import { connect } from 'react-redux';
import { getTitle } from '../Manager';
import R from '../resources/styles.json';


const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.article
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}


class App extends Component{

    constructor(props){
        super(props)
        this.state={
            list:[],
            name:"",
            phone:"",
            id:0
        }
    }

    renderItem=({item})=>{
        return(
            <View>
                <ListItem>
                    <View style={{alignItems:'center'}}>
                        <Avatar
                            source={{uri:item.photo}}
                            rounded
                            title={getTitle(item.name)}
                            size="medium"
                        />
                        <Text>
                            {item.name}
                        </Text>
                    </View>
                </ListItem>
            </View>
        )
    }
    onChangeName=(value)=>{
        try {
            
        } catch (error) {
            
        }
    }
    onChangePhone=(value)=>{
        try {
            
        } catch (error) {
            
        }
    }

    render(){

        return(
            <View style={{flex:1}}>
                <View>
                    <Card>
                        <View>
                            <Input
                                value={this.state.phone}
                                label="Numéro de téléphone"
                                keyboardType="phone-pad"
                                onChangeText={value=>this.onChangePhone(value)}
                            />
                            <Input
                                value={this.state.name}
                                label="Nom du client"
                                onChangeText={value=>this.onChangeName(value)}
                                placeholder="Nom du client"
                                errorMessage={this.state.name.trim().length==0?"Ecrivez un nom significatif":""}
                            />
                            <View style={{alignItems:'center'}}>
                                <Button
                                    title="Ajouter"
                                    buttonStyle={{backgroundColor:R.color.colorPrimary}}
                                />
                            </View>
                        </View>
                    </Card>
                </View>
                <FlatList
                    data={this.state.list}
                    renderItem={this.renderItem}
                    key="list"
                    numColumns={2}
                />
            </View>
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(App)