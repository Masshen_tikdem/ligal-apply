import { StackActions } from '@react-navigation/native';
import React from 'react';
import { Component } from 'react';
import { Text } from 'react-native';
import { StyleSheet } from 'react-native';
import { ScrollView } from 'react-native';
import { Image } from 'react-native';
import { StatusBar } from 'react-native';
import { View } from 'react-native';
import { Avatar, Button, Card, Header, Icon, Input, Overlay } from 'react-native-elements';
import { Dialog, ProgressDialog } from 'react-native-simple-dialogs';
import { connect } from 'react-redux';
import R from '../resources/styles.json';
import S from '../resources/settings.json';
import * as ShopWebModel from '../model/web/society';
import Awaiter from '../components/Waiter';
import Empty from '../components/EmptyMessage';
import { FlatList } from 'react-native';
import { capitalize, downloadImage, getFullName, getImage, getImageLocal,setCodeVerify,getCodeVerify, getRandomString, getTitle, isConnected, UserSetStorage } from '../Manager';
import CardInternet from '../components/InternetError';
import * as AgentModel from '../model/local/agent';
import * as ArticleModel from '../model/local/article';
import * as BuyModel from '../model/local/buy';
import * as ClientModel from '../model/local/client';
import * as DeviseModel from '../model/local/devise';
import * as FamilyModel from '../model/local/family';
import * as RightModel from '../model/local/right';
import * as ShopModel from '../model/local/shop';
import * as StockModel from '../model/local/stock';
import * as UserWebModel from '../model/web/users';
import * as SubscriptionModel from '../model/local/subscription';
import * as SettingModel from '../model/local/setting';
import { Alert } from 'react-native';
import OTP from '../components/OTPComponent';
import OTPVerify from 'react-native-otp-verify';
import auth from '@react-native-firebase/auth'


const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.article
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

const styles=StyleSheet.create({
    btn:{
        margin:15,
        padding:15,
        borderRadius:20
    },
    txt:{
        fontFamily:"Poppins-Light",
        fontSize:16
    },
    card:{
        elevation:20,
        borderTopStartRadius:20,
        borderTopEndRadius:20
    },
    message:{
        fontFamily:'Poppins-Light',padding:10,textAlign:'center'
    },
    title:{
        fontSize:40,fontFamily:"Poppins-Black",padding:20,paddingBottom:0,
        textShadowColor:R.color.colorPrimary,textShadowOffset:{width:10,height:2},textShadowRadius:30
    },
    subTitle:{
        fontSize:20,fontFamily:"Poppins-Thin",padding:10,paddingTop:0,paddingLeft:50,marginTop:-15
    }
})

class App extends Component{

    constructor(props){
        super(props)
        this.state={
            showShop:false,showMessage:false,
            showProgress:false,list:[],
            shop:null,isLoading:false,
            showError:false,message:'',
            refresh:false,code:"",visibleOTP:true,
            time:120,loadOtp:false
        }
    }

    onConnexion=()=>{

    }
    onShop=()=>{
        this.props.navigation.dispatch(
            StackActions.replace("Configuration")
        )
    }
    async componentDidMount(){
        const user=this.props.user.value;
        const rep=await getCodeVerify();
        if(rep.status==200){
            if(rep.response!=null){
                const code=JSON.parse(rep.response);
                if(code.status==true){
                    this.setState({visibleOTP:false})
                }
            }
        }
        if(user.code==null){
            this.setState({visibleOTP:false})
        }else{
            this.listenerOTP();
            this.onTime();
            setTimeout(()=>{
                this.setState({showMessage:false})
            },2500)
        }
        await this.onLoadShop();
    }

    listenerOTP(){
        OTPVerify.getOtp().then(p=>{
            OTPVerify.addListener((message)=>{
                try {
                    if(message!=null){
                        const messages=/(\d{4})/g.exec(message);
                        console.log('messs',messages);
                        //setState to code state
                        let otp="";
                        this.setState({code:otp});
                        OTPVerify.removeListener();
                        //Keyboard.dismiss();
                    }
                } catch (error) {
                    console.log('error',error)
                }
            });
        }).catch(error=>{
            console.log('error',error);
        });
    }
    onTime=()=>{
        setInterval(()=>{
            if(this.state.time>0){
                this.setState({time:this.state.time-1})
            }else{
                return;
            }
        },1000)
    }
    onLoadShop=async()=>{
        try {
            const user=this.props.user.value;
            const rep=await ShopWebModel.getUserShop(user.private_key);
            if(rep.status==200){
                this.setState({list:rep.response})
            }
            if(this.state.isLoading==true){
                this.setState({isLoading:false})
            }
        } catch (error) {
        }
    }
    renderItem=({item})=>{
        const logo=getImage(item.logo);
        return(
            <View>
                <Card containerStyle={{borderRadius:20}}>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <View style={{width:100}}>
                            {
                                logo=='p'?(
                                    <Text style={{textAlign:'center',fontSize:50,fontFamily:'Poppins-Black'}}>
                                        {getTitle(item.name)}
                                    </Text>
                                ):(
                                    <Card.Image
                                        source={{uri:logo}}
                                    />
                                )
                            }
                        </View>
                        <View style={{flex:1}}>
                            <Card.Title style={{fontWeight:'normal',fontFamily:'Poppins-Bold',fontSize:20}}>
                                {capitalize(item.name).toUpperCase()}
                            </Card.Title>
                            <Text>

                            </Text>
                        </View>
                    </View>
                    <View style={{flexDirection:'row-reverse',alignItems:'center'}}>
                        <View style={{alignItems:'flex-end'}}>
                            <Button
                                title="Réjoindre"
                                type="clear"
                                onPress={()=>this.onGo(item)}
                            />
                        </View>
                    </View>
                </Card>
            </View>
        )
    }

    onAddSubscription=async(list=[])=>{
        try {
            const user=this.props.user.value;
            const onEvent=async(p,i)=>{
                if(p.agent_private_key==user.private_key){
                    await SubscriptionModel.store({
                        created_at:p.created_at,
                        price:p.price,
                        private_key:p.private_key,
                        status:p.status,
                        transaction:p.transaction,
                        type:p.type,
                        unity:p.unity,
                        user:user.id,
                        validate_at:p.validate_at
                    })
                }
            }
            await list.map(onEvent);
        } catch (error) {
            
        }
    }

    onAddSettings=async(list=[])=>{
        try {
            const user=this.props.user.value;
            const onEvent=async(p,i)=>{
                await SettingModel.store({
                    id_shop:1,
                    code:p.code,
                    value:p.value,
                    private_key:p.private_key,
                    is_updated:0
                })
            }
            await list.map(onEvent);
        } catch (error) {
            
        }
    }

    onAddDevises=async(list=[])=>{
       try {
            const onEvent=async(p,i)=>{
                await DeviseModel.store({
                    is_updated:0,
                    name:p.name,
                    signe:p.signe,
                    private_key:p.private_key,
                    type:p.type,
                    status:p.status
                });
            }
            await list.map(onEvent);
       } catch (error) {
       }
    }
    downloadUserImage=async(item,id)=>{
        try {
            if(item.photo!=null && item.photo!=undefined){
                const photo=getImage(item.photo);
                const imageName="user"+id+item.private_key+'.ligal';
                const load=await downloadImage(imageName.toLowerCase(),photo,null);
                if(load==true){
                    await AgentModel.update({
                        id:id,
                        photo:imageName.toLowerCase()
                    })
                }
            }
        } catch (error) {
            
        }
    }
    onAddAgents=async(list=[],user,shop)=>{
        try {

            const onEvent=async(p,index)=>{
                if(p.private_key!=user.private_key){
                    await AgentModel.store({
                        address:p.address,
                        email:p.email,
                        first_name:p.first_name,
                        is_updated:0,
                        last_name:p.last_name,
                        password:p.password,
                        phone:p.phone,
                        private_key:p.private_key,
                        sexe:p.sex,
                        status:1
                    })
                }
            }

            

            const onSaveWork=async(p,index)=>{
                let id=0;
                let value=null;
                const onMapAgent=async(x,index)=>{
                    if(x.private_key==p.private_key){
                        value=x;
                        id=p.id;
                    }
                }
                let state=0;
                await list.map(onMapAgent);
                if(p.status==1 || p.status==true){
                    state=1;
                }
                if(id==1){
                    state=1;
                }
                const repWork=await AgentModel.storeWork({
                    created_at:p.created_at,
                    finished_at:p.finished_at,
                    id_agent:id,
                    id_shop:shop.id,
                    is_updated:0,
                    poste:p.poste,
                    private_key:p.work_private_key,
                    status:state,
                    updated_at:p.updated_at
                })
                await this.downloadUserImage(value,id);
            }
            await list.map(onEvent);
            const items=await AgentModel.get();
            await items.map(onSaveWork);
        } catch (error) {
        }
    }
    onAddCategories=async(list=[])=>{
        try {
            const onEvent=async(p,index)=>{
                await FamilyModel.store({
                    is_updated:0,
                    name:p.name,
                    private_key:p.private_key,
                    status:p.status==true?1:0,
                    type:p.type,
                })
            }
            await list.map(onEvent)
        } catch (error) {
        }
    }
    onAddLinks=async(list=[])=>{
        try {
            const familles=await FamilyModel.get();
            const onEvent=async(p,index)=>{
                let id_high=0;
                let id_sub=0;
                for(var x of familles){
                    if(x.private_key==p.high_private_key){
                        id_high=x.id;
                    }
                }
                for(var x of familles){
                    if(x.private_key==p.sub_private_key){
                        id_sub=x.id;
                    }
                }
                if(id_sub>0 && id_high>0){
                    await FamilyModel.storeLink({
                        high_family:id_high,
                        is_updated:0,
                        private_key:p.private_key,
                        status:p.status,
                        sub_family:id_sub
                    });
                }
            }
            await list.map(onEvent);
        } catch (error) {
        }
    }
    onAddArticles=async(list=[],shop)=>{
        try {
            const familles=await FamilyModel.get();
            const devises=await DeviseModel.get();
            const onEvent=async(p,index)=>{
                let id_famille=0;
                let id_devise=0;
                for(var x of familles){
                    if(x.private_key==p.famille_private_key){
                        id_famille=x.id;
                    }
                }
                for(var x of devises){
                    if(x.private_key==p.devise_private_key){
                        id_devise=x.id;
                    }
                }
                if(id_famille>0 && id_devise>0){
                    await ArticleModel.store({
                            description:p.description,
                            devise:id_devise,
                            famille:id_famille,
                            is_updated:0,
                            name:p.name,
                            price:p.price,
                            private_key:p.private_key,
                            quantifiable:p.quantifiable,
                            shop:shop.id,
                            type:p.type,
                            status:p.status==true?1:0,
                            mode:p.mode,
                            stockage:p.stockage
                        })
                }
            }
            await list.map(onEvent);
        } catch (error) {
        }
    }
    onAddStocks=async(list=[])=>{
        try {
            //stocks
            const agents=await AgentModel.get(); 
            const articles=await ArticleModel.get();
            const devises=await DeviseModel.get();
            const onEvent=async(p,index)=>{
                let id_article=0;
                let id_agent=0;
                let id_devise=0;
                for(var x of devises){
                    if(x.private_key==p.devise_private_key){
                        id_devise=x.id;
                    }
                }
                for(var x of articles){
                    if(x.private_key==p.article_private_key){
                        id_article=x.id;
                    }
                }
                for(var x of agents){
                    if(x.private_key==p.agent_private_key){
                        id_agent=x.id;
                    }
                }
                if(id_article>0 && id_agent>0){
                    await StockModel.store({
                        created_at:p.local_created_at,
                        fournisseur:p.provider,
                        id_agent:id_agent,
                        id_article:id_article,
                        private_key:p.private_key,
                        quantity:p.quantity,
                        id_devise:id_devise>0?id_devise:null
                    })
                }
            }
            await list.map(onEvent);
        } catch (error) {
        }
    }

    onAddRights=async(list=[],user)=>{
        try {
            //rights
            const id=user.id;
            const onEvent=async(p,index)=>{
                if(user.private_key==p.agent_private_key){
                    await RightModel.store({
                        code:p.code,
                        value:p.value,
                        id_agent:id,
                        is_updated:0,
                        private_key:p.private_key
                    })
                }
            }
            await list.map(onEvent);
        } catch (error) {
        }
    }

    onAddBuy=async(list=[])=>{
        try {
            const agents=await AgentModel.get(); 
            const articles=await ArticleModel.get();
            const devises=await DeviseModel.get();
            const customers=await ClientModel.get();
            const onEvent=async(p,index)=>{
                let id_article=0;
                let id_agent=0;
                let id_devise=0;
                let id_customer=0;
                for(var x of articles){
                    if(x.private_key==p.article_private_key){
                        id_article=x.id;
                    }
                }
                for(var x of agents){
                    if(x.private_key==p.agent_private_key){
                        id_agent=x.id;
                    }
                }
                for(var x of customers){
                    if(x.private_key==p.customer_private_key){
                        id_customer=x.id;
                    }
                }
                for(var x of devises){
                    if(x.private_key==p.devise_private_key){
                        id_devise=x.id;
                    }
                }

                if(id_article>0 && id_agent>0 && id_devise>0){
                    await BuyModel.store({
                        created_at:p.local_created_at,
                        id_agent:id_agent,
                        id_article:id_article,
                        id_client:id_customer,
                        id_devise:id_devise,
                        is_alter:p.is_alter==true?1:0,
                        price:p.price,
                        private_key:p.private_key,
                        quantity:p.quantity,
                        reference:p.reference,
                        remise:p.remise,
                        status:p.status==true?1:0
                    })
                }
            }
            await list.map(onEvent);
        } catch (error) {
        }
    }
    onAddCustomers=async(list=[])=>{
        try {
            const onEvent=async(item,index)=>{
                await ClientModel.store({
                    is_updated:0,
                    name:item.name,
                    phone:item.phone,
                    private_key:item.private_key
                })
            }
            await list.map(onEvent);
        } catch (error) {
        }
    }

    setRights=async(id)=>{
        let rep=false;
        try {
            const right=await RightModel.get(id);
            const element=new Object();
            const len=right.length;
            element.type=S.EVENT.OnChangeRight;
            const event=(item,index)=>{
                const type=S.ENUM;
                switch (item.code) {
                    case capitalize(type.CATEGORY).toLowerCase():
                        element.category=item.value;
                    break;
                    case capitalize(type.DEVISE).toLowerCase():
                        element.devise=item.value;
                    break;
                    case capitalize(type.VENTE_PRODUIT).toLowerCase():
                        element.vente_produit=item.value;
                    break;
                    case capitalize(type.JOURNAL_PRODUIT).toLowerCase():
                        element.journal=item.value;
                    break;
                    case capitalize(type.DASHBOARD).toLowerCase():
                        element.dashboard=item.value;
                    break;
                    case capitalize(type.STOCK_IN).toLowerCase():
                        element.stock=item.value;
                    break;
                    case capitalize(type.STOCK_OUT).toLowerCase():
                        element.sales=item.value;
                    break;
                    case capitalize(type.CLIENT).toLowerCase():
                        element.customer=item.value;
                    break;
                    case capitalize(type.PRODUCT).toLowerCase():
                        element.article=item.value;
                    break;
                }
            }
            await right.map(event);
            this.props.dispatch(element)
            rep=true;
            return rep;
        } catch (error) {
        }
    }

    onCheckCode=()=>{
        try {
            const user=this.props.user.value;
            setTimeout(()=>{
                if(user.code==this.state.code){
                    this.setState({visibleOTP:false,time:0});
                    setCodeVerify(JSON.stringify({code:user.code,status:true}))
                }else{
                    Alert.alert("Vérification","Le code est incorrect veuillez réssayer")
                }
            },1000);
        } catch (error) {
            console.log('error',error);
        }
    }

    exists=(list=[],x)=>{
        let rep=0;
        list.map(p=>{
            if(p.private_key==x){
                rep=p.id;
                return rep;
            }
        })
        return rep;
    }

    resend=async()=>{
        try {
            const user=this.props.user.value;
            const phone=user.phone;
            const message=this.state.message;
            this.setState({showProgress:true,message:"Recherche d'un nouveau code"})
            const req=await UserWebModel.authentification(JSON.stringify({phone:phone}));
            this.setState({showProgress:false,message:message});
            if(req.status==200){
                this.setState({time:120})
                const item=req.response;
                const agents=await AgentModel.get();
                const exist=this.exists(agents,item.private_key);
                if(exist==0){
                    Alert.alert("Code de vérification","Veuillez réessayer. Votre demande n'a pas abouti! code d'erreur 502")
                }else{
                    //when the user exist
                    await AgentModel.update({
                        id:exist,
                        code:item.code
                    });
                    let user=await AgentModel.show(exist);
                    user=await AgentModel.show(exist);
                    if(user!=null){
                        const action={type:S.EVENT.onChangeUser,status:true,value:user}
                        await this.props.dispatch(action)
                        await UserSetStorage(JSON.stringify({user:user}))
                    }
                }
            }else{
                Alert.alert("Code de vérification","Veuillez réessayer. Votre demande n'a pas abouti!")
            }
        } catch (error) {
            
        }
    }

    onGo=(item)=>{
        try {
            isConnected().then(c=>{
                if(c==true){
                    this.setState({
                        showProgress:true,
                        message:"Recherche des infos"
                    })
                    const user=this.props.user.value;
                    ShopWebModel.recover(item.private_key).then(rep=>{
                        if(rep.status==200){
                            this.setState({message:"Installation de la boutique"})
                            //shop
                            const webShop=rep.response.society;
                            const webDevises=rep.response.devises;
                            const webCategories=rep.response.categories;
                            const webAgents=rep.response.agents;
                            const webArticles=rep.response.articles;
                            const webStocks=rep.response.stocks;
                            const webBuys=rep.response.buys;
                            const webCustomer=rep.response.customers;
                            const webLink=rep.response.links;
                            const webRights=rep.response.rights;
                            const webSubscriptions=rep.response.subscriptions;
                            const webSettings=rep.response.settings;
                            ShopModel.store({
                                address:webShop.address,
                                country:webShop.country,
                                image_updated:0,
                                is_updated:0,
                                latitude:webShop.latitude,
                                logo:null,
                                longitude:webShop.longitude,
                                name:webShop.name,
                                phone:webShop.phone,
                                private_key:webShop.private_key,
                                site:webShop.site,
                                town:webShop.town
                            }).then(a=>{
                                if(a==true){
                                    ShopModel.get().then(shop=>{
                                        this.onAddDevises(webDevises).then(b=>{
                                            this.onAddAgents(webAgents,user,shop).then(c=>{
                                                this.onAddCategories(webCategories).then(d=>{
                                                    this.onAddLinks(webLink).then(e=>{
                                                        this.onAddCustomers(webCustomer).then(f=>{
                                                            this.onAddArticles(webArticles,shop).then(g=>{
                                                                this.onAddStocks(webStocks).then(h=>{
                                                                    this.onAddBuy(webBuys).then(i=>{
                                                                        this.onAddRights(webRights,user).then(j=>{
                                                                            this.setRights(user.id).then(right=>{
                                                                                this.onAddSubscription(webSubscriptions).then(k=>{
                                                                                    this.onAddSettings(webSettings).then(l=>{
                                                                                        AgentModel.show(user.id).then(work=>{
                                                                                            let right=null;
                                                                                            const action={type:S.EVENT.onChangeUser,status:true,value:work,work:right}
                                                                                            this.props.dispatch(action)
                                                                                            UserSetStorage(JSON.stringify({user:work})).then(j=>{
                                                                                                const actionShop={type:S.EVENT.onChangeShop,value:shop};
                                                                                                    this.props.dispatch(actionShop)
                                                                                                    this.props.navigation.dispatch(
                                                                                                        StackActions.replace('Home')
                                                                                                    )
                                                                                                    this.setState({
                                                                                                        showProgress:false
                                                                                                })
                                                                                            })
                                                                                        })
                                                                                    })
                                                                                })
                                                                            })
                                                                        })
                                                                    })
                                                                })
                                                            })
                                                        })
                                                    })
                                                    
                                                })
                                            })
                                        })
                                    })
                                }
                            })

                        }else{
                            this.setState({
                                showProgress:false
                            })
                            Alert.alert("Traitement","La recherche n'a pas abouti, veuillez réessayer!")
                        }
                    })
                }else{
                    this.setState({showError:true})
                }
            })
        } catch (error) {
        }
    }

    render(){
        return(
            <>
            <View style={{flex:1,justifyContent:'center',backgroundColor:R.color.background,alignItems:'center'}}>
                <StatusBar hidden />
                <ScrollView>
                    <Text style={styles.title}>
                        Ligal Apply
                    </Text>
                    <Text style={styles.subTitle}>
                        Gestion spécialisée
                    </Text>
                    {
                        this.props.user.value!=null?(
                            <View style={{ flexDirection:'row',alignItems:'center',padding:5 }}>
                                <View>
                                    <Avatar
                                        source={{ uri:getImageLocal(this.props.user.value.photo) }}
                                        title={getTitle(this.props.user.value.last_name)}
                                        containerStyle={{ backgroundColor:R.color.colorPrimary }}
                                        rounded size='medium'
                                    />
                                </View>
                                <View style={{ flex:1 }}>
                                    <Text numberOfLines={1} style={{ fontFamily:'Poppins-Light',fontSize:14,padding:5 }}>
                                       {getFullName(this.props.user.value.first_name,this.props.user.value.last_name)} 
                                    </Text>
                                    <Text numberOfLines={1} style={{ fontFamily:'Poppins-Thin',paddingLeft:5,margin:0,marginTop:-5,paddingTop:-5 }}>
                                        {this.props.user.value.phone}
                                    </Text>
                                </View>
                            </View>
                        ):null
                    }
                    <View style={{alignItems:'center',margin:30,marginTop:50}}>
                        <Image
                        source={require('../assets/logo/ligal.png')}
                        />
                    </View>
                    <View>
                        <Text style={styles.message}>
                        Voulez-vous rejoindre une boutique connectée à ligal platform ou reprendre vos activités dans cette boutique
                        </Text>
                        <Button
                            title="Rejoindre une boutique"
                            buttonStyle={[styles.btn,{backgroundColor:R.color.colorPrimary}]}
                            titleStyle={[styles.txt]}
                            onPress={()=>this.setState({showShop:true})}
                        />
                        <Card.Divider/>
                        <Text style={styles.message}>
                        Voulez-vous démarrer avec ligal ? Créer votre boutique et profitez des offres disponibles.
                        </Text>
                        <Button
                            title="Créer une boutique"
                            buttonStyle={[styles.btn]}
                            titleStyle={[styles.txt,{color:R.color.colorSecondary}]}
                            onPress={()=>this.onShop()}
                            type="outline"
                        />
                    </View>
                </ScrollView>
            </View>
            <Overlay isVisible={this.state.showShop} fullScreen animationType="slide"
             overlayStyle={{padding:0}} >
                <View style={{ flex:1 }}>
                <Header
                    backgroundColor={R.color.colorPrimary}
                    leftComponent={
                        <Icon 
                            name="remove" color={R.color.background}
                            type="font-awesome"
                            onPress={()=>this.setState({showShop:false})}
                        />
                    }
                    centerComponent={
                        <Text style={{fontFamily:'Poppins-Light',fontSize:14,padding:5,color:R.color.background}}>
                            Réjoindre la boutique
                        </Text>
                    }
                    placement="left"
                    containerStyle={{height:50,alignItems:'center'}}
                />
                {
                    this.state.isLoading==true?(
                        <Awaiter color={R.color.colorSecondary} size={100} key="await" />
                    ):this.state.list.length==0?(
                        <Empty 
                            refresh={this.state.refresh} 
                            onRefresh={()=>this.onLoadShop()}
                            title="Aucune boutique n'est liée à votre compte"
                            key="empty"
                        />
                    ):(
                        <View>
                            <View>
                                <Text style={{fontFamily:'Poppins-Light',padding:20,fontSize:20,textAlign:'center'}}>
                                    Cliquer sur la boutique afin de reprendre vos activités.
                                </Text>
                            </View>
                            <FlatList
                                data={this.state.list}
                                renderItem={this.renderItem}
                                key="list"
                                keyExtractor={item=>item.id}
                            />
                        </View>
                    )
                }
                </View>
            </Overlay>
            <Dialog
                animationType="slide" 
                onTouchOutside={()=>this.setState({showMessage:false})}
                 visible={this.state.showMessage}>
                     <View>

                     </View>
            </Dialog>
            <ProgressDialog
                message={this.state.message}
                title="Traitement..."
                activityIndicatorColor={R.color.colorSecondary}
                activityIndicatorSize="large"
                animationType="fade"
                visible={this.state.showProgress}
            />
            <CardInternet
                onClose={()=>this.setState({showError:false})}
                visible={this.state.showError}
                key="error"
            />
            <OTP
                visible={this.state.visibleOTP}
                key="otp_verify"
                code={this.state.code}
                onCodeChanged={v=>this.setState({code:v})}
                onCodeFilled={()=>this.onCheckCode()}
                time={this.state.time}
                onResend={()=>this.resend()}
                loading={this.state.loadOtp}
            />
            </>
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(App)