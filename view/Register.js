import React from 'react';
import { Component } from 'react';
import { ScrollView } from 'react-native';
import { View } from 'react-native';
import { Input } from 'react-native-elements';
import RadioButton from '../components/RadioButton';
import R from '../resources/styles.json';



class App extends Component{

    constructor(props){
        super(props)
        this.state={
            last_name:"",
            first_name:"",
            phone:"",
            address:"",
            email:"",
            sex:""

        }
    }


    render(){
        return(
            <View style={{flex:1}}>
                <ScrollView>
                    <Input
                        value={this.state.first_name}
                        label="Prénom"
                        placeholder="Votre prénom"
                        onChangeText={value=>this.setState({first_name:value})}
                        maxLength={20}
                    />
                    <Input
                        value={this.state.last_name}
                        label="Nom"
                        placeholder="Votre nom"
                        onChangeText={value=>this.setState({last_name:value})}
                        maxLength={20}
                    />
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <RadioButton
                            checked={this.state.sex}
                            title="Femme"
                            value="f"
                            onPress={()=>this.setState({sex:"f"})}
                            color={R.color.colorPrimary}
                        />
                        <RadioButton
                            checked={this.state.sex}
                            title="Homme"
                            value="m"
                            onPress={()=>this.setState({sex:"m"})}
                            color={R.color.colorPrimary}
                        />
                    </View>
                    <Input
                        value={this.state.phone}
                        label="Téléphone"
                        placeholder="+243"
                        onChangeText={value=>this.setState({phonee:value})}
                        maxLength={20}
                        keyboardType="phone-pad"
                    />
                </ScrollView>
            </View>
        )
    }
}
export default (App);