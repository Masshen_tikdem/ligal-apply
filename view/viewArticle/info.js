import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { View } from 'react-native';
import { Avatar, Card, Icon, Text } from 'react-native-elements';
import { FAB } from 'react-native-paper';
import R from '../../resources/styles.json';
import * as Manager from '../../Manager';
import { connect } from 'react-redux';
import * as ArticleModel from '../../model/local/article';
import S from '../../resources/settings.json';


const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.viewArticle
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

class App extends Component{

    constructor({props,route}){
        super(props)
        this.state={
            opened:false,
            route,
            article:{
                name:'',
                id:0,
                type:'',
                price:0,
                description:'',
                quantity:0,
                devise:'',
                buy:0,
                stock:0,
                famille:"",
                mark:"",
                logo:null,
                quantifiable:0,
                mode:null,
                stockable:0
            },
        }
    }

    async componentDidMount(){
        try {
            const item=this.state.route.params.item;
            this.setState({
                article:{
                    name:item.name,
                    id:item.id,
                    devise:item.devise,
                    type:item.type,
                    price:item.price,
                    description:item.description,
                    buy:item.buy,
                    stock:item.stock,
                    quantity:0,
                    famille:item.famille,
                    mark:null,
                    logo:item.logo,
                    quantifiable:item.quantifiable,
                    mode:item.mode,
                    stockable:item.stockable
                }
            })
        } catch (error) {
            
        }
    }
    onLoad=async()=>{
        try {
            const item=await ArticleModel.show(this.state.article.id);
            this.setState({
                article:{
                    name:item.name,
                    id:item.id,
                    devise:item.devise,
                    type:item.type,
                    price:item.price,
                    description:item.description,
                    buy:item.buy,
                    stock:item.stock,
                    quantity:0,
                    famille:item.famille,
                    mark:null,
                    logo:item.logo,
                    quantifiable:item.quantifiable,
                    mode:item.mode,
                    stockable:item.stockable
                }
            })
        } catch (error) {
            console.log('info error',error)
        }
    }

    getValueStock=(value,mode="",type="")=>{
        let rep="0";
        try {
            if(value!=undefined && value!=null){
                if(value.stock!=null && value.stock!=undefined){
                    const p=Manager.getStock(value.stock,[],mode,type);
                    rep=p.text;
                }else{
                    rep="0";
                }
            }else {
                rep="0"
            }
        } catch (error) {
        }
        return rep;
    }
    getValueBuy=(value)=>{
        let rep="0";
        try {
            if(value!=undefined && value!=null){
                if(value.stock!=null && value.stock!=undefined){
                    const p=Manager.getStock(value.buy,[],value.mode,value.type);
                    rep=p.text;
                }else{
                    rep="0";
                }
            }else {
                rep="0"
            }
        } catch (error) {
        }
        return rep;
    }
    async componentDidUpdate(){
        try {
            const article=this.props.article.info;
            if(article==true){
                const action={type:S.EVENT.onChangeArticle,info:false}
                this.props.dispatch(action);
                await this.onLoad()
            }
        } catch (error) {
            console.log('error',error);
        }
    }
    getStore=(value)=>{
        let rep="0";
        try {
            if(value!=undefined && value!=null){
                if(value.stock!=null && value.stock!=undefined){
                    const p=Manager.getStock(value.stock,value.buy,value.mode,value.type);
                    rep=p.text;
                }else{
                    rep="0";
                }
            }else {
                rep="0"
            }
        } catch (error) {
        }
        return rep;
    }

    render(){

        return(
            <>
            <View style={{flex:1}}>
                <ScrollView>
                    <View style={{alignItems:'center',margin:20}}>
                        <Avatar
                            rounded
                            size='xlarge'
                            icon={{name:'cube',type:'font-awesome'}}
                            source={{uri:Manager.getImageLocal(this.state.article.logo)}}
                            containerStyle={{backgroundColor:R.color.colorPrimary,elevation:20}}
                            placeholderStyle={{backgroundColor:R.color.colorPrimary}}
                        />
                    </View>
                    <Card
                        containerStyle={{borderRadius:20}}
                    >
                        <View>
                            <Text style={{opacity:0.6,fontSize:20,textAlign:'center',fontFamily:'Poppins-Black'}}>
                                {Manager.capitalize(this.state.article.name).toUpperCase()}
                            </Text>
                            {
                                (this.state.article.description!=null && this.state.article.description!="" && this.state.article.description!=undefined)?(
                                    <Text style={{opacity:0.6,textAlign:'center',color:"red"}}>
                                        {Manager.capitalize(this.state.article.description).toUpperCase()}
                                    </Text>
                                ):null
                            }
                            <View style={{flexDirection:'row',alignItems:'center',padding:10}}>
                                {
                                    (this.state.article.type!=S.ENUM.article.service || (this.state.article.type==S.ENUM.article.service && this.state.article.mode==S.ENUM.stockage.devise))?(
                                        <View style={{flex:1}}>
                                            <Text numberOfLines={1} style={{textAlign:'center',fontFamily:'Poppins-Bold'}}>
                                                {this.getValueStock(this.state.article,this.state.article.mode,this.state.article.type)}
                                            </Text>
                                            <Text style={{textAlign:'center',fontFamily:'Poppins-Light'}}>
                                                stock
                                            </Text>
                                        </View>
                                    ):null
                                }
                                {
                                    (this.state.article.type!=S.ENUM.article.service || (this.state.article.type==S.ENUM.article.service && this.state.article.mode==S.ENUM.stockage.devise))?(
                                        <View style={{flex:1}}>
                                            <Text numberOfLines={1} style={{textAlign:'center',fontFamily:'Poppins-Bold',fontSize:14}}>
                                                {this.getStore(this.state.article)}
                                            </Text>
                                            <Text style={{textAlign:'center'}}>
                                                Magasin
                                            </Text>
                                        </View>
                                    ):null
                                }
                                <View style={{flex:1}}>
                                    <Text numberOfLines={1} style={{textAlign:'center',fontFamily:'Poppins-Bold'}}>
                                        {this.getValueBuy(this.state.article)}
                                    </Text>
                                    <Text style={{textAlign:'center',fontFamily:'Poppins-Light'}}>
                                        vente
                                    </Text>
                                </View>
                            </View>
                            {
                                (this.state.article.mode!=S.ENUM.stockage.devise)?(
                                    <View>
                                        <Text style={{textAlign:'center',fontFamily:'Poppins-Black',fontSize:18}}>
                                            {Manager.getPrice(this.state.article.price,this.state.article.devise)}
                                        </Text>
                                        <Text style={{textAlign:'center',fontFamily:'Poppins-Light'}}>
                                            Prix
                                        </Text>
                                    </View>
                                ):null
                            }
                        </View>
                    </Card>
                    <Card containerStyle={{ marginBottom:15 }}>
                        <Card.Title>
                            Autres info
                        </Card.Title>
                        <View style={{padding:10}}>
                            {
                                (this.state.article.type=='service' || this.state.article.type=='product')?(
                                    <Text style={{textAlign:'center',fontFamily:'Poppins-Black',fontSize:20}}>
                                        {this.state.article.type==S.ENUM.article.product?"produit":this.state.article.type}
                                    </Text>
                                ):null
                            }
                            {
                                (this.state.article.mark!=null && this.state.article.mark!=undefined)?(
                                    <View>
                                        <Text style={{fontFamily:'Poppins-Light',fontSize:16}}>
                                            Sous-catégorie
                                        </Text>
                                        <Text style={{fontFamily:'Poppins-Black',fontSize:18}}>
                                            {Manager.capitalize(this.state.article.mark)}
                                        </Text>    
                                    </View>
                                ):null
                            }
                            <Text style={{fontFamily:'Poppins-Light',fontSize:16}}>
                                Catégorie
                            </Text>
                            <Text style={{fontFamily:'Poppins-Black',fontSize:18}}>
                                {Manager.capitalize(this.state.article.famille)}
                            </Text>
                            {
                                (this.state.article.type==S.ENUM.article.service)?
                                (this.state.article.quantifiable==1)?(
                                    <Text style={{fontFamily:'Poppins-Light',textAlign:'justify',fontSize:18}}>
                                        La vente de ce service nécessite <Text style={{fontFamily:'Poppins-Bold'}}>une quantification</Text> (ou pas). Vous pouvez quantifier la vente en plus d’une référence.
                                    </Text>
                                ):(
                                    <Text style={{fontFamily:'Poppins-Light',textAlign:'justify',fontSize:18}}>
                                        La vente de ce service ne nécessite pas <Text style={{fontFamily:'Poppins-Bold'}}>une quantification</Text>. Vous pouvez juste vous servir d’une référence de service pour le vendre.
                                    </Text>
                                ):(this.state.article.mode==S.ENUM.stockage.devise)?(
                                    <View>
                                        <Text style={{fontFamily:'Poppins-Light',textAlign:'justify',fontSize:18}}>
                                        Ce service est approvisionné en <Text style={{fontFamily:'Poppins-Bold'}}>devise monétaire</Text>.
                                        </Text>
                                    </View>
                                ):(this.state.article.mode=="unity")?(
                                    <View>
                                        <Text style={{fontFamily:'Poppins-Light',textAlign:'justify',fontSize:18}}>
                                        Ce service se comporte comme un produit avec <Text style={{fontFamily:'Poppins-Bold'}}>un approvisionnement avant l’exécution de la vente.</Text>
                                        </Text>
                                    </View>
                                ):null
                            }
                        </View>
                    </Card>
                </ScrollView>
            </View>
            

            </>
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(App)