import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import * as StockModel from '../../model/local/stock';
import CardJournal from '../../components/CardJournal';
import DateTime from 'date-and-time';
import fr from 'date-and-time/locale/fr';
import EmptyMessage from '../../components/EmptyMessage';
import Awaiter from '../../components/Waiter';
import R from '../../resources/styles.json';
import { FlatList } from 'react-native';
import * as Manager from '../../Manager';
import { Text } from 'react-native-elements';
import S from '../../resources/settings.json';



const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.viewArticle
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

class App extends Component{

    constructor({props,route}){
        super(props)
        this.state={
            route,
            list:[],
            devises:[],
            isLoading:true,
            count:0,
            total:"0"
        }
    }
    renderItem=({item})=>{
        let date=Manager.getDateList(item.created_at,"full");
        return(
            <CardJournal
                title={Manager.getArticleName(item.name,item.description)}
                date={date}
                count={(item.type==S.ENUM.article.service && item.mode==S.ENUM.stockage.devise)?null:item.quantity}
                size={1}
                price={(item.type==S.ENUM.article.service && item.mode==S.ENUM.stockage.devise)?Manager.getStock(item.quantity,[],item.mode,item.type).text:null}
                logo={Manager.getImageLocal(item.logo)}
                onPress={()=>this.props.navigation.navigate("ShowStock",{id:item.id})}
            />
        )
    }

    async componentDidMount(){
        this.load()
    }

    load=async()=>{
        try {
            const item=this.state.route.params.item;
            const rep=await StockModel.show(item.id)
            this.setState({
                list:rep
            })
            this.setState({
                count:rep.length
            })
            if(this.state.isLoading==true){
                this.setState({
                    isLoading:false
                })
            }
        } catch (error) {
            console.log('d',error)
        }
    }

    async componentDidUpdate(){
        try {
            const article=this.props.article.stock;
            if(article==true){
                const action={type:S.EVENT.onChangeArticle,stock:false}
                this.props.dispatch(action);
                await this.load()
            }
        } catch (error) {
            console.log('d',error)
        }
    }

    render(){

        return(
            this.state.isLoading==true?(
                <Awaiter color={R.color.colorSecondary} size={100} key='awaiter'  />
            ):this.state.list==0?(
                <EmptyMessage title="Aucun stock enregistré pour cet article" key='message' />
            ):(
                <View style={{flex:1}} key='v'>
                    <View style={{backgroundColor:R.color.colorPrimary,padding:10}} key='v2'>
                        <Text style={{color:R.color.background,padding:10,fontSize:18,textAlign:'center'}}>
                            {this.state.count>0?this.state.count+" approvisionnement"+(this.state.count>1?'s':"")+" de stocks":""}
                        </Text>
                    </View>
                    <FlatList
                        data={this.state.list}
                        renderItem={this.renderItem}
                        key='list'
                    />
                </View>
            )
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(App)