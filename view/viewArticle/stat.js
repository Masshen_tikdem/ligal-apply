import React, { Component } from 'react';
import { View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import EmptyMessage from '../../components/EmptyMessage';
import Awaiter from '../../components/Waiter';
import R from '../../resources/styles.json';
import CardAlert from '../../components/CardAlert';
import * as NotificationModel from '../../model/local/notification';

const mapStateToProps = (state) => {
    return {
        user:state.user,
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

class App extends Component{

    constructor(props){
        super(props)
        this.state={
            isLoading:true,
            list:[],
            showAlert:false
        }
    }

    async componentDidMount(){
        try {
            await this.onLoad();
        } catch (error) {
            
        }
    }

    onLoad=async()=>{
        try {
            const params=this.props.route.params.item;
            const rep=await NotificationModel.show(params.id,"article");
            if(rep!=null){
                this.setState({list:rep})
            }
            if(this.state.isLoading==true){
                this.setState({isLoading:false})
            }
        } catch (error) {
            
        }
    }

    onShowAlert=(item)=>{
        try {
            
        } catch (error) {
            
        }
    }

    renderItem=({item,index})=>{
        return(
            <CardAlert
                content={item.content}
                date={item.created_at}
                title={item.title}
                type={item.type}
                key={`index${index}`}
                onPress={()=>this.onShowAlert(item)}
            />
        )
    }

    render(){

        return(
            this.state.isLoading==true?(
                <Awaiter
                    color={R.color.colorSecondary}
                    size={100}
                    key="awaiter"
                />
            ):this.state.list.length==0?(
                <EmptyMessage
                    title="Aucune alerte sur l'article"
                />
            ):(
                <>
                    <FlatList
                        data={this.state.list}
                        renderItem={this.renderItem}
                        key="list"
                        keyExtractor={item=>item.id}
                    />
                </>
            )
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(App)