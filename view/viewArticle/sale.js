import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import * as BuyModel from '../../model/local/buy';
import CardJournal from '../../components/CardJournal';
import DateTime from 'date-and-time';
import fr from 'date-and-time/locale/fr';
import EmptyMessage from '../../components/EmptyMessage';
import Awaiter from '../../components/Waiter';
import R from '../../resources/styles.json';
import { FlatList } from 'react-native';
import * as Manager from '../../Manager';
import { Text } from 'react-native-elements';
import S from '../../resources/settings.json';
import * as DeviseModel from '../../model/local/devise';



const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.viewArticle
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

class App extends Component{

    constructor({props,route}){
        super(props)
        this.state={
            route,
            list:[],
            devises:[],
            isLoading:true,
            count:0,
            total:"0",
            showProgress:false
        }
    }
    getPriceArticle=(price,quantity)=>{
        let rep=0;
        try {
            rep=parseFloat(price)*parseInt(quantity);
        } catch (error) {
            
        }
        return rep;
    }
    renderItem=({item})=>{
        const date=Manager.getDateList(item.created_at,"full");
        return(
            <CardJournal
                title={Manager.getArticleName(item.name,item.description)}
                date={date}
                onPress={()=>this.props.navigation.navigate('Buy',{id:item.id})}
                count={(item.type==S.ENUM.article.service && item.mode==S.ENUM.stockage.devise)?null:item.quantity}
                price={Manager.getPrice(this.getPriceArticle(item.price,item.quantity),item.signe)}
                logo={Manager.getImageLocal(item.logo)}
            />
        )
    }

    async componentDidMount(){
        this.load()
    }

    load=async()=>{
        try {
            const item=this.state.route.params.item;
            if(this.state.devises.length==0){
                const d=await DeviseModel.get();
                this.setState({
                    devises:d
                })
            }
            const rep=await BuyModel.show(item.id);
            this.setState({
                list:rep
            })
            const total=Manager.getTotal(rep,this.state.devises)
            this.setState({
                count:rep.length,
                total
            })
            if(this.state.isLoading==true){
                this.setState({
                    isLoading:false
                })
            }
        } catch (error) {
            console.log('d',error)
        }
    }

    async componentDidUpdate(){
        try {
            const article=this.props.article.sale;
            if(article==true){
                const action={type:S.EVENT.onChangeArticle,sale:false}
                this.props.dispatch(action);
                await this.load()
            }
        } catch (error) {
            console.log('d',error)
        }
    }

    render(){

        return(
            this.state.isLoading==true?(
                <Awaiter color={R.color.colorSecondary} size={100} key='awaiter'  />
            ):this.state.list==0?(
                <EmptyMessage title="Aucune vente enregistrée pour cet article" key='message' />
            ):(
                <View style={{flex:1}} key='v'>
                    <View style={{backgroundColor:R.color.colorPrimary,padding:10}} key='v2'>
                        <Text style={{color:R.color.background,fontSize:18,fontFamily:'Poppins-Black',textAlign:'center'}}>
                            {this.state.total}
                        </Text>
                        <Text style={{color:R.color.background,fontSize:16,textAlign:'center',fontFamily:'Poppins-Thin'}}>
                            {this.state.count>0?this.state.count+" vente"+(this.state.count>1?'s':"")+" réalisée"+(this.state.count>1?'s':""):""}
                        </Text>
                    </View>
                    <FlatList
                        data={this.state.list}
                        renderItem={this.renderItem}
                        key='list'
                    />
                </View>
            )
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(App)