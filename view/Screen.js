import React from 'react';
import { Component } from 'react';
import { Alert, Text } from 'react-native';
import { StyleSheet } from 'react-native';
import {Picker} from '@react-native-picker/picker';
import { ScrollView } from 'react-native';
import { Image } from 'react-native';
import { StatusBar } from 'react-native';
import { View } from 'react-native';
import { Avatar, Button, Card, Divider, Input, Overlay } from 'react-native-elements';
import R from '../resources/styles.json';
import RadioButton from '../components/RadioButton';
import * as AgentModel from '../model/local/agent';
import * as ShopModel from '../model/local/shop';
import { StackActions } from '@react-navigation/native';
import S from '../resources/settings.json';
import { capitalize, downloadImage, getImage, isConnected, removeCodeVerify, UserSetStorage } from '../Manager';
import { connect } from 'react-redux';
import { ProgressDialog } from 'react-native-simple-dialogs';
import {PAYS} from '../resources/All countries';
import * as UserWebModel from '../model/web/users';
import CardInternet from '../components/InternetError';
import {Appbar} from "react-native-paper";
import * as RightModel from '../model/local/right';
import auth from '@react-native-firebase/auth';


const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.article
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

const styles=StyleSheet.create({
    btn:{
        margin:15,
        padding:10,
        borderRadius:20,
        marginLeft:20,
        marginRight:20
    },
    txt:{
        fontFamily:"Poppins-Light",
        fontSize:16
    },
    card:{
        elevation:20,
        borderTopStartRadius:20,
        borderTopEndRadius:20
    },
    label:{
        fontFamily:"Poppins-Bold",
        fontSize:16,
        fontWeight:'normal'
    }
})

class App extends Component{

    constructor(props){
        super(props)
        this.state={
            agent:null,
            shop:null,
            work:null,
            showLogin:false,
            showRegister:false,
            showProgress:false,
            loginPhone:"",
            first_name:"",
            last_name:"",
            sex:"",
            phone:"",
            code:"243",
            showError:false,
            authCode:"243",
            authPhone:"",
            flag:null
        }
    }

    onConnexion=()=>{
        this.setState({showLogin:true})
    }
    onRegister=()=>{
        this.setState({showRegister:true})
    }
    isEmpty=(value="")=>{
        let rep=false;
        if(value==undefined || value==null){
            rep=true;
        }else{
            if(value.trim()==""){
                rep=true
            }
        }
        return rep;
    }
    exists=(list=[],x)=>{
        let rep=0;
        list.map(p=>{
            if(p.private_key==x){
                rep=p.id;
                return rep;
            }
        })
        return rep;
    }
    setRights=async(id)=>{
        let rep=false;
        try {
            const right=await RightModel.get(id);
            const element=new Object();
            const len=right.length;
            element.type=S.EVENT.OnChangeRight;
            const event=(item,index)=>{
                const type=S.ENUM;
                switch (item.code) {
                    case capitalize(type.CATEGORY).toLowerCase():
                        element.category=item.value;
                    break;
                    case capitalize(type.DEVISE).toLowerCase():
                        element.devise=item.value;
                    break;
                    case capitalize(type.VENTE_PRODUIT).toLowerCase():
                        element.vente_produit=item.value;
                    break;
                    case capitalize(type.JOURNAL_PRODUIT).toLowerCase():
                        element.journal=item.value;
                    break;
                    case capitalize(type.DASHBOARD).toLowerCase():
                        element.dashboard=item.value;
                    break;
                    case capitalize(type.STOCK_IN).toLowerCase():
                        element.stock=item.value;
                    break;
                    case capitalize(type.STOCK_OUT).toLowerCase():
                        element.sales=item.value;
                    break;
                    case capitalize(type.CLIENT).toLowerCase():
                        element.customer=item.value;
                    break;
                    case capitalize(type.PRODUCT).toLowerCase():
                        element.article=item.value;
                    break;
                }
            }
            right.map(event);
            this.props.dispatch(element)
            rep=true;
            return rep;
        } catch (error) {
        }
    }
    downloadUserImage=async(item,id)=>{
        try {
            if(item.photo!=null && item.photo!=undefined){
                const photo=getImage(item.photo);
                const imageName="user"+item.private_key+'.ligal';
                const load=await downloadImage(imageName.toLowerCase(),photo,null);
                if(load==true){
                    await AgentModel.update({
                        id:id,
                        photo:imageName.toLowerCase()
                    })
                }
            }
        } catch (error) {
            
        }
    }
    onAuthentification=async()=>{
        try {
            const code=this.state.authCode;
            const phone=this.state.authPhone;
            if(!this.isEmpty(phone)){
                const internet=await isConnected();
                if(internet==false){
                    this.setState({showError:true})
                    return;
                }
                this.setState({showProgress:true})
                const item={phone:code+phone}
                const req=await UserWebModel.authentification(JSON.stringify(item))
                const confirmation=await auth().signInWithPhoneNumber(`+${phone}`);
                if(req.status==200){
                    const item=req.response;
                    const agents=await AgentModel.get();
                    const exist=this.exists(agents,item.private_key);
                    if(exist==0){
                        const rep=await AgentModel.store({
                            first_name:item.first_name,
                            last_name:item.last_name,
                            phone:item.phone,
                            sexe:item.sex,
                            private_key:item.private_key,
                            code:item.code
                        })
                        if(rep==true){
                            let user=await AgentModel.last();
                            const repImage=await this.downloadUserImage(item,user.id);
                            user=await AgentModel.last();
                            if(user!=null){
                                const action={type:S.EVENT.onChangeUser,status:true,value:user}
                                await this.props.dispatch(action)
                                await UserSetStorage(JSON.stringify({user:user}))
                                const s=await ShopModel.get();
                               if(s==null){
                                this.setState({showProgress:false})
                                this.props.navigation.dispatch(
                                    StackActions.replace('Begin')
                                )
                             }else{
                                this.setState({showProgress:false})
                                this.props.navigation.dispatch(
                                StackActions.replace('Begin')
                               )
                             }
                            }
                            this.setState({showProgress:false})
                        }else{
                            Alert.alert("Traitement","L'authentification a échoué! Veuillez réessayer!");
                            this.setState({showProgress:false})
                        }
                    }else{
                        //when the user exist
                        let user=await AgentModel.show(exist);
                        const repImage=await this.downloadUserImage(item,user.id);
                        user=await AgentModel.show(exist);
                        if(user!=null){
                            const action={type:S.EVENT.onChangeUser,status:true,value:user}
                            await this.props.dispatch(action)
                            await UserSetStorage(JSON.stringify({user:user}))
                            const s=await ShopModel.get();
                            if(s==null){
                                //if the shop not exits
                                this.setState({showProgress:false})
                                this.props.navigation.dispatch(
                                    StackActions.replace('Begin')
                                )
                             }else{
                                 //open the shop
                                const actionShop={type:S.EVENT.onChangeShop,value:s};
                                //open right
                                await this.props.dispatch(actionShop);
                                await this.setRights(user.id).then(right=>{
                                    this.props.navigation.dispatch(
                                        StackActions.replace('Home')
                                    )
                                    this.setState({showProgress:false})    
                                })
                                this.setState({showProgress:false})
                                
                             }
                            }
                            this.setState({showProgress:false})
                        }
            }else{
                this.setState({showProgress:false})
                if(req.status>=400 && req.status<500){
                    if(req.response.error.phone!=null || req.response.error.phone!=undefined){
                        Alert.alert("Traitement","Ce numéro de téléphone n'est pas reconnu!")
                    }
                }else{
                    Alert.alert("Traitement","Erreur de réseau, peut-être! Essayez pour une nouvelle fois.")
                }
            }   
            }else{
                Alert.alert("Vérification","Remplissez correctement des informations");
            }
        } catch (error) {
        }
    }
    onSave=async()=>{
        try {
            const first_name=this.state.first_name;
            const last_name=this.state.last_name;
            const sex=this.state.sex;
            const code=this.state.code;
            const phone=this.state.phone;
            if(!this.isEmpty(first_name) && !this.isEmpty(last_name) && !this.isEmpty(sex) && !this.isEmpty(phone)){
                const internet=await isConnected();
                if(internet==false){
                    this.setState({showError:true})
                    return;
                }
                this.setState({showProgress:true})
                const item={first_name:first_name,last_name:last_name,phone:code+phone,sex:sex}
                const req=await UserWebModel.store(JSON.stringify(item))
                if(req.status==200){
                    const rep=await AgentModel.store({
                        first_name:first_name,
                        last_name:last_name,
                        phone:phone,
                        sexe:sex,
                        private_key:req.response.private_key,
                        code:item.code
                    })
                    if(rep==true){
                        const user=await AgentModel.last();
                        if(user!=null){
                            const action={type:S.EVENT.onChangeUser,status:true,value:user}
                            await this.props.dispatch(action)
                            await UserSetStorage(JSON.stringify({user:user}))
                            const s=await ShopModel.get();
                           if(s==null){
                            this.setState({showProgress:false})
                            this.props.navigation.dispatch(
                                StackActions.replace('Begin')
                            )
                         }else{
                            this.setState({showProgress:false})
                            this.props.navigation.dispatch(
                            StackActions.replace('Begin')
                           )
                         }
                        }
                    this.setState({showProgress:false})
                }else{
                    Alert.alert("Traitement","L'enregistrement a échoué");
                    this.setState({showProgress:false})
                }
            }else{
                this.setState({showProgress:false})
                if(req.status>=400 && req.status<500){
                    if(req.response.error.phone!=null || req.response.error.phone!=undefined){
                        Alert.alert("Traitement","Ce numéro de téléphone existe déjà! Allez vous connecter")
                    }
                }
            }   
            }else{
                Alert.alert("Vérification","Remplissez correctement des informations");
            }
        } catch (error) {
        }
    }


    componentDidMount(){
        try {
            removeCodeVerify();
        } catch (error) {
        }
    }

    render(){
        return(
            <>
            <StatusBar hidden />
            <View style={{flex:1}}>
                <ScrollView>
                    <View style={{alignItems:'center',margin:30,marginTop:50}}>
                        <Image
                        source={require('../assets/logo/ligal.png')}
                        />
                    </View>
                    <View>
                        <Text style={{fontFamily:"Poppins-Black",textAlign:'center',padding:20,fontSize:28}}>
                            Ligal Apply
                        </Text>
                        <Text style={{fontFamily:'Poppins-Light',padding:10,textAlign:'center'}}>
                            Gerez vos activités avec sécurité, fiabilité et précision grâce à la plateforme des commerces
                        </Text>
                    </View>
                    <View style={{marginTop:30}}>
                        <Button
                            title="Se connecter"
                            buttonStyle={[styles.btn,{backgroundColor:R.color.colorPrimary,alignSelf:"center",width:200,padding:10}]}
                            titleStyle={[styles.txt]}
                            onPress={()=>this.onConnexion()}
                        />
                        <Button
                            title="Créer un compte"
                            buttonStyle={[styles.btn,{alignSelf:"center",width:200}]}
                            titleStyle={[styles.txt,{color:R.color.colorPrimary}]}
                            type="outline"
                            onPress={()=>this.onRegister()}
                        />
                    </View>
                </ScrollView>
            </View>
            <Overlay 
                isVisible={this.state.showLogin} fullScreen overlayStyle={{padding:0}}
                animationType="slide" key="login"
            >
                <View style={{flex:1}}>
                <Appbar.Header
                    style={{backgroundColor:R.color.colorPrimary}}
                >
                    <Appbar.BackAction onPress={()=>this.setState({showLogin:false})} />
                    <Appbar.Content title="Connexion au compte" />
                </Appbar.Header>
                <View style={{flex:1,justifyContent:'center',padding:20}}>
                    <View style={{alignItems:'center',margin:30}}>
                        <Image
                        source={require('../assets/logo/ligal.png')}
                        />
                    </View>
                <View>
                <Text style={[styles.label,{padding:10,paddingBottom:0,paddingLeft:10}]}>
                        Téléphone
                    </Text>
                    <View style={{paddingLeft:10}}>
                            <Picker
                                style={{fontSize:14}}
                                selectedValue={this.state.authCode}
                                mode="dropdown"
                                onValueChange={(item,index)=>this.setState({authCode:item,flag:PAYS[index].flag})}
                            >
                                {
                                    PAYS.map((item,i)=>(
                                        <Picker.Item key={`country_${i}`} label={item.translations.fr} value={item.callingCodes[0]}/>
                                    ))
                                }
                            </Picker>
                        <View style={{flexDirection:'row',alignItems:"center"}}>
                            <Text style={{paddingBottom:28,fontSize:18}}>
                                +
                            </Text>
                            <View style={{width:50}}>
                                <Input
                                    value={this.state.authCode}
                                    placeholder="code du pays"
                                    keyboardType="phone-pad"
                                    editable={false}
                                    containerStyle={{paddingRight:0,marginRight:0}}
                                    
                                />
                            </View>
                            <View style={{flex:1}}>
                                <Input
                                    value={this.state.authPhone}
                                    placeholder="votre numéro de téléphone"
                                    onChangeText={value=>this.setState({authPhone:value})}
                                    maxLength={10}
                                    keyboardType="phone-pad"
                                    containerStyle={{marginLeft:0,paddingLeft:0}}
                                />
                            </View>
                        </View>
                        <View style={{alignItems:'center'}}>
                                <Button
                                    title="Se connecter"
                                    buttonStyle={[styles.btn]}
                                    titleStyle={[styles.txt,{fontSize:14}]}
                                    onPress={()=>this.onAuthentification()}
                                />
                        </View>
                    </View>
                </View>
                <View style={{alignItems:'center',padding:20}}>
                    <Avatar
                        source={{uri:this.state.flag}}
                    />
                </View>
                </View>
                </View>
            </Overlay>





            <Overlay 
                isVisible={this.state.showRegister} fullScreen overlayStyle={{padding:0}}
                animationType="slide"
                key="register"
            >
                <View style={{flex:1}}>
                <Appbar.Header
                    style={{backgroundColor:R.color.colorPrimary}}
                >
                    <Appbar.BackAction onPress={()=>this.setState({showRegister:false})} />
                    <Appbar.Content title="Création de compte" />
                </Appbar.Header>
                <ScrollView contentContainerStyle={{padding:10}}>
                    <View style={{alignItems:'center',margin:30}}>
                        <Image
                        source={require('../assets/logo/ligal.png')}
                        />
                    </View>
                    <Card containerStyle={[styles.card,{marginBottom:30}]}>
                    <ScrollView>
                    <Input
                        value={this.state.first_name}
                        label="Prénom"
                        placeholder="Votre prénom"
                        onChangeText={value=>this.setState({first_name:value})}
                        maxLength={20}
                        labelStyle={styles.label}
                        onSubmitEditing={()=>this.inputName.focus()}
                        returnKeyType='next'
                    />
                    <Input
                        value={this.state.last_name}
                        labelStyle={styles.label}
                        label="Nom"
                        placeholder="Votre nom"
                        onChangeText={value=>this.setState({last_name:value})}
                        maxLength={20}
                        ref={x=>this.inputName=x}
                    />
                    <Text style={[styles.label,{paddingLeft:10}]}>
                        Genre
                    </Text>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <RadioButton
                            checked={this.state.sex}
                            title="Femme"
                            value="f"
                            onPress={()=>this.setState({sex:"f"})}
                            color={R.color.colorPrimary}
                            direction="row"
                        />
                        <RadioButton
                            checked={this.state.sex}
                            title="Homme"
                            value="m"
                            onPress={()=>this.setState({sex:"m"})}
                            color={R.color.colorPrimary}
                            direction="row"
                        />
                    </View>
                    <Divider/>
                    <Text style={[styles.label,{padding:10,paddingBottom:0,paddingLeft:10}]}>
                        Téléphone
                    </Text>
                    <View style={{paddingLeft:10}}>
                            <Picker
                                style={{fontSize:14}}
                                selectedValue={this.state.code}
                                mode="dropdown"
                                onValueChange={(item,index)=>this.setState({code:item})}
                            >
                                {
                                    PAYS.map((item,i)=>(
                                        <Picker.Item key={`country_2_${i}`} label={item.translations.fr} value={item.callingCodes[0]}/>
                                    ))
                                }
                            </Picker>
                        <View style={{flexDirection:'row',alignItems:"center"}}>
                            <Text style={{paddingBottom:28,fontSize:18}}>
                                +
                            </Text>
                            <View style={{width:50}}>
                                <Input
                                    value={this.state.code}
                                    placeholder="code du pays"
                                    keyboardType="phone-pad"
                                    editable={false}
                                    containerStyle={{paddingRight:0,marginRight:0}}
                                    onSubmitEditing={()=>this.inputPhone.focus()}
                                    returnKeyType='next'
                                    
                                />
                            </View>
                            <View style={{flex:1}}>
                                <Input
                                    value={this.state.phone}
                                    placeholder="ex:123456789"
                                    onChangeText={value=>this.setState({phone:value})}
                                    maxLength={10}
                                    keyboardType="phone-pad"
                                    containerStyle={{marginLeft:0,paddingLeft:0}}
                                    ref={x=>this.inputPhone=x}
                                />
                            </View>
                        </View>
                    </View>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <View style={{flex:1}}>
                            <Button
                                title="Annuler"
                                buttonStyle={[styles.btn]}
                                titleStyle={[styles.txt,{color:R.color.colorPrimary,fontSize:14}]}
                                type="outline"
                                onPress={()=>this.setState({showRegister:false})}
                            />
                        </View>
                        <View style={{flex:1}}>
                            <Button
                                title="Enregistrer"
                                buttonStyle={[styles.btn]}
                                titleStyle={[styles.txt,{fontSize:14}]}
                                onPress={()=>this.onSave()}
                            />
                        </View>
                    </View>
                    </ScrollView>
                    </Card>
                </ScrollView>
                </View>
            </Overlay>
            <ProgressDialog
                message="Patientez svp!"
                title="Chargement"
                activityIndicatorColor={R.color.colorSecondary}
                activityIndicatorSize="large"
                visible={this.state.showProgress}
            />
            <CardInternet
                visible={this.state.showError}
                onClose={()=>this.setState({showError:false})}
            />
            </>
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(App)