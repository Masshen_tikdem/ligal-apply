import React, { Component } from 'react';
import { View } from 'react-native';
import { Avatar, Button, Header, Icon, Input, ListItem, Text } from 'react-native-elements';
import R from '../../resources/styles.json';
import S from '../../resources/settings.json';
import Empty from '../../components/EmptyMessage';
import Awaiter from '../../components/Waiter';
import { FlatList } from 'react-native';
import * as FamilleModel from '../../model/local/family';
import * as ArticleModel from '../../model/local/article';
import * as DeviseModel from '../../model/local/devise';
import * as CustomerModel from '../../model/local/client';
import { addGaps, addGapsWithSeparator, addGapsWithSeparatorMoney, capitalize, getArticleName, getDifference, getImageLocal, getPrice, getTotal, isDifference, isDifferenceStrict, isEmpty, limitLength, removeNoNumber, removeNoNumberDecimal, trim } from '../../Manager';
import CardSale from '../../components/CardSale';
import CardArticle from '../../components/CardSaleArticle';
import { Appbar, FAB, Searchbar } from 'react-native-paper';
import { ConfirmDialog, Dialog, ProgressDialog } from 'react-native-simple-dialogs';
import { Alert } from 'react-native';
import ModalSale from '../../components/ModalSale';
import * as BuyModel from '../../model/local/buy.js';
import DateTime from 'date-and-time';
import { connect } from 'react-redux';
import ModalClient from '../../components/ModalClient';
import { BackHandler } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { Animated } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import { timing, Value,Easing } from 'react-native-reanimated';
import { Dimensions } from 'react-native';
import { ScrollView } from 'react-native';
import InfoSale from '../../components/SaleInfo';
import MenuItem from '../../components/MenuItem';
import AwaiterFull from '../../components/AwaiterFull';
import * as Shared from '../../service/shared';
import { getCustomer } from './treatment/customer';

const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.article,
        client:state.client,
        sale:state.sale,
        shop:state.shop,
        setting:state.setting
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

const AnimatedFlatList=Animated.createAnimatedComponent(FlatList);


class App extends Component{

    constructor({props,route,navigation}){
        super(props)
        this.state={
            isLoading:true,showFullLoad:false,navigation,
            showSearch:"",search:"",isSearched:false,
            list:[],articles:[],familles:[],devises:[],
            filter:[],carts:[],step:1,cat:0,
            showStock:false,showCart:false,showProgress:false,
            showMenu:false,showAlter:false,showDevise:false,
            showRemise:false,showTotal:false,currentArticle:null,
            currentCart:null,update:false,showDescription:false,
            title:"",stock:"0",reference:"",
            count:0,total:"0",price:"0",
            devise:"",id_devise:0,id_client:0,
            cible:0,isOpen:false,showClient:false,
            customers:[],filterCustomer:[],customerName:"",
            customerPhone:"",customer:"",idCustomer:0,remise:"0",
            remiseValue:0,showStockService:false,showTotal:false,stockDevise:0,stockDeviseSign:"",description:"",
            showResponse:false,facture:"",journals:[],
        }
    }

    async componentDidMount(){
        try {
            BackHandler.addEventListener("hardwareBackPress",()=>{
                if(this.state.step>0){
                    this.onReturn();
                    return true;
                }else{
                    return false;
                }
            })
            const fam=await FamilleModel.get();
            const rep=await ArticleModel.get();
            const dev=await DeviseModel.get();
            const cl=await CustomerModel.get();
            if(dev.length>0){
                this.setState({stockDevise:dev[0].id,stockDeviseSign:dev[0].signe})
            }
            this.setState({
                familles:fam,
                devises:dev,
                articles:rep,
                customers:cl,
                filterCustomer:cl
            })
            let k=1;
            const list=[];
            const items=[];
            this.state.familles.map(item=>{
                item.uid=k;
                items.push(item)
                k++;
            })
            this.state.articles.map(item=>{
                item.uid=k;
                items.push(item)
                k++;
            })
            items.map(item=>{
                const ck=this.clickable(item.id,items);
                item.count=ck.count;
                item.click=ck.rep;
                list.push(item);
            })
            list.sort((a,b)=>{
                const x=capitalize(a.name).toLowerCase();
                const y=capitalize(b.name).toLowerCase();
                if(x<y){
                    return -1
                }else if(x>y){
                    return 1;
                }else{
                    return 0;
                }
            })
            const filter=[];
            list.map(p=>{
                if(p.category===null){
                    filter.push(p)
                }
            })
            this.setState({
                list,
                filter:filter
            })

            if(this.state.isLoading==true){
                this.setState({
                    isLoading:false
                })
            }
            this.getJournals();
        } catch (error) {
            
        }
    }
    onReload=async()=>{
        try {
            const rep=await ArticleModel.get();
            this.setState({
                articles:rep
            })
            let k=1;
            const list=[];
            const items=[];
            this.state.familles.map(item=>{
                item.uid=k;
                items.push(item)
                k++;
            })
            this.state.articles.map(item=>{
                item.uid=k;
                items.push(item)
                k++;
            })
            items.map(item=>{
                const ck=this.clickable(item.id,items);
                item.count=ck.count;
                item.click=ck.rep;
                list.push(item);
            })
            list.sort((a,b)=>{
                const x=capitalize(a.name).toLowerCase();
                const y=capitalize(b.name).toLowerCase();
                if(x<y){
                    return -1
                }else if(x>y){
                    return 1;
                }else{
                    return 0;
                }
            })
            const filter=[];
            list.map(p=>{
                if(p.category===null){
                    filter.push(p)
                }
            })
            this.setState({
                list,
                filter:filter,
                step:1
            })

            if(this.state.isLoading==true){
                this.setState({
                    isLoading:false
                })
            }
            this.getJournals();
        } catch (error) {
            console.log('ERROR',error);
        }
    }

    getStock=(stockValue,buyValue,mode,type)=>{
        let rep={count:0,text:"0",devise:null};
        try {
            if(type!="service" || (type=="service" && mode=="unity") ){
                const buy=buyValue>0?buyValue:0;
                const stock=stockValue>0?stockValue:0;
                let count=0;
                if(buy>=0 && stock>=0){
                    count=stock-buy;
                    rep={count:count,text:""+count};
                }
            }else if(type=="service"){
                if(mode=="devise"){
                    if(stockValue.length>0){
                        const value=getDifference(stockValue,buyValue);
                        let text="";
                        let size=0;
                        for (let index = 0; index < value.length; index++) {
                            text+= getPrice(value[index].quantity)+" "+capitalize(value[index].signe).toUpperCase()+" +";
                            if(value[index].quantity>0){
                                size++;
                            }
                        }
                        text=text.substring(0,text.length-1);
                        rep={count:size,text:text,devise:value};
                    }else{
                        rep={count:0,text:"0"};
                    }
                }
            }
        } catch (error) {
            console.log('error stock count',error)
        }
        return rep;
    }

    serviceIsClickable=(item,count)=>{
        let rep=false;
        try {
            if(item.mode!="devise" && item.mode!="unity"){
                rep=true;
            }else{
                if(item.quantifiable==1){
                    rep=true;
                }else{
                    if(count>0){
                        rep=true;
                    }
                }
            }
        } catch (error) {
            
        }
        return rep;
    }
    renderItem=({item})=>{
        let click={count:item.count,rep:item.click}
        let cat=false;
        if(item.buy===undefined && item.stock===undefined && item.description===undefined){
            cat=true;
            return(
                <CardSale
                    logo={getImageLocal(item.logo)}
                    title={capitalize(item.name).toUpperCase()}
                    onPress={click.rep==true?()=>this.onClick(item):null}
                    opacity={click.rep==false?0.6:1}
                    subTitle={(click.count==0 && cat==true)?"Aucun article":(click.count>0 && cat==true)?click.count+" article"+(click.count>1?"s":""):""}
                />
            )
        }else{
            const count=this.getStock(item.stock,item.buy,item.mode,item.type);
            const taked=this.inCart(item);
            return(
                <CardArticle
                    title={getArticleName(item.name,item.description)}
                    onPress={this.articleExecutable(item,taked,count)?item.type==S.ENUM.article.service?()=>this.onCartService(item):()=>this.onCart(item):null}
                    opacity={this.articleExecutable(item,taked,count)?1:0.7}
                    count={count.text}
                    size={(item.type==S.ENUM.article.service && item.mode==S.ENUM.stockage.devise)?count.count:0}
                    showPrice={(item.type!="service" || (item.type=="service" && item.mode!="devise"))}
                    price={getPrice(item.price,item.devise)}
                    taked={taked}
                    logo={getImageLocal(item.logo)}
                    service={(item.type==S.ENUM.article.service)?(item.mode==S.ENUM.stockage.devise)?false:true:false}
                />
            )
        }
    }

    articleExecutable=(item,taked=true,stock={})=>{
        let rep=false;
        try {
            if(item.type!=S.ENUM.article.service){
                if(taked==false && stock.count>0){
                    rep=true;
                }
            }else{
                const serviceClickable=this.serviceIsClickable(item,stock.count);
                if(serviceClickable && !taked){
                    rep=true
                }
            }
        } catch (error) {
            
        }
        return rep;
    }
    onClick=(item)=>{
        const step=this.state.step;
        try {
            const filter=[];
            const id=item.id;
            if(step==1){
                this.state.list.map(item=>{
                    if(item.id_famille===id || item.category_id===id){
                        filter.push(item)
                    }
                })
                if(filter.length>0){
                    this.setState({filter,step:2,cible:id})
                }
            }
            else if(step==2){
                let id_cat=0;
                this.state.list.map(item=>{
                    if(item.id_famille===id || item.category_id===id){
                        filter.push(item)
                    }
                })
                if(filter.length>0){
                    this.setState({filter,step:3})
                }
            }
            this.setState({
                isLoading:false
            })
        } catch (error) {
        }
    }
    onReturn=()=>{
        try {
            const step=this.state.step;
            if(step==3){
                const id=this.state.cible;
                const filter=[];
                this.state.list.map(item=>{
                    if(item.id_famille===id || item.category_id===id){
                        filter.push(item)
                    }
                })
                if(filter.length>0){
                    this.setState({filter,step:2})
                }
            }else if(step==2){
                const filter=[];
                this.state.list.map(p=>{
                    if(p.category===null){
                        filter.push(p)
                    }
                })
                this.setState({
                    filter,
                    step:1
                })
            }
        } catch (error) {
            console.log('error',error);
        }
    }
    clickable=(id,list=[])=>{
        let rep=false;
        let count=0;
        try {
            list.map(item=>{
                if(item.id_famille===id || item.category_id===id){
                    count++;
                }
            })
            if(count>0){
                rep=true;
            }
        } catch (error) {
            
        }
        return {rep,count};
    }

    onCart=(item)=>{
        try {
            const name=item.name;
            this.setState({
                showStock:true,
                currentArticle:item,
                title:capitalize(name).toUpperCase(),
                stock:"",
            })
        } catch (error) {
            
        }
    }
    onCartService=(item)=>{
        try {
            const name=item.name;
            if(item.mode=="devise"){
                this.setState({
                    showStockService:true,
                    currentArticle:item,
                    title:capitalize(name).toUpperCase(),
                    stock:"",
                    reference:""
                })
            }else{
                this.setState({
                    showStockService:true,
                    currentArticle:item,
                    title:capitalize(name).toUpperCase(),
                    stock:"1",
                    reference:""
                })
            }
        } catch (error) {
            console.log('info error ',error)
        }
    }
    inCart=(item)=>{
        let rep=false;
        try {
            rep=this.state.carts.findIndex(p=>p.article==item.id)!=-1?true:false;
        } catch (error) {
            
        }
        return rep;
    }

    trim(value){
        let rep="";
        capitalize(value).split(" ").map((item)=>{
            rep+=item;
        })
        return rep;
    }
    countValidate=()=>{
        let rep=false;
        try {
            const article=this.state.currentArticle
            if(article!=null && article!=undefined){
                const stockage=this.getStock(article.stock,article.buy,article.mode,article.type);
                if(article.type!=S.ENUM.article.service || (article.type==S.ENUM.article.service && article.mode==S.ENUM.stockage.unity)){
                    const quantity=parseFloat(this.trim(this.state.stock));
                    const dispo=stockage.count;
                    if(dispo>=quantity){
                        rep=true;
                    }
                }else if(article.type==S.ENUM.article.service && article.mode==S.ENUM.stockage.devise){
                    const id_devise=this.state.stockDevise;
                    const price=parseFloat(this.trim(this.state.stock));
                    const values=stockage.devise;
                    if(values!=null && values!=undefined){
                        for (let index = 0; index < values.length; index++) {
                            if(values[index].devise==id_devise && values[index].quantity>=price){
                                rep=true;
                                
                            }
                        }
                    }
                }else{
                    rep=true;
                }
            }else{
            }
        } catch (error) {
            console.log('value error',error)
        }
        return rep;
    }

    remiseValidate=()=>{
        let rep=false;
        try {
            if(this.state.remise!=null){
                const remise=parseFloat(this.state.remise);
                if(remise>0 && remise<100){
                    rep=true;
                }
            }
        } catch (error) {
        }
        return rep;
    }
    priceValidate=()=>{
        let rep=false;
        try {
            const price=parseFloat(this.state.price)
            if(price>0){
                rep=true
            }
        } catch (error) {
        }
        return rep;
    }
    deleteLink=()=>{
        this.setState({
            currentArticle:null,
            currentCart:null
        })
    }
    onAddCart=()=>{
        try {
            const taked=this.inCart(this.state.currentArticle);
            const article=this.state.currentArticle;
            const item=new Object();
            const cart=this.state.currentCart;
            const update=this.state.update;
            if(item.price<=0){
                return;
            }
            if(update==false){
                item.quantity=parseFloat(this.state.stock);
                item.article=article.id;
                item.price=article.price;
                item.devise=article.devise;
                item.id_devise=article.id_devise;
                item.name=article.name;
                item.is_alter=false;
                item.logo=article.logo;
                item.remise=this.state.remiseValue;
                item.type=article.type;
            }else{
                item.quantity=parseFloat(this.state.stock);
                item.article=cart.article;
                item.price=cart.price;
                item.devise=cart.devise;
                item.id_devise=cart.id_devise;
                item.name=cart.name;
                item.is_alter=cart.is_alter;
                item.logo=cart.logo;
                item.remise=cart.remise;
                item.reference=cart.reference;
                item.type=cart.type;
            }
            if(item.quantity>0 && this.countValidate()){
               if(update==false){
                    if(taked==true){
                        return;
                    }
                    const list=this.state.carts;
                    list.push(item);
                    const total=getTotal(list,this.state.devises);
                    this.setState({
                        showStock:false,
                        carts:list,
                        count:list.length,
                        total
                    })
               }else{
                   const items=this.state.carts;
                   const list=[];
                   items.map(p=>{
                       if(p.article==cart.article){
                           list.push(item)
                       }else{
                           list.push(p);
                       }
                   })
                   const total=getTotal(list,this.state.devises);
                    this.setState({
                        showStock:false,
                        carts:list,
                        count:list.length,
                        total,
                        update:false,
                    })
               }
            }else{
                Alert.alert("Vérification","La quantité ne peut pas être nulle");
            }
        } catch (error) {
        }
    }

    componentServiceStockable=()=>{
        try {
            const item=this.state.currentArticle;
            return(
                (item.mode==S.ENUM.stockage.devise)?(
                    <this.componentInputPrice/>
                ):null
            )
        } catch (error) {
            console.log('error',error)
        }
    }

    componentServiceNoStockable=()=>{
        const item=this.state.currentArticle;
        /*return(
            (item.quantifiable==1)?(
                <this.componentInputQuantity/>
            ):null
        )*/
        return null;
    }

    onAddCartService=()=>{
        try {
            const taked=this.inCart(this.state.currentArticle);
            const article=this.state.currentArticle;
            const item=new Object();
            const cart=this.state.currentCart;
            const update=this.state.update;
            if(item.price<=0){
                return;
            }
            if(update==false){
                if(article.mode=="devise"){
                    const id_devise=this.state.stockDevise;
                    const price=parseFloat(trim(this.state.stock))>0?parseFloat(trim(this.state.stock)):0
                    const devise=this.state.stockDeviseSign;
                    item.quantity=1;
                    item.article=article.id;
                    item.price=price;
                    item.devise=devise;
                    item.id_devise=id_devise;
                    item.name=article.name;
                    item.is_alter=false;
                    item.logo=article.logo;
                    item.remise=0;
                    item.reference=this.state.reference;
                    item.type=article.type;
                    item.mode=article.mode;
                }else{
                    if(isEmpty(this.state.stock)){
                        item.quantity=1;    
                    }else{
                        item.quantity=parseInt(this.state.stock)>0?parseInt(this.state.stock):1;
                    }
                    item.article=article.id;
                    item.price=article.price;
                    item.devise=article.devise;//quoi faire?
                    item.id_devise=article.id_devise;
                    item.name=article.name;
                    item.is_alter=false;
                    item.logo=article.logo;
                    item.remise=this.state.remiseValue;
                    item.reference=this.state.reference;
                    item.type=article.type;
                    item.mode=article.mode;
                }
                
            }else{
                if(cart.mode=="devise"){
                    const id_devise=this.state.stockDevise;
                    const devise=this.state.stockDeviseSign;
                    const price=parseFloat(this.state.stock)>0?parseFloat(this.state.stock):0
                    item.quantity=1;
                    item.article=cart.article;
                    item.price=price;
                    item.devise=devise;
                    item.id_devise=id_devise;
                    item.name=cart.name;
                    item.is_alter=cart.is_alter;
                    item.logo=cart.logo;
                    item.remise=cart.remise;
                    item.reference=this.state.reference;
                    item.type=cart.type;
                    item.mode=cart.mode;
                }
                else{
                    if(isEmpty(this.state.stock)){
                        item.quantity=1;    
                    }else{
                        item.quantity=parseInt(this.state.stock)>0?parseInt(this.state.stock):1;
                    }
                    item.article=cart.article;
                    item.price=cart.price;
                    item.devise=cart.devise;
                    item.id_devise=cart.id_devise;
                    item.name=cart.name;
                    item.is_alter=cart.is_alter;
                    item.logo=cart.logo;
                    item.remise=cart.remise;
                    item.reference=this.state.reference;
                    item.type=cart.type;
                    item.mode=cart.mode;
                }
                
            }
            if(item.quantity>0 && (this.countValidate() || this.state.currentArticle.type=="service")){
               if(update==false){
                    if(taked==true){
                        return;
                    }
                    const list=this.state.carts;
                    list.push(item);
                    const total=getTotal(list,this.state.devises);
                    this.setState({
                        showStockService:false,
                        carts:list,
                        count:list.length,
                        total
                    })
               }else{
                   const items=this.state.carts;
                   const list=[];
                   items.map(p=>{
                       if(p.article==cart.article){
                           list.push(item)
                       }else{
                           list.push(p);
                       }
                   })
                   const total=getTotal(list,this.state.devises);
                    this.setState({
                        showStockService:false,
                        carts:list,
                        count:list.length,
                        total,
                        update:false,
                    })
               }
            }else{
                Alert.alert("Vérification","La quantité ne peut pas être nulle");
            }
        } catch (error) {
            console.log('error',error);
        }
    }

    onAddRemise=()=>{
        try {
            const remise=parseFloat(this.state.remise)
            if(remise>0 && this.remiseValidate()){
                const cart=this.state.currentCart;
                const list=[];
                this.state.carts.map(item=>{
                    if(item.price>0){
                        if(item.type=="service" && item.mode=="devise"){
                            item.remise=0;
                        }else{
                            item.remise=remise;
                        }
                    }
                    list.push(item)
                })
                const total=getTotal(list,this.state.devises);
                this.setState({
                    total,
                    count:list.length,
                    showAlter:false,
                    carts:list,
                    remiseValue:remise,
                    showRemise:false,
                    isOpen:false
                })
            }else{
                Alert.alert("Vérification","La quantité ne peut pas être nulle");
            }
        } catch (error) {
        }
    }

    renderSale=({item})=>{
        let price=0;
        try {
            price=parseFloat(item.quantity)*parseFloat(item.price);
        } catch (error) {
            console.log('error is',error)
        }
        return(
            <CardArticle
                count={(item.type=="service" && item.mode=="devise")?getPrice(price,item.devise,item.remise):item.quantity}
                title={capitalize(item.name)}
                price={getPrice(price,item.devise,item.remise)}
                onPress={()=>this.onShowMenu(item)}
                logo={getImageLocal(item.logo)}
                size={item.quantity}
                remise={item.remise}
                showPrice={(item.type=="service" && item.mode=="devise")?false:true}
                subTitle={(item.description!=null && item.description!=undefined)?item.description:""}
            />
        )
    }

    onShowMenu=(item)=>{
        try {
            const id=item.article;
            const index=this.state.articles.findIndex(p=>p.id==id);
            let article=item;
            if(index>=0){
                article=this.state.articles[index];
            }
            this.setState({
                currentCart:item,
                currentArticle:article,
                showMenu:true,
            })
        } catch (error) {
            console.log('error',error);
        }
    }
    onUpdate=()=>{
        try {
            const current=this.state.currentCart;
            if(current!=null){
                if(current.type!=S.ENUM.article.service){
                    this.setState({
                        showMenu:false,
                        stock:current.quantity+"",
                        showStock:true,
                        update:true,
                        title:capitalize(current.name)
                    })
                }else{
                    if(current.mode==S.ENUM.stockage.devise){
                        this.setState({
                            showMenu:false,
                            stock:current.price+"",
                            stockDevise:current.id_devise,
                            showStockService:true,
                            update:true,
                            reference:current.reference,
                            title:capitalize(current.name)
                        })
                    }else{
                        this.setState({
                            showMenu:false,
                            stock:current.quantity+"",
                            showStockService:true,
                            update:true,
                            reference:current.reference,
                            title:capitalize(current.name)
                        })
                    }
                }
            }
        } catch (error) {
            
        }
    }
    onDelete=()=>{
        try {
            const cart=this.state.currentCart;
            const list=[];
            this.state.carts.map(item=>{
                if(item.article!=cart.article){
                    list.push(item)
                }
            })
            if(list.length==0){
                this.setState({
                    total:"0",
                    count:0,
                    showMenu:false,
                    showCart:false,
                    carts:list,
                })
            }else{
                const total=getTotal(list,this.state.devises);
                this.setState({
                    total,
                    count:list.length,
                    showMenu:false,
                    carts:list
                })
            }
        } catch (error) {
            
        }
    }
    onAlter=()=>{
        try {
            const devise=this.state.devise;
            const id_devise=this.state.id_devise;
            const price=this.state.price;
            if(this.priceValidate()==true && parseFloat(price)>0){
                const cart=this.state.currentCart;
                const list=[];
                this.state.carts.map(item=>{
                    if(item.article!=cart.article){
                        list.push(item)
                    }else{
                        item.devise=devise;
                        item.id_devise=id_devise;
                        item.price=parseFloat(price);
                        item.is_alter=true;
                        item.remise=0;
                        list.push(item)
                    }
                })
                const total=getTotal(list,this.state.devises);
                this.setState({
                    total,
                    count:list.length,
                    showAlter:false,
                    carts:list
                })
            }
        } catch (error) {
            
        }
    }

    onSetDescription=()=>{
        try {
            const description=this.state.description;
            const cart=this.state.currentCart;
            const list=[];
            this.state.carts.map(item=>{
                if(item.article!=cart.article){
                    list.push(item)
                }else{
                    item.description=description;
                    list.push(item)
                }
            })
            this.setState({
                showDescription:false,
                carts:list
            })
        } catch (error) {
            
        }
    }

    onInitialPrice=()=>{
        try {
            let devise="";
            let id_devise=0;
            let price=0;
            const cart=this.state.currentCart;
            const id=cart.article;
            this.state.articles.map(item=>{
                if(item.id==id){
                    devise=item.devise;
                    id_devise=item.id_devise;
                    price=item.price;
                }
            })
            const list=[];
            this.state.carts.map(item=>{
                if(item.article!=cart.article){
                    list.push(item)
                }else{
                    item.devise=devise;
                    item.id_devise=id_devise;
                    item.price=parseFloat(price);
                    item.is_alter=false;
                    list.push(item)
                }
            })
            const total=getTotal(list,this.state.devises);
            this.setState({
                total,
                count:list.length,
                showAlter:false,
                carts:list,
                showMenu:false
            })
        } catch (error) {
            
        }
    }
    onShowAlter=()=>{
        try {
            const current=this.state.currentCart;
            if(current!=null){
                this.setState({
                    showMenu:false,
                    price:current.price+"",
                    showAlter:true,
                    title:capitalize(current.name),
                    devise:current.devise,
                    id_devise:current.id_devise
                })
            }
        } catch (error) {
            
        }
    }
    onShowDescription=()=>{
        try {
            const current=this.state.currentCart;
            if(current!=null){
                this.setState({
                    showMenu:false,
                    description:(current.description!=null && current.description!=undefined)?current.description:"",
                    showDescription:true,
                    title:capitalize(current.name),
                })
            }
        } catch (error) {
            
        }
    }
    renderDevise=({item})=>{
        return(
            <ListItem 
                containerStyle={{backgroundColor:R.color.background}} 
                onPress={()=>this.onChoiceDevise(item)}
                key='devise-item'
            >
                <Avatar
                    title="P"
                />
                <Text style={{fontSize:18,fontWeight:'bold',padding:5}} numberOfLines={1}>
                    {capitalize(item.signe).toUpperCase()}
                </Text>
            </ListItem>
        )
    }
    onChoiceDevise=(item)=>{
        try {
            this.setState({
                devise:item.signe,
                id_devise:item.id,
                showDevise:false
            })
        } catch (error) {
            
        }
    }
    onCreateSale=async()=>{
        try {
            const facture=this.getBill();
            const shop=this.props.shop.value;
            this.setState({facture});
            let idUser=0;
            const user=this.props.user.value;
            if(user.id>0){
                idUser=user.id
            }
            const list=this.state.carts;
            const k=list.length;
            let i=0;
            let j=0;
            this.setState({showProgress:true})
            const name=this.state.customerName;
            let phone=this.state.customerPhone;
            let idCustomer=this.state.idCustomer;
            idCustomer=await getCustomer(name,phone,this.state.customers,isEmpty(shop.country)?"":shop.country,shop,user,idCustomer);
            const handleCreateSale=async()=>{
                let response=false;
                await Promise.all(list.map(async item=>{
                    const rep=await BuyModel.store({
                        created_at:DateTime.format(new Date(),"YYYY-MM-DD HH:mm:ss"),
                        id_agent:idUser,id_article:item.article,
                        id_client:idCustomer,id_devise:item.id_devise,
                        is_alter:item.is_alter,price:item.price,
                        quantity:item.quantity,remise:item.remise,
                        status:1,reference:isEmpty(item.reference)?null:item.reference,
                        description:item.description,facture:facture!=undefined?facture:"",
                    })
                    i++;
                    if(rep!=null){
                        Shared.buy(rep);
                        response=true;
                        j++;
                    }
                })
                );
                return response;
            }
            await handleCreateSale();
            this.setState({
                carts:[],total:"0",count:0,showCart:false,
                remiseValue:0,remise:"0",
                customerName:"",customerPhone:"",
                idCustomer:0,customer:"",
            })
            if(j>0){
                this.onReload();
                this.setState({showProgress:false,showResponse:true})
            }else{
                this.setState({showProgress:false})
                Alert.alert("Vente","La vente a échoué");
            }
            
        } catch (error) {
            console.log('error sale',error);
        }
    }
    onRefresh=async()=>{
        try {
            const fam=await FamilleModel.get();
            const rep=await ArticleModel.get();
            const dev=await DeviseModel.get();
            this.setState({
                familles:fam,
                devises:dev,
                articles:rep
            })
            let k=1;
            const list=[];
            const items=[];
            this.state.familles.map(item=>{
                item.uid=k;
                items.push(item)
                k++;
            })
            this.state.articles.map(item=>{
                item.uid=k;
                items.push(item)
                k++;
            })
            items.map(item=>{
                const ck=this.clickable(item.id,items);
                item.count=ck.count;
                item.click=ck.rep;
                list.push(item);
            })
            list.sort((a,b)=>{
                const x=capitalize(a.name).toLowerCase();
                const y=capitalize(b.name).toLowerCase();
                if(x<y){
                    return -1
                }else if(x>y){
                    return 1;
                }else{
                    return 0;
                }
            })
            const filter=[];
            list.map(p=>{
                if(p.category===null){
                    filter.push(p)
                }
            })
            this.setState({
                list,
                filter:filter,
                step:1,
                carts:[],
                total:"0",
                count:0,
                customer:"",
                customerName:"",
                customerPhone:""
            })

            if(this.state.isLoading==true){
                this.setState({
                    isLoading:false
                })
            }
        } catch (error) {
            
        }
    }

    onRefreshSale=async()=>{
        try {
            const fam=await FamilleModel.get();
            const rep=await ArticleModel.get();
            const dev=await DeviseModel.get();
            this.setState({
                familles:fam,
                devises:dev,
                articles:rep
            })
            let k=1;
            const list=[];
            const items=[];
            this.state.familles.map(item=>{
                item.uid=k;
                items.push(item)
                k++;
            })
            this.state.articles.map(item=>{
                item.uid=k;
                items.push(item)
                k++;
            })
            items.map(item=>{
                const ck=this.clickable(item.id,items);
                item.count=ck.count;
                item.click=ck.rep;
                list.push(item);
            })
            list.sort((a,b)=>{
                const x=capitalize(a.name).toLowerCase();
                const y=capitalize(b.name).toLowerCase();
                if(x<y){
                    return -1
                }else if(x>y){
                    return 1;
                }else{
                    return 0;
                }
            })
            /*const filter=[];
            list.map(p=>{
                if(p.category===null){
                    filter.push(p)
                }
            })*/
            this.setState({
                list,
            })
        } catch (error) {
            
        }
    }
    async componentDidUpdate(){
        try {
          if(this.props.article.sale==true){
              const action={type:S.EVENT.onChangeMarketArticle,sale:false}
              await this.props.dispatch(action);
              this.onRefresh()
          }
          if(this.props.sale.article==true || this.props.sale.category==true || this.props.sale.devise==true){
            const actionSale={type:S.EVENT.onChangeSaleInfo,article:false,category:false,devise:false}
            await this.props.dispatch(actionSale);  
            if(this.state.carts.length==0){
                await this.onRefresh();
            }else{
                await this.onRefreshSale();
            }
          } 
        } catch (error) {
            
        }
    }

    renderCustomer=({item})=>{
        return(
            <TouchableOpacity style={{ flex:1 }} activeOpacity={0.8} onPress={()=>this.onChoiceClient(item)}>
            <View style={{alignItems:'center',padding:10,flex:1}}>
                <Avatar
                    icon={{name:"user",type:"entypo"}}
                    rounded
                    size="medium"
                    containerStyle={{backgroundColor:R.color.colorSecondary}}
                />
                <Text style={{fontFamily:'Poppins-Light',fontSize:14,padding:5}}>
                    {capitalize(item.name).toUpperCase()}
                </Text>
                {/*<Text style={{fontSize:16,fontFamily:'Poppins-bold',padding:5}}>
                    {item.phone!=""?item.phone:"xxxxxx"}
                </Text>*/}
            </View>
            </TouchableOpacity>
        )
    }

    onAddClient=()=>{
        const name=this.state.customerName;
        const phone=this.state.customerPhone;
        if(isEmpty(name) && isEmpty(phone)){
            Alert.alert("Vérification","Impossible de signaler un client sans nom ni numéro de téléphone")
        }else{
            if(!isEmpty(name)){
                this.setState({
                    customer:name,
                    showClient:false
                })
            }
        }
    }
    onChoiceClient=(item)=>{
        this.setState({
            customerName:item.name,
            customerPhone:item.phone,
            idCustomer:item.id
        })
    }
    onChangeCustomerName=(value)=>{
        this.setState({
            customerName:value
        })
        if(value!=null && value!=undefined){
            if(value.trim()==""){
                this.setState({filterCustomer:this.state.customers})
            }else{
                const list=[];
                this.state.customers.map((item,index)=>{
                    if(item.name.toLowerCase().includes(value.toLowerCase())){
                        list.push(item);
                    }
                })
                this.setState({filterCustomer:list})
            }
        }
        else{
            this.setState({filterCustomer:this.state.customers})
        }

    }

    onChangeCustomerPhone=(value="")=>{
        if(!isEmpty(value)){
            this.setState({customerPhone:value})
            if(value.trim()=="" && isEmpty(this.state.customerName)){
                this.setState({filterCustomer:this.state.customers})
            }else{
                const list=[];
                this.state.customers.map((item,index)=>{
                    if(capitalize(item.phone).toLowerCase().includes(value.toLowerCase())){
                        list.push(item);
                    }
                })
                this.setState({filterCustomer:list})
            }
        }
        else{
            if(isEmpty(this.state.customerName)){
                this.setState({filterCustomer:this.state.customers})
            }
        }
    }

    openDrawer=()=>{
        try {
            this.state.navigation.openDrawer()
        } catch (error) {
            
        }
    }
    width=Dimensions.get('window').width;
    height=Dimensions.get('window').height;
    screen_input_box_translate_x=new Value(this.width);
    screen_back_button_opacity=new Value(0);
    screen_content_translate_y=new Value(this.height);
    screen_content_opacity=new Value(0);
    onShowSearch=()=>{
        try {
            const check=this.state.showSearch;
            if(check==true){
                this.setState({showSearch:!this.state.showSearch})
                const input_box_translate_x_config={
                    duration:200,
                    toValue:0,
                    easing:Easing.inOut(Easing.ease)
                }
                const back_button_opacity_config={
                    duration:200,
                    toValue:1,
                    easing:Easing.inOut(Easing.ease)
                }
                const content_translate_y_config={
                    duration:0,
                    toValue:0,
                    easing:Easing.inOut(Easing.ease)
                }
                const content_opacity_config={
                    duration:200,
                    toValue:1,
                    easing:Easing.inOut(Easing.ease)
                }
                timing(this.screen_input_box_translate_x,input_box_translate_x_config).start();
                timing(this.screen_back_button_opacity,back_button_opacity_config).start();
                timing(this.screen_content_opacity,content_opacity_config).start();
                timing(this.screen_content_translate_y,content_translate_y_config).start();
            }else{
                this.setState({showSearch:!this.state.showSearch})
                const input_box_translate_x_config={
                    duration:200,
                    toValue:0,
                    easing:Easing.inOut(Easing.ease)
                }
                const back_button_opacity_config={
                    duration:200,
                    toValue:1,
                    easing:Easing.inOut(Easing.ease)
                }
                const content_translate_y_config={
                    duration:0,
                    toValue:0,
                    easing:Easing.inOut(Easing.ease)
                }
                const content_opacity_config={
                    duration:200,
                    toValue:1,
                    easing:Easing.inOut(Easing.ease)
                }
                timing(screen_input_box_translate_x,input_box_translate_x_config).start();
                timing(screen_back_button_opacity,back_button_opacity_config).start();
                timing(screen_content_opacity,content_opacity_config).start();
                timing(screen_content_translate_y,content_translate_y_config).start();
            }
        } catch (error) {
            
        }
    }
    onChangeText=(value,type)=>{
        let text=removeNoNumberDecimal(value);
        //text=addGapsWithSeparator(text,[3,6,9,12,15]," ",".");
        text=addGapsWithSeparatorMoney(text,[3,6,9,12,15]," ",".");
        this.setState({stock:text});
    }

    onSearch=()=>{
        try {
            const value=this.state.search;
            const step=this.state.step;
            if(!isEmpty(value)){
                const filter=[];
                this.setState({showFullLoad:true})
                this.state.list.map(item=>{
                    const name=item.name;
                    const description=item.description;
                    if(item.category===null){

                    }else{
                        if(capitalize(description).toLowerCase().includes(capitalize(value).toLowerCase()) || name.toLowerCase().includes(value.toLowerCase())){
                            filter.push(item)
                        }
                    }
                })
                this.setState({showFullLoad:false})
                if(filter.length>0){
                    this.setState({filter,step:2,cible:0})
                }else{
                    Alert.alert('Recherche',"Aucun élément trouvé après la recherche")
                }
            }
        } catch (error) {
            this.setState({showFullLoad:false})
            console.log('error',error)
        }
    }

    componentInputQuantity=()=>(
        <Input
            label="Quantité à vendre"
            placeholder="Quantité ici"
            leftIcon={{name:'shopping-basket',type:'font-awesome',color:R.color.colorSecondary}}
            value={this.state.stock}
            maxLength={5}
            onChangeText={value=>this.onChangeText(value)}
            keyboardType='number-pad'
            errorStyle={{fontSize:12}}
            ref={input=>this.qte=input}
            onSubmitEditing={()=>this.reference.focus()}
            returnKeyType='next'
        />
    )

    componentInputPrice=()=>(
        <View style={{ flexDirection:'row',alignItems:'center' }}>
            <View style={{ flex:1,padding:5 }}>
                <Input
                    label="Montant à vendre"
                    placeholder="Montant ici"
                    leftIcon={{name:'tag',type:'font-awesome',color:R.color.colorSecondary}}
                    value={this.state.stock}
                    maxLength={20}
                    onChangeText={value=>this.onChangeText(value)}
                    keyboardType='number-pad'
                    inputStyle={{ textAlign:'right' }}
                    errorStyle={{fontSize:12}}
                    ref={input=>this.qte=input}
                    onSubmitEditing={()=>this.reference.focus()}
                    returnKeyType='next'
                    errorMessage={this.countValidate()==false?"Ce montant n'est pas valide":""}
                />
            </View>
            <View style={{ width:100 }}>
                <Picker
                    mode="dropdown"
                    selectedValue={this.state.stockDevise}
                    onValueChange={(value,index)=>this.setState({stockDevise:value,stockDeviseSign:this.state.devises[index].signe})}

                >
                    {
                        this.state.devises.map(item=>(
                            <Picker.Item
                                label={capitalize(item.signe).toUpperCase()}
                                value={item.id}
                            />
                        ))
                    }
                </Picker>
            </View>
        </View>

    )

    /** */
    onChangePriceText=(value)=>{
        let rep=removeNoNumber(value);
        rep=limitLength(rep,10);
        //rep=addGapsWithSeparator(rep);
        this.setState({price:rep});
    }

    getJournals=async()=>{
        try {
            const date=new Date();
            const v1=DateTime.format(date,"YYYY-MM-DD 00:00:00");
            const v2=DateTime.format(date,"YYYY-MM-DD 23:59:59");
            const rep=await BuyModel.getBetween(v1,v2);
            this.setState({journals:rep})
        } catch (error) {
            
        }
    }

    getBill=()=>{
        const date=new Date();
        const month=date.getMonth()+1;
        const day=date.getDate();
        const year=date.getFullYear();
        const num=this.state.journals.length+1;
        const random=Math.random()*100;
        let rep=parseInt(random+"")+"";
        rep+=(num+"").length<2?"0"+num:num+"";
        rep+="-";
        rep+=(month+"").length<2?"0"+month:month+"";
        rep+=(day+"").length<2?"0"+day:day+"";
        rep+="/"+year;
        return rep;
    }
    

    render(){
        const y=new Animated.Value(0);
        const onScroll=Animated.event([{nativeEvent:{contentOffset:{y}}}],{useNativeDriver:true})
        return(
            <>
            {
                this.state.showSearch==false?(
                <Animated.View>
                    <Appbar.Header style={{backgroundColor:R.color.colorPrimary}}>
                        <Appbar.Action icon="menu" onPress={()=>this.openDrawer()} />
                        <Appbar.Content title="Ventes" titleStyle={{fontFamily:"Poppins-Black"}} />
                        {/*<Appbar.Action color="red" icon={require('../../assets/images/bell.png')} />*/}
                        <Appbar.Action icon={require('../../assets/baseline_search_white_18dp.png')} onPress={()=>this.onShowSearch()} />
                    </Appbar.Header>
                </Animated.View>
                ):(
                    <Animated.View>
                        <Appbar.Header style={{backgroundColor:R.color.colorPrimary}}>
                            <Appbar.BackAction onPress={()=>this.onShowSearch()} />
                            <Searchbar
                                value={this.state.search}
                                onChangeText={value=>this.setState({search:value})}
                                placeholder="Rechercher un article"
                                onSubmitEditing={()=>this.onSearch()}
                                clearTextOnFocus={true}
                                clearButtonMode="always"
                            />
                        </Appbar.Header>
                    </Animated.View>
                )
            }
               {
                   this.state.isLoading==true?(
                       <Awaiter color={R.color.colorSecondary} size={100}  key="f" />
                   ):this.state.list.length==0?(
                       <InfoSale />
                   ):(
                       <View style={{flex:1}}>
                           <View style={{flexDirection:'row',alignItems:'center',backgroundColor:R.color.colorPrimary}}>
                               <View style={{padding:10}}>
                                   <Icon
                                        name='shopping-cart'
                                        type='font-awesome'
                                        color={R.color.colorSecondary}
                                   />
                               </View>
                               <View style={{padding:10,flex:1}}>
                                   <Text numberOfLines={1} style={{color:R.color.background,fontSize:18,fontFamily:'Poppins-Black',opacity:0.6}}>
                                       {this.state.total}
                                   </Text>
                                   <Text numberOfLines={1} style={{color:R.color.background,fontFamily:'Poppins-Light'}}>
                                       {this.state.count==0?"aucun article sélectionné":this.state.count+" article"+(this.state.count>1?'s':'')+" sélectionné"+(this.state.count>1?'s':'')}
                                   </Text>
                               </View>
                               <View>

                               </View>
                           </View>
                           <AnimatedFlatList
                                data={this.state.filter}
                                renderItem={this.renderItem}
                                keyExtractor={(item,i)=>item.uid.toString()+i}
                                extraData={this.state.carts}
                                bounces={false}
                                scrollEventThrottle={16}
                                {...{onScroll}}
                                key="list-item"
                           />
                           <FAB
                                icon="basket-outline"
                                style={{position:'absolute',
                                bottom:0,right:0,margin:25,
                                elevation:20,
                                borderColor:R.color.colorAccent,
                                borderWidth:1,
                                backgroundColor:this.state.carts.length>0?R.color.colorSecondary:R.color.colorPrimary}}
                                onPress={()=>{
                                    this.state.carts.length>0?this.setState({showCart:true}):null
                                }}
                           />

                           {
                               this.state.step>1?(
                                <FAB
                                    icon="arrow-left-thick"
                                    small
                                    color={R.color.background}
                                    style={{position:'absolute',
                                    top:0,right:0,margin:25,
                                    elevation:20,
                                    borderColor:R.color.colorAccent,
                                    borderWidth:1,
                                    backgroundColor:R.color.colorSecondary}}
                                    onPress={()=>this.onReturn()}
                                />
                               ):null
                           }
                           <ConfirmDialog
                                positiveButton={{
                                    onPress:()=>this.onSetDescription(),
                                    title:"Valider",
                                    titleStyle:{color:'#000'}
                                }}
                                negativeButton={{
                                    onPress:()=>this.setState({showDescription:false}),
                                    title:"Annuler",
                                    titleStyle:{color:'#000'}
                                }}
                                title={this.state.title}
                                keyboardShouldPersistTaps='always'
                                animationType='fade'
                                dialogStyle={{padding:0,backgroundColor:R.color.background,borderWidth:1,borderColor:R.color.colorPrimaryDark}}
                                contentStyle={{padding:0,margin:0,top:0}}
                                visible={this.state.showDescription}
                                onTouchOutside={()=>this.setState({showDescription:false})}
                                titleStyle={{fontSize:14,padding:3,margin:5}}
                                messageStyle={{margin:0,padding:0,height:0,minHeight:0}}
                           >
                               <View style={{flexDirection:'row',alignItems:'center'}}>
                                   <View style={{flex:1}}>
                                       <Input
                                            label="Description de l'article"
                                            leftIcon={{name:'money',type:'font-awesome',color:R.color.colorSecondary}}
                                            placeholder="Ajouter un élément de précision de la vente"
                                            value={this.state.description}
                                            onChangeText={value=>this.setState({description:value})}
                                            keyboardType='ascii-capable'
                                            maxLength={50}
                                            errorStyle={{fontSize:12}}
                                       />
                                   </View>
                               </View>
                           </ConfirmDialog>
                           <ConfirmDialog
                                positiveButton={{
                                    onPress:this.countValidate()?()=>this.onAddCart():()=>{},
                                    title:"Valider",
                                    titleStyle:{color:'#000',opacity:this.countValidate()?1:0.6}
                                }}
                                negativeButton={{
                                    onPress:()=>this.setState({showStock:false}),
                                    title:"Annuler",
                                    titleStyle:{color:'#000'}
                                }}
                                title={this.state.title}
                                keyboardShouldPersistTaps='always'
                                animationType='slide'
                                dialogStyle={{padding:0,backgroundColor:R.color.background,borderWidth:2,borderColor:R.color.colorPrimaryDark}}
                                contentStyle={{padding:0,margin:0,top:0}}
                                visible={this.state.showStock}
                                onTouchOutside={()=>this.setState({showStock:false})}
                                titleStyle={{fontSize:14,padding:3,margin:5}}
                                messageStyle={{margin:0,padding:0,height:0,minHeight:0}}
                           >
                               <View>
                                   <View>
                                       <Input
                                            label="Quantité à vendre"
                                            placeholder="Quantité ici"
                                            leftIcon={{name:'shopping-basket',type:'font-awesome',color:R.color.colorSecondary}}
                                            value={this.state.stock}
                                            maxLength={5}
                                            onChangeText={value=>this.onChangeText(value)}
                                            keyboardType='number-pad'
                                            errorStyle={{fontSize:14}}
                                            errorMessage={this.countValidate()==false?"Quantité non valide":""}
                                       />
                                   </View>
                               </View>
                           </ConfirmDialog>
                           <ConfirmDialog
                                positiveButton={{
                                    onPress:this.countValidate()==true?()=>this.onAddCartService():()=>{},
                                    title:"Valider",
                                    titleStyle:{color:'#000',opacity:this.countValidate()==true?1:0.6}
                                }}
                                negativeButton={{
                                    onPress:()=>this.setState({showStockService:false}),
                                    title:"Annuler",
                                    titleStyle:{color:'#000'}
                                }}
                                title={this.state.title}
                                keyboardShouldPersistTaps='always'
                                animationType='slide'
                                dialogStyle={{padding:0,backgroundColor:R.color.background,elevation:20}}
                                contentStyle={{padding:0,margin:0,top:0}}
                                visible={this.state.showStockService}
                                onTouchOutside={()=>this.setState({showStockService:false})}
                                titleStyle={{fontSize:14,padding:3,margin:5}}
                                messageStyle={{margin:0,padding:0,height:0,minHeight:0}}
                           >
                               <ScrollView>
                                   <View>
                                       {
                                            (this.state.currentArticle===null || this.state.currentArticle===undefined)?null
                                            :(this.state.currentArticle.stockage==1)?
                                                (this.state.currentArticle.mode==S.ENUM.stockage.devise)?(
                                                    <this.componentInputPrice key="trans"/>
                                                    ):null
                                                :(this.state.currentArticle.quantifiable==1)?(
                                                    <this.componentInputQuantity/>
                                                ):null 
                                       }
                                       <Input
                                            label="Référence de service"
                                            placeholder="une référence"
                                            leftIcon={{name:'handshake-o',type:'font-awesome',color:R.color.colorSecondary}}
                                            value={this.state.reference}
                                            maxLength={20}
                                            ref={input=>this.reference=input}
                                            onChangeText={value=>this.setState({reference:value})}
                                            errorStyle={{fontSize:12}}
                                       />
                                   </View>
                               </ScrollView>
                           </ConfirmDialog>
                           <ConfirmDialog
                                positiveButton={{
                                    onPress:this.remiseValidate?()=>this.onAddRemise():()=>{},
                                    title:"Valider",
                                    titleStyle:{color:'#000',opacity:this.remiseValidate()?1:0.6}
                                }}
                                negativeButton={{
                                    onPress:()=>this.setState({showRemise:false}),
                                    title:"Annuler",
                                    titleStyle:{color:'#000'}
                                }}
                                title="Remise"
                                animationType='slide'
                                keyboardShouldPersistTaps='always'
                                dialogStyle={{padding:0,backgroundColor:R.color.background,borderRadius:20,elevation:20}}
                                contentStyle={{padding:0,margin:0,top:0}}
                                visible={this.state.showRemise}
                                onTouchOutside={()=>this.setState({showRemise:false})}
                                titleStyle={{fontSize:14,padding:3,margin:5}}
                                messageStyle={{margin:0,padding:0,height:0,minHeight:0}}
                           >
                               <View>
                                   <View>
                                       <Input
                                            label="La remise"
                                            placeholder="Votre remise ici"
                                            leftIcon={{name:'percent',type:'font-awesome',color:R.color.colorSecondary}}
                                            value={this.state.remise}
                                            maxLength={5}
                                            onChangeText={value=>this.setState({remise:value})}
                                            keyboardType='number-pad'
                                            errorStyle={{fontSize:18}}
                                            errorMessage={this.remiseValidate()==false?"Remise non valide":""}
                                       />
                                   </View>
                               </View>
                           </ConfirmDialog>
                           <ConfirmDialog
                                positiveButton={{
                                    onPress:this.priceValidate()?()=>this.onAlter():()=>{},
                                    title:"Valider",
                                    titleStyle:{color:'#000',opacity:this.priceValidate()?1:0.6}
                                }}
                                negativeButton={{
                                    onPress:()=>this.setState({showAlter:false}),
                                    title:"Annuler",
                                    titleStyle:{color:'#000'}
                                }}
                                title={this.state.title}
                                animationType='slide'
                                keyboardShouldPersistTaps='always'
                                dialogStyle={{padding:0,backgroundColor:R.color.background,borderRadius:20,elevation:20}}
                                contentStyle={{padding:0,margin:0,top:0}}
                                visible={this.state.showAlter}
                                onTouchOutside={()=>this.setState({showStock:false})}
                                titleStyle={{fontSize:14,padding:3,margin:5}}
                                messageStyle={{margin:0,padding:0,height:0,minHeight:0}}
                           >
                               <View style={{flexDirection:'row',alignItems:'center'}}>
                                   <View style={{flex:1}}>
                                       <Input
                                            label="Nouveau prix"
                                            leftIcon={{name:'money',type:'font-awesome',color:R.color.colorSecondary}}
                                            placeholder="le prix ici"
                                            value={this.state.price}
                                            onChangeText={value=>this.onChangePriceText(value)}
                                            keyboardType='number-pad'
                                            maxLength={10}
                                            errorStyle={{fontSize:18}}
                                            inputStyle={{textAlign:'right'}}
                                            errorMessage={this.priceValidate()==false?"Prix non validé":""}
                                       />
                                   </View>
                                   <View style={{width:100}}>
                                       <Picker
                                            selectedValue={this.state.id_devise}
                                            mode="dropdown"
                                            onValueChange={(item,index)=>{this.setState({id_devise:item,devise:this.state.devises[index].signe})}}
                                       >
                                           {
                                               this.state.devises.map((item,index)=>(
                                                   <Picker.Item key={`deviseIndex${index}`} label={capitalize(item.signe).toUpperCase()} value={item.id} />
                                               ))
                                           }
                                       </Picker>
                                   </View>
                               </View>
                           </ConfirmDialog>
                           <ModalSale
                                key="modal-sale"
                                onClose={()=>this.setState({showCart:false,isOpen:false})}
                                visible={this.state.showCart}
                                renderItem={this.renderSale}
                                onRemise={()=>this.setState({showRemise:true})}
                                list={this.state.carts}
                                onPress={()=>this.onCreateSale()}
                                isOpen={this.state.isOpen}
                                client={this.state.customer}
                                onOpen={()=>this.setState({isOpen:!this.state.isOpen})}
                                total={this.state.total}
                                onTotal={()=>{this.setState({showTotal:true})}}
                                onClient={()=>this.setState({showClient:true,isOpen:!this.state.isOpen})}
                                count={this.state.count==0?"aucun article n'est sélectionné":this.state.count+" article"+(this.state.count>1?'s':'')+" sélectionné"+(this.state.count>1?'s':'')}
                           />

                            <Dialog 
                                animationType='slide'
                                visible={this.state.showTotal}
                                onTouchOutside={()=>this.setState({showTotal:false})}
                                title="Total"
                            >
                                <View>
                                    <Text style={{ fontFamily:'Poppins-Bold',fontSize:14,padding:10,textAlign:'center' }} >
                                        {
                                            (this.state.total!=null && this.state.total!=undefined)?this.state.total:""
                                        }
                                    </Text>
                                </View>
                            </Dialog>
                           <ModalClient
                                visible={this.state.showClient}
                                key="modal-customer"
                                onClose={()=>this.setState({showClient:false})}
                                list={this.state.filterCustomer}
                                name={this.state.customerName}
                                phone={this.state.customerPhone}
                                onName={value=>this.onChangeCustomerName(value)}
                                onPhone={value=>this.onChangeCustomerPhone(value)}
                                onPress={()=>this.onAddClient()}
                                renderItem={this.renderCustomer}
                           />
                           <ProgressDialog
                                message="Attendez svp!"
                                title="Chargement"
                                visible={this.state.showProgress}
                                activityIndicatorColor={R.color.colorSecondary}
                                activityIndicatorSize='large'
                           />

                           <Dialog
                                key='diag-menu'
                                animationType='fade'
                                visible={this.state.showMenu}
                                onTouchOutside={()=>this.setState({showMenu:false})}
                                dialogStyle={{maxWidth:300,alignSelf:'center',padding:0,elevation:50}}
                           >
                               <View>
                                    <MenuItem
                                        onPress={()=>this.onDelete()}
                                        title="Supprimer"
                                        key='delete'
                                        icon="trash-o"
                                        type="font-awesome"
                                        color="red"
                                    />
                                    <MenuItem
                                        onPress={()=>this.onUpdate()}
                                        title="Modifier"
                                        key='update'
                                        icon="edit"
                                        type="font-awesome"
                                    />
                                    {
                                        (this.state.currentCart!=null && this.state.currentCart.mode!="devise")?(
                                            <MenuItem
                                                onPress={()=>this.onShowAlter()}
                                                title="Ajuster le prix"
                                                key='aj'
                                                icon="tag"
                                                type="font-awesome"
                                            />
                                        ):null
                                    }
                                    {
                                        this.state.currentCart!=null && this.state.currentCart.is_alter==true?
                                        (
                                        <MenuItem
                                            onPress={()=>this.onInitialPrice()}
                                            title="Revenir au prix initial"
                                            key='init'
                                            icon="share-square-o"
                                            type="font-awesome"
                                        />
                                        ):null
                                    }
                                    <MenuItem
                                        onPress={()=>this.onShowDescription()}
                                        title="Auxiliaire"
                                        key='descript'
                                        icon="barcode"
                                        type="font-awesome"
                                    />
                               </View>
                               
                           </Dialog>
                           <Dialog
                                key='diag-menu-devise'
                                animationType='fade'
                                visible={this.state.showDevise}
                                onTouchOutside={()=>this.setState({showDevise:false})}
                                dialogStyle={{maxWidth:300,alignSelf:'center',padding:0,elevation:50}}
                           >
                               <AnimatedFlatList
                                    data={this.state.devises}
                                    renderItem={this.renderDevise}
                                    key='list-devise'
                                    keyExtractor={(item,i)=>item.id.toString()+i}
                                    bounces={false}
                                    scrollEventThrottle={16}
                                    {...{onScroll}}
                               />
                           </Dialog>
                           <AwaiterFull
                               color={R.color.colorSecondary}
                               size={100}
                               visible={this.state.showFullLoad}
                               key="load-full"
                           />
                           <ConfirmDialog
                               title="Exécution de la vente"
                               keyboardShouldPersistTaps='always'
                               visible={this.state.showResponse}
                               onTouchOutside={()=>this.setState({showResponse:false})}
                               animationType='fade'
                               positiveButton={{ 
                                   onPress:()=>this.setState({showResponse:false}),
                                   title:"OK",
                                   titleStyle:{color:"#000",fontFamily:"Poppins-Black"}
                                }}
                           >
                           <View>
                                <Text style={{ textAlign:'center',fontFamily:'Poppins-Italic' }}>
                                    La vente a été effectuée avec succès
                                </Text>
                               <Text numberOfLines={1} style={{ fontFamily:'Poppins-Bold',paddingLeft:20 }}>
                                   Facture: numéro <Text numberOfLines={1} style={{ fontFamily:'Poppins-Black',fontSize:15 }}>{this.state.facture}</Text>
                               </Text>
                           </View>
                           </ConfirmDialog>
                       </View>
                   )
               } 
            </>
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(App);