import React from 'react';
import { Component } from 'react';
import { ScrollView } from 'react-native';
import { ImageBackground } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { View } from 'react-native';
import { Avatar, Card, Text } from 'react-native-elements';
import { Divider } from 'react-native-paper';
import { connect } from 'react-redux';
import R from '../../resources/styles.json';

const CardSetting=({title="",description="",onPress,icon="person",type="ionicon",inactive=false})=>(
    <TouchableOpacity onPress={onPress} activeOpacity={inactive?1:0.9} style={{flex:1,padding:10}}>
    <View style={{flexDirection:'row',alignItems:"center",opacity:inactive?0.4:1}}>
        <Avatar
            rounded
            size="large"
            icon={{name:icon,type}}
            containerStyle={{backgroundColor:R.color.colorPrimary}}
        />
        <View style={{padding:10,flex:1}}>
            <Text numberOfLines={1} style={{fontFamily:"Poppins-Bold",color:'#070707',fontSize:18}}>
                {title}
            </Text>
            <Text numberOfLines={2} style={{fontFamily:'Poppins-Light',color:'#070707',fontSize:16}}>
                {description}
            </Text>
        </View>
    </View>
    </TouchableOpacity>
)

const mapStateToProps = (state) => {
    return {
        user:state.user,
        subscription:state.subscription
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

class App extends Component{

    constructor(props){
        super(props);
    }

    render(){
        return(
            <View style={{ flex:1 }}>
            <ImageBackground 
                blurRadius={2.5} imageStyle={{ opacity:0.15 }}
                source={require('../../assets/images/fond.jpg')}
                style={{ flex:1 }}
            >
            <ScrollView>

                <Card containerStyle={{ padding:0,borderRadius:30,marginBottom:30,elevation:10 }}>
                    <Card.Image
                        source={require('../../assets/images/interactive_society.webp')}
                        containerStyle={{ borderRadius:30 }}
                    />
                </Card>
                <CardSetting 
                    title="Présentation"
                    description="gérez la présentation de boutique"
                    icon="building"
                    type="font-awesome"
                    onPress={()=>this.props.navigation.navigate('Shop')}
                />
                <Divider/>
                <CardSetting 
                    type="entypo"
                    icon="users"
                    title="Agents"
                    onPress={()=>this.props.navigation.navigate('Agent')}
                    description="Ajouter, supprimer, ou voir tous les agents disponibles dans votre boutique"
                />
                <Divider/>
                <CardSetting 
                    title="Abonnement & Bureau"
                    description="Ligal et vos paiements"
                    type="entypo"
                    icon="bell"
                    onPress={()=>this.props.navigation.navigate('ShowSubscription')}
                />
                <CardSetting 
                    title="Activités de la boutique"
                    description="Contrôlez les annexes de la société"
                    type="entypo"
                    inactive={!this.props.subscription.status}
                    icon="briefcase"
                    onPress={()=>this.props.subscription.status?this.props.navigation.navigate('ShowActivity'):null}
                />
                <Divider/>
                {/*<CardSetting 
                    type="entypo"
                    inactive={!this.props.subscription.status}
                    icon="emoji-flirt"
                    onPress={()=>this.props.subscription.status?this.props.navigation.navigate('Customer'):null}
                    title="Relation client"
                    description="Gérez votre liaison permanente avec vos clients, cherchez des nouveaux clients, étudiez l'impact des clients"
                />*/}
            </ScrollView>
            </ImageBackground>
            </View>
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(App);