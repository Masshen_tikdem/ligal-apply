import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { View } from 'react-native';
import EmptyMessage from  '../../components/EmptyMessage';
import Awaiter from '../../components/Waiter';
import R from '../../resources/styles.json';
import CardJournal from '../../components/CardJournal';
import * as BuyModel from '../../model/local/buy';
import * as DeviseModel from '../../model/local/devise';
import * as Manager from '../../Manager';
import DateTime from 'date-and-time';
import { Text } from 'react-native-elements';
import { connect } from 'react-redux';
import S from '../../resources/settings.json';

const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.article
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}


class App extends Component{

    constructor(props){
        super(props)
        this.state={
            isLoading:true,
            list:[],
            devises:[],
            total:"0",
            count:0
        }
    }
    getPriceArticle=(price,quantity)=>{
        let rep=0;
        try {
            rep=parseFloat(price)*parseInt(quantity);
        } catch (error) {
            
        }
        return rep;
    }
    renderItem=({item})=>{
        let date="";
        try {
            const value=DateTime.parse(item.created_at,"YYYY-MM-DD HH:mm:ss");
            date=DateTime.format(value,"HH:mm");
        } catch (error) {
            
        }
        return(
            <CardJournal
                title={Manager.getArticleName(item.name,item.description)}
                date={date}
                logo={Manager.getImageLocal(item.logo)}
                count={(item.type==S.ENUM.article.service && item.mode==S.ENUM.stockage.devise)?null:item.quantity}
                onPress={()=>this.props.navigation.navigate("Buy",{id:item.id})}
                price={Manager.getPrice(this.getPriceArticle(item.price,item.quantity,item.signe),item.signe,item.remise)}
            />
        )
    }
    async componentDidMount(){
        try {
            await this.load();
        } catch (error) {
            
        }
    }

    load=async()=>{
        try {
            const date=new Date();
            const v1=DateTime.format(date,"YYYY-MM-DD 00:00:00");
            const v2=DateTime.format(date,"YYYY-MM-DD 23:59:59");
            const d=await DeviseModel.get();
            this.setState({
                devises:d
            })
            const rep=await BuyModel.getBetween(v1,v2);
            this.setState({
                list:rep
            })
            const total=Manager.getTotal(rep,d);
            this.setState({
                total,
                count:rep.length
            })
            if(this.state.isLoading==true){
                this.setState({
                    isLoading:false
                })
            }
        } catch (error) {
            
        }
    }

    

    render(){
        return(
            <>
            {
                this.state.isLoading==true?(
                    <Awaiter color={R.color.colorSecondary} size={100} key='awaiter' />
                ):this.state.list.length==0?(
                    <EmptyMessage title="Aucune vente enregistrée aujourd'hui" key='message' />
                ):(
                    <View style={{flex:1}} key='v'>
                        <View style={{backgroundColor:R.color.colorPrimary,padding:10}} key='v2'>
                            <Text numberOfLines={1} style={{fontSize:18,textAlign:'center',fontWeight:'bold',color:R.color.background}}>
                                {this.state.total}
                            </Text>
                            <Text style={{color:R.color.background,fontSize:16,textAlign:'center'}}>
                                {this.state.count>0?this.state.count+" vente"+(this.state.count>1?'s':"")+" réalisée"+(this.state.count>1?'s':""):""}
                            </Text>
                        </View>
                        <FlatList
                            data={this.state.list}
                            renderItem={this.renderItem}
                            key='list'
                        />
                    </View>
                )
            }
            </>
        )
    }
}
export default (App)