import React, { Component } from 'react';
import { View,FlatList } from 'react-native';
import { FAB,Appbar,Searchbar } from 'react-native-paper';
import R from '../../../resources/styles.json';
import ModalCreation from '../../../components/ModalCreateCategory';
import Empty from '../../../components/EmptyMessage';
import Awaiter from '../../../components/Waiter';
import * as Family from '../../../model/local/family';
import { Alert } from 'react-native';
import * as Manager from '../../../Manager';
import { connect } from 'react-redux';
import S from "../../../resources/settings.json";
import CardCategory from '../../../components/CardCategory';
import ImagePicker from 'react-native-image-picker';
import { ProgressDialog } from 'react-native-simple-dialogs';
import { Dimensions } from 'react-native';
import * as Shared from '../../../service/shared';


const mapStateToProps = (state) => {
    return {
        user:state.user,
        category:state.category,
        right:state.right,
        shop:state.shop
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}




class App extends Component{

    constructor(props){
        super(props)
        this.state={
            showDialog:false,
            cat:'',
            isLoading:true,
            list:[],
            type:null,
            logo:null,
            showProgress:false,
            showSearch:false,
            all:[],
            search:"",
            showCreate:false,
            showRead:false,
            column:1
        }
    }

    onLoad=async()=>{
        try {
            const list=[];
            const rep=await Family.get();
            this.setState({
                list:rep,
                all:rep
            })
            if(this.state.isLoading==true){
                this.setState({isLoading:false})
            }
        } catch (error) {
            
        }
    }

    async componentDidMount(){
        if(Manager.write(this.props.right.category)){
            this.setState({showCreate:true})
        }
        if(Manager.read(this.props.right.category)){
            this.setState({showRead:true})
        }
        await this.onLoad();
        const width=Dimensions.get('window').width;
        const cl=Math.round(width/200);
        this.setState({column:cl});
        Dimensions.addEventListener("change",({window,screen})=>{
            const w=window.width;
            const cl=Math.round(w/200);
            this.setState({column:cl});
        })
    }
    async componentDidUpdate(){
        try {
            const cat=this.props.category;
            if(cat.market==true){
                const action={type:S.EVENT.onChangeMarketCategory,market:false}
                this.props.dispatch(action);
                await this.onLoad();
            }
        } catch (error) {
            
        }
    }
    onShowSearch=()=>{
        this.setState({showSearch:!this.state.showSearch})
    }

    onSearch=(value)=>{
        try {
            this.setState({search:value})
            if(value=="" || value==null || value==undefined){
                this.setState({list:this.state.all})
            }else{
                const list=[];
                this.state.all.map((item,index)=>{
                    const cible=Manager.capitalize(value).toLowerCase();
                    const name=Manager.capitalize(item.name).toLowerCase();
                    if(name.includes(cible)){
                        list.push(item);
                    }
                });
                this.setState({list})
            }
        } catch (error) {
        }
    }

    onSaveLogo=()=>{
        ImagePicker.launchImageLibrary({},rep=>{
            if(rep.didCancel){
                
            }else if(rep.error){
                
            }
            else if(rep.customButton){
                
            }else{
                const uri=rep.uri
                this.setState({
                    logo:uri
                })
            }
        })
    }

    onAdd=async()=>{
        try {
            const cat=this.state.cat;
            const type=this.state.type;
            const user=this.props.user.value;
            const shop=this.props.shop.value;
            let passer=false;
            if(cat.trim()!='' && type!=null && type!=undefined && this.isUsed()==false){
                this.setState({showProgress:true})
                let logo=null;
                const uri=this.state.logo;
                if(uri!=null){
                    const image=Manager.getRandomString(20)+".ligal";
                    const l=await Manager.saveImage(image.toLowerCase(),uri,null);
                    if(l==true){
                        logo=image.toLowerCase();
                    }
                }
                setTimeout(async()=>{
                    if(passer==false){
                        passer=true;
                        const rep=await Family.store({
                            name:cat,
                            status:1,
                            type:type,
                            logo:logo
                        })
                        if(rep!=null){
                            const action={type:S.EVENT.onChangeMarketArticle,sale:false}
                            this.props.dispatch(action);
                            Shared.category({
                                id:rep.id,
                                name:rep.name,
                                private_key:rep.private_key,
                                shop,
                                status:rep.status,
                                type:rep.type,
                                user
                            })
                            await this.onLoad();
                            this.setState({
                                cat:'',
                                showDialog:false,
                                showProgress:false,
                                logo:null
                            })
                        }else{
                            this.setState({showProgress:false})
                            Alert.alert("Enregistrement","Impossible")
                        }
                    }else{
                    }
                },500)
            }else{
                Alert.alert('Vérification',"Veuillez remplir toutes les rubriques!")
            }
        } catch (error) {
        }
    }
    
    renderItem=({item})=>{
        return(
            <CardCategory
                logo={Manager.getImageLocal(item.logo)}
                onPress={()=>this.openCategory(item)}
                title={Manager.capitalize(item.name).toUpperCase()}
            />
        )
    }
    isUsed=()=>{
        let rep=false;
        const cat=this.state.cat;
        this.state.list.map((item,i)=>{
            if(Manager.capitalize(item.name).toLowerCase()==Manager.capitalize(cat).toLowerCase()){
                rep=true;
                return rep;
            }
        })
        return rep;
    }
    openCategory=(item)=>{
        try {
            if(this.state.showRead)
                this.props.navigation.navigate('ViewCategory',{item:item})
            else{
                Alert.alert("Vérification","Vous n'avez pas d'accès");
            }
        } catch (error) {   
        }
    }

    render(){
        
        return(
            <>
            {
                this.state.showSearch==false?(
                <View>
                    <Appbar.Header style={{backgroundColor:R.color.colorPrimary}}>
                        <Appbar.Content 
                            title={this.state.list.length>0?this.state.list.length+" catégorie"+(this.state.list.length>1?"s":""):"Aucune catégorie"} 
                            titleStyle={{fontFamily:"Poppins-Thin"}}
                        />
                        <Appbar.Action icon={require('../../../assets/baseline_search_white_18dp.png')} onPress={()=>this.onShowSearch()} />
                        </Appbar.Header>
                </View>
                ):(
                    <View>
                        <Appbar.Header style={{backgroundColor:R.color.colorPrimary}}>
                            <Appbar.BackAction onPress={()=>this.onShowSearch()} />
                            <Searchbar
                                value={this.state.search}
                                onChangeText={value=>this.onSearch(value)}
                                placeholder="Rechercher une catégorie"
                                clearTextOnFocus={true}
                                clearButtonMode="always"
                            />
                        </Appbar.Header>
                    </View>
                )
            }
            {
                this.state.isLoading==true?(
                    <Awaiter color={R.color.colorSecondary} size={100} key='awaiter' />
                ):this.state.list.length==0?(
                    <Empty title="Aucune catégorie d'articles" />
                ):(
                    <FlatList
                        data={this.state.list}
                        key={`list${this.state.column}`}
                        renderItem={this.renderItem}
                        numColumns={this.state.column}
                    />
                )
            }
            <ModalCreation
                value={this.state.cat}
                visible={this.state.showDialog}
                logo={this.state.logo==null?"p":this.state.logo}
                onClose={()=>this.setState({showDialog:false})}
                onChangeText={value=>this.setState({cat:value})}
                onPress={()=>this.onAdd()}
                status={this.isUsed()}
                onProduct={()=>this.setState({type:"product"})}
                onService={()=>this.setState({type:"service"})}
                checked={this.state.type}
                onImage={()=>this.onSaveLogo()}
            />
            {
                this.state.showCreate?(
                    <FAB
                        style={{position:'absolute',bottom:0,margin:25,right:0,backgroundColor:R.color.colorPrimaryDark,elevation:10}}
                        icon="plus"
                        animated
                        onPress={()=>this.setState({showDialog:true})}
                    />
                ):null
            }
            <ProgressDialog
                message="Patientez svp"
                title="Chargement..."
                visible={this.state.showProgress}
                activityIndicatorColor={R.color.colorSecondary}
                activityIndicatorSize="large"
                animationType="fade"
                
            />
            </>
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(App)