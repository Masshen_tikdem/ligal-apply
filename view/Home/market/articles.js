import React, { Component } from 'react';
import { View,FlatList } from 'react-native';
import { FAB, Searchbar,Appbar } from 'react-native-paper';
import R from '../../../resources/styles.json';
import ModalCreation from '../../../components/ModalCreateArticle';
import Empty from '../../../components/EmptyMessage';
import Awaiter from '../../../components/Waiter';
import CardArticle from '../../../components/CardArticle';
import * as ArticleModel from '../../../model/local/article';
import { capitalize, getPrice } from '../../../Manager';
import { Dimensions } from 'react-native';
import { connect } from 'react-redux';
import S from '../../../resources/settings.json';
import * as Manager from '../../../Manager';
import { Alert } from 'react-native';

const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.article,
        right:state.right
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}


class App extends Component{

    constructor(props){
        super(props)
        
        this.state={
            showDialog:false,
            cat:'',
            isLoading:true,
            list:[],
            width:Dimensions.get("window").width,
            showSearch:false,
            search:"",
            all:[],
            showRead:false,
            column:1
        }
    }

    async componentDidMount(){
        try {
            if(Manager.read(this.props.right.article) || Manager.update(this.props.right.article)){
                this.setState({showRead:true})
            }
            await this.onLoad();
            const width=Dimensions.get('window').width;
            const cl=Math.round(width/350);
            this.setState({column:cl});
            Dimensions.addEventListener("change",({window,screen})=>{
                const w=window.width;
                const cl=Math.round(w/350);
                this.setState({column:cl});
            })
        } catch (error) {
            
        }
    }
    onLoad=async()=>{
        try {
            const rep=await ArticleModel.get();
            this.setState({
                list:rep,
                all:rep
            })
            if(this.state.isLoading==true){
                this.setState({
                    isLoading:false
                })
            }
        } catch (error) {
            
        }
    }

    onSearch=(value)=>{
        try {
            this.setState({search:value})
            if(value=="" || value==null || value==undefined){
                this.setState({list:this.state.all})
            }else{
                const list=[];
                this.state.all.map((item,index)=>{
                    const cible=capitalize(value).toLowerCase();
                    const name=capitalize(item.name).toLowerCase();
                    const description=capitalize(item.description).toLowerCase();
                    if(name.includes(cible) || description.includes(cible)){
                        list.push(item);
                    }
                });
                this.setState({list})
            }
        } catch (error) {
            console.log('error',error);
        }
    }

    openArticle=(item)=>{
        try {
            if(this.state.showRead)
                this.props.navigation.navigate('ViewArticle',{item:item})
            else{
                Alert.alert("Vérification","Vous n'avez pas d'accès");
            }
        } catch (error) {
        }
    }
    renderItem=({item})=>{
        const stock=item.stock>0?item.stock:0;
        const buy=item.buy>0?item.buy:0;
        const quantity=stock-buy;
        return(
            <CardArticle
                familly={capitalize(item.famille)}
                name={Manager.getCategoryName(item.name,item.category)}
                price={
                    (item.type==S.ENUM.article.service && item.mode==S.ENUM.stockage.devise)?Manager.getStock(item.stock,item.buy,item.mode,item.type).text:
                    getPrice(item.price,item.devise)
                }
                center={this.state.column==1}
                showPrice={item.mode!=S.ENUM.stockage.devise}
                quantity={Manager.getStock(item.stock,item.buy,item.mode,item.type).text}
                key='card'
                onPress={()=>this.openArticle(item)}
                logo={Manager.getImageLocal(item.logo)}
                service={item.type==S.ENUM.article.service && item.mode!=S.ENUM.stockage.devise}
            />
        )
    }
    getNumcolumnns=()=>{
        let rep=2
        const width=this.state.width;
        if(width>1200){
            rep=6
        }
        else if(width<1200 && width>=700){
            rep=4;
        }
        else if(width<700 && width>=500){
            rep=3;
        }else if(width<500 && width>200){
            rep=2;
        }else{
            rep=1;
        }
        return rep;
    }
    async componentDidUpdate(){
        try {
            const article=this.props.article.added;
            if(article==true){
                const action={type:S.EVENT.onChangeMarketArticle,added:false}
                this.props.dispatch(action);
                await this.onLoad()
            }
        } catch (error) {
        }
    }

    onShowSearch=()=>{
        this.setState({showSearch:!this.state.showSearch})
    }

    render(){
        return(
            <>
            {
                this.state.showSearch==false?(
                <View>
                    <Appbar.Header style={{backgroundColor:R.color.colorPrimary}}>
                        <Appbar.Content 
                            title={this.state.list.length>0?this.state.list.length+" article"+(this.state.list.length>1?"s":""):"Aucun article"} 
                            titleStyle={{fontFamily:"Poppins-Thin"}}
                        />
                        <Appbar.Action icon={require('../../../assets/baseline_search_white_18dp.png')} onPress={()=>this.onShowSearch()} />
                        </Appbar.Header>
                </View>
                ):(
                    <View>
                        <Appbar.Header style={{backgroundColor:R.color.colorPrimary}}>
                            <Appbar.BackAction onPress={()=>this.onShowSearch()} />
                            <Searchbar
                                value={this.state.search}
                                onChangeText={value=>this.onSearch(value)}
                                placeholder="Rechercher un article"
                                clearTextOnFocus={true}
                                clearButtonMode="always"
                            />
                        </Appbar.Header>
                    </View>
                )
            }
            {
                this.state.isLoading==true?(
                    <Awaiter color={R.color.colorSecondary} size={100} key='awaiter' />
                ):this.state.list.length==0?(
                    <Empty title="Aucun article" />
                ):(
                    <View style={{ flex:1 }}>
                        <FlatList
                            data={this.state.list}
                            key={`list${this.state.column}`}
                            renderItem={this.renderItem}
                            numColumns={this.state.column}
                        />
                    </View>
                )
            }
            <ModalCreation
                value={this.state.cat}
                visible={this.state.showDialog}
                logo={'p'}
                onClose={()=>this.setState({showDialog:false})}
            />
            </>
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(App)