import React, { Component } from 'react';
import { View } from 'react-native';
import R from '../../resources/styles.json';
import S from '../../resources/settings.json';
import EmptyMessage from '../../components/EmptyMessage';
import Awaiter from '../../components/Waiter';
import { FlatList } from 'react-native';
import * as CustomerModel from '../../model/local/client';
import { Avatar, ListItem, Text } from 'react-native-elements';
import { capitalize, isEmpty } from '../../Manager';




class App extends Component{

    constructor(props){
        super(props)
        this.state={
            isLoading:true,
            refresh:false,
            list:[]
        }
    }
    renderItem=({item})=>{
        return(
            <ListItem>
                <Avatar
                    icon={{name:"user-o",type:"font-awesome",color:R.color.background}}
                    placeholderStyle={{backgroundColor:R.color.colorPrimary}}
                    containerStyle={{backgroundColor:R.color.colorPrimary}}
                    rounded size="medium"
                />
                <View style={{padding:5}}>
                    <Text numberOfLines={1} style={{fontFamily:'Poppins-Regular',fontSize:16,padding:10,paddingBottom:0}}>
                        {capitalize(item.name).toUpperCase()}
                    </Text>
                    {
                        isEmpty(item.phone)?(
                            <Text numberOfLines={1} style={{fontFamily:"Poppins-Light",fontSize:14,padding:10,paddingTop:0}}>
                                {isEmpty(item.phone)?"Téléphone non renseigné":""}
                            </Text>
                        ):null
                    }
                    
                </View>
            </ListItem>
        )
    }
    onLoad=async()=>{
        try {
            const rep=await CustomerModel.get();
            this.setState({
                list:rep
            })
            if(this.state.isLoading==true){
                this.setState({isLoading:false})
            }
        } catch (error) {
            
        }
    }
    onRefresh=async()=>{
        try {
            this.setState({refresh:true});
            await this.onLoad();
            this.setState({refresh:false})
        } catch (error) {
            
        }
    }

    async componentDidMount(){
        try {
            await this.onLoad();
        } catch (error) {
            
        }
    }

    render(){
        return(
            this.state.isLoading==true?(
                <Awaiter  color={R.color.colorSecondary} size={100} key="awaiter" />
            ):this.state.list.length==0?(
                <EmptyMessage onRefresh={()=>this.onRefresh()} title="Liste vide" key="empty" refresh={this.state.refresh} />
            ):(
                <FlatList
                    data={this.state.list}
                    renderItem={this.renderItem}
                    key="list"
                    keyExtractor={item=>item.id}
                />
            )
        )
    }
}
export default (App)