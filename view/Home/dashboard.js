import React, { Component } from 'react';
import { View } from 'react-native';
import Awaiter from '../../components/Waiter';
import R from '../../resources/styles.json';
import S from '../../resources/settings.json';
import * as StockModel from '../../model/local/stock';
import * as ArticleModel from '../../model/local/article';
import * as FamilyModel from '../../model/local/family';
import * as BuyModel from '../../model/local/buy';
import * as CustomerModel from '../../model/local/client';
import * as AgentModel from '../../model/local/agent';
import * as DeviseModel from '../../model/local/devise';
import CardDashbord from '../../components/CardDashboard';
import * as ArticleWebModel from '../../model/web/article';
import * as FamilleWebModel from '../../model/web/famille';
import * as SocietyWebModel from '../../model/web/society';
import { ScrollView } from 'react-native';
import { Button, Card, Icon, Text } from 'react-native-elements';
import { Banner } from 'react-native-paper';
import { ProgressDialog } from "react-native-simple-dialogs";
import InternetError from '../../components/InternetError';
import CardDashboardInfo from '../../components/CardDashInfo';
import { getFilePath, getImageLocal, isConnected } from '../../Manager';
import { connect } from 'react-redux';
import File from 'react-native-fs'; 
import { StyleSheet } from 'react-native';
import * as WebData from '../../service/webRecoveryData';
import { Alert } from 'react-native';


const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.article,
        service:state.service,
        shop:state.shop,
        setting:state.setting
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

const styles=StyleSheet.create({
    text:{
        color:R.color.background,
        fontFamily:'Poppins-Light',
    }
})

class App extends Component{

    constructor(props){
        super(props)
        this.images=[];
        this.state={
            sales:{list:[],listCustomer:[],title:"",length:0,lengthCustomer:0,content:"",icon:"",type:"",num:0,icon:"user-o",type:"font-awesome"},
            stocks:{list:[],title:"",length:0,content:"",icon:"",type:"",num:0,icon:"user-o",type:"font-awesome"},
            categories:{list:[],listLink:[],title:"",length:0,lengthLink:0,content:"",icon:"",type:"",num:0,icon:"user-o",type:"font-awesome"},
            articles:{list:[],title:"",length:0,content:"",icon:"",type:"",num:0,icon:"user-o",type:"font-awesome"},
            devises:{list:[],title:"",length:0,content:"",icon:"",type:"",num:0,icon:"user-o",type:"font-awesome"},
            images:[],
            isLoading:true,
            showBanner:true,
            showBannerFile:true,
            showBannerWeb:true,
            showProgress:false,
            showError:false,
            info:{agent:0,devise:0,article:0,category:0,service:0,product:0,customer:0,},
            shop:{
                webShop:null,
                webDevises:[],
                webCategories:[],
                webAgents:[],
                webArticles:[],
                webStocks:[],
                webBuys:[],
                webCustomer:[],
                webLink:[],
                webRights:[],
            },
            message:"",
            societyChecking:false,
            data:0,
            imageSize:"O o"
        }
    }

    checkCategories=async()=>{
        try {
            const rep=await FamilyModel.get();
            const link=await FamilyModel.getLinks();
            let count=0;
            let all=rep.length+link.length;
            const list=this.state.images;
            this.setState({
                info:{
                    ...this.state.info,
                    category:rep.length
                }
            })
            const onEvent=async(item,index)=>{
                if(item.private_key==null || item.is_updated==1){
                    count++;
                }
                if(item.image_updated==1 && item.private_key!=null){
                    await this.getStat(item);
                }
            }
            await rep.map(onEvent);
            await link.map(onEvent);
            if(count>0){
                this.setState({
                    categories:{
                        title:"Catégories",
                        content:"Quelques informations de catégories ne sont pas encore publiées",
                        num:(count/all)*100,
                        icon:"shopping-bag",
                        length:rep.length,
                        lengthLink:link.length,
                        list:rep,
                        listLink:link
                    }
                })
            }else{
                this.setState({
                    categories:{
                        title:"Catégories",
                        content:"Toutes les informations sont publiés",
                        num:0,
                        icon:"shopping-bag",
                        length:rep.length,
                        lengthLink:link.length
                    }
                })
            }
        } catch (error) {
            
        }
    }

    getStat=async(item)=>{
        try {
            const element=new Object();
            console.log("size "+item.name)
            const size=(await File.stat(getFilePath(item.logo))).size;
            console.log("size "+item.name,size)
            element.size=parseInt(size);
            element.path=item.logo;
            element.name="";
            //const list=this.state.images;
            const list=this.images;
            const key=list.findIndex(p=>p.path==item.logo);
            console.log('index',key)
            if(key==-1){
                //list.push(element);
                this.images.push(element);
                this.setState({images:list})
            }
        } catch (error) {
            
        }
    }
    checkArticles=async()=>{
        try {
            const rep=await ArticleModel.get();
            let count=0;
            let all=rep.length;
            let count_service=0;
            let count_product=0;
            const list=this.state.images;
            const onEvent=async(item,index)=>{
                if(item.private_key==null || item.is_updated==1){
                    count++;
                }
                if(item.image_updated==1 && item.private_key!=null){
                    await this.getStat(item);
                }
            }
            const onEventType=async(item,index)=>{
                if(item.type=="service"){
                    count_service++;
                }else{
                    count_product++;
                }
            }
            await rep.map(onEvent);
            await rep.map(onEventType);
            this.setState({
                info:{
                    ...this.state.info,
                    article:rep.length,
                    product:count_product,
                    service:count_service,
                }
            })
            if(count>0){
                this.setState({
                    articles:{
                        title:"Produits et services",
                        content:"Quelques informations ne sont pas encore publiées",
                        num:(count/all)*100,
                        icon:"shopping-basket",
                        length:rep.length,
                        list:rep
                    }
                })
            }else{
                this.setState({
                    articles:{
                        title:"Produits et services",
                        content:"Toutes les informations sont publiés",
                        num:0,
                        icon:"shopping-basket",
                        length:rep.length,
                        list:rep
                    }
                })
            }
        } catch (error) {
            
        }
    }

    checkAgents=async()=>{
        try {
            const rep=await AgentModel.get();
            this.setState({
                info:{
                    ...this.state.info,
                    agent:rep.length
                }
            })
        } catch (error) {
            
        }
    }

    checkStocks=async()=>{
        try {
            const rep=await StockModel.get();
            let count=0;
            let all=rep.length;
            const onEvent=async(item,index)=>{
                if(item.private_key==null || item.is_updated==1){
                    count++;
                }
            }
            await rep.map(onEvent);
            if(count>0){
                this.setState({
                    stocks:{
                        title:"Approvisionnements",
                        content:"Quelques informations ne sont pas encore publiées",
                        num:(count/all)*100,
                        icon:"bus",
                        length:rep.length,
                        list:rep
                    }
                })
            }else{
                this.setState({
                    stocks:{
                        title:"Approvisionnements",
                        content:"Toutes les informations sont publiés",
                        num:0,
                        icon:"bus",
                        length:rep.length,
                        list:rep
                    }
                })
            }
        } catch (error) {
            
        }
    }

    checkDevises=async()=>{
        try {
            const rep=await DeviseModel.get();
            let count=0;
            let all=rep.length;
            this.setState({
                info:{
                    ...this.state.info,
                    devise:rep.length
                }
            })
            const onEvent=async(item,index)=>{
                if(item.private_key==null || item.is_updated==1){
                    count++;
                }
            }
            await rep.map(onEvent);
            if(count>0){
                this.setState({
                    devises:{
                        title:"Devises",
                        content:"Quelques informations ne sont pas encore publiées",
                        num:(count/all)*100,
                        icon:"money",
                        length:rep.length,
                        list:rep
                    }
                })
            }else{
                this.setState({
                    devises:{
                        title:"Devises",
                        content:"Toutes les informations sont publiés",
                        num:0,
                        icon:"money",
                        length:rep.length,
                        list:rep
                    }
                })
            }
        } catch (error) {
            
        }
    }

    checkSales=async()=>{
        try {
            const rep=await BuyModel.get();
            const customer=await CustomerModel.get();
            let count=0;
            let all=rep.length+customer.length;
            this.setState({
                info:{
                    ...this.state.info,
                    customer:customer.length
                }
            })
            const onEvent=async(item,index)=>{
                if(item.private_key==null || item.is_updated==1){
                    console.log('sale yango '+index,item);
                    count++;
                }
            }
            await rep.map(onEvent);
            await customer.map(onEvent);
            if(count>0){
                this.setState({
                    sales:{
                        title:"Ventes et clients",
                        content:"Quelques informations ne sont pas encore publiées",
                        num:(count/all)*100,
                        length:rep.length,
                        lengthCustomer:customer.length,
                        list:rep,
                        listCustomer:rep,
                    }
                })
            }else{
                this.setState({
                    sales:{
                        title:"Ventes et clients",
                        content:"Toutes les informations sont publiés",
                        num:0,
                        length:rep.length,
                        lengthCustomer:customer.length,
                        list:rep,
                        listCustomer:rep,
                    }
                })
            }
        } catch (error) {
            
        }
    }

    checkInfo=()=>{
        try {
            
        } catch (error) {
            
        }
    }

    getDataImpact=async()=>{
        try {
            const sale=this.state.shop.webBuys.length-this.state.sales.length;
            const customer=this.state.shop.webCustomer.length-this.state.sales.lengthCustomer;
            const stock=this.state.shop.webStocks.length-this.state.stocks.length;
            const article=this.state.shop.webArticles.length-this.state.articles.length;
            const cat=this.state.shop.webCategories.length-this.state.categories.length;
            const link=this.state.shop.webLink.length-this.state.categories.lengthLink;
            const devise=this.state.shop.webDevises.length-this.state.devises.length
            console.log('SALE',sale);
            console.log('CUSTOMER',customer);
            console.log('STOCK',stock);
            console.log('ARTICLE',article);
            console.log('CAT',cat);
            console.log('LINK',link);
            console.log('DEVISE',devise);
            let count=0;
            let sum=this.state.shop.webBuys.length+this.state.shop.webCustomer.length;
            sum+=this.state.shop.webStocks.length+this.state.shop.webArticles.length;
            sum+=this.state.shop.webCategories.length+this.state.shop.webLink.length;
            sum+=this.state.shop.webDevises.length;
            console.log('sum',sum);
            if(sale>0){
                count+=sale;
            }
            if(customer>0){
                count+=customer;
            }
            if(stock>0){
                count+=stock;
            }
            if(article>0){
                count+=article;
            }
            if(cat>0){
                count+=cat;
            }
            if(link>0){
                count+=link;
            }
            if(devise>0){
                count+=devise;
            }
            let percent=0;
            if(sum>0){
                percent=(count/sum)*100
            }
            console.log('count',count);
            this.setState({data:percent});
        } catch (error) {
            console.log('error',error);
        }
    }

    getSize=(list=[])=>{
        let rep="0 octets";
        console.log('LIST:'+list.length,list);
        try {
            let size=0;
            list.map((item,index)=>{
                size+=item.size;
            })
            if(size<Math.pow(2,10)){
                rep=size+" octets"
            }else if(size>=Math.pow(2,10) && size<Math.pow(2,20)){
                //kilo
                const calc=size/Math.pow(2,10);
                rep=calc.toFixed(2)+" Ko";
            }else if(size>=Math.pow(2,20) && size<Math.pow(2,30)){
                //mega
                const calc=size/Math.pow(2,20);
                rep=calc.toFixed(2)+" Mo";
            }else if(size>=Math.pow(2,30) && size<Math.pow(2,40)){
                //giga
                const calc=size/Math.pow(2,30);
                rep=calc.toFixed(2)+" Go";
            }
        } catch (error) {
            
        }
        this.setState({imageSize:rep})
    }
     
    onLoad=async()=>{
        try {
            this.setState({images:[]});
            this.images=[];
            if(this.props.setting.internet==true){
                /*await this.checkSociety();
                await this.getDataImpact();*/
            }
            this.setState({message:`Chargement... ${((1/6)*100).toFixed(0)} %`});
            await this.checkAgents();
            this.setState({message:`Chargement... ${((2/6)*100).toFixed(0)} %`});
            await this.checkCategories();
            this.setState({message:`Chargement... ${((3/6)*100).toFixed(0)} %`});
            await this.checkArticles();
            this.setState({message:`Chargement... ${((4/6)*100).toFixed(0)} %`});
            await this.checkStocks();
            this.setState({message:`Chargement... ${((5/6)*100).toFixed(0)} %`});
            await this.checkSales();
            this.setState({message:`Chargement... ${((6/6)*100).toFixed(0)} %`});
            await this.checkDevises();
            if(this.state.isLoading==true){
                await this.getSize(this.images);
                this.setState({isLoading:false})
            }
            if(this.state.showProgress==true){
                await this.getSize(this.images);
                this.setState({showProgress:false})
            }
        } catch (error) {
            
        }
    }

    checkSociety=async()=>{
        try {
            const shop=this.props.shop.value;
            const rep=await SocietyWebModel.recover(shop.private_key);
            if(rep.status==200){
                //shop
                console.log('awa normal');
                const webShop=rep.response.society;
                const webDevises=rep.response.devises;
                const webCategories=rep.response.categories;
                const webAgents=rep.response.agents;
                const webArticles=rep.response.articles;
                const webStocks=rep.response.stocks;
                const webBuys=rep.response.buys;
                const webCustomer=rep.response.customers;
                const webLink=rep.response.links;
                const webRights=rep.response.rights;
                this.setState({
                    shop:{
                        webShop,
                        webDevises,
                        webCategories,
                        webAgents,
                        webArticles,
                        webStocks,
                        webBuys,
                        webCustomer,
                        webLink,
                        webRights,
                    },
                    societyChecking:true
                })
            }
        } catch (error) {
            console.log('shop',error);
        }
    }

    async componentDidMount(){
        try {
            this.setState({
              images:[]  
            })
            await this.onLoad();
        } catch (error) {
            
        }
    }
    async componentDidUpdate(){
        try {
            if(this.props.service.dashboard==true && this.state.showProgress==true){
                await this.onLoad();
                const action={type:S.EVENT.OnExecuteService,dashboard:false}
                this.props.dispatch(action);
            }
        } catch (error) {
            
        }
    }

    componentStat=()=>{
        const count=(this.state.articles.num>0 || this.state.categories.num>0 || this.state.sales.num>0 || this.state.stocks.num>0);
        return(
            <View>
                <View>
                    <Text style={{ fontFamily:'Poppins-Black',paddingLeft:10,fontSize:16 }}>
                        Transfère des données
                    </Text>
                </View>
                <Banner 
                    visible={this.state.showBanner}
                    actions={[
                        {
                            label:"Fermer",
                            onPress:()=>this.setState({showBanner:false}),
                        }
                    ]}
                >
                    <Text style={{ fontFamily:'Poppins-Light' }}><Text style={{ fontFamily:"Poppins-Bold" }}>Ligal platform</Text> vous permet de gérer votre boutique à partir de n’importe où ; vos données traitées seront automatiquement transférées dans votre espace en ligne.
                     Il peut se faire que ce transfert automatique ne fonctionne pas pour quelques raisons ; ainsi, <Text style={{ fontFamily:'Poppins-Italic' }}>ligal-apply</Text> vous permet de faire un transfert manuel.</Text>
                </Banner>
                <View>
                    <CardDashbord
                        content={this.state.categories.content}
                        title={this.state.categories.title}
                        num={this.state.categories.num}
                        icon={this.state.categories.icon}
                        type={this.state.categories.type}
                    />
                    <CardDashbord
                        content={this.state.articles.content}
                        title={this.state.articles.title}
                        num={this.state.articles.num}
                        icon={this.state.articles.icon}
                        type={this.state.articles.type}
                    />
                    <CardDashbord
                        content={this.state.stocks.content}
                        title={this.state.stocks.title}
                        num={this.state.stocks.num}
                        icon={this.state.stocks.icon}
                        type={this.state.stocks.type}
                    />
                    <CardDashbord
                        content={this.state.sales.content}
                        title={this.state.sales.title}
                        num={this.state.sales.num}
                        icon={this.state.sales.icon}
                        type={this.state.sales.type}
                    />
                    <CardDashbord
                        content={this.state.devises.content}
                        title={this.state.devises.title}
                        num={this.state.devises.num}
                        icon={this.state.devises.icon}
                        type={this.state.devises.type}
                    />
                </View>
                <View style={{ alignItems:'flex-end',padding:20,paddingTop:10,paddingBottom:10 }}>
                    <Button
                        title="Transfert manuel"
                        disabled={!count}
                        onPress={count==true?()=>this.onUpload():()=>{}}
                    />
                </View>
            </View>
        )
    }

    onUpload=()=>{
        try {
            this.setState({message:"Patientez SVP"})
            this.showProgress=true;
            const action={type:S.EVENT.OnExecuteService,article:true,buy:true,stock:true,category:true,shop:true,devise:true,customer:true,work:true,right:true,link:true}
            this.props.dispatch(action)

        } catch (error) {
            
        }
    }

    getElement(num,rubrique="",feminin=""){
        let rep="";
        try {
            if(num==0){
                rep=`aucun${feminin} ${rubrique}`;
            }
            else if(num>0){
                rep=num+ " "+rubrique+""+(num>1?'s':"");
            }
        } catch (error) { 
        }
        return rep;
    }

    componentInfo=()=>{
        return(
            <View style={{ paddingBottom:10 }}>
                <Text style={{ fontFamily:'Poppins-Black',padding:2,paddingLeft:10,paddingRight:10,fontSize:16 }}>
                    Extrait de toutes les informations
                </Text>
                <ScrollView horizontal>
                    <CardDashboardInfo
                        title="Agent"
                        content={this.getElement(this.state.info.agent,"agent","")}
                        color={R.color.colorAccent}
                    />
                    <CardDashboardInfo
                        title="Devises"
                        content={this.getElement(this.state.info.devise,'monnaie','e')}
                        color={R.color.colorPrimary}
                    />
                    <CardDashboardInfo
                        title="Tous les articles"
                        content={this.getElement(this.state.info.article,'article','')}
                        color={R.color.colorPrimaryDark}
                    />
                    <CardDashboardInfo
                        title="Produits"
                        content={this.getElement(this.state.info.product,'produit','')}
                        color={R.color.colorAccent}
                    />
                    <CardDashboardInfo
                        title="Services"
                        content={this.getElement(this.state.info.service,'service','')}
                        color={R.color.background}
                    />
                    <CardDashboardInfo
                        title="Clients"
                        content={this.getElement(this.state.info.customer,'client','')}
                        color={R.color.colorPrimaryDark}
                    />
                </ScrollView>
            </View>
        )
    }

    onUploadImage=async()=>{
        try {
            const count=this.state.images.length;
            let step=0;
            isConnected().then(async p=>{
                if(p==true){
                    this.setState({showProgress:true});
                    const familles=await FamilyModel.get();
                    const articles=await ArticleModel.get();
                    let passer=false;
                    const onEvent=async(item,index)=>{
                        if(item.image_updated==1 && item.private_key!=null){
                            const uri=getImageLocal(item.logo);
                            const type="image/jpeg";
                            const name=item.logo;
                            const data=new FormData();
                            data.append('logo',{uri,type,name});
                            console.log('data photo',data);
                            const web=await FamilleWebModel.updatePhoto(item.private_key,data);
                            console.log('web',web);
                            if(web.status==200){
                                await this.onUpdateFamille(item);
                                passer=true;
                                if(step<count){
                                    step++;
                                    this.setState({message:`${((step/count)*100)} % ... image transférée`})
                                }
                            } 
                        }                        
                    }
                    const onEventArticle=async(item,index)=>{
                        if(item.image_updated==1 && item.private_key!=null){
                            console.log('UPLOAD ARTICLE')
                            const uri=getImageLocal(item.logo);
                            const type="image/jpeg";
                            const name=item.logo;
                            const data=new FormData();
                            data.append('logo',{uri,type,name});
                            const web=await ArticleWebModel.updatePhoto(item.private_key,data);
                            console.log('web article',web);
                            if(web.status==200){
                                passer=true;
                                await this.onUpdateArticle(item);
                                if(step<count){
                                    step++;
                                    this.setState({message:`${((step/count)*100)} % ... image transférée`})
                                }
                            } 
                        }                        
                    }
                    await familles.map(onEvent);
                    await articles.map(onEventArticle);
                    if(passer){
                        await this.onLoad();
                    }
                    this.setState({showProgress:false})
                    if(!passer){
                        Alert.alert("Transfère des données","Le transfère n'a as abouti");
                    }
                }else{
                    this.setState({showError:true})
                    this.setState({showProgress:false})
                }
            })
        } catch (error) {
            console.log('erd',error)
        }
    }

    onUpdateFamille=async(item)=>{
        try {
            await FamilyModel.update({
                image_updated:0,
                id:item.id
            })
        } catch (error) {
            
        }
    }
    onUpdateArticle=async(item)=>{
        try {
            await ArticleModel.update({
                image_updated:0,
                id:item.id
            })
        } catch (error) {
            
        }
    }

    componentFile=()=>{
        return(
            <View>
                <Banner 
                    visible={this.state.showBannerFile}
                    actions={[
                        {
                            label:"Fermer",
                            onPress:()=>this.setState({showBannerFile:false}),
                        }
                    ]}
                >
                    <Text style={{ fontFamily:'Poppins-Light' }}><Text style={{ fontFamily:"Poppins-Bold" }}>Ligal Apply </Text> ne transfère ni ne télécharge automatiquement des images liées à vos activités.
                    C’est vous qui décidez quand transférer et télécharger des images. Ces images concernent des logos des articles et catégories, des photos des clients.</Text>
                </Banner>
            <Card containerStyle={{ backgroundColor:R.color.colorPrimary,marginBottom:20 }}>
                <Card.Title style={{ fontFamily:'Poppins-Black',color:R.color.background,fontWeight:'normal',fontSize:18 }}>
                    Gestion sur des fichiers
                </Card.Title>
                <View style={{ flexDirection:'row',alignItems:'center' }}>
                    <View style={{ padding:8 }}>
                        <Icon
                            name="folder" type="font-awesome" color={R.color.background}
                        />   
                    </View>
                    <View style={{ flex:1,alignItems:'center' }}>
                        <Text style={styles.text}>Taille des images: <Text style={{ fontFamily:'Poppins-Black',color:R.color.background }}>{this.state.imageSize}</Text></Text>
                    </View>
                </View>
                <View style={{ alignItems:'flex-end',paddingTop:10 }}>
                    <Button
                        title="Transférer les images"
                        buttonStyle={{ 
                            backgroundColor:R.color.colorSecondary,
                            padding:10,paddingLeft:20,paddingRight:20
                         }}
                         //type="outline"
                         titleStyle={{ color:R.color.background }}
                         disabled={this.images.length==0}
                         onPress={this.images.length==0?()=>{}:()=>this.onUploadImage()}
                    />
                </View>
            </Card>
            </View>
        )
    }

    onRecoveryData=async()=>{
        try {
            const sale=this.state.shop.webBuys.length-this.state.sales.length;
            const customer=this.state.shop.webCustomer.length-this.state.sales.lengthCustomer;
            const stock=this.state.shop.webStocks.length-this.state.stocks.length;
            const article=this.state.shop.webArticles.length-this.state.articles.length;
            const cat=this.state.shop.webCategories.length-this.state.categories.length;
            const link=this.state.shop.webLink.length-this.state.categories.lengthLink;
            const devise=this.state.shop.webDevises.length-this.state.devises.length
            let count=0;
            let sum=this.state.shop.webBuys.length+this.state.shop.webCustomer.length;
            sum+=this.state.shop.webStocks.length+this.state.shop.webArticles.length;
            sum+=this.state.shop.webCategories.length+this.state.shop.webLink.length;
            sum+=this.state.shop.webDevises.length;
            this.setState({showProgress:true})
            if(cat>0){
                await WebData.category(this.state.shop.webCategories,this.state.categories.list);
            }
            if(link>0){
                
            }
            if(devise>0){
                await WebData.devise(this.state.shop.webDevises,this.state.devises.list);
            }
            if(article>0){
                await WebData.article(this.state.shop.webArticles,this.state.articles.list,this.state.devises.list,this.state.categories.list);
            }
            if(customer>0){
                await WebData.customer(this.state.shop.webCustomer,this.state.sales.listCustomer);
            }
            if(stock>0){
                //await WebData.stock(this.state.shop.webStocks,this.state.articles.list,this.state.stocks.list,this.state.a);
            }
            if(sale>0){
                //await WebData.buy(this.state.shop.webBuys,this.state.)
            }
            this.setState({showProgress:false})
        } catch (error) {
            console.log('error recovery',error)
        }
    }
    componentWeb=()=>{
        return(
            <View>
                <Banner 
                    visible={this.state.showBannerWeb}
                    actions={[
                        {
                            label:"Fermer",
                            onPress:()=>this.setState({showBannerWeb:false}),
                        }
                    ]}
                >
                    <Text style={{ fontFamily:'Poppins-Light' }}><Text style={{ fontFamily:"Poppins-Bold" }}>Ligal Apply </Text> communique automatiquement à vos associées les informations modifiées dans la boutique. Ligal-Apply veuille sur la transmission correcte de ces informations et vous présente la statistique afin que vous vous rassurez de la transmission correcte des données.</Text>
                </Banner>
            <Card containerStyle={{ backgroundColor:R.color.colorPrimary,marginBottom:20 }}>
                <Card.Title style={{ fontFamily:'Poppins-Black',color:R.color.background,fontWeight:'normal',fontSize:18 }}>
                    Ligature des données
                </Card.Title>
                <View style={{ flexDirection:'row',alignItems:'center' }}>
                    <View style={{ padding:8 }}>
                        <Icon
                            name="folder" type="font-awesome" color={R.color.background}
                        />   
                    </View>
                    <View style={{ flex:1,alignItems:'center' }}>
                        <Text style={styles.text}>Risque de perte des données: <Text style={{ fontFamily:'Poppins-Black',color:R.color.background }}>{this.state.data<=100?this.state.data.toFixed(0)+"%":"100%"}</Text></Text>
                    </View>
                </View>
                <View style={{ alignItems:'flex-end',paddingTop:10 }}>
                    <Button
                        title="Récupérer des données "
                        buttonStyle={{ 
                            backgroundColor:R.color.colorSecondary,
                            padding:10,paddingLeft:20,paddingRight:20
                         }}
                         //type="outline"
                         titleStyle={{ color:R.color.background }}
                         disabled={this.state.data==0}
                         onPress={this.state.data==0?()=>{}:()=>this.onRecoveryData()}
                    />
                </View>
            </Card>
            </View>
        )
    }

    render(){
        return(
            this.state.isLoading==true?(
                <Awaiter message={this.state.message} color={R.color.colorSecondary} size={100} key="awaiter" />
            ):(
                <>
                <ScrollView>
                    <this.componentInfo/>
                    <this.componentStat/>
                    <this.componentFile/>
                    {
                        /*this.props.setting.internet==true?(
                            <this.componentWeb />
                        ):null*/
                    }
                </ScrollView>
                 <ProgressDialog
                    message={this.state.message}
                    title="Chargement"
                    activityIndicatorColor={R.color.colorSecondary}
                    activityIndicatorSize="large"
                    visible={this.showProgress}
                    animationType="slide"
                    buttons={
                        ()=>(
                            <Button
                                type='clear'
                                onPress={()=>this.setState({showProgress:false})}
                            />
                        )
                    }
                 />
                 <ProgressDialog
                    message={this.state.message}
                    title="Chargement normal"
                    activityIndicatorColor={R.color.colorSecondary}
                    activityIndicatorSize="large"
                    visible={this.state.showProgress}
                    animationType="slide"
                    key="progress"
                    buttons={
                        ()=>(
                            <Button
                                type='clear'
                                onPress={()=>this.setState({showProgress:false})}
                                title="boom"
                            />
                        )
                    }
                 />
                 <InternetError
                    onClose={()=>this.setState({showError:false})}
                    visible={this.state.showError}
                    key="internet"
                 />
                </>
            )
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(App);