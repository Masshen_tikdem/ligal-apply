import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { View } from 'react-native';
import { Avatar, Button, ListItem, Text } from 'react-native-elements';
import Empty from '../../components/EmptyMessage';
import Awaiter from '../../components/Waiter';
import R from '../../resources/styles.json';
import * as DeviseModel from '../../model/local/devise';
import * as Manager from '../../Manager';
import { FAB } from 'react-native-paper';
import ModalDevise from '../../components/ModalCreateDevise';
import { Alert } from 'react-native';
import { Dialog, ProgressDialog } from 'react-native-simple-dialogs';
import { connect } from 'react-redux';
import S from '../../resources/settings.json';
import MenuItem from '../../components/MenuItem';
import * as Shared from '../../service/shared';


const mapStateToProps = (state) => {
    return {
        user:state.user,
        shop:state.shop,
        article:state.article,
        right:state.right
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

class App extends Component{

    constructor(props){
        super(props)
        this.state={
            isLoading:true,
            list:[],
            showDialog:false,
            showDeviseUpdate:false,
            devise:'',
            signe:'',
            showMenu:false,
            currentDevise:{
                name:"",
                signe:"",
                id:0,
                type:0,
                logo:null
            },
            showProgress:false,
            logo:null,
            update:false,
            showCreate:false,
            showUpdate:false
        }
    }
    async componentDidMount(){
        try {
            console.log('right devise',this.props.right)
            if(Manager.write(this.props.right.devise)){
                this.setState({showCreate:true})
            }
            if(Manager.update(this.props.right.devise)){
                this.setState({showUpdate:true})
            }
            
            await this.onLoad();
        } catch (error) {
            
        }
    }
    onLoad=async()=>{
        try {
            const rep=await DeviseModel.get();
            this.setState({
                list:rep
            })
            if(this.state.isLoading==true){
                this.setState({isLoading:false})
            }
        } catch (error) {
            
        }
    }
    onShowMenu=(item)=>{
        try {
            if(this.state.showUpdate){
                this.setState({
                    currentDevise:{
                        name:item.name,
                        signe:item.signe,
                        type:item.type,
                        id:item.id,
                        logo:item.logo
                    },
                    showMenu:true,
                    update:true
                })
            }else{
                Alert.alert("Vérification","Vous n'avez pas d'éccès");
            }
        } catch (error) {
            
        }
    }
    renderItem=({item})=>{
        return(
            <View style={{margin:10,elevation:10,flex:1}}>
                <ListItem onPress={()=>this.onShowMenu(item)} containerStyle={{backgroundColor:R.color.background,elevation:10,padding:5}}>
                    <Avatar
                        title={Manager.capitalize(item.signe).toUpperCase()}
                        titleStyle={{
                            fontSize:28,fontFamily:'Poppins-Black',color:R.color.colorPrimaryDark,
                            textShadowColor:R.color.colorPrimary,
                            textShadowOffset:{height:1,width:2},
                            textShadowRadius:1
                        }}
                        size='large'
                    />
                    <View style={{paddingLeft:10}}>
                        <Text numberOfLines={1} style={{padding:3,fontSize:14,fontFamily:'Poppins-Regular'}}>
                            {Manager.capitalize(item.name).toUpperCase()}
                        </Text>
                        <Text numberOfLines={1} style={{fontFamily:'Poppins-Medium',opacity:0.8,fontSize:12,padding:2,paddingTop:0}}>
                            {Manager.capitalize(item.signe).toUpperCase()}
                        </Text>
                        <Text numberOfLines={1} style={{padding:5,paddingLeft:10,paddingRight:10,marginBottom:5,elevation:10,fontFamily:'Poppins-Black',borderRadius:10,opacity:0.8,backgroundColor:R.color.colorSecondary,color:R.color.background}}>
                            {item.type==1?"Devise de base":"Devise auxilliaire"}
                        </Text>
                    </View>
                </ListItem>
            </View>
        )
    }
    
    signeIsUsed=()=>{
         let rep=false;
         try {
            this.state.list.map(item=>{
                if(Manager.capitalize(item.signe).toLowerCase()==Manager.capitalize(this.state.signe).toLowerCase()){
                    rep=true;
                    return rep;
                }
            })
        } catch (error) {
            
        }
        return rep;
    }
    signeIsUsedUpdate=()=>{
        let rep=false;
        try {
           this.state.list.map(item=>{
               if(item.id!=this.state.currentDevise.id && Manager.capitalize(item.signe).toLowerCase()==Manager.capitalize(this.state.signe).toLowerCase()){
                   rep=true;
                   return rep;
               }
           })
       } catch (error) {
           
       }
       return rep;
   }
    nameIsUsed=()=>{
        let rep=false;
        try {
           this.state.list.map(item=>{
               if(Manager.capitalize(item.name).toLowerCase()==Manager.capitalize(this.state.devise).toLowerCase()){
                   rep=true;
                   return rep;
               }
           })
       } catch (error) {
           
       }
       return rep;
   }
   nameIsUsedUpdate=()=>{
    let rep=false;
    try {
       this.state.list.map(item=>{
           if(Manager.capitalize(item.name).toLowerCase()==Manager.capitalize(this.state.devise).toLowerCase() && item.id!=this.state.currentDevise.id){
               rep=true;
               return rep;
           }
       })
   } catch (error) {
       
   }
   return rep;
}
   onAdd=async()=>{
       try {
           const name=this.state.devise;
           const signe=this.state.signe;
           const user=this.props.user.value;
           const shop=this.props.shop.value;
           if(name.trim()!='' && signe.trim()!='' && !this.nameIsUsed() && !this.signeIsUsed()){
               const rep=await DeviseModel.store({
                   name:name,
                   signe:signe,
                   status:1,
                   type:0,
                   is_updated:1
               })
               if(rep!=null){
                   Alert.alert("Enregistrement","Vous avez ajouté une devise avec succès");
                   this.setState({
                       showDialog:false,
                       signe:'',
                       devise:''
                   })
                    Shared.devise({
                        id:rep.id,
                        name:rep.name,
                        private_key:rep.private_key,
                        shop:shop,
                        signe:rep.signe,
                        status:rep.status,
                        type:rep.type,
                        user:user
                    })
                    await this.onLoad()
               }
           }else{
               Alert.alert("Vérification","Remplissez toutes les cases sans erreur!")
           }
       } catch (error) {
           
       }
   }
   onUpdate=async()=>{
    try {
        const name=this.state.devise;
        const signe=this.state.signe;
        const user=this.props.user.value;
        const shop=this.props.shop.value;
        const id=this.state.currentDevise.id;
        if(name.trim()!='' && signe.trim()!='' && !this.nameIsUsedUpdate() && !this.signeIsUsedUpdate()){
            this.setState({ProgressDialog:true})
            const rep=await DeviseModel.update({
                name:name,
                signe:signe,
                status:1,
                id,
                is_updated:1
            })
            if(rep!=null){
                Alert.alert("Modification","Vous avez modifié la devise avec succès");
                this.setState({
                    showDeviseUpdate:false,
                    signe:'',
                    devise:''
                })
                await this.onLoad()
                Shared.devise({
                    id:rep.id,
                    name:rep.name,
                    private_key:rep.private_key,
                    shop:shop,
                    signe:rep.signe,
                    status:rep.status,
                    type:rep.type,
                    user:user
                })
            }
            this.setState({showProgress:false})
        }else{
            Alert.alert("Vérification","Remplissez toutes les cases sans erreur!")
        }
    } catch (error) {
        
    }
   }
   onShowUpdate=()=>{
       try {
           this.setState({
               signe:this.state.currentDevise.signe,
               devise:this.state.currentDevise.name,
               showMenu:false,
               showDeviseUpdate:true
           })
       } catch (error) {
           
       }
   }
   onDefine=async()=>{
       try {
           const type=this.state.currentDevise.type;
           const id=this.state.currentDevise.id;
           const user=this.props.user.value;
            const shop=this.props.shop.value;
           this.setState({showMenu:false})
           if(type==0){
               this.setState({showProgress:true})
               this.state.list.map(item=>{
                   if(item.id!=id){
                       DeviseModel.update({id:item.id,type:0,is_updated:1}).then(p=>{
                           if(p!=null){
                            Shared.devise({
                                id:p.id,
                                name:p.name,
                                private_key:p.private_key,
                                shop:shop,
                                signe:p.signe,
                                status:p.status,
                                type:p.type,
                                user:user
                            })
                           }
                       })
                   }
               })
               DeviseModel.update({id:id,type:1,is_updated:1}).then(p=>{
                   if(p!=null){
                        this.onLoad()
                        Shared.devise({
                            id:p.id,
                            name:p.name,
                            private_key:p.private_key,
                            shop:shop,
                            signe:p.signe,
                            status:p.status,
                            type:p.type,
                            user:user
                        })
                   }
               })
               this.setState({showProgress:false})
           }
       } catch (error) {
       }
   }
    render(){
        return(
            <>
            {
                this.state.isLoading==true?(
                    <Awaiter color={R.color.colorSecondary} size={100} key='awaiter' />
                ):this.state.list.length==0?(
                    <Empty title="Aucune devise dans la shop" />
                ):(
                    <FlatList
                        data={this.state.list}
                        renderItem={this.renderItem}
                        key='list'
                        ref={ref=>this.flat=ref}
                    />
                )
            }
            <ModalDevise
                logo='p'
                onChangeSigne={value=>this.setState({signe:value})}
                onChangeText={value=>this.setState({devise:value})}
                onClose={()=>this.setState({showDialog:false})}
                signe={this.state.signe}
                value={this.state.devise}
                visible={this.state.showDialog}
                status={this.nameIsUsed()}
                statusSigne={this.signeIsUsed()}
                key='modal' 
                onPress={()=>this.onAdd()}
            />
            <ModalDevise
                logo={this.state.logo!=null?this.state.logo:Manager.getImageLocal(this.state.currentDevise.logo)}
                onChangeSigne={value=>this.setState({signe:value})}
                onChangeText={value=>this.setState({devise:value})}
                onClose={()=>this.setState({showDeviseUpdate:false})}
                signe={this.state.signe}
                value={this.state.devise}
                visible={this.state.showDeviseUpdate}
                status={this.nameIsUsedUpdate()}
                statusSigne={this.signeIsUsedUpdate()}
                key='modal-update' 
                onPress={()=>this.onUpdate()}
                update={this.state.update}
            />
            {
                this.state.showCreate?(
                    <FAB
                        style={{position:'absolute',bottom:0,margin:30,right:0,backgroundColor:R.color.colorPrimaryDark,elevation:10}}
                        icon="plus"
                        animated
                        onPress={()=>this.setState({showDialog:true,update:false})}
                    />
                ):null
            }
            <Dialog
                key='diag-menu'
                animationType='fade'
                visible={this.state.showMenu}
                onTouchOutside={()=>this.setState({showMenu:false})}
                dialogStyle={{maxWidth:300,alignSelf:'center',padding:0,elevation:50}}
            >
                <MenuItem
                    title="Modifier"
                    key='update'
                    onPress={()=>this.onShowUpdate()}
                    icon="edit"
                    type="font-awesome"
                />
                {
                    this.state.currentDevise.type==0?(
                        <MenuItem
                            title="Comme monnaie de base"
                            key='type-devise'
                            onPress={()=>this.onDefine()}
                            color={R.color.colorAccent}
                        />
                    ):null
                }
                <MenuItem
                    title="Conversions"
                    key='conversion'
                    icon="archive"
                    type="entypo"
                />
                               
        </Dialog>
        <ProgressDialog
            message="Patientez svp!"
            title="Chargement"
            activityIndicatorColor={R.color.colorSecondary}
            activityIndicatorSize='large'
            animationType="fade"
            visible={this.state.showProgress}
        />
            </>
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(App)