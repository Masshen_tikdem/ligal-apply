import React from 'react';
import { Button } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import { ProgressDialog } from 'react-native-simple-dialogs';
import { useSelector } from 'react-redux';
import Waiter from '../../components/Waiter';
import R from '../../resources/styles.json';
import { DashDownload } from './dashViews/DashDownload';
import { DashFile } from './dashViews/DashFile';
import { DashInfo } from './dashViews/DashInfo';
import { DashStat } from './dashViews/DashStat';
import { checkAgents, checkArticles, checkCategories, checkDevises, checkSales, checkStocks, getWebData } from './dashViews/treatment';


function DashboardScreen(){

    const[loading,setLoading]=React.useState(true);
    const[list,setList]=React.useState([]);
    const[countStat,setCountStat]=React.useState(0);
    const[showProgress,setShowProgress]=React.useState(false);
    const[message,setMessage]=React.useState("");
    const[webData,setWebData]=React.useState({});
    const shop=useSelector(state=>state.shop.value);
    const[articles,setArticle]=React.useState([]);
    const[categories,setCategory]=React.useState([]);

    const onLoading=async()=>{
        let count=0;
        const items=[];
        const elements=[];
        const stats=[];
        const images=[];
        const agent=await checkAgents();
        items.push(agent.info);
        const article=await checkArticles();
        items.push(article.info);
        items.push(article.info_product);
        items.push(article.info_service);
        stats.push(article.status);
        setArticle(article.articles);
        article.images.map(p=>{images.push(p)});
        count+=article.status.num;
        const cat=await checkCategories();
        items.push(cat.info)
        stats.push(cat.status);
        setCategory(cat.categories);
        cat.images.map(p=>{images.push(p)});
        const sale=await checkSales();
        items.push(sale.info);
        stats.push(sale.status);
        const devise=await checkDevises();
        items.push(devise.info)
        stats.push(devise.status);
        const supply=await checkStocks();
        stats.push(supply.status);
        elements.push({uid:"1",info:items})
        elements.push({uid:"2",download:{}})
        elements.push({uid:"3",status:stats})
        elements.push({uid:"4",img:images})
        const web=await getWebData(shop);
        setWebData(web);
        setList(elements)
        setLoading(false);
    }
    React.useEffect(()=>{
        onLoading();
    },[]);

    const renderItem=({item})=>{
        return(
            <React.Fragment>
                {(item.info!=undefined && item.info!=null) && (
                    <DashInfo
                        list={item.info}
                    />
                )}
                {item.status!=null && (
                    <DashStat
                        list={item.status}
                        count={countStat}
                        setLoad={setShowProgress}
                    />
                )}
                {item.img!=null && (
                    <DashFile
                        images={item.img}
                        setLoad={setShowProgress}
                    />
                )}
                {item.download!=null && (
                    <DashDownload
                        web={webData}
                        articles={articles}
                        categories={categories}
                        setLoad={setShowProgress}
                    />
                )}
            </React.Fragment>
        )
    }


    return(
        <React.Fragment>
            {loading && (
                <Waiter
                    size={100} color={R.color.colorPrimary}
                    message="chargement des données"
                />
            )}
            {!loading && (
                <FlatList
                    data={list}
                    renderItem={renderItem}
                    key={"dashboard"}
                    keyExtractor={(v,i)=>v.uid+i}
                />
            )}
            <ProgressDialog
                message={message}
                title="Chargement"
                activityIndicatorColor={R.color.colorSecondary}
                activityIndicatorSize="large"
                visible={showProgress}
                animationType="slide"
                buttons={
                    ()=>(
                        <Button
                            type='clear'
                            onPress={()=>setShowProgress(false)}
                        />
                    )
                }
                 />
        </React.Fragment>
    )
}

export {DashboardScreen}