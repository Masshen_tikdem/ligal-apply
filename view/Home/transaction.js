import React, { Component } from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-elements';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import stock from './transaction/stock';
import sale from './transaction/sale';
import alert from './transaction/alert';
import balanceSheet from './transaction/balanceSheet';
import { enableScreens } from 'react-native-screens';


const Tab=createMaterialTopTabNavigator();



function App(){

    return(
        <>
            <Tab.Navigator tabBarOptions={{ scrollEnabled:true }}>
                <Tab.Screen name="Stock" component={stock} options={{title:"Les stocks"}} />
                <Tab.Screen name="Sale" component={sale} options={{title:"Les ventes"}} />
                <Tab.Screen name="Alert" component={alert} options={{title:"Alertes"}} />
                {/*<Tab.Screen name="BalanceSheet" component={balanceSheet} options={{title:"Bilan"}} />*/}
            </Tab.Navigator>
        </>
    )
}
export default (App)