import React, { Component } from 'react';
import { View } from 'react-native';
import EmptyMessage from '../../components/EmptyMessage';
import Awaiter from '../../components/Waiter';
import R from '../../resources/styles.json';
import CardInfo from '../../components/info';
import { FlatList } from 'react-native';
import * as InfoWebModel from '../../model/web/info';
import { capitalize, getFullName, getImage } from '../../Manager';
import FormInfo from '../../components/FormInfo';
import { FAB } from 'react-native-paper';



class App extends Component{

    constructor(props){
        super(props)
        this.state={
            isLoading:true,
            list:[],
            filter:[],
            showInfo:false,
            current:{
                photo:null,
                title:"",
                content:"",

            }
        }
    }

    renderItem=({item})=>{
        return(
            <CardInfo
                title={item.title}
                content={item.content}
                avatar={(item.avatar==null || item.avatar==undefined)?item.user_photo:item.avatar}
                autor={(item.author==null || item.author==undefined)?getFullName(item.first_name,item.last_name):capitalize(item.author).toUpperCase()}
                titleAvatar={item.author}
                date={item.created_at}
                image={getImage(item.photo)}
                onDetail={()=>this.onShow(item)}
            />
        )
    }
    async componentDidMount(){
        try {
            await this.onLoad();
        } catch (error) {
        }
    }
    onLoad=async()=>{
        try {
            const rep=await InfoWebModel.get();
            if(rep.status==200){
                this.setState({
                    list:rep.response
                })
            }
            if(this.state.isLoading==true){
                this.setState({isLoading:false})
            }
        } catch (error) {
            
        }
    }
    onShow=(item)=>{
        try {
            this.setState({
                current:{
                    photo:item.photo,
                    title:item.title,
                    content:item.content,
                },
                showInfo:true
            })
        } catch (error) {
            
        }
    }

    render(){
        return(
            this.state.isLoading==true?(
                <Awaiter color={R.color.colorSecondary} size={100} key='awaiter' />
            ):this.state.list.length==0?(
                <EmptyMessage title="Aucune information disponible" />
            ):(
                <>
                <View style={{ flex:1 }}>
                    <FlatList
                        data={this.state.list}
                        renderItem={this.renderItem}
                        key="ddf"
                        keyExtractor={item=>item.id}
                    />
                    <FAB
                        icon="refresh"
                        style={{ position:"absolute",bottom:0,right:0,margin:20,backgroundColor:R.color.colorSecondary }}
                        onPress={()=>this.onLoad()}
                        color={R.color.background}
                    />
                </View>
                <FormInfo
                    content={this.state.current.content}
                    onClose={()=>this.setState({showInfo:false})}
                    photo={this.state.current.photo}
                    title={this.state.current.title}
                    visible={this.state.showInfo}
                    key="ssds"
                />
                </>

            )
        )
    }
}
export default (App)