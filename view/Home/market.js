import React, { Component } from 'react';
import { View } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import LinearGradient from 'react-native-linear-gradient';
import R from '../../resources/styles.json';
import category from './market/category';
import articles from './market/articles';
import stats from './market/stats';


const Tab=createMaterialBottomTabNavigator();

class App extends Component{

    constructor(props){
        super(props)
    }

    

    render(){
        return(
            <Tab.Navigator
                barStyle={{backgroundColor:R.color.colorPrimary}}
                activeColor={R.color.colorSecondary}
                sceneAnimationEnabled={true}
                shifting
                
            >
                <Tab.Screen name="Category" component={category}
                    options={{
                        title:'Catégories',
                        tabBarIcon:({color})=>(
                            <Icon name='shopping-bag' type='font-awesome' color={color} />
                        ),
                    }}
                />
                <Tab.Screen name="Article" component={articles}
                    options={{
                        title:'Articles',
                        tabBarIcon:({color})=>(
                            <Icon name='shopping-basket' type='font-awesome' color={color} />
                        ),
                    }}
                />

                <Tab.Screen name="Stats" component={stats}
                    options={{
                        title:'Stats',
                        tabBarIcon:({color})=>(
                            <Icon name='bar-chart-o' type='font-awesome' color={color} />
                        )
                    }}
                />
            </Tab.Navigator>
        )
    }
}

export default (App);