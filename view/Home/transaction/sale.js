import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import * as BuyModel from '../../../model/local/buy';
import CardJournal from '../../../components/CardJournal';
import DateTime from 'date-and-time';
import fr from 'date-and-time/locale/fr';
import EmptyMessage from '../../../components/EmptyMessage';
import Awaiter from '../../../components/Waiter';
import R from '../../../resources/styles.json';
import { FlatList } from 'react-native';
import * as Manager from '../../../Manager';
import { Text } from 'react-native-elements';
import S from '../../../resources/settings.json';
import * as DeviseModel from '../../../model/local/devise';
import { FAB } from 'react-native-paper';
import { ConfirmDialog, ProgressDialog } from 'react-native-simple-dialogs';
import { StyleSheet } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import DateTimePicker from '../../../components/DatePicker';



const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.viewArticle
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

const styles=StyleSheet.create({
    title:{

    }
})

class App extends Component{

    constructor({props,route}){
        super(props)
        this.state={
            route,
            list:[],
            filter:[],
            devises:[],
            isLoading:true,
            showProgress:false,
            count:0,
            total:"0",
            opened:false,
            showDay:false,
            showMonth:false,
            showYear:false,
            title:"",
            mode:"day",
            date:new Date(),
            years:["2021","2022","2023","2024"],
            year:0,
            month:1,
            months:["Janvier",'Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Decembre']
        }
    }
    getPriceArticle=(price,quantity)=>{
        let rep=0;
        try {
            rep=parseFloat(price)*parseInt(quantity);
        } catch (error) {
            
        }
        return rep;
    }
    renderItem=({item})=>{
        const date=Manager.getDateList(item.created_at,this.state.mode);
        return(
            <CardJournal
                title={Manager.getArticleName(item.name,item.description)}
                date={date}
                onPress={()=>this.props.navigation.navigate("Buy",{id:item.id})}
                count={(item.type==S.ENUM.article.service && item.mode==S.ENUM.stockage.devise)?null:item.quantity}
                subTitle={item.description}
                price={Manager.getPrice(this.getPriceArticle(item.price,item.quantity),item.signe,item.remise)}
                logo={Manager.getImageLocal(item.logo)}
            />
        )
    }

    async componentDidMount(){
        this.load()
    }

    openMonth=async()=>{
        try {
            const years=[];
            const date=new Date();
            const year=date.getFullYear()+"";
            this.setState({year,showMonth:true})
        } catch (error) {
        }
    }
    openYear=()=>{
        const date=new Date();
        const year=date.getFullYear()+"";
        this.setState({showYear:true,year})
    }
    searchDay=async()=>{
        try {
            this.setState({showProgress:true})
            const date=this.state.date;
            DateTime.locale(fr);
            const date1=DateTime.format(date,'YYYY-MM-DD 00:00:00');
            const date2=DateTime.format(date,"YYYY-MM-DD 23:59:59");
            let title="Aujourd'hui";
            if(DateTime.isSameDay(new Date(),date)==false){
                title=DateTime.format(date,"dddd, DD MMMM YYYY")
            }
            const rep=await BuyModel.getBetween(date1,date2);
            const total=Manager.getTotal(rep,this.state.devises)
            this.setState({
                count:rep.length,
                total
            })
            this.setState({
                filter:rep,
                title,
                mode:"day"
            })
            this.setState({showDay:false,showProgress:false})
        } catch (error) {
        }
    }
    searchMonth=async()=>{
        try {
            this.setState({showProgress:true})
            let month=this.state.month;
            const year=this.state.year;
            DateTime.locale(fr);
            let format=`${year}-${month}-1 00:00:00`;
            let date=DateTime.parse(format,"YYYY-M-D HH:mm:ss");
            const date1=DateTime.format(date,"YYYY-MM-DD HH:mm:ss");
            let day=30;
            if(month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12){
                day=31;
            }else if(month==2){
                if(year%4==0){
                    day=29;
                }else{
                    day=28
                }
            }
            format=`${year}-${month}-${day} 23:59:59`;
            date=DateTime.parse(format,"YYYY-M-D HH:mm:ss");
            const date2=DateTime.format(date,"YYYY-MM-DD HH:mm:ss");
            let title=DateTime.format(date,"MMMM YYYY");
            const rep=await BuyModel.getBetween(date1,date2);
            const total=Manager.getTotal(rep,this.state.devises)
            this.setState({
                count:rep.length,
                total
            })
            this.setState({
                filter:rep,
                title,
                mode:"month"
            })
            this.setState({showMonth:false,showProgress:false})
        } catch (error) {
        }
    }
    searchYear=async()=>{
        try {
            this.setState({showProgress:true})
            const year=this.state.year;
            let format=`${year}-1-1 00:00:00`;
            DateTime.locale(fr);
            let date=DateTime.parse(format,"YYYY-M-D HH:mm:ss");
            const date1=DateTime.format(date,"YYYY-MM-DD HH:mm:ss");
            format=`${year}-12-31 23:59:59`;
            date=DateTime.parse(format,"YYYY-M-D HH:mm:ss");
            const date2=DateTime.format(date,"YYYY-MM-DD HH:mm:ss");
            let title=year+"";
            const rep=await BuyModel.getBetween(date1,date2);
            const total=Manager.getTotal(rep,this.state.devises)
            this.setState({
                count:rep.length,
                total
            })
            this.setState({
                filter:rep,
                title,
                mode:"year"
            })
            this.setState({showYear:false,showProgress:false})
        } catch (error) {
        }
    }

    load=async()=>{
        try {
            if(this.state.devises.length==0){
                const d=await DeviseModel.get();
                this.setState({
                    devises:d
                })
            }
            let rep=await BuyModel.get()
            this.setState({
                list:rep
            })
            const date1=DateTime.format(new Date(),'YYYY-MM-DD 00:00:00');
            const date2=DateTime.format(new Date(),"YYYY-MM-DD 23:59:59");
            rep=await BuyModel.getBetween(date1,date2);
            this.setState({
                filter:rep,
                title:"Ajourd'hui"
            })
            const total=Manager.getTotal(rep,this.state.devises)
            this.setState({
                count:rep.length,
                total
            })
            if(this.state.isLoading==true){
                this.setState({
                    isLoading:false
                })
            }
        } catch (error) {
        }
    }

    async componentDidUpdate(){
        try {
            /*const article=this.props.article.sale;
            if(article==true){
                const action={type:S.EVENT.onChangeArticle,sale:false}
                this.props.dispatch(action);
                await this.load()
            }*/
        } catch (error) {
        }
    }

    render(){

        return(
            <>
            {
                this.state.isLoading==true?(
                    <Awaiter color={R.color.colorSecondary} size={100} key='awaiter'  />
                ):this.state.filter==0?(
                    <EmptyMessage subTitle={this.state.title} title={"Aucune vente enregistrée"} key='message' />
                ):(
                    <View style={{flex:1}} key='v'>
                        <View style={{backgroundColor:R.color.colorPrimary,padding:10}} key='v2'>
                            <Text style={{color:R.color.background,fontSize:18,fontFamily:'Poppins-Black',textAlign:'center'}}>
                                {this.state.total}
                            </Text>
                            <Text style={{color:R.color.background,fontSize:16,textAlign:'center',fontFamily:'Poppins-Thin'}}>
                                {this.state.count>0?this.state.count+" vente"+(this.state.count>1?'s':"")+" réalisée"+(this.state.count>1?'s':""):""}
                            </Text>
                        </View>
                        <Text style={{fontFamily:'Poppins-light',fontSize:16,padding:10,elevation:10,alignSelf:'center',color:R.color.background,margin:5,backgroundColor:R.color.colorAccent,borderRadius:15}}>
                            {this.state.title}
                        </Text>
                        <FlatList
                            data={this.state.filter}
                            renderItem={this.renderItem}
                            key='list'
                        />
                    </View>
                )
            }
            <FAB.Group
                open={this.state.opened}
                onStateChange={open=>this.setState({opened:!this.state.opened})}
                icon={this.state.opened?'close':'plus'}
                color={R.color.background}
                theme={{
                    animation:{scale:5},
                    colors:{text:R.color.colorPrimaryDark},
                    dark:true,
                    roundness:30
                }}
                fabStyle={{backgroundColor:R.color.colorSecondary}}
                actions={
                    [
                        {
                            icon:'calendar-clock',
                            label:'Un jour',
                            style:{backgroundColor:R.color.colorPrimary},
                            onPress:()=>this.setState({showDay:true})
                        },
                        {
                            icon:'calendar-month-outline',
                            label:'Un mois',
                            style:{backgroundColor:R.color.colorPrimary},
                            onPress:()=>this.openMonth()
                        },
                        {
                            icon:'calendar',
                            label:'Une année',
                            style:{backgroundColor:R.color.colorPrimary},
                            onPress:()=>this.openYear()
                        },
                    ]
                }
            />
            <ConfirmDialog
                key="month"
                visible={this.state.showMonth}
                animationType='slide'
                title="Choisissez un mois"
                onTouchOutside={()=>this.setState({showMonth:false})}
                positiveButton={{title:"Chercher",titleStyle:styles.title,onPress:()=>this.searchMonth()}}
                negativeButton={{title:"Annuler",titleStyle:styles.title,onPress:()=>this.setState({showMonth:false})}}
            >
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    <View style={{flex:2}}>
                        <Picker
                            mode='dropdown'
                            selectedValue={this.state.month}
                            onValueChange={(item,index)=>this.setState({month:item})}
                        >
                            {
                                this.state.months.map((item,index)=>(
                                    <Picker.Item label={item} value={index+1} key={`index-month${index}`} />
                                ))
                            }
                        </Picker>
                    </View>
                    <View style={{flex:1}}>
                        <Picker
                            mode='dropdown'
                            selectedValue={this.state.year}
                            onValueChange={(item,index)=>this.setState({year:item})}
                        >
                            {
                                this.state.years.map((item,index)=>(
                                    <Picker.Item label={item} value={parseInt(item)} key={`index-year${index}`} />
                                ))
                            }
                        </Picker>
                    </View>
                </View>
            </ConfirmDialog>
            <ConfirmDialog
                key="year"
                visible={this.state.showYear}
                animationType='slide'
                onTouchOutside={()=>this.setState({showYear:false})}
                positiveButton={{title:"Chercher",titleStyle:styles.title,onPress:()=>this.searchYear()}}
                negativeButton={{title:"Annuler",titleStyle:styles.title,onPress:()=>this.setState({showYear:false})}}
            >
                <View>
                    <View style={{flex:1}}>
                        <Picker
                            mode='dropdown'
                            selectedValue={this.state.year}
                            onValueChange={(item,index)=>this.setState({year:item})}
                        >
                            {
                                this.state.years.map((item,index)=>(
                                    <Picker.Item label={item} value={parseInt(item)} key={`index-year-year${index}`} />
                                ))
                            }
                        </Picker>
                    </View>
                </View>
            </ConfirmDialog>
            <DateTimePicker
                onTouchOutside={()=>this.setState({showDay:false})}
                onValidate={()=>this.searchDay()}
                onDateChange={value=>this.setState({date:value})}
                date={this.state.date}
                visible={this.state.showDay}
                backgroundColor={R.color.colorPrimaryDark}
                fadeToColor={R.color.colorPrimaryDark}
            />
            <ProgressDialog
                visible={this.state.showProgress}
                message="Patientez SVP!"
                title="Chargement..."
                animationType="slide"
                activityIndicatorSize='large'
                activityIndicatorColor={R.color.colorSecondary}
            />
            </>
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(App)