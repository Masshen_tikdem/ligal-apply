import React,{useState,useEffect} from 'react';
import Awaiter from '../../../components/Waiter';
import EmptyMessage from '../../../components/EmptyMessage';
import R from '../../../resources/styles.json';
import S from '../../../resources/settings.json';
import * as ActivityModel from '../../../model/local/activity';
import * as AgentModel from '../../../model/local/agent';
import * as BuyModel from '../../../model/local/buy';
import * as DeviseModel from '../../../model/local/devise';
import { View } from 'react-native';
import {Picker} from '@react-native-picker/picker';
import DateTime from 'date-and-time';
import fr from 'date-and-time/locale/fr';
import { Text } from 'react-native-paper';
import { Button, Icon } from 'react-native-elements';
import DateTimePicker from '../../../components/DatePicker';
import ModalBalance from '../../../components/ModalBalanceSheet';
import AwaiterFullscreen from '../../../components/AwaiterFull';
import { getTotal, getTotalBuy, isEmpty } from '../../../Manager';

export default function App(props){

    const[agents,setAgent]=useState([]);
    const[devises,setDevise]=useState([]);
    const[buys,setBuy]=useState([]);
    const[activities,setActivity]=useState([]);
    const[loading,setLoading]=useState(true);
    const[loadingScreen,setLoadingScreen]=useState(false);
    const[date,setDate]=useState(new Date());
    const[date1,setDate1]=useState(new Date());
    const[openDate,setOpenDate]=useState(false);
    const[visible,setVisible]=useState(false);
    const[actives,setActive]=useState([]);
    const[passives,setPassive]=useState([]);
    const[total1,setTotal1]=useState("0");
    const[total2,setTotal2]=useState("0");
    const[numBuy,setNumBuy]=useState(null);
    const[numSpent,setNumSpent]=useState(null);

    const getDate=(value)=>{
        let rep="";
        try {
            if(value!=null && value!=undefined){
                if(DateTime.isSameDay(new Date,value)){
                   rep="Aujourd'hui" 
                }else{
                    rep=DateTime.format(value,"dddd, DD MMMM YYYY");
                }
            }
        } catch (error) {
            
        }
        return rep;
    }

    const onLoading=async()=>{
        try {
            const agent=await AgentModel.get();
            const buy=await BuyModel.get();
            const activity=await ActivityModel.get();
            const devise=await DeviseModel.get();
            setAgent(agent);
            setBuy(buy);
            setActivity(activity);
            setDevise(devise);
            if(loading==true){
                setLoading(false);
            }
        } catch (error) {
            
        }
    }
    useEffect(()=>{
        onLoading().then();
    },[]);

    const onSetBalance=async()=>{
        try {
            const list1=[];
            setLoadingScreen(true);
            buys.map((item,index)=>{
                if(item.created_at!=null && item.created_at!=undefined){
                    const value=DateTime.parse(item.created_at,"YYYY-MM-DD HH:mm:ss");
                    if(DateTime.isSameDay(value,date)){
                        list1.push(item);
                    }
                }
            })
            let t1=await getTotal(list1,devises);
            if(isEmpty(t1)){
                t1="0";
            }
            const tBuy=await getTotalBuy(list1,devises,true);
            setNumBuy(tBuy);
            setTotal1(t1);
            const list2=[];
            activities.map((item,index)=>{
                if(item.created_at!=null && item.created_at!=undefined){
                    const value=DateTime.parse(item.created_at,"YYYY-MM-DD HH:mm:ss");
                    if(DateTime.isSameDay(value,date)){
                        const indexDevise=devises.findIndex(p=>p.id==item.id_devise);
                        if(indexDevise!=-1){
                            item.signe=devises[indexDevise].signe;
                        }
                        item.quantity=1;
                        list2.push(item);
                    }
                }
            })
            let t2=await getTotal(list2,devises);
            console.log('T',t2);
            if(isEmpty(t2)){
                t2="0";
            }else{
                t2="-"+t2;
            }
            const f=await getTotalBuy(list2,devises);
            setNumSpent(f);
            setTotal2(t2);
            setPassive(list2);
            setActive(list1);
            setLoadingScreen(false);
            setVisible(true);
        } catch (error) {
            setLoadingScreen(false);
        }
    }

    return(
        loading?(
            <Awaiter
                color={R.color.colorPrimary}
                size={100}
                key="awaiter"
                message="chargement des données"
            />
        ):(
            <View style={{ flex:1,alignItems:'center',justifyContent:'center' }}>
                <View>
                    <Text style={{ fontFamily:'Poppins-Black',fontSize:25,padding:5 }}>
                        Bilan au
                    </Text>
                    <View style={{ flexDirection:'row',alignItems:'center' }}>
                        <View >
                            <Text numberOfLines={1} style={{ fontFamily:'Poppins-Light',fontSize:18,padding:5 }}>
                                {getDate(date)}
                            </Text>
                        </View>
                        <Icon
                            name="caretright"
                            type="antdesign"
                            onPress={()=>{setDate1(date);setOpenDate(true)}}
                        />
                    </View>
                    <View style={{ padding:10 }}>
                        <Button
                            title="Afficher le bilan"
                            buttonStyle={{ padding:15,borderRadius:20 }}
                            onPress={()=>onSetBalance()}
                        />
                    </View>
                </View>
                <DateTimePicker
                    date={date1}
                    onDateChange={v=>setDate1(v)}
                    onValidate={()=>{setDate(date1);setOpenDate(false)}}
                    visible={openDate}
                    key="datepicker"
                    onTouchOutside={()=>setOpenDate(false)}
                    backgroundColor={R.color.colorPrimary}
                    fadeToColor={R.color.colorPrimaryDark}
                    textColor={R.color.background}
                />
                <ModalBalance
                    date={date}
                    onClose={()=>setVisible(false)}
                    actives={actives}
                    key="modal_balance"
                    passives={passives}
                    visible={visible}
                    totalBuy={total1}
                    totalSpent={total2}
                    numBuy={numBuy}
                    numSpent={numSpent}
                />
                <AwaiterFullscreen
                    visible={loadingScreen}
                    color={R.color.colorAccent}
                    size={100}
                    key="awaiter_full"
                />
            </View>
        )
    )
}