import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import * as StockModel from '../../../model/local/stock';
import CardJournal from '../../../components/CardJournal';
import DateTime from 'date-and-time';
import fr from 'date-and-time/locale/fr';
import EmptyMessage from '../../../components/EmptyMessage';
import Awaiter from '../../../components/Waiter';
import R from '../../../resources/styles.json';
import { FlatList } from 'react-native';
import * as Manager from '../../../Manager';
import { Text } from 'react-native-elements';
import S from '../../../resources/settings.json';
import { FAB } from 'react-native-paper';
import { ConfirmDialog, ProgressDialog } from 'react-native-simple-dialogs';
import { StyleSheet } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import DateTimePicker from '../../../components/DatePicker';
import { enableScreens } from 'react-native-screens';



const mapStateToProps = (state) => {
    return {
        user:state.user,
        article:state.viewArticle
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        dispatch:(action)=>{dispatch(action)}
    }
}

const styles=StyleSheet.create({
    title:{

    }
})

enableScreens(true);

class App extends Component{

    constructor({props,route}){
        super(props)
        this.state={
            route,
            list:[],
            filter:[],
            devises:[],
            isLoading:true,
            opened:false,
            showDay:false,
            showMonth:false,
            showYear:false,
            showProgress:false,
            count:0,
            total:"0",
            title:"",
            mode:"day",
            date:new Date(),
            years:["2021","2022","2023","2024"],
            year:0,
            month:1,
            months:["Janvier",'Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Decembre']
        }
    }
    renderItem=({item})=>{
        const date=Manager.getDateList(item.created_at,this.state.mode);
        return(
            <CardJournal
                title={Manager.getArticleName(item.name,item.description)}
                date={date}
                count={(item.type==S.ENUM.article.service && item.mode==S.ENUM.stockage.devise)?null:item.quantity}
                size={1}
                price={(item.type==S.ENUM.article.service && item.mode==S.ENUM.stockage.devise)?Manager.getStock(item.quantity,[],item.mode,item.type).text:null}
                logo={Manager.getImageLocal(item.logo)}
                onPress={()=>this.props.navigation.navigate("ShowStock",{id:item.id})}
            />
        )
    }

    async componentDidMount(){
        this.load()
    }

    openMonth=async()=>{
        try {
            const years=[];
            const date=new Date();
            const year=date.getFullYear()+"";
            this.setState({year,showMonth:true})
        } catch (error) {
        }
    }
    openYear=()=>{
        const date=new Date();
        const year=date.getFullYear()+"";
        this.setState({showYear:true,year})
    }
    searchDay=async()=>{
        try {
            this.setState({showProgress:true})
            DateTime.locale(fr);
            const date=this.state.date;
            const date1=DateTime.format(date,'YYYY-MM-DD 00:00:00');
            const date2=DateTime.format(date,"YYYY-MM-DD 23:59:59");
            let title="Aujourd'hui";
            let mode="day";
            if(DateTime.isSameDay(new Date(),date)==false){
                title=DateTime.format(date,"dddd, DD MMMM YYYY");
            }
            const rep=await StockModel.getBetween(date1,date2);
            this.setState({
                filter:rep,
                title,
                mode
            })
            this.setState({
                count:rep.length
            })
            this.setState({showDay:false,showProgress:false})
        } catch (error) {
        }
    }
    searchMonth=async()=>{
        try {
            this.setState({showProgress:true})
            let month=this.state.month;
            const year=this.state.year;
            let format=`${year}-${month}-1 00:00:00`;
            DateTime.locale(fr);
            let date=DateTime.parse(format,"YYYY-M-D HH:mm:ss");
            const date1=DateTime.format(date,"YYYY-MM-DD HH:mm:ss");
            let day=30;
            if(month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12){
                day=31;
            }else if(month==2){
                if(year%4==0){
                    day=29;
                }else{
                    day=28
                }
            }
            format=`${year}-${month}-${day} 23:59:59`;
            date=DateTime.parse(format,"YYYY-M-D HH:mm:ss");
            const date2=DateTime.format(date,"YYYY-MM-DD HH:mm:ss");
            let title=DateTime.format(date,"MMMM YYYY");
            const rep=await StockModel.getBetween(date1,date2);
            this.setState({
                filter:rep,
                title,
                mode:"month"
            })
            this.setState({
                count:rep.length
            })
            this.setState({showMonth:false,showProgress:false})
        } catch (error) {
        }
    }
    searchYear=async()=>{
        try {
            this.setState({showProgress:true})
            const year=this.state.year;
            let format=`${year}-1-1 00:00:00`;
            DateTime.locale(fr);
            let date=DateTime.parse(format,"YYYY-M-D HH:mm:ss");
            const date1=DateTime.format(date,"YYYY-MM-DD HH:mm:ss");
            format=`${year}-12-31 23:59:59`;
            date=DateTime.parse(format,"YYYY-M-D HH:mm:ss");
            const date2=DateTime.format(date,"YYYY-MM-DD HH:mm:ss");
            let title=year+"";
            const rep=await StockModel.getBetween(date1,date2);
            this.setState({
                filter:rep,
                title,
                mode:"year"
            })
            this.setState({
                count:rep.length
            })
            this.setState({showYear:false,showProgress:false})
        } catch (error) {
        }
    }

    load=async()=>{
        try {
            let rep=await StockModel.get();
            this.setState({
                list:rep,
            })
            const date1=DateTime.format(new Date(),'YYYY-MM-DD 00:00:00');
            const date2=DateTime.format(new Date(),"YYYY-MM-DD 23:59:59");
            rep=await StockModel.getBetween(date1,date2);
            this.setState({
                filter:rep,
                title:"Ajourd'hui"
            })
            this.setState({
                count:rep.length
            })
            if(this.state.isLoading==true){
                this.setState({
                    isLoading:false
                })
            }
        } catch (error) {
        }
    }

    async componentDidUpdate(){
        try {
            /*const article=this.props.article.stock;
            if(article==true){
                const action={type:S.EVENT.onChangeArticle,stock:false}
                this.props.dispatch(action);
                await this.load()
            }*/
        } catch (error) {
        }
    }

    render(){

        return(
            <>
            {
                this.state.isLoading==true?(
                    <Awaiter color={R.color.colorSecondary} size={100} key='awaiter'  />
                ):this.state.filter==0?(
                    <EmptyMessage subTitle={this.state.title} title={"Aucun stock enregistré"} key='message' />
                ):(
                    <View style={{flex:1}} key='v'>
                        <View style={{backgroundColor:R.color.colorPrimary,padding:10}} key='v2'>
                            <Text style={{color:R.color.background,fontFamily:'Poppins-Light',padding:10,fontSize:18,textAlign:'center'}}>
                                {this.state.count>0?this.state.count+" approvisionnement"+(this.state.count>1?'s':"")+" de stocks":""}
                            </Text>
                        </View>
                        <Text style={{fontFamily:'Poppins-light',fontSize:16,padding:10,elevation:10,alignSelf:'center',color:R.color.background,margin:5,backgroundColor:R.color.colorAccent,borderRadius:15}}>
                            {this.state.title}
                        </Text>
                        <FlatList
                            data={this.state.filter}
                            renderItem={this.renderItem}
                            key='list'
                        />
                    </View>
                )
            }
            <FAB.Group
                open={this.state.opened}
                onStateChange={open=>this.setState({opened:!this.state.opened})}
                icon={this.state.opened?'close':'plus'}
                color={R.color.background}
                theme={{
                    animation:{scale:5},
                    colors:{text:R.color.colorPrimaryDark},
                    dark:true,
                    roundness:30
                }}
                fabStyle={{backgroundColor:R.color.colorSecondary}}
                actions={
                    [
                        {
                            icon:'calendar-clock',
                            label:'Un jour',
                            style:{backgroundColor:R.color.colorPrimary},
                            onPress:()=>this.setState({showDay:true})
                        },
                        {
                            icon:'calendar-month-outline',
                            label:'Un mois',
                            style:{backgroundColor:R.color.colorPrimary},
                            onPress:()=>this.openMonth()
                        },
                        {
                            icon:'calendar',
                            label:'Une année',
                            style:{backgroundColor:R.color.colorPrimary},
                            onPress:()=>this.openYear()
                        },
                    ]
                }
            />
            <ConfirmDialog
                key="month"
                visible={this.state.showMonth}
                animationType='slide'
                title="Choisissez un mois"
                onTouchOutside={()=>this.setState({showMonth:false})}
                positiveButton={{title:"Chercher",titleStyle:styles.title,onPress:()=>this.searchMonth()}}
                negativeButton={{title:"Annuler",titleStyle:styles.title,onPress:()=>this.setState({showMonth:false})}}
            >
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    <View style={{flex:2}}>
                        <Picker
                            mode='dropdown'
                            selectedValue={this.state.month}
                            onValueChange={(item,index)=>this.setState({month:item})}
                        >
                            {
                                this.state.months.map((item,index)=>(
                                    <Picker.Item label={item} value={index+1} key={`index-month${index}`} />
                                ))
                            }
                        </Picker>
                    </View>
                    <View style={{flex:1}}>
                        <Picker
                            mode='dropdown'
                            selectedValue={this.state.year}
                            onValueChange={(item,index)=>this.setState({year:item})}
                        >
                            {
                                this.state.years.map((item,index)=>(
                                    <Picker.Item label={item} value={parseInt(item)} key={`index-year${index}`} />
                                ))
                            }
                        </Picker>
                    </View>
                </View>
            </ConfirmDialog>
            <ConfirmDialog
                key="year"
                visible={this.state.showYear}
                animationType='slide'
                onTouchOutside={()=>this.setState({showYear:false})}
                positiveButton={{title:"Chercher",titleStyle:styles.title,onPress:()=>this.searchYear()}}
                negativeButton={{title:"Annuler",titleStyle:styles.title,onPress:()=>this.setState({showYear:false})}}
            >
                <View>
                    <View style={{flex:1}}>
                        <Picker
                            mode='dropdown'
                            selectedValue={this.state.year}
                            onValueChange={(item,index)=>this.setState({year:item})}
                        >
                            {
                                this.state.years.map((item,index)=>(
                                    <Picker.Item label={item} value={parseInt(item)} key={`index-year-year${index}`} />
                                ))
                            }
                        </Picker>
                    </View>
                </View>
            </ConfirmDialog>
            <DateTimePicker
                onTouchOutside={()=>this.setState({showDay:false})}
                onValidate={()=>this.searchDay()}
                onDateChange={value=>this.setState({date:value})}
                date={this.state.date}
                visible={this.state.showDay}
                backgroundColor={R.color.colorPrimaryDark}
                fadeToColor={R.color.colorPrimaryDark}
            />
            <ProgressDialog
                visible={this.state.showProgress}
                message="Patientez SVP!"
                title="Chargement..."
                activityIndicatorColor={R.color.colorSecondary}
                animationType="slide"
                activityIndicatorSize="large"
            />
            </>
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(App)