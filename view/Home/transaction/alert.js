import React from 'react';
import { Component } from 'react';
import { View } from 'react-native';
import EmptyMessage from '../../../components/EmptyMessage';
import R from '../../../resources/styles.json';
import S from '../../../resources/settings.json';
import * as NotificationModel from '../../../model/local/notification';
import CardAlert from '../../../components/CardAlert';
import Awaiter from '../../../components/Waiter';
import { FlatList } from 'react-native';
import { Avatar, Button, Overlay, Text } from 'react-native-elements';
import { Appbar } from 'react-native-paper';
import { ScrollView } from 'react-native';
import MenuItem from '../../../components/MenuItem';
import { Dialog } from 'react-native-simple-dialogs';
import { capitalize } from '../../../Manager';


class App extends Component{

    constructor(props){
        super(props);
        this.state={
            isLoading:true,
            list:[],
            visible:false,
            showMenu:false,
            content:"",
            title:"",
            type:"",
            icon:"user-o",
            typeIcon:"font-awesome"
        }
    }

    async componentDidMount(){
        try {
            await this.onLoad();
        } catch (error) {
            
        }
    }
    onClose=()=>{
        try {
            this.setState({
                visible:false,
                content:"",
                title:"",
                icon:"user-o",
                typeIcon:"font-awesome"
            })
        } catch (error) {
            
        }
    }

    onLoad=async()=>{
        try {
            const rep=await NotificationModel.get();
            if(rep!=null){
                this.setState({list:rep})
            }
            if(this.state.isLoading==true){
                this.setState({isLoading:false})
            }
        } catch (error) {
            
        }
    }

    onShowAlert=(item)=>{
        try {
            this.setState({
                title:item.title,
                content:item.content,
                visible:true,
                type:item.type
            })
        } catch (error) {
            
        }
    }

    renderItem=({item,index})=>{
        return(
            <CardAlert
                content={item.content}
                date={item.created_at}
                title={item.title}
                type={item.type}
                key={`index${index}`}
                onPress={()=>this.onShowAlert(item)}
                onReport={()=>this.setState({showMenu:true})}
            />
        )
    }

    getIcon=(type)=>{
        const value=capitalize(type).toLowerCase();
        let rep={icon:"user-secret",type:"font-awesome"};
        let icon="";
        let cat="";
        switch (value) {
            case capitalize(S.GENERAL.TYPE.CUSTOMER).toLowerCase():
                icon="customerservice";
                cat="antdesign";
            break;
            case capitalize(S.GENERAL.TYPE.BUY).toLowerCase():
                icon="shoppingcart";
                cat="antdesign";
            break;
            case capitalize(S.GENERAL.TYPE.STOCK).toLowerCase():
                icon="truck";
                cat="font-awesome";
            break;
            case capitalize(S.GENERAL.TYPE.USING).toLowerCase():
                icon="plug";
                cat="font-awesome";
            break;
            default:
                icon="info";
                cat="font-awesome";
            break;
        }
        rep={icon:icon,type:cat};
        return rep;
    }

    componentView=()=>{

        const rep=this.getIcon(this.state.type);
        return(
        <Overlay isVisible={this.state.visible} fullScreen animationType='slide' overlayStyle={{ padding:0 }}>
            <View style={{ flex:1 }}>
                <Appbar.Header style={{ backgroundColor:R.color.colorPrimary }}>
                    <Appbar.BackAction
                        onPress={()=>this.onClose()}
                    />
                    <Appbar.Content
                        title={this.state.title}
                    />
                </Appbar.Header>
                <ScrollView>
                    <View style={{ alignItems:'center',padding:10 }}>
                        <Avatar
                            size='xlarge' rounded
                            icon={{ name:rep.icon,type:rep.type }}
                            containerStyle={{ backgroundColor:R.color.colorPrimaryDark }}
                        />
                        <Text style={{ textAlign:'center',fontFamily:'Poppins-Black',fontSize:22,padding:10 }}>
                            {this.state.title}
                        </Text>
                    </View>
                    <View>
                        <Text style={{ padding:5,textAlign:'justify',fontFamily:"Poppins-Light",fontSize:18 }}>
                            {this.state.content.replace("/*","").replace("*/","")}
                        </Text>
                    </View>
                    <View style={{ flexDirection:'row',padding:10,paddingTop:35,alignItems:'center',justifyContent:'flex-end' }}>
                        <View style={{ padding:10 }}>
                            <Button
                                title="Signaler"
                                icon={{ name:"close",type:"antdesign",color:R.color.background }}
                                buttonStyle={{ padding:10,paddingLeft:20,paddingRight:20,backgroundColor:'red' }}
                                titleStyle={{ color:R.color.background,fontFamily:'Poppins-Regular',fontWeight:'normal' }}
                            />
                        </View>
                        <View style={{ padding:10 }}>
                            <Button
                                title="j'ai compris"
                                icon={{ name:"like1",type:"antdesign",color:R.color.background }}
                                onPress={()=>this.onClose()}
                                buttonStyle={{ padding:10,paddingLeft:20,paddingRight:20,backgroundColor:R.color.colorPrimaryDark }}
                                titleStyle={{ color:R.color.background,fontFamily:'Poppins-Regular',fontWeight:'normal' }}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        </Overlay>
    )
        }

    render(){
        return(
            this.state.isLoading==true?(
                <Awaiter
                    color={R.color.colorSecondary}
                    size={100}
                    key="awaiter"
                />
            ):this.state.list.length==0?(
                <EmptyMessage title="Aucune information disponible" key="ddd" />
            ):(
                <View>
                    <FlatList
                        data={this.state.list}
                        renderItem={this.renderItem}
                        key="list"
                        keyExtractor={item=>item.id}
                    />
                    <this.componentView

                    />
                    <Dialog
                        key='diag-menu'
                        animationType='fade'
                        visible={this.state.showMenu}
                        onTouchOutside={()=>this.setState({showMenu:false})}
                        dialogStyle={{maxWidth:300,alignSelf:'center',padding:0,elevation:50}}
                    >
                        <MenuItem
                            title="Ne plus afficher ces alertes"
                            key='update'
                            icon="edit"
                            type="font-awesome"
                            onPress={()=>this.setState({showMenu:false})}
                        />
                        <MenuItem
                            title="Ne rien faire"
                            key='conversion'
                            icon="close"
                            type="antdesign"
                            onPress={()=>this.setState({showMenu:false})}
                        />
                               
                    </Dialog>
                </View>
            )
        )
    }
}
export default (App);