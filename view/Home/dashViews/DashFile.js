import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Card, Icon } from 'react-native-elements';
import { Banner, Text } from 'react-native-paper';
import R from '../../../resources/styles.json';
import { getSize } from './treatment';

const styles=StyleSheet.create({
    text:{
        color:R.color.background,
        fontFamily:'Poppins-Light',
    }
})

function DashFile({images=[],setLoad}){
    const[visible,setVisible]=React.useState(true);
    const[imageSize,setImageSize]=React.useState(0);

    React.useEffect(()=>{
        const rep=getSize(images);
        setImageSize(rep);
    },[]);

    
    return(
        <View>
            <Banner 
                visible={visible}
                actions={[
                    {
                        label:"Fermer",
                        onPress:()=>setVisible(false),
                    }
                ]}
            >
                <Text style={{ fontFamily:'Poppins-Light' }}><Text style={{ fontFamily:"Poppins-Bold" }}>Ligal Apply </Text> ne transfère ni ne télécharge automatiquement des images liées à vos activités.
                    C’est vous qui décidez quand transférer et télécharger des images. Ces images concernent des logos des articles et catégories, des photos des clients.</Text>
                </Banner>
            <Card containerStyle={{ backgroundColor:R.color.colorPrimary,marginBottom:20 }}>
                <Card.Title style={{ fontFamily:'Poppins-Black',color:R.color.background,fontWeight:'normal',fontSize:18 }}>
                    Gestion sur des fichiers
                </Card.Title>
                <View style={{ flexDirection:'row',alignItems:'center' }}>
                    <View style={{ padding:8 }}>
                        <Icon
                            name="folder" type="font-awesome" color={R.color.background}
                        />   
                    </View>
                    <View style={{ flex:1,alignItems:'center' }}>
                        <Text style={styles.text}>Taille des images: <Text style={{ fontFamily:'Poppins-Black',color:R.color.background }}>{imageSize}</Text></Text>
                    </View>
                </View>
                <View style={{ alignItems:'flex-end',paddingTop:10 }}>
                    <Button
                        title="Transférer les images"
                        buttonStyle={{ 
                            backgroundColor:R.color.colorSecondary,
                            padding:10,paddingLeft:20,paddingRight:20
                         }}
                         //type="outline"
                         titleStyle={{ color:R.color.background }}
                         disabled={images.length==0}
                    />
                </View>
            </Card>
            </View>
    )

}

export {DashFile}