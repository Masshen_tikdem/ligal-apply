import * as StockModel from '../../../model/local/stock';
import * as ArticleModel from '../../../model/local/article';
import * as FamilyModel from '../../../model/local/family';
import * as BuyModel from '../../../model/local/buy';
import * as CustomerModel from '../../../model/local/client';
import * as AgentModel from '../../../model/local/agent';
import * as DeviseModel from '../../../model/local/devise';
import * as ArticleWebModel from '../../../model/web/article';
import * as FamilleWebModel from '../../../model/web/famille';
import * as SocietyWebModel from '../../../model/web/society';
import R from '../../../resources/styles.json';
import File from 'react-native-fs'; 
import { getFilePath } from '../../../Manager';
import { sumBy } from 'lodash';

const checkCategories=async()=>{
    let categories=[];
    let links=[];
    let status={};
    let info={};
    const images=[];
    try {
        const rep=await FamilyModel.get();
        categories=rep;
        const link=await FamilyModel.getLinks();
        links=link;
        let count=0;
        let all=rep.length+link.length;
        info={
            uid:"cat-info",
            title:"Catégories",
            content:rep.length,
            color:R.color.colorSecondary
        }
        const onEvent=async(item,index)=>{
            if(item.private_key==null || item.is_updated==1){
                count++;
            }
            if(item.image_updated==1 && item.private_key!=null){
                const img=await getStat(item);
                if(img.size>0){
                    images.push(img);
                }
            }
        }
        await Promise.all(rep.map(onEvent));
        await Promise.all(link.map(onEvent));
        if(count>0){
            status={
                title:"Catégories",
                content:"Quelques informations de catégories ne sont pas encore publiées",
                num:(count/all)*100,
                icon:"shopping-bag",
                length:rep.length,
                lengthLink:link.length,
                //list:rep,
                listLink:link,
                uid:"cat"
            }
        }else{
            status={
                title:"Catégories",
                content:"Toutes les informations sont publiées",
                num:0,
                icon:"shopping-bag",
                length:rep.length,
                lengthLink:link.length,
                uid:"cat"
            }
        }
    } catch (error) {
        
    }
    return {categories,links,status,info,images};
}

const getStat=async(item)=>{
    const element={};
    try {
        console.log("size "+item.name)
        const size=(await File.stat(getFilePath(item.logo))).size;
        console.log("size "+item.name,size)
        element.size=parseInt(size);
        element.path=item.logo;
        element.name="";
    } catch (error) {
        
    }
    return element;
}

const checkArticles=async()=>{
    let articles=[];
    let info={};
    let status={};
    let info_product={};
    let info_service={};
    let images=[];
    try {
        const rep=await ArticleModel.get();
        articles=rep;
        let count=0;
        let all=rep.length;
        let count_service=0;
        let count_product=0;
        const onEvent=async(item,index)=>{
            /*if(item.logo!=null){
                console.log("ITEMSSS NNNN",item)
                const img=await getStat(item);
                if(img.size>0){
                    images.push(img);
                }
            }*/
            if(item.private_key==null || item.is_updated==1){
                count++;
            }
            if(item.image_updated==1 && item.private_key!=null){
                const img=await getStat(item);
                if(img.size>0){
                    images.push(img);
                }
            }
        }
        const onEventType=async(item,index)=>{
            if(item.type=="service"){
                count_service++;
            }else{
                count_product++;
            }
        }
        await Promise.all(rep.map(onEvent));
        await Promise.all(rep.map(onEventType));
        info={
            uid:"article-info",
            title:"Articles",
            content:rep.length,
            color:R.color.colorPrimary
        }
        info_product={
            uid:"product-info",
            title:"Produits",
            content:count_product,
            color:R.color.colorPrimary
        }
        info_service={
            uid:"service-info",
            title:"Services",
            content:count_service,
            color:R.color.colorPrimaryDark
        }
        if(count>0){
            status={
                title:"Produits et services",
                content:"Quelques informations ne sont pas encore publiées",
                num:(count/all)*100,
                icon:"shopping-basket",
                length:rep.length,
                list:rep,
                uid:"art-stat"
            }
        }else{
            status={
                title:"Produits et services",
                content:"Toutes les informations sont publiées",
                num:0,
                icon:"shopping-basket",
                length:rep.length,
                list:rep,
                uid:"art-stat"
            }
        }
    } catch (error) {
        console.log("PPP",error);
    }
    return {articles,info,info_product,info_service,status,images};
}

const checkAgents=async()=>{
    let users=[];
    let info={};
    try {
        const rep=await AgentModel.get();
        users=rep;
        info={
            uid:"user-info",
            title:"Agents",
            content:rep.length,
            color:R.color.colorAccent
        }
        
    } catch (error) {
        
    }
    return {info}
}

const checkStocks=async()=>{
    let supplies=[];
    let status={};
    try {
        const rep=await StockModel.get();
        supplies=rep;
        let count=0;
        let all=rep.length;
        const onEvent=async(item,index)=>{
            if(item.private_key==null || item.is_updated==1){
                count++;
            }
        }
        await Promise.all(rep.map(onEvent));
        if(count>0){
            status={
                title:"Approvisionnements",
                content:"Quelques informations ne sont pas encore publiées",
                num:(count/all)*100,
                icon:"bus",
                length:rep.length,
                list:rep,
                uid:"stat-stock"
            }
        }else{
            status={
                title:"Approvisionnements",
                content:"Toutes les informations sont publiées",
                num:0,
                icon:"bus",
                length:rep.length,
                list:rep,
                uid:"stat-stock"
            }
        }
    } catch (error) {
        
    }
    return {supplies,status};
}

const checkDevises=async()=>{
    let devises=[];
    let info={};
    let status={};
    try {
        const rep=await DeviseModel.get();
        devises=rep;
        let count=0;
        let all=rep.length;
        info={
            uid:"devise-info",
            title:"Devise",
            content:rep.length,
            color:R.color.radiobutton
        }
        const onEvent=async(item,index)=>{
            if(item.private_key==null || item.is_updated==1){
                count++;
            }
        }
        await Promise.all(rep.map(onEvent));
        if(count>0){
            status={
                title:"Devises",
                content:"Quelques informations ne sont pas encore publiées",
                num:(count/all)*100,
                icon:"money",
                length:rep.length,
                list:rep,
                uid:"dev-stat"
            }
        }else{
            status={
                title:"Devises",
                content:"Toutes les informations sont publiées",
                num:0,
                icon:"money",
                length:rep.length,
                list:rep,
                uid:"dev-stat"
            }
        }
    } catch (error) {
        
    }
    return {devises,info,status}
}

const checkSales=async()=>{
    let sales=[];
    let customers=[];
    let status={};
    let info={};
    try {
        const rep=await BuyModel.get();
        sales=rep;
        const customer=await CustomerModel.get();
        customers=customer;
        let count=0;
        let all=rep.length+customer.length;
        info={
            uid:"devise-info",
            title:"Clients",
            content:customer.length,
            color:R.color.colorPrimary
        }
        const onEvent=async(item,index)=>{
            if(item.private_key==null || item.is_updated==1){
                count++;
            }
        }
        await Promise.all(rep.map(onEvent));
        await Promise.all(customer.map(onEvent));
        if(count>0){
            status={
                title:"Ventes et clients",
                content:"Quelques informations ne sont pas encore publiées",
                num:(count/all)*100,
                length:rep.length,
                lengthCustomer:customer.length,
                list:rep,
                listCustomer:rep,
                uid:"sale-stat"
            }
        }else{
            status={
                title:"Ventes et clients",
                content:"Toutes les informations sont publiées",
                num:0,
                length:rep.length,
                lengthCustomer:customer.length,
                list:rep,
                listCustomer:rep,
                uid:"sale-stat"
            }
        }
    } catch (error) {
        
    }
    return {customers,sales,status,info}
}

const getSize=(list=[])=>{
    let rep="0 octets";
    try {
        let size=sumBy(list,p=>p.size);
        if(size<Math.pow(2,10)){
            rep=size+" octets"
        }else if(size>=Math.pow(2,10) && size<Math.pow(2,20)){
            //kilo
            const calc=size/Math.pow(2,10);
            rep=calc.toFixed(2)+" Ko";
        }else if(size>=Math.pow(2,20) && size<Math.pow(2,30)){
            //mega
            const calc=size/Math.pow(2,20);
            rep=calc.toFixed(2)+" Mo";
        }else if(size>=Math.pow(2,30) && size<Math.pow(2,40)){
            //giga
            const calc=size/Math.pow(2,30);
            rep=calc.toFixed(2)+" Go";
        }
    } catch (error) {
        
    }
    return rep;
}


async function getWebData(shop){

    const rep=await SocietyWebModel.recover(shop.private_key);
    let webShop=[];
    let webDevises=[];
    let webCategories=[];
    let webAgents=[];
    let webArticles=[];
    let webStocks=[];
    let webBuys=[];
    let webCustomer=[];
    let webLink=[];
    let webRights=[];
    if(rep.status==200){
        const p=rep.response;
        webShop=p.society;
        webDevises=p.devises;
        webCategories=p.categories;
        webAgents=p.agents;
        webArticles=p.articles;
        webStocks=p.stocks;
        webBuys=p.buys;
        webCustomer=p.customers;
        webLink=p.links;
        webRights=p.rights;
    }

    return {webShop,webDevises,webCategories,webAgents,webArticles,webStocks,webBuys,webCustomer,webLink,webRights}
}

export {checkCategories,checkAgents,checkArticles,checkStocks,checkDevises,checkSales,getSize,getWebData}