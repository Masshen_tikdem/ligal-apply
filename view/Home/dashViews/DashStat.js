import React from 'react';
import { View } from 'react-native';
import { Button } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import { Banner, Text } from 'react-native-paper';
import CardDashbord from '../../../components/CardDashboard';

function DashStat({list=[],count=0,setLoad}){

    const[visible,setVisible]=React.useState(true);

    const renderItem=({item})=>{


        return(
            <CardDashbord
                content={item.content}
                title={item.title}
                num={item.num}
                icon={item.icon}
                type={item.type}
            />
        )
    }

    return(
        <View>
            <View>
                <Text style={{ fontFamily:'Poppins-Black',paddingLeft:10,fontSize:16 }}>
                    Transfère des données
                </Text>
            </View>
                <Banner 
                    visible={visible}
                    actions={[
                        {
                            label:"Fermer",
                            onPress:()=>setVisible(false),
                        }
                    ]}
                >
                    <Text style={{ fontFamily:'Poppins-Light' }}><Text style={{ fontFamily:"Poppins-Bold" }}>Ligal platform</Text> vous permet de gérer votre boutique à partir de n’importe où ; vos données traitées seront automatiquement transférées dans votre espace en ligne.
                     Il peut se faire que ce transfert automatique ne fonctionne pas pour quelques raisons ; ainsi, <Text style={{ fontFamily:'Poppins-Italic' }}>ligal-apply</Text> vous permet de faire un transfert manuel.</Text>
                </Banner>
            <FlatList
                data={list}
                renderItem={renderItem}
                key="list-"
                keyExtractor={(v,index)=>v.uid+index}
            />
            <View style={{ alignItems:'flex-end',padding:20,paddingTop:10,paddingBottom:10 }}>
                <Button
                    title="Transfert manuel"
                    disabled={!count}
                />
            </View>
        </View>
    )
}

export {DashStat}