import React from 'react';
import { View } from 'react-native';
import { Button, Card } from 'react-native-elements';
import { useDispatch } from 'react-redux';
import { downloadImage, getImage, getRandomString } from '../../../Manager';
import * as ArticleModel from '../../../model/local/article';
import * as CategoryModel from '../../../model/local/family';
import S from '../../../resources/settings.json';
import R from '../../../resources/styles.json';


function DashDownload({web={},articles=[],categories=[],setLoad}){

    const[count,setCount]=React.useState(0);
    const[items,setItems]=React.useState([]);
    const[loading,setLoading]=React.useState(false);
    const dispatch=useDispatch();

    const onDownload=async()=>{
        try {
            let articleDownload=false;
            setLoading(true);
            await Promise.all(items.map(async p=>{
                let id=0;
                const{type,logo,private_key}=p;
                let toDel=null;
                if(type=='article'){
                    await Promise.all(articles.map(x=>{
                        if(x.private_key==private_key){
                            id=x.id;
                            toDel=x.logo;
                        }
                    }))
                    if(id>0){
                        const uri=getImage(logo);
                        const imageName='article'+id+getRandomString(20)+'.ligal';
                        await downloadImage(imageName.toLowerCase(),uri,toDel).then(async load=>{
                            console.log('LOAD',load);
                            if(load==true){
                                articleDownload=true;
                                const rep=await ArticleModel.update({
                                    id:id,
                                    logo:imageName.toLowerCase(),
                                })
                            }
                        });
                    }
                }else if(type=='category'){
                    await Promise.all(categories.map(x=>{
                        if(x.private_key==private_key){
                            id=x.id;
                        }
                    }))
                    if(id>0){
                        const uri=getImage(logo);
                        const imageName='category'+id+getRandomString(20)+'.ligal';
                        await downloadImage(imageName.toLowerCase(),uri,toDel).then(async load=>{
                            if(load==true){
                                const rep=await CategoryModel.update({
                                    id:id,
                                    logo:imageName.toLowerCase(),
                                })
                            }
                        });
                    }
                }
            }));
            setLoading(false);
            if(articleDownload==true){
                action={type:S.EVENT.onChangeSaleInfo,article:true}
                dispatch(action);
            }
        } catch (error) {
            
        }
    }

    React.useEffect(()=>{
        const list=[];
        web.webArticles.map(p=>{
            if(p.logo!=null){
                list.push({
                    private_key:p.private_key,
                    logo:p.logo,
                    type:"article"
                })
            }
        });
        web.webCategories.map(p=>{
            if(p.logo!=null){
                list.push({
                    private_key:p.private_key,
                    logo:p.logo,
                    type:"category"
                })
            }
        })
        setCount(list.length);
        setItems(list);
    },[web]);

    return(
        <View style={{ padding:20 }}>
            <Card>
                <View style={{ flex:1 }}>
                    <Card.Title>
                        {`${count} image${count>1?'s':''} à télécharger`}
                    </Card.Title>
                    <Button 
                        title="Télécharger"
                        disabled={count==0 || loading} 
                        onPress={()=>onDownload()}
                        loading={loading}
                        buttonStyle={{ backgroundColor:R.color.colorSecondary }}
                    />
                </View>
            </Card>
        </View>
    )
}

export {DashDownload}