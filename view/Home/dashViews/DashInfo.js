import React from 'react';
import { FlatList } from 'react-native-gesture-handler';
import CardDashboardInfo from '../../../components/CardDashInfo';

function DashInfo({list=[]}){

    const renderItem=({item})=>{
        return(
            <CardDashboardInfo
                title={item.title}
                content={item.content}
                color={item.color}
            />
        )
    }

    return(
        <FlatList
            data={list}
            renderItem={renderItem}
            keyExtractor={(v,i)=>"cool"+i}
            key="list"
            horizontal
        />
    )
}

export {DashInfo}