import { capitalize, isEmpty } from "../../../Manager";
import * as CustomerModel from '../../../model/local/client';
import * as Shared from '../../../service/shared';

async function getCustomer(name="",phone="",customers=[],code="",shop={},user={},idCustomer=0){
    let rep=0;
    rep=idCustomer;
    if( (!isEmpty(name) || !isEmpty(phone)) && idCustomer==0 ){
        const items=customers.filter(p=>(capitalize(p.item).toLowerCase()==capitalize(name).toLowerCase() || p.phone==code+phone));
        items.map(p=>{
            rep=p.id;
        })
    }
    if(rep==0 && !isEmpty(name) && !isEmpty(phone)){
        const customer=await CustomerModel.store({
            name:name,
            phone:code+phone
        })
        if(customer!=null){
            rep=customer.id;
            Shared.customer({
                id:customer.id,
                name:customer.name,
                phone:customer.phone,
                private_key:customer.private_key,
                shop:shop,
                user:user
            })
        }
    }

    return rep;
}

export {getCustomer};