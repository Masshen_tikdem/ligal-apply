import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Awaiter from '../components/Waiter';
import R from '../resources/styles.json';
import { View } from 'react-native';
import contribution from './viewAgent/contribution';
import info from './viewAgent/info';
import role from './viewAgent/role';
import * as AgentModel from '../model/local/agent';



const Tab=createMaterialTopTabNavigator();


class App extends React.Component{
    constructor(props){
        super(props);
        this.state={
            isLoading:true,
            agent:null
        }
    }
    async componentDidMount(){
        try {
            const id=this.props.route.params.id;
            const rep=await AgentModel.show(id);
            if(rep!=null){
                this.setState({
                    agent:rep
                })
            }
            this.setState({
                isLoading:false
            })
        } catch (error) {
            
        }
    }


    render(){
        return(
            this.state.isLoading==true?(
                <Awaiter color={R.color.colorSecondary} size={100} key="awaiter" /> 
            ):(
                <Tab.Navigator
                    tabBarOptions={{ 
                        scrollEnabled:true
                     }}
                >
                    <Tab.Screen
                        name="InfoAgent"
                        component={info}
                        options={{ 
                            title:"Présentation"
                         }}
                         initialParams={{ item:this.state.agent }}
                    />
                    <Tab.Screen
                        name="RoleAgent"
                        component={role}
                        options={{ 
                            title:"Rôles"
                         }}
                         initialParams={{ item:this.state.agent }}
                    />
                    <Tab.Screen
                        name="ContributionAgent"
                        component={contribution}
                        options={{ 
                            title:"Contributions"
                         }}
                         initialParams={{ item:this.state.agent }}
                    />
                </Tab.Navigator>
            )
        )
    }
}
export default (App);