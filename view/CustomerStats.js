import { useRoute } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { Dimensions } from 'react-native';
import { View } from 'react-native';
import * as DeviseModel from '../model/local/devise';
import * as ArticleModel from '../model/local/article';
import * as BuyModel from '../model/local/buy';
import * as CustomerModel from '../model/local/client';
import Waiter from '../components/Waiter';
import EmptyMessage from '../components/EmptyMessage';
import R from '../resources/styles.json';
import DateTime from 'date-and-time';
import fr from 'date-and-time/locale/fr';
import LinearGraphic from '../components/graphic/Linear';
import CircularGraphic from '../components/graphic/Circular';
import { FAB, Portal, Provider, Text } from 'react-native-paper';
import { capitalize, getArticleName } from '../Manager';
import MenuItem from '../components/MenuItem';
import {ConfirmDialog, Dialog} from 'react-native-simple-dialogs';
import { Input } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import { ScrollView } from 'react-native';


export default function App(){

    DateTime.locale(fr);
    const route=useRoute();
    const[labels,setLabel]=useState([]);
    const[values,setValue]=useState([]);
    const[articleData,setArticleData]=useState([]);
    const[articles,setArticle]=useState([]);
    const[useArticles,setUseArticle]=useState([]);
    const[devises,setDevise]=useState([]);
    const[buys,setBuys]=useState([]);
    const[loading,setLoading]=useState(true);
    const[visibleFab,setVisibleFab]=useState(false);
    const[currentTime,setCurrentTime]=useState("");
    const[currentDevise,setCurrentDevise]=useState("");

    const[showDevise,setShowDevise]=useState(false);
    const[showConversion,setShowConversion]=useState(false);
    const[showMenuType,setShowMenuType]=useState(false);
    const[conversions,setConversions]=useState([]);
    const[principalDevise,setPrincipal]=useState(null);

    const renderConversion=({item,index})=>{

        return(
            <View style={{ flexDirection:'row',alignItems:'center',padding:10 }}>
                <View style={{ padding:5 }}>
                    <Text>
                        {"1 "+capitalize(item.signe).toUpperCase()+"="}
                    </Text>
                </View>
                <View style={{ flex:1 }}>
                    <Input
                        value={item.value}
                        onChangeText={v=>item.value=v}
                        keyboardType='numeric'
                        inputStyle={{ textAlign:'right' }}
                    />
                </View>
                <View style={{ padding:5 }}>
                    <Text>
                        {capitalize(principalDevise.signe).toUpperCase()}
                    </Text>
                </View>
            </View>
        )
    }

    const onLoading=()=>{
        try {
            DeviseModel.get().then(devise=>{
                setDevise(devise);
                let typeDevise=null;
                devise.map((item,index)=>{
                    if(item.type==1 && typeDevise==null){
                        setPrincipal(item);
                        typeDevise=item;
                    }
                });
                const convs=[];
                if(typeDevise!=null){
                    devise.map((item,index)=>{
                        if(typeDevise.id!=item.id){
                            convs.push(item);
                        }
                    });
                    setConversions(convs);
                }

                ArticleModel.get().then(article=>{
                    setArticle(article);
                    CustomerModel.getCustomerBuy(route.params.item.id)
                    .then(buy=>{
                        setBuys(buy);
                        if(buy.length>0){
                            const date=new Date();
                            const list=setDeviseYear(buy,date.getFullYear(),devise[0]);
                            setArticleGraphic();
                            const items=[];
                            const labels=[];
                            list.map((item,index)=>{
                                labels.push(item.month);
                                items.push(item.income);
                                setLabel(labels);
                                setValue(items);
                            })
                            setCurrentDevise(capitalize(devise[0].signe).toUpperCase());
                            setCurrentTime("l'année "+date.getFullYear());
                            setLoading(false);
                        }else{
                            setLoading(false);
                        }
                    });
                });
            });
        } catch (error) {
            console.log("error",error);
        }
    }

    setArticleGraphic=()=>{
        try {
            const items=useArticles;
            const colors=[R.color.colorPrimary,R.color.colorSecondary,R.color.colorPrimaryDark,"teal","punk","red"];
            const data=[];
            let rep=0;
            items.map((item,index)=>{
                const name=item.name;
                const value=item.value;
                const color="#fee";
                const legendFontColor="red";
                const legendFontSize= 15;
                if(index<5){
                    data.push({
                        value,
                        color:colors[index],
                        name,
                        legendFontColor,
                        legendFontSize
                    })
                }else{
                    rep+=value;
                }
                if(index>=5 && rep>0){
                    data.push({
                        value,
                        color,
                        name:"Autres",
                        legendFontColor,
                        legendFontSize
                    })
                }
            });
            setArticleData(data);
        } catch (error) {
            
        }
    }

    

    const showGraphic=(buy=[],date=new Date(),devise)=>{
        try {
            setShowDevise(false);
            setLoading(true);
            const list=setDeviseYear(buy,date.getFullYear(),devise);
            const items=[];
            const labels=[];
            list.map((item,index)=>{
                labels.push(item.month);
                items.push(item.income);
                setLabel(labels);
                setValue(items);
             })
            setCurrentDevise(capitalize(devise.signe).toUpperCase());
            setCurrentTime("l'année "+date.getFullYear());
            setLoading(false);
        } catch (error) {
            console.log('error',error)
        }
    }

    const setDeviseYear=(items=[],year,devise,convers)=>{
        const list=[];
        try {
            for (let index = 0; index < 12; index++) {
                list.push({index:index+1});
            }
            list.map((item,index)=>{
                let days=0;
                if(item.index==1 || item.index==3 || item.index==5 || item.index==7 || item.index==8 || item.index==10 || item.index==12){
                    days=31;
                }else if(item.index==4 || item.index==6 || item.index==9 || item.index==11){
                    days=30;
                }else if(item.index==2){
                    if(year%4==0){
                        days=29;
                    }else{
                        days=28;
                    }
                }
                const month=(""+item.index).length==1?"0"+item.index:item.index;
                const y1=year+"-"+month+"-01 00:00:00";
                const y2=year+"-"+month+"-"+days+" 23:59:59";
                const time1=DateTime.parse(y1,"YYYY-MM-DD HH:mm:ss");
                const time2=DateTime.parse(y2,"YYYY-MM-DD HH:mm:ss");
                if(devise!=null){
                    const rep=getByTime(items,time1,time2,devise.id);
                    item.income=rep;
                    item.month=DateTime.format(time1,"MMM");
                }else{
                    const rep=getByTime(items,time1,time2,null,convers);
                    item.income=rep;
                    item.month=DateTime.format(time1,"MMM");
                }
            });
        } catch (error) {
            
        }
        return list;
    }

    const getByTime=(list=[],time1=new Date(),time2=new Date(),id,convers=[])=>{
        try {
            let rep=0;
            const contents=useArticles;
            list.map((item,index)=>{
                const date=DateTime.parse(item.created_at,"YYYY-MM-DD HH:mm:ss");
                const time=date.getTime();
                const indice=contents.findIndex(p=>p.id==item.id_article);
                if(id>0){
                    if(item.id_devise==id){
                        if(time>=time1.getTime()&& time<=time2.getTime()){
                            let som=0;
                            if(item.quantity>0){
                                som+=item.price*item.quantity;
                            }else{
                                som+=item.price;
                            }
                            rep+=som-som*(item.remise);
                            if(indice==-1){
                                contents.push({
                                    id:item.id_article,
                                    value:rep,
                                    name:getArticleName(item.name,item.description),
                                    description:item.description
                                })
                            }else{
                                contents[indice].value+=rep;
                            }
                        }
                    }
                }else if(convers.length>0 && principalDevise!=null && principalDevise!=undefined){
                    if(time>=time1.getTime()&& time<=time2.getTime()){
                        let som=0;
                        if(item.quantity>0){
                            som+=item.price*item.quantity;
                        }else{
                            som+=item.price;
                        }
                        som=som-som*(item.remise);
                        if(item.id_devise==principalDevise.id){
                            rep+=som;
                        }else{
                            const value=getValueConversion(item.id_devise);
                            if(value>0){
                                rep+=som*value;
                            }else{
                                rep+=som;
                            }
                        }
                        if(indice==-1){
                            contents.push({
                                id:item.id_article,
                                value:rep,
                                name:item.name,
                                description:item.description
                            })
                        }else{
                            contents[indice].value+=rep;
                        }
                    }
                }
                
            });
            //setUseArticle(contents);
            return rep;
        } catch (error) {
            
        }
    }

    const getValueConversion=(id)=>{
        let rep=0;
        try {
            const index=conversions.findIndex(p=>p.id==id);
            if(index!=-1){
                const value=conversions[index].value;
                if(value>0){
                    rep=value;
                }
            }
        } catch (error) {
            
        }
        return rep;
    }

    useEffect(()=>{
        console.log('AWA')
        onLoading();
    },[]);

    const onIncomeTotal=()=>{
        try {
            setShowConversion(false);
            setLoading(true);
            const date=new Date();
            const list=setDeviseYear(buys,date.getFullYear(),null,conversions);
            const items=[];
            const labels=[];
            list.map((item,index)=>{
                labels.push(item.month);
                items.push(item.income);
                setLabel(labels);
                setValue(items);
             })
            setCurrentDevise(capitalize("TOTAL REVENU").toUpperCase());
            setCurrentTime("l'année "+date.getFullYear());
            setLoading(false);
        } catch (error) {
            console.log('error',error)
        }
    }

    return(
        <>
        {
        loading==true?(
            <Waiter
                color={R.color.colorSecondary}
                size={100}
                key="awaiter"
                message=""
            />
        ):values.length==0?(
            <EmptyMessage
                title="aucune info"
                key="key"
            />
        ):(
            <View style={{ flex:1 }}>
                <ScrollView>
                    <LinearGraphic
                        data={values}
                        labels={labels}
                        devise={capitalize(currentDevise).toUpperCase()}
                        key="graphic"
                        legend={"Les achats du client durant "+currentTime+" en "+currentDevise}
                    />
                    {
                        articleData.length>0?(
                            <CircularGraphic
                                list={articleData}
                                key="circularKey"
                            />
                        ):(
                            <View style={{ padding:30 }}>
                                <Text>
                                    MASS
                                </Text>
                            </View>
                        )
                    }
                </ScrollView>
            </View>
        )
        }
        {
            loading==false?(
                    <FAB.Group
                        open={visibleFab}
                        icon={visibleFab?'close':"plus"}
                        onStateChange={()=>setVisibleFab(!visibleFab)}
                        fabStyle={{ backgroundColor:R.color.colorPrimary }}
                        actions={[
                            {
                                icon:"calendar",
                                label:"Temps"
                            },
                            {
                                icon:"plus",
                                label:"Categorie",
                                onPress:()=>setShowMenuType(true)
                            }
                        ]}
                    />
            ):null
        }
        <ConfirmDialog 
            visible={showConversion} animationType='fade'
            positiveButton={{ 
                onPress:()=>onIncomeTotal(),
                title:"Valider"
             }}
            negativeButton={{ 
                onPress:()=>setShowConversion(false),
                title:"Annuler"
             }}
            onTouchOutside={()=>setShowConversion(false)}
        >
            <View>
                <FlatList
                    data={conversions}
                    renderItem={renderConversion}
                    keyExtractor={item=>item.id}
                    key="conversionsKeys"
                />
            </View>
        </ConfirmDialog>
        <Dialog
            visible={showDevise} animationType='fade'
            positiveButton={()=>setShowDevise(false)}
            negativeButton={()=>setShowDevise(false)}
            onTouchOutside={()=>setShowDevise(false)}
        >
            <View>
                <FlatList
                    data={devises}
                    renderItem={({item,index})=>(
                        <MenuItem
                            title={item.name}
                            onPress={()=>showGraphic(buys,new Date(),item)}
                        />
                    )}
                    keyExtractor={item=>item.id}
                    key="conversionsKeys"
                />
            </View>
        </Dialog>

        <Dialog
            visible={showMenuType} animationType='fade'
            positiveButton={()=>setShowMenuType(false)}
            negativeButton={()=>setShowMenuType(false)}
            onTouchOutside={()=>setShowMenuType(false)}
        >
            <View>
                <FlatList
                    data={[
                        {id:1,name:"Une devise",use:true,onPress:()=>{setShowDevise(true);setShowMenuType(false)}},
                        {id:2,name:"Revenu total",use:conversions.length>0,onPress:()=>{setShowConversion(true);setShowMenuType(false)}},
                        {id:3,name:"Meilleur article",use:true},
                    ]}
                    renderItem={({item,index})=>(
                        <MenuItem
                            title={item.name}
                            onPress={item.use?item.onPress:null}
                        />
                    )}
                    keyExtractor={item=>item.id}
                    key="menuKeys"
                />
            </View>
        </Dialog>
        </>
    )
}