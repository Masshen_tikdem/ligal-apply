import S from '../settings.json';

const initialState={
    user:{status:false,value:null,work:null},//about user
    article:{sale:false,market:false,added:false,category:false,journal:false},//manage article for sale space,market and for show category
    category:{sale:false,market:false,category:false},
    viewArticle:{info:false,stock:false,sale:false,stat:false},
    client:{name:"",phone:"",id:0,updated:false},
    subscription:{status:false,validate_at:null},
    shop:{value:null},
    block:{value:false},
    sale:{devise:false,article:false,category:false},
    setting:{internet:true,addArticle:false,addDevise:false,addCategory:false},
    service:{shop:false,user:false,customer:false,article:false,buy:false,stock:false,category:false,devise:false,right:false,work:false,link:false,dashboard:false},
    right:{devise:0,vente_produit:0,journal:0,dashboard:0,stock:0,sales:0,customer:0,article:0,category:0}
}

const type=S.EVENT;

export default function App(state=initialState,action){
    let nextState;
    switch(action.type){
        case type.onChangeMarketArticle:
            nextState={
                ...state,
                article:{
                    sale:(action.sale==true || action.sale==false)?action.sale:state.article.sale,
                    market:(action.market==true || action.market==false)?action.market:state.article.market,
                    added:(action.added==true || action.added==false)?action.added:state.article.added,
                    category:(action.category==true || action.category==false)?action.category:state.article.category,
                    journal:(action.journal==true || action.journal==false)?action.journal:state.article.journal,
                }
            }
        return nextState || state;

        case type.onChangeArticle:
            nextState={
                ...state,
                viewArticle:{
                    info:(action.info==true || action.info==false)?action.info:state.viewArticle.info,
                    stock:(action.stock==true || action.stock==false)?action.stock:state.viewArticle.stock,
                    sale:(action.sale==true || action.sale==false)?action.sale:state.viewArticle.sale,
                    stat:(action.stat==true || action.stat==false)?action.stat:state.viewArticle.stat,
                }
            }
        return nextState || state;

        case type.onChangeSubscription:
            nextState={
                ...state,
                subscription:{
                    status:(action.status==true || action.status==false)?action.status:state.subscription.status,
                    validate_at:(action.date!=null && action.date!=undefined)?action.date:state.subscription.validate_at,
                }
            }
        return nextState || state;

        case type.onChangeClient:
            nextState={
                ...state,
                client:{
                    name:(action.name!=null && action.name!=undefined)?action.name:state.client.name,
                    phone:(action.phone!=null && action.phone!=undefined)?action.phone:state.client.phone,
                    id:(action.id>0)?action.id:state.client.id,
                    updated:(action.updated==true || action.updated==false)?action.updated:state.client,
                }
            }
        return nextState || state;

        case type.onChangeMarketCategory:
            nextState={
                ...state,
                category:{
                    category:(action.category==true || action.category==false)?action.category:state.category.category,
                    market:(action.market==true || action.market==false)?action.market:state.category.market,
                    sale:(action.sale==true || action.sale==false)?action.sale:state.category.sale,
                }
            }
        return nextState || state;

        case type.onChangeUser:
            nextState={
                ...state,
                user:{
                    status:(action.status==true || action.status==false)?action.status:state.user.status,
                    value:(action.value!=null && action.value!=undefined)?action.value:state.user.value,
                    work:(action.work!=null && action.work!=undefined)?action.work:state.user.work,
                }
            }
        return nextState || state;

        case type.OnExecuteService:
            nextState={
                ...state,
                service:{
                    shop:(action.shop==true || action.shop==false)?action.shop:state.service.shop,
                    user:(action.user==true || action.user==false)?action.user:state.service.user,
                    customer:(action.customer==true || action.customer==false)?action.customer:state.service.customer,
                    article:(action.article==true || action.article==false)?action.article:state.service.article,
                    buy:(action.buy==true || action.buy==false)?action.buy:state.service.buy,
                    stock:(action.stock==true || action.stock==false)?action.stock:state.service.stock,
                    category:(action.category==true || action.category==false)?action.category:state.service.category,
                    devise:(action.devise==true || action.devise==false)?action.devise:state.service.devise,
                    right:(action.right==true || action.right==false)?action.right:state.service.right,
                    work:(action.work==true || action.work==false)?action.work:state.service.work,
                    link:(action.link==true || action.link==false)?action.link:state.service.link,
                    dashboard:(action.dashboard==true || action.dashboard==false)?action.dashboard:state.service.dashboard
                }
            }
        return nextState || state;

        case type.onChangeShop:
            nextState={
                ...state,
                shop:{
                    value:(action.value!=null && action.value!=undefined)?action.value:state.shop.value,
                }
            }
        return nextState || state;

        case type.OnChangeRight:
            nextState={
                ...state,
                right:{
                    devise:action.devise>=0?action.devise:state.right.devise,
                    vente_produit:action.vente_produit>=0?action.vente_produit:state.right.vente_produit,
                    journal:action.journal>=0?action.journal:state.right.journal,
                    dashboard:action.dashboard>=0?action.dashboard:state.right.dashboard,
                    stock:action.stock>=0?action.stock:state.right.stock,
                    sales:action.sales>=0?action.sales:state.right.sales,
                    customer:action.customer>=0?action.customer:state.right.customer,
                    article:action.article>=0?action.article:state.right.article,
                    category:action.category>=0?action.category:state.right.category,
                }
            }
        return nextState || state;

        case type.OnChangeSetting:
            nextState={
                ...state,
                setting:{
                    internet:(action.internet==true || action.internet==false)?action.internet:state.setting.internet,
                }
            }
        return nextState || state;

        case type.OnChangeBlock:
            nextState={
                ...state,
                block:{
                    value:(action.value==true || action.value==false)?action.value:state.block.value,
                }
            }
        return nextState || state;

        case type.onChangeSaleInfo:
            nextState={
                ...state,
                sale:{
                    article:(action.article==true || action.article==false)?action.article:state.sale.article,
                    devise:(action.devise==true || action.devise==false)?action.devise:state.sale.devise,
                    category:(action.category==true || action.category==false)?action.category:state.sale.category,
                    
                }
            }
        return nextState || state;

        default:
            return state;
        }
}
