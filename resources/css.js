import { StyleSheet } from "react-native"

export const fab=()=>{
    const p= StyleSheet.create({
        fab:{
            position:'absolute',
            bottom:0,
            margin:30
        }
    })
    
    return p;
}

