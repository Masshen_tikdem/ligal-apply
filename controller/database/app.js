import * as AppModel from '../../model/local/app'
import Logiciel from '../database/logiciel';


export async function checkVersion(){
    let rep=false;
    const currentVersion=13;
    try {
        const info=await AppModel.get();
        let query="";
        let version=info==null?0:info.version;
        if(version<currentVersion){
            if(info==null){
                AppModel.store({version:currentVersion});
            }else{
                AppModel.update({id:info.id,version:currentVersion})
            }
            const list=[];
            query="ALTER TABLE subscriptions ADD COLUMN private_key TEXT;";
            list.push({query:query});
            query="ALTER TABLE subscriptions ADD COLUMN unity INTEGER;";
            list.push({query:query});
            query="ALTER TABLE stocks ADD COLUMN id_devise INTEGER;";
            list.push({query:query})
            query="ALTER TABLE articles ADD COLUMN mode TEXT;";
            list.push({query:query})
            query="ALTER TABLE articles ADD COLUMN stockage INTEGER;";
            list.push({query:query})
            query="ALTER TABLE articles ADD COLUMN unity TEXT;";
            list.push({query:query});
            query="ALTER TABLE buys ADD COLUMN bill TEXT;";
            list.push({query:query})
            query="ALTER TABLE buys ADD COLUMN description TEXT;";
            list.push({query:query});
            query="ALTER TABLE works ADD COLUMN identity INTEGER;";
            list.push({query:query});
            query="ALTER TABLE subscriptions ADD COLUMN id_user INTEGER;";
            list.push({query:query});
            list.map(async(value)=>{
                const logiciel=new Logiciel();
                const table=await logiciel.ExecuteQuery(value.query);
                if(table.rowsAffected>0){
                    rep=true;
                }
            })
            
        }
    } catch (error) {
        console.log('app error',error);
    }
    return rep;
}