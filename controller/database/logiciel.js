import React from 'react';
import SQLite from 'react-native-sqlite-storage';

const db_name="extract_ligal.db";
export default class App extends React.Component{

    constructor(props){
        super(props)
        SQLite.DEBUG=true;
    }
    /**
  * Execute sql queries
  * 
  * @param sql
  * @param params
  * 
  * @returns {resolve} results
  */
  ExecuteQuery =(sql, params = []) => new Promise((resolve, reject) => {
    //const db=this.openDatabase();
    /*SQLite.openDatabase({
      name: 'extract_ligal.db',
      location: 'default',
    }).then(database=>{
      database.transaction((trans) => {
        trans.executeSql(sql, params, (trans, results) => {
          resolve(results);
        },
          (error) => {
            reject(error);
          });
      });
      database.close();
    })*/
    db.transaction((trans) => {
      trans.executeSql(sql, params, (trans, results) => {
        resolve(results);
      },
        (error) => {
          reject(error);
        });
    });
  });

}
