import React from 'react'


let list=[]
class App{
    
    constructor(){
        list=[];
    }

    addValue=(attr="",value="")=>{
        list.push({attr,value});
    }
    getValues=()=>{
        return list;
    }
    getInsertCommand=({list=[],table=""})=>{
        let rep="";
        const items=[];
        try {
            if(list==undefined || list==null || table==undefined || table==null){
                return null;
            }
            list.map((item)=>{
                if(!(item.value==null || item.value==undefined)){
                    items.push(item);
                }
            })
            rep="INSERT INTO "+table+" (";
            let i=0;
            items.map((item,i)=>{
                if(!(item.value==null || item.value==undefined)){
                    rep+=i>0?",":""
                    rep+=item.attr;
                    i++;
                }
            })
            rep+=") VALUES (";
            i=0;
            items.map((item,i)=>{
                if(!(item.value==null || item.value==undefined)){
                    let value=item.value;
                    if(typeof item.value=='string'){
                        value="'"+item.value.replace("'","''").toLowerCase()+"'";
                    }else if(typeof item.value=='number'){
                        value="'"+item.value+"'";
                    }
                    rep+=i>0?",":"";
                    rep+=value;
                    i++;
                }
            })
            rep+=");";
        } catch (error) {
            
        }
        return rep;
    }

    getUpdateCommand=({list=[],table="",whereClause=""})=>{
        let rep="";
        try {
            if(list==undefined || list==null || table ==undefined || table==null){
                return null;
            }
            rep="UPDATE "+table+" SET ";
            let i=0;
            list.map((item)=>{
                if(!(item.value==null || item.value==undefined)){
                    rep+=i>0?",":"";
                    rep+=item.attr+"="
                    let value=item.value;
                    if(typeof item.value=='string'){
                        value="'"+item.value.replace("'","''").toLowerCase()+"'";
                    }else if(typeof item.value=='number'){
                        value="'"+item.value+"'";
                    }
                    rep+=value;
                    i++;
                }
            })
            const where=(whereClause!=undefined && whereClause!=null)?whereClause:null;
            if(where!=null){
                rep+=" WHERE "+where;
            }
        } catch (error) {
            
        }
        return rep;
    }
}
export default App;