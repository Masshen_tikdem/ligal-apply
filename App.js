import React from 'react';
import { DarkTheme, NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack';
import {Provider as PaperProvider} from 'react-native-paper';
import Home from './view/Home';
import Splash from './view/Splash';
import R from './resources/styles.json';
import { StatusBar } from 'react-native';
import Logiciel from './controller/database/logiciel';
import SQLite from 'react-native-sqlite-storage';
import S from './resources/sqlite.json';
import ViewCategory from './view/ViewCategory';
import ViewArticle from './view/ViewArticle';
import { Provider } from 'react-redux';
import Store from './resources/redux/store';
import ClientInterface from './view/CustomerInterface';
import Screen from './view/Screen';
import Login from './view/Begin';
import GotoShop from './view/GotoShop';
import Profil from './view/Profil';
import { Component } from 'react';
import Agent from './view/shop/Agent';
import Customer from './view/shop/Customer';
import Shop from './view/shop/showShop';
import ShowBuy from './view/ShowBuy';
import ShowStock from './view/ShowStock';
import viewAgent from "./view/ViewAgent";
import Subscriptions from './view/shop/Subscriptions';
import CustomerImpact from './view/CustomerImpact';
//customers
import CustomerList from './view/ViewCustomer/List';
import CustomerFavorite from './view/ViewCustomer/ListFavorite';
import CustomerHonor from './view/ViewCustomer/ListHonor';
import CustomerPromotion from './view/ViewCustomer/ListPromotion';
import CustomerRare from './view/ViewCustomer/ListRare';
//stats
import customerStat from './view/CustomerStats';
//
import ShowWebDesktop from './view/ShowWebDestop';
import ShowWebContract from './view/ShowWebContract';
//activities
import ActivityScreen from './view/Activity';
import ShowActivity from './view/activity/showActivity';
//home navigator
import journal from './view/Home/journal';
import dashboard from './view/Home/dashboard';
import devise from './view/Home/devise';
import info from './view/Home/info';
import transaction from './view/Home/transaction';
import customer from './view/Home/customer';
import market from './view/Home/market';
import * as AppController from './controller/database/app';
import { HomeScreen } from './view/HomeScreen';
import { DashboardScreen } from './view/Home/dashboardScreen';

//SQLite.DEBUG(false);

global.db = SQLite.openDatabase(
  {
    name: 'extract_ligal.db',
    location: 'default',
  },
  () => { },
  error => {
  }
);


async function createTable(){
  try {
    const logiciel=new Logiciel();
    await logiciel.ExecuteQuery(S.AGENT);
    await logiciel.ExecuteQuery(S.ARTICLE);
    await logiciel.ExecuteQuery(S.BUY);
    await logiciel.ExecuteQuery(S.CLIENT);
    await logiciel.ExecuteQuery(S.CONVERSIONS);
    await logiciel.ExecuteQuery(S.DEVISE);
    await logiciel.ExecuteQuery(S.FAMILLY);
    await logiciel.ExecuteQuery(S.LINK);
    await logiciel.ExecuteQuery(S.NOTIFICATION);
    await logiciel.ExecuteQuery(S.REDUCE);
    await logiciel.ExecuteQuery(S.RIGHT);
    await logiciel.ExecuteQuery(S.SETTING);
    await logiciel.ExecuteQuery(S.SHOP);
    await logiciel.ExecuteQuery(S.STOCK);
    await logiciel.ExecuteQuery(S.SUBSCRIPTIONS);
    await logiciel.ExecuteQuery(S.WORKS);
    await logiciel.ExecuteQuery(S.APP);
    await logiciel.ExecuteQuery(S.ACTIVITY);
    await logiciel.ExecuteQuery(S.TARGET);
    await AppController.checkVersion();
  } catch (error) {
    
  }
}


const Stack=createStackNavigator()


export default class App extends Component{

  constructor(props){
    super(props)
  }


render(){
  
  createTable();

  

  return(
    <Provider  store={Store} >
    <StatusBar animated={true} backgroundColor={R.color.colorPrimary} />
    <PaperProvider >  
    <NavigationContainer
       theme={{colors:{
        border:R.color.colorAccent,
        primary:R.color.colorSecondary,
        notification:R.color.colorPrimaryDark,
        card:R.color.colorPrimary,
        text:R.color.background,
        background:R.color.background
      },dark:false
     }}
    >
      <Stack.Navigator>
        <Stack.Screen name="Splash" component={Splash} options={{headerShown:false}} />
        <Stack.Screen name="Screen" component={Screen} options={{headerShown:false}} />
        <Stack.Screen name="Begin" component={Login} options={{headerShown:false}} />
        <Stack.Screen name="Configuration" component={GotoShop} options={{headerShown:false}} />
        <Stack.Screen name='Home' component={HomeScreen} options={{
          headerShown:false
        }} />
        
        <Stack.Screen 
          name="ViewCategory"
          component={ViewCategory}
          options={{
            title:"Détails"
          }}
        />
        <Stack.Screen 
          name="ViewSubCategory"
          component={ViewCategory}
          options={{
            title:"Détails"
          }}
        />
        <Stack.Screen 
          name="ViewArticle"
          component={ViewArticle}
          options={{
            title:"Détails"
          }}
        />
        <Stack.Screen 
          name="WebDesktop"
          component={ShowWebDesktop}
          options={{
            title:"Bureau Ligal"
          }}
        />
        <Stack.Screen 
          name="WebContract"
          component={ShowWebContract}
          options={{
            title:"Terme et contrat"
          }}
        />
        <Stack.Screen 
          name="ViewClient"
          component={ClientInterface}
          options={{
            title:"Chercher le client"
          }}
        />
        <Stack.Screen 
          name="Profil"
          component={Profil}
          options={{
            title:"Profil"
          }}
        />
        <Stack.Screen 
          name="Shop"
          component={Shop}
          options={{
            title:"Votre boutique"
          }}
        />
        <Stack.Screen 
          name="Customer"
          component={Customer}
          options={{
            title:"Espace client"
          }}
        />
        <Stack.Screen 
          name="Agent"
          component={Agent}
          options={{
            title:"Gestion des agents"
          }}
        />

        <Stack.Screen 
          name="Buy"
          component={ShowBuy}
          options={{
            title:"Détails de la vente"
          }}
        />
        <Stack.Screen 
          name="ShowStock"
          component={ShowStock}
          options={{
            title:"Détails sur l'approvisionnement"
          }}
        />
        <Stack.Screen 
          name="ShowAgent"
          component={viewAgent}
          options={{
            title:"Fiche de l'agent"
          }}
        />
        <Stack.Screen 
          name="ShowSubscription"
          component={Subscriptions}
          options={{
            title:"Abonnements"
          }}
        />
        <Stack.Screen 
          name="ShowCustomerImpact"
          component={CustomerImpact}
          options={{
            title:"Stat des clients"
          }}
        />
        <Stack.Screen 
          name="ShowCustomerList"
          component={CustomerList}
          options={{
            title:"Liste de clients"
          }}
        />
        <Stack.Screen 
          name="ShowCustomerHonor"
          component={CustomerHonor}
          options={{
            title:"Liste de clients"
          }}
        />
        <Stack.Screen 
          name="ShowCustomerFavorite"
          component={CustomerFavorite}
          options={{
            title:"Liste de clients"
          }}
        />
        <Stack.Screen 
          name="ShowCustomerRare"
          component={CustomerRare}
          options={{
            title:"Liste de clients"
          }}
        />
        <Stack.Screen 
          name="ShowCustomerPromotion"
          component={CustomerPromotion}
          options={{
            title:"Liste de clients"
          }}
        />
        <Stack.Screen 
          name="ShowActivity"
          component={ActivityScreen}
          options={{
            title:"Les activités de la boutique"
          }}
        />
        {/**side navigation */}
        <Stack.Screen name='Journal' component={journal} options={{
            title:"Journal",
            }}  />
        <Stack.Screen 
          name='Info'
          component={info}
          options={{
            title:"Informations",
          }}  />
        <Stack.Screen 
          name='Dashbord'
          component={DashboardScreen}
          options={{
            title:"Tableau de bord",
              }}  />
        <Stack.Screen 
          name='Market'
          component={market}
          options={{
            title:"Marché"
              }}
        />
        <Stack.Screen 
          name='Devises'
          component={devise} 
          options={{
            title:"Devises",
          }}  />
        <Stack.Screen 
          name='Transaction' 
          component={transaction} 
          options={{title:"Comptes",
        }}  />
        <Stack.Screen 
          name='Clients' 
          component={customer} 
          options={{title:"Clients",
          }}  />
        <Stack.Screen 
          name='CustomerStat' 
          component={customerStat} 
          options={{title:"Statitiques",
          }}  />
        <Stack.Screen 
          name='ShowActivityCat' 
          component={ShowActivity} 
          options={{title:"Les activités",
        }}  />
      </Stack.Navigator>
    </NavigationContainer>
    </PaperProvider>   
    </Provider>
  )
}
}