import React from 'react';
import { View } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { Avatar, Card, Text } from 'react-native-elements';
import * as Manager from '../Manager';
import R from '../resources/styles.json';


export default function App({onPress,logo,title}){

    return(
        <TouchableOpacity onPress={onPress} style={{flex:1}} activeOpacity={0.5}>
            <View style={{flex:1,elevation:10,maxWidth:200}}>
                <Card containerStyle={{borderRadius:20,backgroundColor:R.color.background,elevation:10}}>
                    <View style={{padding:20,alignItems:'center',opacity:1}}>
                        <Avatar
                            size="large"
                            icon={{name:'cube',type:'font-awesome',size:50,color:R.color.colorPrimary}}
                            source={{uri:logo}}
                            placeholderStyle={{backgroundColor:'transparent'}}
                        />
                    </View>
                </Card>
                <Text numberOfLines={1} style={{textAlign:'center',fontSize:16,padding:10,fontFamily:'Poppins-Black'}}>
                    {Manager.capitalize(title).toUpperCase()}
                </Text>
            </View>
        </TouchableOpacity>
    )
}