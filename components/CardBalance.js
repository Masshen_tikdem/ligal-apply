import React from 'react';
import { View } from 'react-native';
import {Card, Icon,Tooltip} from 'react-native-elements';
import { Divider, Text } from 'react-native-paper';
import R from '../resources/styles.json';
import DateTime from 'date-and-time';
import { StyleSheet } from 'react-native';

const styles=StyleSheet.create({
    article:{
        fontFamily:'Poppins-Light',fontSize:12,padding:2
    }
})

export default function App({name,price,article,date,color=R.color.colorPrimary}){

    const getHour=(value)=>{
        let rep="";
        try {
            if(value!=null && value!=undefined){
                const my=DateTime.parse(value,"YYYY-MM-DD HH:mm:ss");
                rep=DateTime.format(my,"HH:mm");
            }
        } catch (error) {
        }
        return rep;
    }

    return(
        <View style={{ flex:1 }}>
            <Card containerStyle={{ borderRadius:20,padding:0,borderColor:color }}>
                <View>
                    <View style={{ alignItems:'center' }}>
                        <Text style={{ padding:10,paddingLeft:20,paddingRight:20,fontFamily:'Poppins-Black',fontSize:16 }}>
                            {price}
                        </Text>
                    </View>
                    <Divider/>
                    <View style={{ flexDirection:'row',alignItems:'center',padding:5,paddingTop:0,paddingBottom:0 }}>
                        <View style={{ alignItems:'center',padding:5,paddingTop:0,paddingBottom:0 }}>
                            <View style={{ padding:2 }}>
                                <Icon
                                    type="entypo"
                                    name="back-in-time"
                                    size={20}
                                />
                            </View>
                            <Text numberOfLines={1} style={{ fontFamily:"Poppins-Bold" }}>
                                {getHour(date)}
                            </Text>
                        </View>
                        <View>
                            <View style={{ width:1,flex:1,borderWidth:0.5,margin:2 }} />
                        </View>
                        <View style={{ flex:1 }}>
                            <View style={{ flexDirection:'row',alignItems:'center' }}>
                                <View>
                                    <Icon
                                        type="entypo"
                                        name="bowl"
                                    />
                                </View>
                                <View style={{ flex:1 }}>
                                    <Tooltip backgroundColor={R.color.colorAccent} popover={<Text style={styles.article}>{article}</Text>}>
                                        <Text numberOfLines={1} style={styles.article}>
                                            {article}
                                        </Text>
                                    </Tooltip>
                                </View>
                            </View>
                            
                        </View>
                    </View>
                </View>
            </Card>
        </View>
    )
}