import React from 'react';
import { Image } from 'react-native';
import { View,ScrollView } from 'react-native';
import { Card, Header, Icon, Overlay, Text } from 'react-native-elements';
import R from '../resources/styles.json';
import * as Manager from '../Manager';

export default function App({visible,onClose,title,content,photo}){
    App.defaultProps={
        visible:false,
        title:'',
        content:'',
        photo:null
    }

    return(
        <View>
            <Overlay
                fullScreen
                isVisible={visible}
                visible={visible}
                key='detail'
                animationType='slide'
                overlayStyle={{padding:0}}
            >
            <View style={{ flex:1 }}>
                <Header
                    rightComponent={
                        <Icon name='chevron-down' type='ionicon' color={R.color.background} onPress={onClose}  />
                    }
                    centerComponent={{text:"Détails de l'information",style:{fontSize:18,color:R.color.background}}}
                    placement='left'
                    barStyle='light-content'
                    backgroundColor={R.color.colorPrimary}
                />
                <View style={{flex:1}}>
                <ScrollView>
                    <Text  h3 h3Style={{textAlign:'center',opacity:0.7,padding:10}} >
                        {title}
                    </Text>
                    <Card.Divider/>
                    {
                        photo!=null?(
                            <Image 
                                source={{uri:Manager.getImage(photo)}} 
                                resizeMode='cover'
                                style={{flex:1}}
                                height={300}
                            />
                        ):null
                    }
                    <Text style={{textAlign:'justify',fontSize:18,padding:10}}>
                        {content}
                    </Text>
                </ScrollView>
                </View>
                </View>
            </Overlay>
            </View>
    )
}