import React from 'react';
import { View } from 'react-native';
import { Avatar, ListItem, Text } from 'react-native-elements';
import { getTitle } from '../Manager';
import R from '../resources/styles.json';



export default function App({title,subTitle,count,opacity,onPress,logo="p"}){

    App.defaultProps={
        title:'',
        subTitle:'',
        count:0,
        opacity:1
    }

    return(
        <View style={{flex:1,margin:5}}>
        <ListItem
            onPress={onPress}
            activeOpacity={0.90}
            containerStyle={{backgroundColor:R.color.background}}
        >
            <Avatar
                rounded
                title={getTitle(title)}
                placeholderStyle={{backgroundColor:R.color.background,elevation:0}}
                containerStyle={{backgroundColor:R.color.background,elevation:10}}
                titleStyle={{fontSize:22,elevation:10,color:R.color.colorPrimaryDark,fontFamily:'Poppins-Black'}}
                size='medium'
                source={{uri:logo,scale:10}}
            />
            <View style={{flexDirection:'row',alignItems:'center',
                backgroundColor:R.color.colorSecondary,
                flex:1,marginLeft:-30,paddingLeft:20,
                borderRadius:15,opacity:opacity
            }}>
                <View style={{flex:1}}>
                    <Text style={{fontSize:18,padding:5,fontFamily:'Poppins-Black'}}>
                       {title}
                    </Text>
                    <Text style={{fontSize:14,fontFamily:"Poppins-Light",padding:5,color:opacity<1?"#000":R.color.background}}>
                        {subTitle}
                    </Text>
                </View>
                <Avatar
                    icon={{name:'chevron-right',type:'font-awesome',color:'#000'}}
                    rounded
                    containerStyle={{backgroundColor:R.color.background,margin:10,elevation:10}}
                />
            </View>
        </ListItem>
        </View>
    )
}