import React from 'react';
import { TouchableOpacity } from 'react-native';
import { View } from 'react-native';
import { Avatar, Button, Card, Icon, Text } from 'react-native-elements';
import { Divider } from 'react-native-paper';
import { capitalize } from '../Manager';
import R from '../resources/styles.json';
import S from '../resources/settings.json';
import DateTime from 'date-and-time';
import fr from 'date-and-time/locale/fr';

const getIcon=(type)=>{
    const value=capitalize(type).toLowerCase();
    let rep={icon:"user-secret",type:"font-awesome"};
    let icon="";
    let cat="";
    switch (value) {
        case capitalize(S.GENERAL.TYPE.CUSTOMER).toLowerCase():
            icon="customerservice";
            cat="antdesign";
        break;
        case capitalize(S.GENERAL.TYPE.BUY).toLowerCase():
            icon="shoppingcart";
            cat="antdesign";
        break;
        case capitalize(S.GENERAL.TYPE.STOCK).toLowerCase():
            icon="truck";
            cat="font-awesome";
        break;
        case capitalize(S.GENERAL.TYPE.USING).toLowerCase():
            icon="plug";
            cat="font-awesome";
        break;
        default:
            icon="info";
            cat="font-awesome";
        break;
    }
    rep={icon:icon,type:cat};
    return rep;
}

const getDate=(value)=>{
    let rep="";
    try {
        DateTime.locale(fr);
        const date=DateTime.parse(value,"YYYY-MM-DD HH:mm:ss");
        const today=new Date();
        if(DateTime.isSameDay(date,today)){
            rep="aujourd'hui"
        }else if(DateTime.isSameDay(DateTime.addDays(date,1),today)){
            rep="hier"
        }else{
            rep=value;
        }
    } catch (error) {
        
    }
    return rep;
}

const replaceCharacter=(value="")=>{
    let rep="";
    try {
        rep=value.replace("/*",``);
        rep=rep.replace("*/",``);
    } catch (error) {
        
    }
    return rep;
}

export default function App({type,content,title,date,onPress,onReport}){

    const rep=getIcon(type);
    return(
            <View style={{ flex:1,backgroundColor:R.color.background,elevation:5,margin:5 }}>
            <View style={{ flexDirection:'row',alignItems:'center',padding:10 }}>
                <View style={{ padding:5 }}>
                    <Avatar
                        icon={{ name:rep.icon,type:rep.type }}
                        size='large' rounded
                        containerStyle={{ backgroundColor:R.color.colorPrimary }}
                    />
                </View>
                <View style={{ flex:1 }}>
                    <Text numberOfLines={1} style={{ fontFamily:'Poppins-Bold',fontSize:16,padding:5 }}>
                        {capitalize(title).toUpperCase()}
                    </Text>                    
                    <Text numberOfLines={2} style={{ fontFamily:'Poppins-Light',paddingLeft:5,paddingRight:5,fontSize:14,textAlign:'justify' }}>
                        {replaceCharacter(content)}
                    </Text>
                    <View style={{ flexDirection:'row',alignItems:'center',alignSelf:'flex-end' }}>
                        <View>
                            <Text style={{ fontFamily:'Poppins-Italic', }}>
                                {getDate(date)}
                            </Text>
                        </View>
                        <View style={{ padding:5 }}>
                            <Icon
                                name="checkmark"
                                type='ionicon'

                            />
                        </View>

                    </View>
                </View>
            </View>
            <View style={{ flexDirection:'row',alignItems:'center',justifyContent:'flex-end' }}>
                <View>
                    <Button
                        title="Afficher"
                        type='outline'
                        onPress={onPress}
                        buttonStyle={{ padding:5,paddingLeft:20,paddingRight:20 }}
                        titleStyle={{ color:R.color.colorPrimary,fontFamily:'Poppins-Light',fontWeight:'normal' }}
                    />
                </View>
                <View>
                    <Button
                        title="Signaler"
                        type='outline'
                        onPress={onReport}
                        buttonStyle={{ padding:5,paddingLeft:20,paddingRight:20 }}
                        titleStyle={{ color:R.color.colorPrimary,fontFamily:'Poppins-Light',fontWeight:'normal' }}
                    />
                </View>
            </View>
            </View>
    )
}