import React from 'react';
import { View } from 'react-native';
import { Avatar, ListItem, Text } from 'react-native-elements';
import { getTitle } from '../Manager';
import R from '../resources/styles.json';



export default function App({title,remise=null,size=0,showPrice=true,subTitle="",service=true,count,opacity,onPress,logo="p",price="",taked=false}){

    App.defaultProps={
        title:'',
        subTitle:'',
        count:0,
        opacity:1,
        price:"",
        service:false
    }

    return(
        <View style={{flex:1,margin:5}}>
        <ListItem
            onPress={onPress}
            activeOpacity={0.91}
            containerStyle={{elevation:3}}
            style={{margin:5,backgroundColor:taked==true?R.color.colorPrimary:R.color.background,opacity:opacity}}
        >
            <Avatar
                title={getTitle(title)}
                placeholderStyle={{backgroundColor:'transparent'}}
                containerStyle={{backgroundColor:'transparent'}}
                titleStyle={{fontSize:20,color:R.color.colorPrimaryDark,fontFamily:'Poppins-Black'}}
                size='medium'
                source={{uri:logo,scale:10}}
            />
            <View style={{flex:1}}>
                <Text style={{fontSize:16,padding:5,paddingBottom:0,fontFamily:'Poppins-Black'}}>
                    {title}
                </Text>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    {
                        service==false?(
                            <Text style={{fontSize:12,fontFamily:'Poppins-Black',padding:3,paddingLeft:15,paddingRight:15,
                                backgroundColor:(count>0 || size>0)?R.color.colorSecondary:'brown',color:R.color.background
                                ,borderRadius:8
                            }}>
                                {count}
                            </Text>
                        ):null
                    }
                    {
                        showPrice==true?(
                            <Text style={{color:R.color.colorPrimary,padding:3,fontSize:12,fontFamily:'Poppins-Bold'}}>
                                {price}
                            </Text>
                        ):null
                    }
                </View>
                {
                    (subTitle!="" && subTitle!=null && subTitle!=undefined)?(
                        <Text>
                            {subTitle}
                        </Text>
                    ):null
                }
            </View>
            {
                (remise>0)?(
                    <View>
                        <Text style={{ padding:5,fontSize:16,fontFamily:'Poppins-Black',color:'red' }}>
                            <Text style={{ padding:5,fontSize:16,fontFamily:'Poppins-Thin',color:'red' }}>-</Text>{remise+" %"}
                        </Text>
                    </View>
                ):null
            }
        </ListItem>
        </View>
    )
}