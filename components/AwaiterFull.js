import React from 'react';
import { ActivityIndicator, View } from 'react-native';
import { Overlay } from 'react-native-elements';


export default function App({visible,color,size}){

    App.defaultProps={
        visible:false,
        color:'#fff',
        size:50
    }

    return(
        <Overlay animationType='slide' fullScreen={true} isVisible={visible} visible={visible}
        overlayStyle={{backgroundColor:'transparent'}}>
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <ActivityIndicator
                    size={size}
                    color={color}
                />
            </View>
        </Overlay>
    )
}