import React from 'react';
import { Text } from 'react-native';
import { ActivityIndicator, View } from 'react-native';


export default function App({color,size,message=""}){

    return(
        <View style={{justifyContent:'center',alignItems:'center',flex:1}}>
            <ActivityIndicator color={color} size={size} />
            <Text style={{ fontFamily:"Poppins-Light",textAlign:'center',padding:8 }}>
                {message}
            </Text>
        </View>
    )
}