import React from 'react';
import { View } from 'react-native';
import { Avatar, CheckBox, Text } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { capitalize } from '../Manager';
import R from '../resources/styles.json';


export default function App({subtitle="",last=null,days=0,name="",checked=false,onPress,onChecked}){

    return(
        <TouchableOpacity onPress={onPress}>
            <View style={{ flexDirection:'row',alignItems:'center',padding:5 }}>
                <Avatar
                    icon={{name:"user-o",type:"font-awesome",color:R.color.background}}
                    placeholderStyle={{backgroundColor:R.color.colorPrimary}}
                    containerStyle={{backgroundColor:R.color.colorPrimary}}
                    rounded size="medium"
                />
                <View style={{padding:5,flex:1}}>
                    <View>
                        <Text numberOfLines={1} style={{fontFamily:'Poppins-Regular',fontSize:16,padding:10,paddingBottom:0}}>
                            {capitalize(name).toUpperCase()}
                        </Text>
                        <Text numberOfLines={1} style={{fontFamily:"Poppins-Bold",fontSize:14,padding:10,paddingTop:0}}>
                            {subtitle}
                        </Text>
                    </View>
                    {
                    (last!=null && last!=undefined && days>0)?(
                    <View style={{ flexDirection:'row',padding:5,alignItems:'center' }}>
                        <View style={{ flex:1 }}>
                            <Text numberOfLines={1}>
                                {last}
                            </Text>
                        </View>
                        <View>
                            <Text numberOfLines={1}>
                                {"depuis "+days+" jours"}
                            </Text>
                        </View>
                    </View>):null}
                </View>
                <View>
                    <CheckBox
                        checked={checked}
                        checkedColor={R.color.colorSecondary}
                        onPressIn={onChecked}
                    />
                </View>
            </View>
        </TouchableOpacity>
    )
}