import React,{useEffect,useState} from 'react';
import Awaiter from './Waiter';
import R from '../resources/styles.json';
import EmptyMessage from './EmptyMessage';
import {Overlay} from 'react-native-elements';
import {View,FlatList} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler'
import { Appbar,Text } from 'react-native-paper';
import CardBalance from './CardBalance';
import { getArticleName, getPrice } from '../Manager';
import { StyleSheet } from 'react-native';
import DateTime from 'date-and-time';

const styles=StyleSheet.create({
    total:{
        padding:20,
        fontFamily:"Poppins-Bold",
        fontSize:16
    }
})


export default function App({visible=false,onClose,actives=[],passives=[],date,totalBuy="0",totalSpent="0",numBuy=null,numSpent=null}){

    console.log("Passives-r",passives);

    const getDateInfo=()=>{
        let rep=""
        try {
            if(date!=null && date!=undefined){
                rep="Bilan au "+DateTime.format(date,"DD/MM/YYYY");
            }
        } catch (error) {
            
        }
        return rep;
    }

    const[loading,setLoading]=useState(true);

    const renderItemActive=({item,index})=>{
        let value=item.price;
        if(item.quantity>0){
            value*=item.quantity;
        }
        return(
            <CardBalance
                article={getArticleName(item.name,item.description)}
                date={item.created_at}
                price={getPrice(value,item.signe,item.remise)}
                color={R.color.colorPrimary}
                key={`active_value${index}`}
            />
        )
    }
    const renderItemPassive=({item,index})=>{
        return(
            <CardBalance
                article={item.description}
                date={item.created_at}
                price={getPrice(item.price,item.signe)}
                color={"red"}
                key={`passive_value${index}`}
            />
        )
    }
    return(
        <Overlay isVisible={visible} fullScreen overlayStyle={{ padding:0 }} animationType='fade'>
            <View style={{ flex:1 }}>
                <Appbar.Header style={{ backgroundColor:R.color.colorPrimary }}>
                    <Appbar.BackAction
                        onPress={onClose}
                    />
                    <Appbar.Content
                        title={getDateInfo()}
                    />
                </Appbar.Header>
                <View style={{ flex:1 }}>
                    <View>
                        <View style={{ flexDirection:'row',alignItems:'center' }}>
                            <View style={{ flex:1 }}>
                                <FlatList
                                    data={actives}
                                    renderItem={renderItemActive}
                                    keyExtractor={item=>item.id+""}
                                    key="active"
                                />
                                <View style={{ backgroundColor:R.color.colorPrimary,margin:5 }}>
                                    <Text numberOfLines={1} style={[styles.total,{color:R.color.background}]}>
                                        {totalBuy}
                                    </Text>
                               </View>
                                
                            </View>
                            <View>
                                <View style={{flex:1,borderWidth:0.2,borderColor:R.color.colorAccent}}/>
                            </View>
                            <View style={{ flex:1 }}>
                                <FlatList
                                    data={passives}
                                    renderItem={renderItemPassive}
                                    keyExtractor={item=>item.id+""}
                                    key="passive"
                                />
                                <View style={{ backgroundColor:'red',margin:5 }}>
                                    <Text numberOfLines={1} style={[styles.total]}>
                                        {totalSpent}
                                    </Text>
                               </View>
                            </View>
                        </View>
                    </View>
                </View>
                <View>

                </View>
            </View>
        </Overlay>
    )
}