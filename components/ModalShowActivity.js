import React from 'react';
import { StyleSheet } from 'react-native';
import { View } from 'react-native';
import { Overlay,Divider, Avatar } from 'react-native-elements';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import { Appbar, Text } from 'react-native-paper';
import R from '../resources/styles.json';
import S from '../resources/settings.json';
import { Icon } from 'react-native-elements';
import { getDateList, getImageLocal, getPrice, isEmpty,getTitle as getNameTitle, getFullName } from '../Manager';

const styles=StyleSheet.create({
    title:{
        fontSize:22,
        fontFamily:"Poppins-Black",
        textAlign:'center',
        padding:10
    },
    description:{
        fontSize:18,
        fontFamily:"Poppins-Light",
        textAlign:'justify',
        padding:10
    },
    data:{
        fontSize:15,
        padding:5,
        fontFamily:"Poppins-Regular",
    }
})

export default function App({
    visible=false,type="",onClose,description,agents=[],
    price,devise,created_at
}){

    const renderItem=({item,index})=>{

        return(
            <View style={{ flex:1,padding:10,alignItems:'center' }}>
                <View style={{ alignItems:'center' }}>
                    <Avatar
                        size='large' rounded
                        source={{ uri:getImageLocal(item.photo) }}
                        title={getNameTitle(item.last_name)}
                        containerStyle={{ backgroundColor:R.color.colorPrimary }}
                    />
                    <Text numberOfLines={1} style={{ fontFamily:"Poppins-Light",fontSize:16 }}>
                        {getFullName(item.first_name,item.last_name)}
                    </Text>
                </View>
            </View>
        )
    }

    const getTitle=(value)=>{
        let rep="";
        switch (value) {
            case S.ENUM.activity_type.purpose:
                rep="Activité d'objectif"
            break;
            case S.ENUM.activity_type.stock:
                rep="Activité d'approvisionnement"
            break;
            case S.ENUM.activity_type.cancel_sale:
                rep="Retour de produit vendu"
            break;
            case S.ENUM.activity_type.cancel_stock:
                rep="Retour de marchandise achetée"
            break;
            case S.ENUM.activity_type.contrat:
                rep="Contrat"
            break;
            case S.ENUM.activity_type.spent:
                rep="Dépense du jour"
            break;
            default:
                rep="Aucun titre"
            break;
        }
        return rep;
    }
    const getTitleImage=(value)=>{
        let rep={icon:"user",type:"entypo"};
        switch (value) {
            case S.ENUM.activity_type.purpose:
                rep={icon:"archive",type:"entypo"};
            break;
            case S.ENUM.activity_type.stock:
                rep={icon:"bucket",type:"entypo"};
            break;
            case S.ENUM.activity_type.cancel_sale:
                rep={icon:"open-book",type:"entypo"};
            break;
            case S.ENUM.activity_type.cancel_stock:
                rep={icon:"power-plug",type:"entypo"};
            break;
            case S.ENUM.activity_type.contrat:
                rep={icon:"book",type:"entypo"};
            break;
            case S.ENUM.activity_type.spent:
                rep={icon:"v-card",type:"entypo"};
            break;
            default:
                rep={icon:"book",type:"entypo"};
            break;
        }
        return rep;
    }

    const icon=getTitleImage(type);
    const title=getTitle(type);

    return(
        <Overlay isVisible={visible} fullScreen animationType='fade' overlayStyle={{ padding:0 }}>
            <View style={{ flex:1 }}>
                <Appbar.Header style={{ backgroundColor:R.color.colorPrimary }}>
                    <Appbar.BackAction
                        onPress={onClose}
                    />
                    <Appbar.Action
                        icon={()=>(
                            <Icon
                                name={icon.icon}
                                type={icon.type}
                                color={R.color.background}
                            />
                        )}
                    />
                    <Appbar.Content
                        title={title}
                    />
                </Appbar.Header>
                <ScrollView>
                    <View style={{ flexDirection:'row',alignItems:'center' }}>
                        <View>

                        </View>
                        <View style={{ flex:1 }}>
                            <Text style={styles.title}>
                                {title}
                            </Text>
                        </View>
                    </View>
                    <Divider/>
                    <View style={{ flexDirection:'row',alignItems:'center',padding:5 }}>
                        {
                            (!isEmpty(price+"") && !isEmpty(devise))?(
                                <View style={{ flex:1,alignItems:'center' }}>
                                    <Icon
                                        type='entypo'
                                        name='price-tag'
                                        size={30}
                                    />
                                    <Text style={styles.data} numberOfLines={1}>
                                        {getPrice(price,devise)}
                                    </Text>
                                </View>
                            ):null
                        }
                        {
                            !isEmpty(created_at)?(
                                <View style={{ flex:1,alignItems:'center',padding:5 }}>
                                    <Icon
                                        type='entypo'
                                        name='stopwatch'
                                        size={30}
                                    />
                                    <Text numberOfLines={1} style={styles.data}>
                                        {getDateList(created_at)}
                                    </Text>
                                </View>
                            ):null
                        }
                        {
                            agents.length>0?(
                                <View style={{ flex:1,alignItems:'center',padding:5 }}>
                                    <Icon
                                        type='entypo'
                                        name='users'
                                        size={30}
                                    />
                                    <Text style={styles.data} numberOfLines={1}>
                                        {agents.length}
                                    </Text>
                                </View>
                            ):null
                        }
                    </View>
                    <Divider/>
                    <View>
                        <Text style={styles.description}>
                            {description}
                        </Text>
                    </View>
                    {
                        (agents!=null && agents!=undefined)?agents.length>0?(
                            <View>
                                <View>
                                    <Text style={{ fontFamily:"Poppins-Black",textAlign:'center',fontSize:20,padding:10 }}>
                                        Les personnes concernées dans l'activité
                                    </Text>
                                </View>
                                <FlatList
                                    data={agents}
                                    renderItem={renderItem}
                                    horizontal
                                    key="agents"
                                    keyExtractor={item=>item.id}
                                />
                            </View>
                        ):null:null
                    }
                </ScrollView>
            </View>
        </Overlay>
    )
}