import React from 'react';
import { View } from 'react-native';
import { Avatar, ListItem, Text } from 'react-native-elements';
import { getTitle } from '../Manager';
import R from '../resources/styles.json';



export default function App({title,size=null,subTitle,logo="p",date="",count,opacity,onPress,price="",taked=false}){

    App.defaultProps={
        title:'',
        subTitle:'',
        count:0,
        opacity:1,
        price:""
    }

    return(
        <View style={{flex:1,margin:5}}>
        <ListItem
            onPress={onPress}
            activeOpacity={0.91}
            style={{margin:5,backgroundColor:taked==true?R.color.colorPrimary:R.color.background,elevation:10,opacity:opacity}}
        >
            <Avatar
                title={getTitle(title)}
                placeholderStyle={{backgroundColor:'transparent'}}
                titleStyle={{fontSize:20,color:R.color.colorPrimaryDark,fontFamily:'Poppins-Black'}}
                size='large'
                source={{uri:logo}}
                containerStyle={{backgroundColor:'transparent'}}
            />
            <View style={{flex:1}}>
                <Text style={{fontSize:14,padding:2,fontFamily:'Poppins-Black'}}>
                    {title}
                </Text>
                {
                    price!=null?(
                        <Text style={{color:R.color.colorPrimary,padding:2,fontSize:14,fontFamily:'Poppins-Light'}}>
                            {price}
                        </Text>
                    ):null
                }
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    <View style={{flex:1}}>
                        <View style={{flexDirection:'row'}}>
                            {
                                (count!=null && count!=undefined && count!="")?(
                                    <Text style={{fontSize:14,padding:5,paddingLeft:15,paddingRight:15,
                                        backgroundColor:(count>0 || size>0)?R.color.colorSecondary:'red',color:R.color.background,
                                        fontFamily:'Poppins-Black',borderRadius:8,flex:0
                                    }}>
                                        {count}
                                    </Text>
                                ):null
                            }
                        </View>
                    </View>
                    <View>
                        <Text numberOfLines={1} style={{color:R.color.colorPrimary,padding:5,fontSize:12,fontWeight:'bold'}}>
                            {date}
                        </Text>
                    </View>
                </View>
            </View>
        </ListItem>
        </View>
    )
}