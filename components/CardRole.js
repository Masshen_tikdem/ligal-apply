import React from 'react';
import { View } from 'react-native';
import { Avatar, Button, Card, Text } from 'react-native-elements';
import { Divider } from 'react-native-paper';
import R from '../resources/styles.json';

export default function App({title,content,code=0,type="font-awesome",icon="user-o",value,onPress}){
    return(
        <Card containerStyle={{ margin:2,padding:3 }}>
        <View style={{ flexDirection:'row',alignItems:'center',padding:10,paddingBottom:0 }}>
            <View style={{ padding:5 }}>
                <Avatar
                    rounded size="large"
                    icon={{type:type,name:icon,}}
                    containerStyle={{ backgroundColor:R.color.colorPrimary }}
                />
            </View>
            <View style={{flex:1}}>
                <Text style={{ fontFamily:'Poppins-Black',fontSize:16,padding:5 }}>
                    {title}
                </Text>
                <Text style={{ fontFamily:'PoppinsLight',fontSize:14,textAlign:'justify',padding:5,paddingTop:0,paddingBottom:0 }}>
                    {content}
                </Text>
                <View style={{ flexDirection:'row',alignItems:'center',padding:5,paddingTop:0 }}>
                    <View style={{ flex:2 }}>
                        <Text style={{ color:code>0?R.color.colorSecondary:'red' }}>
                           {value}
                        </Text>
                    </View>
                    <View style={{ flex:1 }}>
                        <Button
                            title="Modifier"
                            type="clear"
                            onPress={onPress}
                            titleStyle={{ color:R.color.colorPrimary }}
                        />
                    </View>
                </View>
                <Divider/>
            </View>
        </View>
        </Card>
    )
}