import React from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-elements';
import { RadioButton } from 'react-native-paper';



export default function App({value,color,tintColor,textSize,checked,title,onPress,direction}){

    App.defaultProps={
        title:'',
        value:'',
        textSize:16,
        tintColor:'#000',
        direction:"column"
    }

    return(
        <View style={{alignItems:'center',padding:0,flexDirection:direction}}>
            <RadioButton
                value={value}
                color={color}
                onPress={onPress}
                status={checked==value?"checked":"unchecked"}
            />
            <Text style={{color:tintColor,fontSize:textSize,fontFamily:'Poppins-Light'}}>
                {title}
            </Text>
        </View>
    )
}