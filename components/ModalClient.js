import React from 'react';
import { FlatList } from 'react-native';
import { ScrollView } from 'react-native';
import { View } from 'react-native';
import { Avatar, Button, Card, Input, Overlay } from 'react-native-elements';
import R from '../resources/styles.json';
import { Appbar } from 'react-native-paper';
import { isEmpty } from '../Manager';


export default function App({visible,phone,onPhone,name,onName,onPress,list=[],renderItem,onClose}){

    App.defaultProps={
        visible:false,
        phone:"",
        name:""
    }

    const checkNumber=(v)=>{
        let reg=/^[1-9][0-9]{6,8}$/;
        return reg.test(v);
    }
    return(
            <Overlay
                isVisible={visible}
                fullScreen
                animationType='slide'
                overlayStyle={{padding:0}}
                key="modal-client"
            >
                <View style={{flex:1}}>
                    <Appbar.Header style={{backgroundColor:R.color.colorPrimary}}>
                        <Appbar.BackAction onPress={onClose} />
                        <Appbar.Content title="Signaler le client" />
                    </Appbar.Header>
                    <ScrollView style={{flex:1}}>
                        <View style={{alignItems:'center',padding:5}}>
                            <Avatar
                                icon={{name:"people",type:"ionicon"}}
                                rounded
                                containerStyle={{backgroundColor:R.color.colorPrimary}}
                                size="medium"
                            />
                        </View>
                        <View style={{zIndex:3000}}>
                            <Card containerStyle={{borderRadius:20}}>
                                <View>
                                    <Input
                                        label="Téléphone du client"
                                        placeholder="ex:820045236"
                                        value={phone}
                                        onChangeText={onPhone}
                                        keyboardType="phone-pad"
                                        maxLength={20}
                                        inputStyle={{fontFamily:'Poppins-Light'}}
                                        labelStyle={{fontFamily:'Poppins-Bold',fontWeight:'normal'}}
                                        errorMessage={!checkNumber(phone)?"Format non correct":""}
                                    />
                                    <Input
                                        label="Nom du client"
                                        placeholder="Nom du client"
                                        value={name}
                                        onChangeText={onName}
                                        maxLength={25}
                                        inputStyle={{fontFamily:'Poppins-Light'}}
                                        labelStyle={{fontFamily:'Poppins-Bold',fontWeight:'normal'}}
                                    />
                                </View>
                                <View style={{alignItems:'center'}}>
                                    <Button
                                        title="Valider"
                                        onPress={onPress}
                                        disabled={!checkNumber(phone)}
                                        buttonStyle={{backgroundColor:R.color.colorPrimary,borderRadius:20,paddingLeft:20,paddingRight:20,padding:15}}
                                    />
                                </View>
                            </Card>
                        </View>
                        <View style={{padding:10,margin:10}}>
                            <FlatList
                                data={list}
                                renderItem={renderItem}
                                key="list-customer"
                                keyExtractor={item=>item.id}
                                numColumns={3}
                            />
                        </View>
                    </ScrollView>
                </View>
            </Overlay>
    )
}