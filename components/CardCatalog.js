import React from 'react';
import { Image } from 'react-native';
import { View } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { Avatar, Card, Text } from 'react-native-elements';
import { capitalize, getTitle } from '../Manager';


export default function App({image,name="",onPress}){

    App.defaultProps={
        image:"",
        name:""
    }
    return(
        <View style={{flex:1}}>
        <TouchableOpacity onPress={onPress}>
            <Card>
                <View style={{alignItems:'center'}}>
                    <Avatar
                        source={{uri:image}}
                        size="large"
                        title={getTitle(name)}
                    />
                    <Text numberOfLines={1} style={{fontFamily:'Poppins-Light',fontSize:16,padding:5}}>
                        {capitalize(name).toUpperCase()}
                    </Text>
                </View>
            </Card>
        </TouchableOpacity>
        </View>
    )
}