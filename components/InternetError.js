import React from 'react';
import { View } from 'react-native';
import { Avatar, Text } from 'react-native-elements';
import { ConfirmDialog } from 'react-native-simple-dialogs';
import R from '../resources/styles.json';

export default function App({visible=true,onClose}){

    App.defaultProps={
        visible:false
    }

    return(
        <ConfirmDialog 
            visible={visible} animationType='slide'
            positiveButton={{onPress:onClose,title:"OK",titleStyle:{fontFamily:'Poppins-Black',fontSize:18,color:'#000'}}}
            onTouchOutside={onClose}
        >
            <View style={{alignItems:'center'}}>
                <Avatar
                    icon={{name:'wifi-off',type:"feather",color:R.color.colorPrimary}}
                    size='xlarge'
                />
                <Text style={{fontFamily:"Poppins-BlackItalic",fontSize:20,color:R.color.colorPrimaryDark}} >
                    Whoops
                </Text>
                <Text style={{textAlign:'center',fontFamily:'Poppins-Light',fontSize:12}}>
                    Aucune connexion Internet trouvée, veuillez vérifier vos paramètres Internet!
                </Text>
            </View>
        </ConfirmDialog>
    )
}