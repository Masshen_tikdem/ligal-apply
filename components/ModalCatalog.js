import React from 'react';
import { ScrollView } from 'react-native';
import { FlatList } from 'react-native';
import { View } from 'react-native';
import { Overlay } from 'react-native-elements';
import { Appbar } from 'react-native-paper';
import R from '../resources/styles.json';
import EmptyMessage from './EmptyMessage';
import Awaiter from './Waiter';


export default function App({list=[],renderItem,visible,onRefresh,refreshing=false,isLoading=true,onClose}){

    App.defaultProps={
        visible:false
    }
    return(
        <Overlay
            animationType="slide"
            isVisible={visible}
            overlayStyle={{padding:0}}
            key="cat-list-cat"
            fullScreen
        >
            <View style={{flex:1}}>
                <Appbar.Header style={{backgroundColor:R.color.colorPrimary}}>
                    <Appbar.BackAction onPress={onClose} />
                    <Appbar.Content title="Catalogues Ligal" />
                </Appbar.Header>
                {
                    isLoading==true?(
                        <Awaiter color={R.color.colorPrimaryDark} size={100} key="awaiter-cat" />
                    ):list.length==0?(
                        <EmptyMessage onRefresh={onRefresh} refresh={refreshing} key="empty-message-catalog" title="Aucun logo n'est trouvé" />
                    ):(
                        <FlatList
                            data={list}
                            renderItem={renderItem}
                            key="list-catalog"
                            keyExtractor={item=>item.id}
                            numColumns={2}
                        />
                    )
                }
            </View>
        </Overlay>
    )
}