import React from 'react';
import { View } from 'react-native';
import DatePicker from 'react-native-date-picker';
import { Button, Overlay } from 'react-native-elements';
import { Dialog } from 'react-native-simple-dialogs';



export default function App({date,visible,mode,backgroundColor,textColor,onDateChange,fadeToColor,onTouchOutside,onValidate}){
    App.defaultProps={
        date:new Date(),
        visible:false,
        backgroundColor:'#ddd',
        textColor:"#fff",
        fadeToColor:"#000",
        mode:'date'
    }


    return(
        <Overlay isVisible={visible} visible={visible} animationType='fade' onBackdropPress={onTouchOutside}
            overlayStyle={{maxWidth:400,alignSelf:'center',padding:0,borderColor:textColor,borderWidth:2}}
        >
                <DatePicker
                    date={date}
                    style={{backgroundColor:backgroundColor,alignSelf:'center',margin:0,borderBottomWidth:0}}
                    fadeToColor={fadeToColor}
                    mode={mode}
                    textColor={textColor}
                    onDateChange={onDateChange}
                />
                <View style={{backgroundColor:fadeToColor,padding:10}}>
                    <View style={{flexDirection:'row',alignItems:'center',alignSelf:'flex-end'}}>
                        <Button title="Annuler" onPress={onTouchOutside} type='clear' titleStyle={{color:textColor,fontSize:18}} />
                        <Button title="Valider" onPress={onValidate} type='clear' titleStyle={{color:textColor,fontSize:18}} />
                    </View>
                </View>
        </Overlay>
    )
}