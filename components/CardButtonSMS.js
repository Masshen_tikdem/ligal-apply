import React from 'react';
import { View } from 'react-native';
import { Button, CheckBox, Text } from 'react-native-elements';
import R from '../resources/styles.json';


export default function App({checked=false,disabled=false,titleCheck="",countSMS=0,onChecked,onPress}){

    return(
        <View style={{ flexDirection:'row',alignItems:'center',padding:5 }}>
            <View style={{ padding:5 }}>
                <CheckBox
                    checked={checked}
                    title={titleCheck}
                    onPressIn={onChecked}
                    containerStyle={{ margin:0,backgroundColor:'transparent' }}
                    style={{ backgroundColor:'red' }}
                />
            </View>
            <View style={{ alignItems:'center' }}>
                <Button
                    title="Envoyez"
                    onPress={onPress}
                    icon={{ name:"mail",type:'ant-design',color:R.color.background }}
                    iconRight
                    disabled={disabled}
                    buttonStyle={{ padding:10,backgroundColor:R.color.colorPrimary,borderRadius:10 }}
                />
            </View>
            <View style={{ flex:1,padding:5,alignItems:'center' }}>
                <Text style={{ fontFamily:'Poppins-Black',textAlign:"center" }} numberOfLines={2}>
                    {countSMS<=0?"Aucun SMS":countSMS+" SMS"}
                </Text>
            </View>
        </View>
    )
}