import React from 'react';
import { RefreshControl } from 'react-native';
import { ScrollView } from 'react-native';
import { View } from 'react-native';
import { Text } from 'react-native-elements';
import R from '../resources/styles.json';


const refreshComponent=(refresh,onRefresh)=>(
    <RefreshControl
        refreshing={refresh} onRefresh={onRefresh}
        tintColor={R.color.colorPrimary}
        colors={[R.color.colorPrimaryDark,R.color.colorPrimary,R.color.colorSecondary,R.color.colorAccent]}
    />
)
export default function App({title,subTitle=null,refresh,onRefresh}){
    App.defaultProps={
        title:'',
        refresh:false
    }


    return(
        <ScrollView contentContainerStyle={{flex:1}} refreshControl={refreshComponent(refresh,onRefresh)}>
            <View style={{flex:1,justifyContent:'center',margin:25,alignItems:'center'}}>
                <Text style={{fontSize:20,textAlign:'center',fontFamily:'Poppins-Black',opacity:0.8}}>
                    {title}
                </Text>
                {
                    subTitle!=null?(
                        <Text style={{fontSize:16,textAlign:'center',fontFamily:'Poppins-Light'}}>
                            {subTitle}
                        </Text>
                    ):null
                }
            </View>
            
        </ScrollView>
    )
    
}