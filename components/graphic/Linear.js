import React from 'react';
import { ScrollView } from 'react-native';
import { useWindowDimensions } from 'react-native';
import { Dimensions } from 'react-native';
import { View } from 'react-native';
import {LineChart,BarChart} from 'react-native-chart-kit';
import { capitalize, getPrice } from '../../Manager';
import R from '../../resources/styles.json';

export default function App({labels=[],data=[],devise="",legend=""}){

    const dimension=useWindowDimensions();
    return(
        <ScrollView>
        <ScrollView horizontal>
        <View style={{ flex:1 }}>
            <LineChart
    data={{
      labels: labels,
      legend:[
          legend
      ],
      datasets: [
        {
          data: data,
          withScrollableDot:true
        }
      ]
    }}
    width={dimension.width<500?800:dimension.width} // from react-native
    height={500}
    yAxisLabel=""
    yAxisSuffix={" "+devise}
    yAxisInterval={1} // optional, defaults to 1
    xLabelsOffset={10}
    horizontalLabelRotation={10}
    formatYLabel={(value)=>getPrice(parseFloat(value),"",0)}
    formatXLabel={(value)=>capitalize(value).toUpperCase()}
    chartConfig={{
      backgroundColor: R.color.colorPrimaryDark,
      backgroundGradientFrom: R.color.colorPrimary,
      backgroundGradientTo: R.color.colorAccent,
      decimalPlaces: 1, // optional, defaults to 2dp
      color: (opacity = 1) => R.color.background,
      labelColor: (opacity = 1) => R.color.background,
      style: {
        borderRadius: 0,
        padding:10,
        margin:5
      },
      propsForDots: {
        r: "5",
        strokeWidth: "1",
        stroke: R.color.colorAccent
      },
      propsForLabels:{
        fontFamily:"Poppins-Black",
        fontSize:20,
      },
      propsForHorizontalLabels:{
        fontFamily:"Poppins-Light",
        dx:1,
        fontSize:12
      },
      propsForVerticalLabels:{
        fontFamily:"Poppins-Regular",
        dx:5,
        fontSize:11
      }
    }}
    bezier
    style={{
      marginVertical: 8,
      borderRadius: 20,
      margin:10
    }}
    fromZero
  />
        </View>
        </ScrollView>
        </ScrollView>
    )
}