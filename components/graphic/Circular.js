import React from 'react';
import { useWindowDimensions } from 'react-native';
import { View,ScrollView } from 'react-native';
import {PieChart} from 'react-native-chart-kit';
import R from '../../resources/styles.json';


export default function App({list=[],items=[]}){

    const dimension=useWindowDimensions();
    return(
        <ScrollView contentContainerStyle={{ margin:10,marginBottom:40,padding:10,borderRadius:20,elevation:10}}>
            <PieChart
                data={list}
                backgroundColor={"white"}
                height={300}
                width={400}
                center={[10,0]}
                accessor='value'
                paddingLeft={20}
                chartConfig={{ 
                    backgroundColor: R.color.colorPrimaryDark,
                    backgroundGradientFrom: R.color.colorPrimary,
                    backgroundGradientTo: R.color.colorAccent,
                    decimalPlaces: 2, // optional, defaults to 2dp
                    color: (opacity = 1) => R.color.background,
                    labelColor: (opacity = 1) => R.color.background,
                    style:{
                        borderRadius:20
                    }
                 }}
            />
        </ScrollView>
    )
}