import { Picker } from '@react-native-picker/picker';
import React from 'react';
import { View } from 'react-native';
import { ConfirmDialog } from 'react-native-simple-dialogs';



export default function App({list=[],title,value,onPress,onChangeValue,onClose,visible=false}){


    return(
        <ConfirmDialog
            visible={visible}
            animationType='fade'
            key="dialogModalRole"
            positiveButton={{ 
                title:"Valider",
                onPress:onPress,
                titleStyle:{color:"#111"}
             }}
             negativeButton={{ 
                title:"Annuler",
                onPress:onClose,
                titleStyle:{color:"#111"}
              }}
            onTouchOutside={onClose}
        >
        <View>
            <Picker
                onValueChange={onChangeValue}
                mode="dropdown"
                key="PickerModalRole"
                selectedValue={value}
            >
                {
                    list.map((item,index)=>(
                        <Picker.Item
                            label={item.label}
                            value={item.value}
                            key={`modalRole${index}`}
                        />
                    ))
                }
            </Picker>
        </View>
        </ConfirmDialog>
    )
}