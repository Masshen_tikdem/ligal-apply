import React from 'react';
import { TouchableOpacity } from 'react-native';
import { View } from 'react-native';
import { Avatar, Text } from 'react-native-elements';
import R from '../resources/styles.json';


export default function App({showPrice=true,center=false,name,price,quantity,familly,onPress,logo,service=true}){

    App.defaultProps={
        name:'',
        price:'',
        quantity:0,
        familly:'',
        service:false
    }

    return(
        <TouchableOpacity activeOpacity={0.9} style={{flex:1,alignItems:center?'center':'flex-start'}} onPress={onPress}>
        <View style={{flex:1,elevation:10,maxWidth:300,borderRadius:15,backgroundColor:R.color.background,alignItems:'center',margin:15,paddingBottom:10}}>
               <View style={{padding:20,alignItems:'center',opacity:1}}>
                    <Avatar
                        size="medium"
                        rounded
                        icon={{name:'cube',type:'font-awesome',size:50,color:R.color.colorPrimary}}
                        source={{uri:logo}}
                        containerStyle={{backgroundColor:'transparent'}}
                        placeholderStyle={{backgroundColor:'transparent',borderWidth:0.5}}
                    />
                </View>
                <Text numberOfLines={1} style={{fontSize:16,fontFamily:'Poppins-Black',padding:5}}>
                    {name}
                </Text>
                <Text numberOfLines={1} style={{fontSize:14,fontFamily:'Poppins-Light',opacity:0.8,padding:2}}>
                    {familly}
                </Text>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    {
                        (showPrice==true)?(
                            <View style={{flex:1}}>
                                <Text numberOfLines={1} style={{fontSize:16,fontWeight:'bold',color:R.color.colorSecondary,padding:10}}>
                                    {price}
                                </Text>
                            </View>
                        ):null
                    }
                    {
                        service==false?(
                            <View style={{flex:showPrice==false?1:0,alignItems:'flex-end'}}>
                                <View style={{ backgroundColor:R.color.colorSecondary,margin:5,elevation:10,marginRight:10,borderRadius:10}}>
                                    <Text numberOfLines={1} style={{padding:10,color:R.color.background,fontFamily:'Poppins-Black'}}>
                                        {quantity}
                                    </Text>
                                </View>
                            </View>
                        ):null
                    }
                </View>
        </View>
        </TouchableOpacity>
    )
}