import React from 'react';
import { ScrollView } from 'react-native';
import { FlatList } from 'react-native';
import { View } from 'react-native';
import { Button, Card, Input, Overlay, Text } from 'react-native-elements';
import {CreditCardInput} from 'react-native-input-credit-card';
import { Appbar } from 'react-native-paper';
import R from '../resources/styles.json';


const GsmPayment=({type="",onChangeText,value="",onChangeCount,num,count="",placeholder})=>{
    let inputNum=null;
    let inputCount=null;
    return(
        <View style={{ padding:10 }}>
            <View style={{ flexDirection:'row',alignItems:'center' }}>
                <Text style={{ fontFamily:'Poppins-Medium',fontSize:16 }}>
                    +243
                </Text>
                <View style={{ flex:1 }}>
                    <Input
                        value={num}
                        label="Numéro de téléphone"
                        onChangeText={onChangeText}
                        placeholder={placeholder}
                        maxLength={15}
                        keyboardType="phone-pad"
                        returnKeyType={(type=="sms")?'next':'none'}
                        onSubmitEditing={()=>{
                            (type=="sms")?inputCount.focus():{}
                        }}
                    />
                </View>
            </View>
            {
                type=="sms"?(
                    <View>
                        <View style={{ flexDirection:'row',alignItems:'center',padding:10 }}>
                            <View>
                                <Text style={{ fontFamily:'Poppins-Bold',fontSize:16 }}>
                                    Nombre de messages:
                                </Text>
                            </View>
                            <View style={{ flex:1 }}>
                                <Input
                                    value={count}
                                    maxLength={10}
                                    onChangeText={onChangeCount}
                                    keyboardType="numeric"
                                    ref={x=>inputCount=x}
                                />
                            </View>
                        </View>
                        <View>
                            <Text style={{ padding:10,textAlign:'center',fontFamily:'Poppins-Black' }}>
                                {value}
                            </Text>
                        </View>
                    </View>
                ):null
            }
            <View style={{ alignItems:'center' }}>
                <Button
                    title="Payer"
                />
            </View>
        </View>
    )
}

export default function App({visible=false,title="",type="",count=0,onClose,onChangeText,onChangeCardValue,onPress,onConfirm,methodSelected="",methods=[],renderMethod,num="",cvc="",expiry=""}){

    return(
        <Overlay 
            isVisible={visible} 
            fullScreen
            animationType="slide"
            overlayStyle={{ padding:0 }}
        >
            <View>
                <Appbar.Header style={{ backgroundColor:R.color.colorPrimary }}>
                    <Appbar.BackAction onPress={onClose} />
                    <Appbar.Content title={title} />
                </Appbar.Header>
            <ScrollView>
                <Card containerStyle={{ borderRadius:20,padding:0 }}>
                    <View style={{ padding:15,backgroundColor:R.color.colorAccent }}>
                        <Text style={{ textAlign:'center',fontFamily:'Poppins-Bold',fontSize:16 }}>
                            Méthodes de paiement
                        </Text>
                    </View>
                    <View>
                        <FlatList
                            data={methods}
                            renderItem={renderMethod}
                            key="method-component"
                            keyExtractor={item=>item.id}
                            horizontal
                        />
                    </View>
                </Card>
                {
                    methodSelected=="visa"?(
                        <View>
                            <View>

                            </View>
                            <CreditCardInput
                                onChange={onChangeCardValue}
                                allowScroll
                                labels={{ expiry:"Expiration",cvc:"CVC",name:"Nom",number:"Numéro",postalCode:"code postal" }}
                            />
                        </View>
                    ):methodSelected=="vodacom"?(
                        <GsmPayment
                            num={num}
                            onChangeText={onChangeText}
                            type={type}
                            key="vd"
                        />
                    ):methodSelected=="airtel"?(
                        <GsmPayment
                            num={num}
                            onChangeText={onChangeText}
                            type={type}
                            key="airt"
                        />
                    ):methodSelected=="africell"?(
                        <GsmPayment
                            num={num}
                            onChangeText={onChangeText}
                            type={type}
                            key="afri"
                        />
                    ):methodSelected=="orange"?(
                        <GsmPayment
                            num={num}
                            onChangeText={onChangeText}
                            type={type}
                            key="org"
                        />
                    ):(
                        <View>

                        </View>
                    )
                }
            </ScrollView>
            </View>
        </Overlay>
    )
}