import React from 'react';
import { View } from 'react-native';
import { Avatar, Card, Text } from 'react-native-elements';
import R from '../resources/styles.json';

export default function App({num=0,title,content,icon="user-o",type="font-awesome"}){


    return(
        <Card containerStyle={{ padding:0 }}>
        <View style={{ flexDirection:'row',alignItems:'center',padding:10 }}>
            <View>
                <Avatar
                    icon={{ name:icon,type:type }}
                    containerStyle={{ backgroundColor:R.color.colorPrimary }}
                    size='large'
                />
            </View>
            <View style={{ flex:1,padding:5 }}>
                <Text numberOfLines={1} style={{ fontFamily:'Poppins-Black',fontSize:17,padding:3,paddingBottom:0 }}>
                    {title}
                </Text>
                <Text numberOfLines={2} style={{ fontFamily:'Poppins-Light',textAlign:'justify',padding:3}}>
                    {content}
                </Text>
            </View>
            <View style={{ height:20,margin:5,width:2,opacity:0.4,backgroundColor:R.color.colorPrimaryDark }} />
            <View style={{ alignItems:'center',padding:2 }}>
                <Text style={{ fontFamily:'Poppins-Light' }}>
                    impact
                </Text>
                <Text style={{ fontFamily:'Poppins-Bold',fontSize:16 }}>
                    {`${num.toFixed(0)} %`}
                </Text>
            </View>
            <View style={{ width:5,margin:5 }}>
                <View style={{ flex:100-num,backgroundColor:R.color.colorPrimary }} />
                <View style={{ flex:num,backgroundColor:R.color.colorSecondary }} />
            </View>
        </View>
        </Card>
    )
}