import React from 'react';
import { View } from 'react-native';
import { Avatar, Button, Card, Icon, Text } from 'react-native-elements';
import R from '../resources/styles.json';
import S from '../resources/settings.json';
import { getDateList } from '../Manager';

const cardTarget=({title=""})=>{
    
    return(
        <View>
            <Card>
                <Card.Title>
                    {title}
                </Card.Title>
            </Card>
        </View>
    )
}


export default function App({title,content,count=0,onPress,date,agents=[]}){

    const getTitle=(value)=>{
        let rep="";
        switch (value) {
            case S.ENUM.activity_type.purpose:
                rep="Activité d'objectif"
            break;
            case S.ENUM.activity_type.stock:
                rep="Activité d'approvisionnement"
            break;
            case S.ENUM.activity_type.cancel_sale:
                rep="Retour de produit vendu"
            break;
            case S.ENUM.activity_type.cancel_stock:
                rep="Retour de marchandise achetée"
            break;
            case S.ENUM.activity_type.contrat:
                rep="Contrat"
            break;
            case S.ENUM.activity_type.spent:
                rep="Dépense du jour"
            break;
            default:
                rep="Aucun titre"
            break;
        }
        return rep;
    }
    const getTitleImage=(value)=>{
        let rep={icon:"user",type:"entypo"};
        switch (value) {
            case S.ENUM.activity_type.purpose:
                rep={icon:"archive",type:"entypo"};
            break;
            case S.ENUM.activity_type.stock:
                rep={icon:"bucket",type:"entypo"};
            break;
            case S.ENUM.activity_type.cancel_sale:
                rep={icon:"open-book",type:"entypo"};
            break;
            case S.ENUM.activity_type.cancel_stock:
                rep={icon:"power-plug",type:"entypo"};
            break;
            case S.ENUM.activity_type.contrat:
                rep={icon:"book",type:"entypo"};
            break;
            case S.ENUM.activity_type.spent:
                rep={icon:"v-card",type:"entypo"};
            break;
            default:
                rep={icon:"book",type:"entypo"};
            break;
        }
        return rep;
    }
    const image=getTitleImage(title);
    return(
        <View style={{ flex:1 }}>
        <Card containerStyle={{ marginTop:0,marginBottom:10 }}>
            <View style={{ flexDirection:'row',alignItems:'center' }}>
                <View style={{ padding:5 }}>
                    <Icon
                        name={image.icon}
                        type={image.type}
                        size={50}
                    />
                </View>
                <View style={{ padding:5,flex:1 }}>
                    <Text style={{ fontFamily:'Poppins-Black',fontSize:18,padding:5 }}>
                        {getTitle(title)}
                    </Text>
                    <Text numberOfLines={3} style={{ fontFamily:'Poppins-Light',fontSize:16 ,paddingLeft:5}}>
                        {content}
                    </Text>
                </View>
                {
                    count>0?(
                        <View>
                            <Avatar
                                title={count.toString()}
                                rounded size="small"
                                containerStyle={{ backgroundColor:R.color.colorSecondary }}
                                titleStyle={{ fontFamily:"Poppins-Black",fontSize:14 }}
                            />
                        </View>
                    ):null
                }
            </View>
            <View style={{ flexDirection:'row',justifyContent:'flex-end',alignItems:'center' }}>
                <View style={{ flex:1 }}>
                    <Text numberOfLines={1}>
                        {getDateList(date,'')}
                    </Text>
                </View>
                <View style={{ alignItems:'flex-end' }}>
                    <Button
                        title="Voir"
                        type='outline'
                        titleStyle={{ color:R.color.colorPrimary }}
                        buttonStyle={{ padding:15,paddingLeft:20,paddingRight:20 }}
                        icon={{ name:'triangle-right',type:'entypo' }}
                        iconRight
                        onPress={onPress}
                    />
                </View>
            </View>
        </Card>
        </View>
    )
}