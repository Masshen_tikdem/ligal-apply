import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Avatar, Button, Card, Icon, ListItem, Text } from 'react-native-elements';
import R from "../resources/styles.json";
import * as Manager from '../Manager';
import DateTime from 'date-and-time';
import fr from 'date-and-time/locale/fr';

const styles=StyleSheet.create({
    container:{
        paddingTop:15,
        paddingLeft:10,
        paddingRight:10,
        flex:1,
        backgroundColor:R.color.background,
        borderRadius:25,margin:20,elevation:10,
        marginBottom:10
    },
    containerAuthor:{
        flexDirection:'row',
        alignItems:'center'
    }
})

const getDate=(date)=>{
    let rep="";
    try {
        DateTime.locale(fr);
        if(date!=undefined && date!=null){
            const value=DateTime.parse(date,"YYYY-MM-DD HH:mm:ss");
            if(DateTime.isSameDay(new Date(),new Date(date))){
                rep="Aujourd'hui à "+DateTime.format(new Date(date),"HH:mm");
            }else{
                rep=DateTime.format(new Date(date),"dddd, DD MMM YYYY à HH:mm");
            }
        }
    } catch (error) {
    }
    return rep;
}

export default function App({title,content,view,like,image,autor,avatar,titleAvatar,onDetail,date}){
    App.defaultProps={
        title:'',
        content:'',
        view:0,
        like:0,
        author:'',
        created_at:null,
        titlAvatar:'',
        date:null
    }

    

    return(
        <View style={styles.container}>
            <View style={styles.containerAuthor}>
                <Avatar 
                    rounded title={Manager.getTitle(titleAvatar)}
                    source={{uri:Manager.getImage(avatar)}} 
                    containerStyle={{backgroundColor:R.color.colorSecondary,marginRight:10,marginLeft:10}}
                    placeholderStyle={{backgroundColor:R.color.colorSecondary}}
                    size='medium' />
                <View>
                    <Text style={{fontSize:18}}>
                        {autor}
                    </Text>
                    <Text>
                        {getDate(date)}
                    </Text>
                </View>
            </View>
            <Card.Divider style={{marginTop:10,marginLeft:20,marginRight:20}} />
            <View style={{flex:1,alignItems:'center',justifyContent:'center',padding:5,paddingLeft:15,paddingRight:15}}>
                <Text h4  numberOfLines={1}>
                    {title}
                </Text>
            </View>
            <View style={{padding:10,paddingTop:0,paddingBottom:5}}>
                <Text numberOfLines={2} style={{fontSize:18}}>
                    {content}
                </Text>
            </View>
            <View>
                <Card containerStyle={{padding:0,margin:5,borderRadius:25}}> 
                    <Card.Image source={{uri:image}} containerStyle={{padding:0,borderRadius:25}} />
                </Card>
            </View>
            <View style={styles.containerAuthor}>
                <View style={{flex:1}}>
                    <Button 
                        title='' type='clear'
                    />
                </View>
                <View style={{flex:1}}>
                    <Button 
                        title='' type='clear'
                    />
                </View>
                <View style={{flex:1}}>
                    <Button 
                        icon={{name:'information-circle',type:'ionicon'}}
                        title='Voir' type='clear'
                        onPress={onDetail}
                        buttonStyle={{padding:15,paddingLeft:25,paddingRight:25}}
                        titleStyle={{fontSize:18}}
                    />
                </View>
            </View>
        </View>
    )
}