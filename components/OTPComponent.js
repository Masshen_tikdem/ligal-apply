import React from 'react';
import { ActivityIndicator, View } from 'react-native';
import { Button, Overlay, Text } from 'react-native-elements';
import RNOtpVerify from 'react-native-otp-verify';
import OTP from '@twotalltotems/react-native-otp-input';
import R from '../resources/styles.json';



export default function App({visible=false,code,onCodeChanged,onCodeFilled,time=120,loading=false,onResend}){

    const getTime=(value)=>{
        let rep="";
        try {
            let sec=value%60;
            let min=Math.round((value-sec)/60);
            rep=(min+"".length<2?"0"+min:min+"")+":"+((sec+"").length<2?("0"+sec):sec+"")
            //rep=sec+"";
        } catch (error) {
            
        }
        return rep;
    }
    return(
        <Overlay isVisible={visible} fullScreen animationType='fade'>
            <View style={{ flex:1,alignItems:'center',justifyContent:'center' }}>
                <Text style={{ fontFamily:'Poppins-Black',fontSize:20,textAlign:'center' }}>
                    Veuillez écrire le code de vérification reçu par SMS
                </Text>
                <View style={{ margin:30,height:20,marginBottom:5 }}>
                    <OTP
                        pinCount={4}
                        code={code}
                        onCodeChanged={onCodeChanged}
                        placeholderTextColor={R.color.colorSecondary}
                        codeInputFieldStyle={{ 
                            color:R.color.colorPrimaryDark,
                            fontFamily:"Poppins-Black",
                            fontSize:16
                            }}
                        codeInputHighlightStyle={{ 
                            backgroundColor:R.color.colorSecondary,
                            color:R.color.background
                         }}
                        onCodeFilled={onCodeFilled}
                    />
                </View>
                <View style={{ flexDirection:'row',alignItems:'center',margin:30,padding:30 }}>
                    <View>
                        <Button
                            title={time>0?"Essayer dans "+getTime(time):"Renvoyer"}
                            type='clear'
                            titleStyle={{ color:R.color.colorPrimaryDark,fontFamily:'Poppins-Bold' }}
                            buttonStyle={{ padding:10 }}
                            disabled={time>0}
                            onPress={onResend}
                        />
                    </View>
                    <View style={{ padding:5 }}>
                        {
                            loading?(
                                <ActivityIndicator
                                    color={R.color.colorSecondary}
                                    size='small'
                                />
                            ):null
                        }
                    </View>
                </View>
            </View>
        </Overlay>
    )
}