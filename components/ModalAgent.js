import React from 'react';
import { View } from 'react-native';
import { ScrollView } from 'react-native';
import { Avatar, Button, Card, Header, Icon, Input, Overlay, Text } from 'react-native-elements';
import R from '../resources/styles.json';
import RadioButton from './RadioButton';
import {Picker} from '@react-native-picker/picker';


const list=[
    {label:"Aucun droit",value:0},
    {label:"Lire seulement",value:1},
    {label:"Ajouter seulement",value:2},
    {label:"Ajouter et modifier",value:6},
    {label:"Tous les droits",value:7},
]

export default function App({
    name,photo,onPress,onClose,visible,title,label,
    vente=3,sales=0,stocks=0,dashboard=0,customer=1,article=0,category=0,devise=2,journal=1,
    onVente,onSale,onStock,onCustomer,onJournal,
    onDevise,onArticle,onCategory,poste,onChangePoste,
    date
}){

    return(
        <Overlay
            isVisible={visible}
            fullScreen
            overlayStyle={{padding:0}}
        >
            <View style={{ flex:1 }}>
            <Header
                backgroundColor={R.color.colorPrimary}
                leftComponent={
                    <Icon name="arrow-back" type='ionicon' color={R.color.background} onPress={onClose} />
                }
                placement="left"
                centerComponent={
                    <Text style={{fontFamily:"Poppins-Light",fontSize:16,padding:5,color:R.color.background}}>
                        {label}
                    </Text>
                }
            />
            <ScrollView>
                <View style={{alignItems:'center',padding:20}}>
                    <Avatar
                        source={{uri:photo}}
                        rounded size="xlarge"
                        title={title}
                        containerStyle={{backgroundColor:R.color.colorPrimary}}
                        placeholderStyle={{backgroundColor:R.color.colorPrimary}}
                    />
                </View>
                <View style={{padding:10}}>
                    <Text style={{fontFamily:'Poppins-Thin',fontSize:16}}>
                        Nom de l'agent
                    </Text>
                    <Text numberOfLines={1} style={{fontFamily:'Poppins-Medium',fontSize:18}}>
                        {name}
                    </Text>
                </View>
                <Card containerStyle={{borderTopEndRadius:20,borderTopStartRadius:20,elevation:10,marginBottom:20}}>
                    <Card.Title>
                        Expériences avec Ligal
                    </Card.Title>
                    <View>
                        {
                            (date!=null && date!=undefined)?(
                                <View style={{ flexDirection:'row',alignItems:'center' }}>
                                    <View style={{ padding:5 }}>
                                        <Text style={{ fontFamily:'Poppins-Light',fontSize:16 }}>
                                            Actif depuis
                                        </Text>
                                    </View>
                                    <View style={{ flex:1 }}>
                                        <Text style={{ textAlign:'center',fontFamily:'Poppins-Bold',fontSize:16 }}>
                                            {date}
                                        </Text>
                                    </View>

                                </View>

                            ):null
                        }

                    </View>

                </Card>
                <Card containerStyle={{borderTopEndRadius:20,borderTopStartRadius:20,elevation:10,marginBottom:20}}>
                    <Card.Title>
                        Droits accordés dans la boutique
                    </Card.Title>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <View style={{flex:1}}>
                            <Text style={{fontFamily:'Poppins-Light',fontSize:16,padding:10}}>
                                L'utilisateur peut vendre avec remise et marchandage
                            </Text>
                        </View>
                        <View style={{padding:10}}>
                            <RadioButton
                                checked={vente}
                                color={R.color.colorPrimary}
                                title="oui"
                                value={2}
                                direction="row"
                                onPress={onVente}
                            />
                            <RadioButton
                                checked={vente}
                                color={R.color.colorPrimary}
                                title="non"
                                value={0}
                                direction="row"
                                onPress={onVente}
                            />
                        </View>
                    </View>
                    <Card.Divider/>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <View style={{flex:1}}>
                            <Text style={{fontFamily:'Poppins-Light',fontSize:16,padding:10}}>
                                L'utilisateur peut lire les ventes du jour
                            </Text>
                        </View>
                        <View style={{padding:10}}>
                            <RadioButton
                                checked={journal}
                                color={R.color.colorPrimary}
                                title="oui"
                                value={1}
                                direction="row"
                                onPress={onJournal}
                            />
                            <RadioButton
                                checked={journal}
                                color={R.color.colorPrimary}
                                title="non"
                                value={0}
                                direction="row"
                                onPress={onJournal}
                            />
                        </View>
                    </View>
                    <Card.Divider/>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <View style={{flex:1}}>
                            <Text style={{fontFamily:'Poppins-Light',fontSize:16,padding:10}}>
                                L'utilisateur peut suivre tous les stocks
                            </Text>
                        </View>
                        <View style={{padding:10}}>
                            <RadioButton
                                checked={stocks}
                                color={R.color.colorPrimary}
                                title="oui"
                                value={1}
                                direction="row"
                                onPress={onStock}
                            />
                            <RadioButton
                                checked={stocks}
                                color={R.color.colorPrimary}
                                title="non"
                                value={0}
                                direction="row"
                                onPress={onStock}
                            />
                        </View>
                    </View>
                    <Card.Divider/>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <View style={{flex:1}}>
                            <Text style={{fontFamily:'Poppins-Light',fontSize:16,padding:10}}>
                                L'utilisateur peut suivre toutes les ventes
                            </Text>
                        </View>
                        <View style={{padding:10}}>
                            <RadioButton
                                checked={sales}
                                color={R.color.colorPrimary}
                                title="oui"
                                value={1}
                                direction="row"
                                onPress={onSale}
                            />
                            <RadioButton
                                checked={sales}
                                color={R.color.colorPrimary}
                                title="non"
                                value={0}
                                direction="row"
                                onPress={onSale}
                            />
                        </View>
                    </View>
                    <Card.Divider/>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <View style={{flex:1}}>
                            <Text style={{fontFamily:'Poppins-Light',fontSize:16,padding:10}}>
                                L'utilisateur peut  suivre l'évolution des clients
                            </Text>
                        </View>
                        <View style={{padding:10}}>
                            <RadioButton
                                checked={customer}
                                color={R.color.colorPrimary}
                                title="oui"
                                value={1}
                                direction="row"
                                onPress={onCustomer}
                            />
                            <RadioButton
                                checked={customer}
                                color={R.color.colorPrimary}
                                title="non"
                                value={0}
                                direction="row"
                                onPress={onCustomer}
                            />
                        </View>
                    </View>
                    <Card.Divider/>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <View style={{flex:1}}>
                            <Text style={{fontFamily:'Poppins-Light',fontSize:16,padding:10}}>
                                Gestion des articles
                            </Text>
                        </View>
                        <View style={{padding:10,flex:1}}>
                            <Picker
                                mode="dropdown"
                                selectedValue={article}
                                onValueChange={onArticle}
                                itemStyle={{fontFamily:'Poppins-Light'}}
                            >
                                {
                                    list.map((item,index)=>(
                                        <Picker.Item  label={item.label} value={item.value} key={`ìndex-article${index}`} />
                                    ))
                                }
                            </Picker>
                        </View>
                    </View>
                    <Card.Divider/>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <View style={{flex:1}}>
                            <Text style={{fontFamily:'Poppins-Light',fontSize:16,padding:10}}>
                                Gestion des catégories
                            </Text>
                        </View>
                        <View style={{padding:10,flex:1}}>
                            <Picker
                                mode="dropdown"
                                selectedValue={category}
                                onValueChange={onCategory}
                                itemStyle={{fontFamily:'Poppins-Light'}}
                            >
                                {
                                    list.map((item,index)=>(
                                        <Picker.Item  label={item.label} value={item.value} key={`ìndex-category${index}`} />
                                    ))
                                }
                            </Picker>
                        </View>
                    </View>
                    <Card.Divider/>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <View style={{flex:1}}>
                            <Text style={{fontFamily:'Poppins-Light',fontSize:16,padding:10}}>
                                Gestion des devises
                            </Text>
                        </View>
                        <View style={{padding:10,flex:1}}>
                            <Picker
                                mode="dropdown"
                                selectedValue={devise}
                                onValueChange={onDevise}
                                itemStyle={{fontFamily:'Poppins-Light'}}
                            >
                                {
                                    list.map((item,index)=>(
                                        <Picker.Item  label={item.label} value={item.value} key={`ìndex-devise${index}`} />
                                    ))
                                }
                            </Picker>
                        </View>
                    </View>
                    <Card.Divider/>
                </Card>
                <View style={{padding:10}}>
                    <Input
                        label="Poste à occuper dans la boutique"
                        labelStyle={{fontFamily:"Poppins-Medium",fontWeight:'normal',fontSize:16}}
                        value={poste}
                        onChangeText={onChangePoste}
                        inputStyle={{fontFamily:"Poppins-Light",fontSize:15}}
                        maxLength={15}
                    />
                </View>
            </ScrollView>
            <View>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    <View style={{flex:1,margin:10}}>
                        <Button
                            title="Annuler"
                            type="clear"
                            titleStyle={{fontFamily:"Poppins-Light",fontSize:16}}
                            onPress={onClose}
                        />
                    </View>
                    <View style={{flex:1,margin:10}}>
                        <Button
                            title="Ajouter"
                            buttonStyle={{backgroundColor:R.color.colorPrimary}}
                            titleStyle={{fontFamily:"Poppins-Light",fontSize:16}}
                            onPress={onPress}
                        />
                    </View>
                </View>
                </View>
            </View>
        </Overlay>
    )
}