import React, { useEffect, useState } from "react";
import { StyleSheet } from "react-native";
import { View } from 'react-native';
import { Avatar, Button, Card, Icon, Input, Overlay } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import { Appbar, FAB, List, Text } from 'react-native-paper';
import { connect } from 'react-redux';
import {capitalize, getImageLocal, getTitle, isConnected, isEmpty} from '../Manager';
import R from '../resources/styles.json';
import S from '../resources/settings.json';
import * as ShopModel from '../model/local/shop';
import * as ShopWebModel from '../model/web/society';
import {Picker} from '@react-native-picker/picker';
import {PAYS as countries} from '../resources/All countries';
import { Alert } from "react-native";
import {ProgressDialog} from 'react-native-simple-dialogs';

const styles=StyleSheet.create({
    btn:{
        padding:15,backgroundColor:R.color.colorAccent
    },
    title:{
        fontFamily:"Poppins-Bold",
        fontSize:18
    }
})

export default function App({onClose,props,visible=false,e_name,e_country,e_town,e_site,e_phone,e_address,e_description,e_number}){
    
    const[name,setName]=useState("");
    const[country,setCountry]=useState("");
    const[town,setTown]=useState("");
    const[address,setAddress]=useState("");
    const[site,setSite]=useState("");
    const[description,setDescription]=useState("");
    const[phone,setPhone]=useState("");
    const[logo,setLogo]=useState("");
    const[num,setNum]=useState("");
    const[loading,setLoading]=useState(false);
    const shop=props.shop.value;

    useEffect(()=>{
        if(visible==true){
            console.log('cc',e_country);
            setLoading(false);
            setName(e_name);
            setCountry(e_country);
            setTown(e_town)
            setSite(e_site);
            setPhone(e_phone);
            setAddress(e_address);
            setDescription(e_description);
            setNum(e_number);
        }
    },[visible]);

    const onUpdateTitle=async()=>{
        setLoading(true);
        const user=props.user.value;
        const internet=await isConnected();
        if(!internet){
            setLoading(false);
            Alert.alert("Vérification","Vous devez vous connecter à l'internet");
            return;
        }
        try {
            if(isEmpty(name)){
                setLoading(false);
                Alert.alert("Vérification","Le nom de la boutique ne peut pas être vide!")
            }else{
                const rep=await ShopWebModel.update(shop.private_key,JSON.stringify({name:name,user:user.private_key}));
                if(rep.status==200){
                    const response=rep.response;
                    const repL=await ShopModel.update({
                        name:response.name,id:shop.id
                    });
                    if(repL){
                        const info=await ShopModel.get();
                        props.dispatch({type:S.EVENT.onChangeShop,value:info});
                        setLoading(false);
                    }
                }else{
                    setLoading(false);
                    Alert.alert("Modification","Les modifications apportées n'ont pas abouties");
                }
            }
        } catch (error) {
            console.log('error',error);
            setLoading(false);
        }
    }

    const onUpdateDescription=async()=>{
        setLoading(true);
        const user=props.user.value;
        const internet=await isConnected();
        if(!internet){
            setLoading(false);
            Alert.alert("Vérification","Vous devez vous connecter à l'internet");
            return;
        }
        try {
            if(isEmpty(description)){
                setLoading(false);
                Alert.alert("Vérification","La description de la boutique ne peut pas être vide!")
            }else{
                const rep=await ShopWebModel.update(shop.private_key,JSON.stringify({user:user.private_key,description:description}));
                if(rep.status==200){
                    const response=rep.response;
                    const repL=await ShopModel.update({
                        description:response.description,id:shop.id
                    });
                    if(repL){
                        const info=await ShopModel.get();
                        props.dispatch({type:S.EVENT.onChangeShop,value:info});
                        setLoading(false);
                    }
                }else{
                    setLoading(false);
                    Alert.alert("Modification","Les modifications apportées n'ont pas abouties");
                }
            }
        } catch (error) {
            console.log('error',error);
            setLoading(false);
        }
    }

    const onUpdateZone=async()=>{
        setLoading(true);
        const user=props.user.value;
        const internet=await isConnected();
        if(!internet){
            setLoading(false);
            Alert.alert("Vérification","Vous devez vous connecter à l'internet");
            return;
        }
        try {
                const item=new Object();
                if(!isEmpty(country)){
                    item.country=country;
                }
                if(!isEmpty(town)){
                    item.town=town;
                }
                if(!isEmpty(address)){
                    item.address=address;
                }
                item.user=user.private_key;
                const rep=await ShopWebModel.update(shop.private_key,JSON.stringify(item));
                if(rep.status==200){
                    const response=rep.response;
                    const repL=await ShopModel.update({
                        address:response.address,country:response.country,town:response.town,
                        id:shop.id
                    });
                    if(repL){
                        const info=await ShopModel.get();
                        props.dispatch({type:S.EVENT.onChangeShop,value:info});
                        setLoading(false);
                    }
                }else{
                    setLoading(false);
                    Alert.alert("Modification","Les modifications apportées n'ont pas abouties");
                }
        } catch (error) {
            console.log('error',error);
            setLoading(false);
        }
    }

    const onUpdateContact=async()=>{
        setLoading(true);
        const user=props.user.value;
        const internet=await isConnected();
        if(!internet){
            setLoading(false);
            Alert.alert("Vérification","Vous devez vous connecter à l'internet");
            return;
        }
        try {
                const item=new Object();
                if(!isEmpty(phone)){
                    item.phone=phone;
                }
                if(!isEmpty(site)){
                    item.site=site;
                }
                item.user=user.private_key;
                const rep=await ShopWebModel.update(shop.private_key,JSON.stringify(item));
                if(rep.status==200){
                    const response=rep.response;
                    const repL=await ShopModel.update({
                        phone:response.phone,site:response.site,
                        id:shop.id
                    });
                    if(repL){
                        const info=await ShopModel.get();
                        props.dispatch({type:S.EVENT.onChangeShop,value:info});
                        setLoading(false);
                    }
                }else{
                    setLoading(false);
                    Alert.alert("Modification","Les modifications apportées n'ont pas abouties");
                }
        } catch (error) {
            console.log('error',error);
            setLoading(false);
        }
    }


    return(
        <Overlay overlayStyle={{ padding:0 }} animationType='fade' fullScreen isVisible={visible}>
            <View style={{ flex:1 }}>
                <Appbar.Header style={{ backgroundColor:R.color.colorPrimary }}>
                    <Appbar.BackAction
                        onPress={onClose}
                    />
                    <Appbar.Content
                        title="Modification des informations"
                    />
                </Appbar.Header>
                <ScrollView>
                    <List.Section>
                        <List.Accordion title="Identité" description="nom de la boutique" titleStyle={styles.title}>
                            <View>
                                <Input
                                    value={name}
                                    onChangeText={v=>setName(v)}
                                    label="Nom de la boutique"
                                    maxLength={30}
                                    placeholder="le nom"
                                />
                                <View style={{ alignItems:'flex-end',padding:5,paddingRight:20 }}>
                                    <Button
                                        title="Modifier"
                                        buttonStyle={styles.btn}
                                        disabled={e_name==name}
                                        onPress={()=>onUpdateTitle()}
                                    />
                                </View>
                            </View>
                        </List.Accordion>
                        <List.Accordion title="Zone" description="Pays, ville, adresse de la boutique" titleStyle={styles.title}>
                            <View>
                                <View>
                                    <Text style={{ padding:2,fontSize:16,paddingLeft:10,fontFamily:"Poppins-Bold",opacity:0.4 }}>
                                        Votre pays
                                    </Text>
                                    <Picker onValueChange={v=>setCountry(v)} selectedValue={country} mode='dropdown' >
                                        <Picker.Item
                                            label={"Séléctionner le pays"}
                                            value={null}
                                            key={`country-0`}
                                        />
                                        {
                                            countries.map((item,index)=>{
                                                if(!isEmpty(item.callingCodes[0])){
                                                    return(
                                                        <Picker.Item
                                                            label={item.translations.fr}
                                                            value={item.callingCodes[0]}
                                                            key={`country-${index}${item.callingCodes[0]}`}
                                                        />
                                                    )
                                                }
                                            })
                                        }
                                    </Picker>
                                </View>
                                <Input
                                    value={town}
                                    onChangeText={v=>setTown(v)}
                                    maxLength={30}
                                    label="Nom de la ville"
                                    placeholder="ville ici"
                                />
                                <Input
                                    value={address}
                                    onChangeText={v=>setAddress(v)}
                                    maxLength={256}
                                    label="Adresse physique"
                                    placeholder="adresse ici"
                                    multiline
                                />
                                <View style={{ alignItems:'flex-end',padding:5,paddingRight:20 }}>
                                    <Button
                                        title="Modifier"
                                        buttonStyle={styles.btn}
                                        onPress={()=>onUpdateZone()}
                                        disabled={e_country==country && e_address==address && e_town==town}
                                    />
                                </View>
                            </View>
                        </List.Accordion>
                        <List.Accordion title="Contact" description="Téléphone et site web de la boutique" titleStyle={styles.title}>
                        <View>
                                <Input
                                    value={phone}
                                    onChangeText={v=>setPhone(v)}
                                    maxLength={10}
                                    label="Numéro de téléphone"
                                    placeholder="phone sans code de pays"
                                    keyboardType='phone-pad'
                                />
                                <Input
                                    value={site}
                                    onChangeText={v=>setSite(v)}
                                    maxLength={20}
                                    keyboardType='url'
                                    label="Site web"
                                    placeholder="site internet"
                                />
                                <View style={{ alignItems:'flex-end',padding:5,paddingRight:20 }}>
                                    <Button
                                        title="Modifier"
                                        buttonStyle={styles.btn}
                                        disabled={e_phone==phone && site==e_site}
                                        onPress={()=>onUpdateContact()}
                                    />
                                </View>
                            </View>
                        </List.Accordion>
                        <List.Accordion title="Présentation" description="Texte de présentation de la boutique" titleStyle={styles.title}>
                        <View>
                                <Input
                                    value={description}
                                    onChangeText={v=>setDescription(v)}
                                    label="Description de la boutique"
                                    placeholder="libre description"
                                    multiline
                                    maxLength={255}
                                />
                                <View style={{ alignItems:'flex-end',padding:5,paddingRight:20 }}>
                                    <Button
                                        title="Modifier"
                                        buttonStyle={styles.btn}
                                        disabled={description==e_description}
                                        onPress={()=>onUpdateDescription()}
                                    />
                                </View>
                            </View>
                        </List.Accordion>
                    </List.Section>
                    <View style={{ alignItems:'center',padding:20 }}>
                        <Button
                            type="outline"
                            title="Annuler"
                            buttonStyle={{ padding:15 }}
                            onPress={onClose}
                        />
                    </View>
                </ScrollView>
                <ProgressDialog
                    message="Patientez SVP!!!"
                    title="Chargement"
                    visible={loading}
                    animationType='fade'
                    activityIndicatorColor={R.color.colorAccent}
                    activityIndicatorSize='large'
                />
            </View>
        </Overlay>
    )
}