import React from 'react';
import { View } from 'react-native';
import { Avatar, Text } from 'react-native-elements';
import R from '../resources/styles.json';

export default function App({icon="user-0",type="font-awesome",title,num,content}){

    return(
        <View style={{ alignItems:'center',padding:10 }}>
            <View>
                <Avatar
                    rounded size='xlarge'
                    icon={{ name:icon,type:type }}
                    containerStyle={{ backgroundColor:R.color.colorPrimary }}
                />
            </View>
            <Text numberOfLines={1} style={{fontFamily:'Poppins-Light',textAlign:'center'}}>
                {title}
            </Text>
            <Text numberOfLines={1} style={{ fontFamily:'Poppins-Black',textAlign:'center' }}>
                {content}
            </Text>
            <Text numberOfLines={1} style={{ fontFamily:'Poppins-Italic',textAlign:'center' }}>
                {num}
            </Text>
        </View>
    )
}