import React, { useState } from 'react';
import { FlatList } from 'react-native';
import { View } from 'react-native';
import { Avatar, Button, Divider, Icon, Input, Overlay, Text } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import { ConfirmDialog, Dialog } from 'react-native-simple-dialogs';
import R from '../resources/styles.json';
import RadioButton from './RadioButton';
import {Picker} from '@react-native-picker/picker';
import { capitalize } from '../Manager';
import { Appbar, FAB } from 'react-native-paper';
import S from '../resources/settings.json';
import UnityList from '../resources/unity';


export default function App({visible,onChangeUnity,unityValue,viewPrice=true,mode,onChangeMode,stock=0,onRadioStockNo,onRadioStockYes,service=true,onCatalog,catalog=true,onChangeDevise,onCamera,update=true,signe,devises=[],onRadioYes,onRadioNo,type,onPress,statusName,statusPrice,logo,valuePrice,onChangePrice,value,onChangeText,valueDescription,onChangeDescrption,onClose,category}){
    App.defaultProps={
        visible:false,
        logo:null,
        type:null,
        signe:null,
        update:false,
        catalog:false,service:false
    }
    const unities=UnityList;
    const[visibleQuantifiable,setVisibleQuatifiable]=useState(false);
    const[visibleStcok,setVisibleStock]=useState(false);
    let inputName=null;
    let inputDescription=null;
    let inputPrice=null;
    let inputdevise=null;
    const modes=[
        //{label:"Unité",value:"unity",key:"unity"},
        {label:"Devise",value:S.ENUM.stockage.devise,key:S.ENUM.stockage.devise},
        {label:"Autres",value:null,key:"other"},
    ]
    return(
        <>
        <Overlay
            isVisible={visible}
            fullScreen
            animationType='slide'
            overlayStyle={{padding:0}}
        >
            <View style={{flex:1}}>
                <Appbar.Header style={{backgroundColor:R.color.colorPrimary}}>
                    <Appbar.BackAction onPress={onClose} />
                    <Appbar.Content title={service==true?"Service":"Produit"} />
                    {
                        catalog==true?(
                            <Appbar.Action
                                icon={()=>(
                                <Icon onPress={onCatalog} color='#f7f7f7' name="camera" type="font-awesome" />
                                )}
                            />
                        ):null
                    }
                </Appbar.Header>
            <View style={{flex:1,justifyContent:'center',padding:10,paddingBottom:40}}>
                <ScrollView bounces>
                    <View style={{alignItems:'center',padding:20}}>
                        <Avatar
                            title='logo'
                            source={{uri:logo}}
                            rounded
                            size='xlarge'
                            titleStyle={{fontSize:50,fontWeight:'bold'}}
                            containerStyle={{backgroundColor:R.color.colorPrimary}}
                            placeholderStyle={{backgroundColor:R.color.colorPrimary}}
                        >
                            <Avatar.Accessory 
                                Component={()=>(
                                    <Icon onPress={onCamera} name='camera-outline'  type='ionicon'  />
                                )}
                                size={50}
                                style={{backgroundColor:R.color.colorSecondary,elevation:12}}
                            />
                        </Avatar>
                    </View>
                    <View>
                        <Input
                            label="Nom de l'article"
                            value={value}
                            onChangeText={onChangeText}
                            maxLength={20}
                            ref={input=>inputName=input}
                            returnKeyType='next'
                            onSubmitEditing={()=>inputDescription.focus()}
                            errorMessage={statusName==true?"Cet article existe déjà dans cette catégorie":null}
                        />
                        <Input
                            label="Description de l'article"
                            value={valueDescription}
                            ref={rep=>inputDescription=rep}
                            onChangeText={onChangeDescrption}
                            maxLength={20}
                            //returnKeyType='next'
                            //blurOnSubmit={false}
                            //onSubmitEditing={()=>inputPrice.focus()}
                        />
                        <Divider/>
                        {/*<View style={{ padding:10 }}>
                            <Text>
                                Unité de stockage
                            </Text>
                            <Picker
                                mode='dropdown'
                                onValueChange={onChangeUnity}
                                selectedValue={unityValue}
    
                            >
                                {
                                    unities.map((p,index)=>(
                                        <Picker.Item
                                            label={p.label}
                                            value={p.value}
                                            key={`unity${index}`}
                                        />
                                    ))
                                }
                            </Picker>

                        </View>*/}
                        {
                            (viewPrice==true && stock==false)?(
                        <View style={{flexDirection:'row',alignItems:'center'}}>
                            <View style={{flex:1}}>
                                <Input
                                    label="Prix de l'article"
                                    value={valuePrice}
                                    onChangeText={onChangePrice}
                                    maxLength={10}
                                    keyboardType='decimal-pad'
                                    keyboardAppearance='dark'
                                    returnKeyType='join'
                                    inputStyle={{textAlign:'right'}}
                                    ref={input=>inputPrice=input}
                                    errorMessage={statusPrice==true?"Définissez le prix":null}
                                    returnKeyType='next'
                                />
                            </View>
                            <View style={{marginLeft:10,width:100}}>
                                <Picker
                                    selectedValue={signe}
                                    onValueChange={onChangeDevise}
                                    mode="dropdown"
                                    ref={input=>inputdevise=input}
                                    collapsable={true}
                                    itemStyle={{fontFamily:'Poppins-Black'}}
                                    style={{fontSize:12,fontFamily:'Poppins-Black',padding:0,margin:0}}
                                >
                                    {
                                        devises.map((item,i)=>(
                                            <Picker.Item value={item.id} label={capitalize(item.signe).toUpperCase()} key={item.signe+i} />
                                        ))
                                    }
                                </Picker>
                            </View>
                        </View>):null
                        }
                        {
                            service==true?(
                            <View style={{ paddingLeft:10,paddingRight:10 }}>
                                <View>
                                <Text style={{fontFamily:"Poppins-Light",fontSize:16}}>
                                    Quantification de la vente de service
                                </Text>
                                <View style={{flexDirection:'row',alignItems:'center'}}>
                                    <View style={{padding:10}}>
                                        <RadioButton
                                            title="oui"
                                            value={true}
                                            checked={type}
                                            color={R.color.colorPrimary}
                                            textSize={16}
                                            direction="row"
                                            onPress={onRadioYes}
                                        />
                                    </View>
                                    <View style={{padding:10}}>
                                        <RadioButton
                                            title="non"
                                            value={false}
                                            checked={type}
                                            color={R.color.colorPrimary}
                                            textSize={16}
                                            onPress={onRadioNo}
                                            direction="row"
                                        />
                                    </View>
                                    <View style={{padding:10,alignItems:'flex-end'}}>
                                        <Icon
                                            color={R.color.colorSecondary}
                                            size={30}
                                            type='font-awesome'
                                            name="question"
                                            onPress={()=>setVisibleQuatifiable(true)} />
                                    </View>
                                </View>
                            </View>
                            <View>
                                <Text style={{fontFamily:"Poppins-Light",fontSize:16}}>
                                    Stockage du service
                                </Text>
                                <View style={{flexDirection:'row',alignItems:'center'}}>
                                    <View style={{padding:10}}>
                                        <RadioButton
                                            title="oui"
                                            value={true}
                                            checked={stock}
                                            color={R.color.colorPrimary}
                                            textSize={16}
                                            direction="row"
                                            onPress={onRadioStockYes}
                                        />
                                    </View>
                                    <View style={{padding:10}}>
                                        <RadioButton
                                            title="non"
                                            value={false}
                                            checked={stock}
                                            color={R.color.colorPrimary}
                                            textSize={16}
                                            onPress={onRadioStockNo}
                                            direction="row"
                                        />
                                    </View>
                                    <View style={{padding:10,alignItems:'flex-end'}}>
                                        <Icon
                                            color={R.color.colorSecondary}
                                            size={30}
                                            type='font-awesome'
                                            name="question"
                                            onPress={()=>setVisibleStock(true)} />
                                    </View>
                                </View>
                            </View>
                            {
                                stock==1?(
                                    <View>
                                    <Text style={{fontFamily:"Poppins-Light",fontSize:16}}>
                                        Mode de stockage 
                                    </Text>
                                    <Picker
                                        mode='dropdown'
                                        onValueChange={onChangeMode}
                                        selectedValue={mode}
                                    >
                                        {
                                            modes.map((item,index)=>(
                                                <Picker.Item
                                                    label={item.label}
                                                    key={item.key}
                                                    value={item.value}
                                                />
                                            ))
                                        }
                                    </Picker>
                                    </View>
                                ):null
                            }
                        </View>
                            ):null
                        }
                        <View>
                            {category!=null?(<Text style={{fontFamily:"Poppins-Light",fontSize:16,padding:10}}>
                                Vous enregistrez cet article comme un élément
                                <Text style={{fontSize:18,padding:10,paddingTop:0,fontFamily:'Poppins-Black'}}>
                                    {(category!=null && category!=undefined)?" "+category:"Catégorie non défini"}
                                </Text>
                            </Text>):"Catégorie non définie"}
                        </View>
                    </View>
                    <View style={{alignItems:'center',padding:10,marginBottom:20}}>
                        <Button
                            onPress={onPress}
                            title={update==true?"Modifier":"Ajouter"}
                            buttonStyle={{padding:15,borderRadius:20,paddingLeft:25,paddingRight:25,backgroundColor:R.color.colorPrimary}}
                        />
                    </View>
                </ScrollView>
            </View>
            </View>
        </Overlay>
        <Dialog
            message="Si oui, vous pouvez quantifier la vente de ce service"
            visible={visibleQuantifiable}
            onTouchOutside={()=>setVisibleQuatifiable(false)}
            title="Aide"
            animationType='fade'
        >
            <ScrollView>
                <View style={{padding:10}}>
                    <Text style={{fontFamily:'Poppins-Light'}}>
                    Il est possible de quantifier un service lors de la vente avec Ligal Apply.
                    </Text>
                </View>
                    <View style={{padding:10}}>
                        <Text style={{fontFamily:'Poppins-Black',fontSize:16}}>
                            Le service quantifiable
                        </Text>
                        <Text style={{fontFamily:'Poppins-Light',textAlign:'justify'}}>
                        Certains services nécessitent une vente quantifiée ; et, Ligal Apply tient compte de ce mécanisme de vente.
                        </Text>
                    </View>
            </ScrollView>
        </Dialog>
        <Dialog
            message="Si oui, vous pouvez enregistrer des stocks dans cet article"
            visible={visibleStcok}
            onTouchOutside={()=>setVisibleStock(false)}
            title="Aide"
            animationType='fade'
        >
            <ScrollView>
                <View style={{padding:10}}>
                    <Text style={{fontFamily:'Poppins-Light'}}>
                    {"Vous pouvez aussi stocker des services avec Ligal-Apply.\nDeux mode de stockage sont disponibles :"} 
                    </Text>
                </View>
                    <View style={{padding:10}}>
                        <Text style={{fontFamily:'Poppins-Black',fontSize:16}}>
                            Devise
                        </Text>
                        <Text style={{fontFamily:'Poppins-Light',textAlign:'justify'}}>
                            Des services de paie mobile, ou la vente de monnaie électronique exigent un stockage de monnaie. Ligal-Apply vous permet de stocker des monnaies en plusieurs devises.
                        </Text>
                    </View>
                    <View style={{padding:10}}>
                        <Text style={{fontFamily:'Poppins-Black',fontSize:16}}>
                            Unité
                        </Text>
                        <Text style={{fontFamily:'Poppins-Light',textAlign:'justify'}}>
                            Stocker vos services comme un produit avec Ligal-Apply.
                        </Text>
                    </View>
            </ScrollView>
        </Dialog>
        </>
    )
    
}