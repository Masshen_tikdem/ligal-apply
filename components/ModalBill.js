import React from 'react';
import { View,FlatList, StyleSheet } from 'react-native';
import { Avatar, Overlay } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import { Appbar, Text } from 'react-native-paper';
import { getArticleName, getPrice, getTotal, getTotalBuy } from '../Manager';

const styles=StyleSheet.create({
    view:{
        padding:5
    }
})
const ComponentHeader=({bill})=>{
    return(
        <View style={{ padding:5 }}>
            <View style={{ flexDirection:'row',alignItems:'center' }}>
                <View style={{ flex:1 }}>
                    <Text style={{ fontFamily:'Poppins-Bold',fontSize:18,padding:5 }} numberOfLines={1}>
                        {bill}
                    </Text>
                </View>
                <View style={{ padding:5 }}>
                    <Avatar
                        rounded size='medium'
                    />
                </View>
            </View>
        </View>
    )
}

const renderInfo=({item,index})=>{
    console.log('article',item)
    return(
        <View style={{ flexDirection:'row',alignItems:'center' }}>
            <View>
                <Text>
                    {item.id}
                </Text>
            </View>
            <View>
                <Text>
                    {getArticleName(item.name,item.description)}
                </Text>
            </View>
            <View>
                <Text>
                    {item.quantity}
                </Text>
            </View>
            <View>
                <Text>
                    {getPrice(item.price,item.signe)}
                </Text>
            </View>
            <View>
                <Text>
                    {item.quantity>0?getPrice(item.price*item.quantity,item.signe,item.remise):getPrice(item.price,item.signe,item.remise)}
                </Text>
            </View>
        </View>
    )
}
const RenderHeader=()=>{
    return(
        <View style={{ flexDirection:'row',alignItems:'center' }}>
            <View style={[styles.view]}>
                <Text>
                    ID
                </Text>
            </View>
            <View style={[styles.view]}>
                <Text>
                    Nom de l'article
                </Text>
            </View>
            <View style={[styles.view]}>
                <Text>
                    Qte
                </Text>
            </View>
            <View style={[styles.view]}>
                <Text>
                    PU
                </Text>
            </View>
            <View style={[styles.view]}>
                <Text>
                    Total
                </Text>
            </View>
        </View>
    )
}
const ComponentInfo=({list})=>{
    return(
        <FlatList
            data={list}
            renderItem={renderInfo}
            key="infos"
            keyExtractor={item=>item.id}
            ListHeaderComponent={()=>(
                <RenderHeader/>
            )}
        />
    )
}


export default function App({visible=false,bill="",shop,list,onClose}){

    const items=[
        {
            id:1,
            component:ComponentHeader({bill:bill}),
            list:[]
        },
        {
            id:2,
            component:ComponentInfo({list:list}),
            list:[]
        }
    ];

    const renderItem=({item,index})=>{
        return(
            <item.component />
        )
    }
    
    return(
        <Overlay isVisible={visible} animationType='fade' overlayStyle={{ padding:0 }} fullScreen>
            <View style={{ flex:1 }}>
                <Appbar.Header>
                    <Appbar.BackAction
                        onPress={onClose}
                    />
                    <Appbar.Content
                        title={bill}
                    />
                </Appbar.Header>
                <ScrollView>
                    <ComponentHeader bill={bill} />
                    <ComponentInfo list={list} />
                </ScrollView>
            </View>
        </Overlay>
    )
}