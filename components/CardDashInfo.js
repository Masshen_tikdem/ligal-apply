import React from 'react';
import { View } from 'react-native';
import { Card, Text } from 'react-native-elements';
import R from '../resources/styles.json';

export default function App({color,title,content}){

    App.defaultProps={
        color:R.color.colorSecondary
    }
    return(
        <View>
            <Card containerStyle={{ borderRadius:15,backgroundColor:color }}>
                <View>
                    <Text style={{ fontFamily:'Poppins-Regular',color:color==R.color.background?"#000":R.color.background,padding:3 }}>
                        {title}
                    </Text>
                    <Text style={{ fontFamily:'Poppins-Bold',color:color==R.color.background?"#000":R.color.background,padding:3 }}>
                        {content}
                    </Text>
                </View>
            </Card>
        </View>
    )
}