import React from 'react';
import { Image } from 'react-native';
import { StyleSheet } from 'react-native';
import { ScrollView } from 'react-native';
import { View } from 'react-native';
import { Text } from 'react-native-elements';

const styles=StyleSheet.create({
    num:{
        fontFamily:'Poppins-Black',
        fontSize:15,
        padding:5,
        paddingLeft:10
    },
    content:{
        fontFamily:'Poppins-Light',
        fontSize:14
    }
})

export default function App(){

    return(
        <View style={{ justifyContent:'center',flex:1,padding:10,paddingTop:20 }}>
            <ScrollView>
                <View style={{ alignItems:'center',padding:10 }}>
                    <Image
                        source={require('../assets/logo/ligal.png')}
                    />
                    <Text style={{ fontFamily:'Poppins-Black',fontSize:18,textAlign:'center',padding:5 }}>
                        C’est dans cet espace que vous allez réaliser vos ventes.
                    </Text>
                    <Text style={{ fontFamily:'Poppins-Regular',textAlign:'justify',fontSize:16,padding:5 }}>
                        Actuellement, vous n’avez enregistré aucun article qui pourra vous permettre de réaliser vos ventes.
                    </Text>
                    <View style={{ flexDirection:'row',alignItems:'center' }}>
                        <View>
                            <Text style={styles.num}>1.</Text>
                        </View>
                        <View>
                            <Text style={styles.content}>
                                Allez dans Marché, ajoutez une ou plusieurs catégories d’articles,
                            </Text>
                        </View>
                    </View>
                    <View style={{ flexDirection:'row',alignItems:'center' }}>
                        <View>
                            <Text style={styles.num}>2.</Text>
                        </View>
                        <View>
                            <Text style={styles.content}>
                                Accéder dans votre catégorie pour y ajouter des articles,
                            </Text>
                        </View>
                    </View>
                    <View style={{ flexDirection:'row',alignItems:'center' }}>
                        <View>
                            <Text style={styles.num}>3.</Text>
                        </View>
                        <View>
                            <Text style={styles.content}>
                                Si l’article ajouté est un produit, veuillez approvisionner son stock avant de passer à la vente 
                            </Text>
                        </View>
                    </View>
                    
                </View>
            </ScrollView>
        </View>
    )
}