import React, { useState } from 'react';
import { View } from 'react-native';
import { Avatar, Button, Icon, Input, Overlay, Text } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import R from '../resources/styles.json';
import RadioButton from '../components/RadioButton';
import { Appbar, FAB } from 'react-native-paper';
import { Dialog } from 'react-native-simple-dialogs';



export default function App({visible,onCatalog,mark=true,catalog=true,onImage,update=true,logo,value,onChangeText,onClose,onPress,status=true,checked,onProduct,onService}){
    App.defaultProps={
        visible:false,
        logo:null,
        status:true,
        update:false,
        mark:false,
        catalog:false
    }

    const[showDialog,setVisible]=useState(false);

    return(
        <Overlay
            isVisible={visible}
            fullScreen
            animationType='slide'
            overlayStyle={{padding:0}}
        >
            <View style={{flex:1}}>
                <Appbar.Header style={{backgroundColor:R.color.colorPrimary}}>
                    <Appbar.BackAction onPress={onClose} />
                    <Appbar.Content title="Catégorie" />
                </Appbar.Header>
            <View style={{flex:1,justifyContent:'center',padding:10}}>
                <ScrollView>
                    <View style={{alignItems:'center',padding:20}}>
                        <Avatar
                            title='logo'
                            source={{uri:logo}}
                            rounded
                            size='xlarge'
                            titleStyle={{fontSize:40,fontFamily:"Poppins-Black"}}
                            placeholderStyle={{backgroundColor:R.color.colorPrimary}}
                            containerStyle={{backgroundColor:R.color.colorPrimary,elevation:10}}
                        >
                            <Avatar.Accessory 
                                Component={()=>(
                                    <Icon name='camera' type='font-awesome' color={R.color.background} />
                                )}
                                size={50}
                                style={{backgroundColor:R.color.colorSecondary,elevation:12}}
                                onPress={onImage}
                            />
                        </Avatar>
                    </View>
                    <View>
                        <Input
                            label={mark==false?"Nom de la catégorie":"Nom de la sous-catégorie"}
                            value={value}
                            onChangeText={onChangeText}
                            maxLength={20}
                            errorMessage={status==true?"Cette catégorie existe déjà":""}
                            textBreakStrategy='highQuality'
                        />
                        {
                            mark==false?(
                                <View>
                                    <Text style={{fontFamily:'Poppins-Light',fontSize:18}}>
                                        Type de catégorie
                                    </Text>
                                    <View style={{flexDirection:'row',alignItems:'center'}}>
                                        <View style={{padding:10}}>
                                            <RadioButton
                                                value="product"
                                                title="Produit"
                                                onPress={onProduct}
                                                checked={checked}
                                                direction='row'
                                                color={R.color.colorSecondary}
                                            />
                                        </View>
                                        <View style={{padding:10}}>
                                            <RadioButton
                                                value="service"
                                                title="Service"
                                                direction='row'
                                                onPress={onService}
                                                checked={checked}
                                                color={R.color.colorSecondary}
                                            />
                                        </View>
                                        <View style={{padding:10,alignItems:'flex-end'}}>
                                            <Icon 
                                                color={R.color.colorSecondary}
                                                size={30}
                                                type='font-awesome'
                                                name="question"
                                                onPress={()=>setVisible(true)} />
                                        </View>
                                    </View>
                                </View>
                            ):null
                        }
                    </View>
                    <View style={{alignItems:'center'}}>
                        <Button
                            title={update==true?"Modifier":"Ajouter"}
                            onPress={onPress}
                            buttonStyle={{padding:15,borderRadius:20,paddingLeft:25,paddingRight:25,backgroundColor:R.color.colorPrimary}}
                        />
                    </View>
                </ScrollView>
            </View>
            {
                catalog==true?(
                    <FAB
                        icon="camera"
                        onPress={onCatalog}
                        label="logo de catalogue"
                        style={{position:'absolute',bottom:0,margin:25,right:0,backgroundColor:R.color.colorPrimary}}
                    />
                ):null
            }
            </View>
            <Dialog 
                animationType='slide'
                visible={showDialog}
                onTouchOutside={()=>setVisible(false)}
                key='dialog-view'
                title="Aide"
            >
                <ScrollView>

                    <View style={{padding:10}}>
                        <Text style={{fontFamily:'Poppins-Light'}}>
                            Ligal Apply distingue un produit d'un service lors de processus de vente
                        </Text>
                    </View>
                    <View style={{padding:10}}>
                        <Text style={{fontFamily:'Poppins-Black',fontSize:16}}>
                            Le service
                        </Text>
                        <Text style={{fontFamily:'Poppins-Light',textAlign:'justify'}}>
                        Le service étant immatériel, il ne requit aucun approvisionnement de stock. Vous pouvez vendre le service sans un stock.
                        </Text>
                    </View>
                    <View style={{padding:10}}>
                        <Text style={{fontFamily:'Poppins-Black',fontSize:16}}>
                            Le Produit
                        </Text>
                        <Text style={{fontFamily:'Poppins-Light',textAlign:'justify'}}>
                        Le produit est un article tangible que vous devez approvisionner avant la vente
                        </Text>
                    </View>
                    <Text style={{fontFamily:'Poppins-Italic',textAlign:'justify',padding:10}}>
                    Ce choix est quasi-unique pour votre boutique
                    </Text>
                </ScrollView>

            </Dialog>
        </Overlay>
    )
    
}