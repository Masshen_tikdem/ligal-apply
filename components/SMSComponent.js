import React from 'react';
import { ScrollView } from 'react-native';
import { View } from 'react-native';
import { Button, CheckBox, Input, Overlay, Text } from 'react-native-elements';
import { Appbar } from 'react-native-paper';
import R from '../resources/styles.json';

export default function App({visible=false,check=false,personnal="",societyName="",onChangeSocietyName,content="",onChangeContent,onClose,onPress}){

    return(
        <Overlay overlayStyle={{ padding:0 }} fullScreen visible={visible} isVisible={visible} animationType='slide'>
            <View style={{ flex:1 }}>
                <Appbar.Header style={{ backgroundColor:R.color.colorPrimary }}>
                    <Appbar.BackAction
                        onPress={onClose}
                    />
                    <Appbar.Content
                        title="Redaction de SMS"
                        subtitle={content.length}
                    />
                </Appbar.Header>
                <View style={{ flexDirection:'row',alignItems:"center" }}>
                    <View style={{ flex:1 }}>
                        <CheckBox
                            checked={check}
                            title="Utiliser SMS personnalisé"
                        />
                    </View>
                    <View style={{ padding:5 }}>
                        <Button
                            title="Envoyez"
                            style={{ padding:15,borderRadius:10,backgroundColor:R.color.colorPrimary }}
                            onPress={onPress}
                            disabled={content.length==0}
                        />
                    </View>
                </View>
                <ScrollView>
                <View style={{ padding:10,alignItems:'center' }}>
                    <Input
                        value={societyName}
                        disabled
                        label="Titre du message"
                    />
                </View>
                <View style={{ flex:1 }}>
                    <Input
                        value={content}
                        multiline
                        onChangeText={onChangeContent}
                        label="Contenu du message"
                    />
                    <View style={{ flexDirection:'row',alignItems:'center',justifyContent:'flex-end',padding:5 }}>
                        <Text style={{ fontFamily:'Poppins-Light',fontSize:16 }}>
                            {content.length+'/'}
                        </Text>
                        <Text style={{ fontFamily:'Poppins-Black',padding:5,fontSize:16 }}>
                            {(Math.round(content.length/150)+1)+" SMS"}
                        </Text>
                    </View>
                </View>
                </ScrollView>
            </View>
        </Overlay>
    )
}