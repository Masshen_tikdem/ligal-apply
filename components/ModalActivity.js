import React from 'react';
import { View } from 'react-native';
import { Avatar, Button, Card, Divider, Input, Overlay, Text } from 'react-native-elements';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import { Appbar, Banner } from 'react-native-paper';
import S from '../resources/settings.json';
import R from '../resources/styles.json';
import {Picker} from '@react-native-picker/picker';
import { capitalize } from '../Manager';

const openDevise=(value)=>{
    let rep=false;
    try {
        switch (value) {
            case S.ENUM.activity_type.stock:
                rep=true;    
            break;
            case S.ENUM.activity_type.contrat:
                rep=true;    
            break;
            case S.ENUM.activity_type.purpose:
                rep=true;    
            break;
            case S.ENUM.activity_type.spent:
                rep=true;    
            break;
        
            default:
                break;
        }
    } catch (error) {
        
    }
    return rep;
}

const getTitle=(value)=>{
    let rep="";
    try {
        switch (value) {
            case S.ENUM.activity_type.stock:
                rep="stock";    
            break;
            case S.ENUM.activity_type.contrat:
                rep="contrat";    
            break;
            case S.ENUM.activity_type.purpose:
                rep="l'bjectif";    
            break;
            case S.ENUM.activity_type.spent:
                rep="la dépense";    
            break;
        
            default:
                break;
        }
    } catch (error) {
        
    }
    return rep;
}

export default function App({
    visible=false,onClose,
    devises=[],devise=null,onChangeDevise,
    value="",onChangeValue,title="",price="",onChangePrice,quantity="",onChangeQuantity,
    onPress,onChangeType,description="",onChangeDescription,
    agents=[],onAddAgent,renderAgent,agentSelections=[],
    max=0
}){

    return(
        <Overlay overlayStyle={{ padding:0 }} fullScreen animationType='slide' isVisible={visible}>
            <View style={{ flex:1 }}>
                <Appbar.Header style={{ backgroundColor:R.color.colorPrimary }}>
                    <Appbar.BackAction
                        onPress={onClose}
                    />
                    <Appbar.Content
                        title={title}
                    />
                </Appbar.Header>
                <ScrollView style={{ padding:10 }}>
                    {
                        value==S.ENUM.activity_type.stock?(
                            <View>
                                <Input
                                    label="Quantité à approvisionner"
                                    value={quantity}
                                    onChangeText={onChangeQuantity}
                                    keyboardType='number-pad'
                                />
                            </View>
                        ):null
                    }
                    {
                        openDevise(value)?(
                            <View style={{ flexDirection:'row',alignItems:'center' }}>
                                <View style={{ flex:1 }}>
                                    <Input
                                        label="Valeur totale"
                                        value={price}
                                        onChangeText={onChangePrice}
                                        inputStyle={{ textAlign:'right' }}
                                        keyboardType='number-pad'
                                        enablesReturnKeyAutomatically
                                    />
                                </View>
                                <View style={{ width:100 }}>
                                    <Picker
                                        selectedValue={devise}
                                        onValueChange={onChangeDevise}
                                        mode='dropdown'

                                    >
                                        {
                                            devises.map((item,index)=>(
                                                <Picker.Item
                                                    label={capitalize(item.signe).toUpperCase()}
                                                    value={item.id}
                                                    key={`devise_activity${index}`}
                                                />
                                            ))
                                        }
                                    </Picker>
                                </View>
                            </View>
                        ):null
                    }
                    <View>
                        <Input
                            value={description}
                            onChangeText={onChangeDescription}
                            label={"Description de "+getTitle(value)}
                            multiline
                            inputStyle={{ maxHeight:200 }}
                        />
                    </View>
                    <Card>
                    <View>
                        <View style={{ flexDirection:'row',alignItems:'center',padding:5 }}>
                            <View style={{ flex:1 }}>
                                <Text style={{ fontFamily:'Poppins-Black',textAlign:"center",fontSize:16,padding:5 }}>
                                    Des agents concernés par cette activité ({agentSelections.length})
                                </Text>
                            </View>
                            <View style={{ marginRight:5 }}>
                                <Avatar
                                    icon={{ name:"add",type:'ionicon',color:R.color.background }}
                                    rounded
                                    onPress={max==0?onAddAgent:agentSelections.length<max?onAddAgent:null}
                                    containerStyle={{ backgroundColor:R.color.colorPrimary,opacity:max==0?1:agentSelections.length<max?1:0.4 }}
                                />
                            </View>
                        </View>
                        <Divider/>
                        {
                            agentSelections.length>0?(
                                <FlatList
                                    data={agentSelections}
                                    renderItem={renderAgent}
                                    key="list_agent"
                                    keyExtractor={item=>item.id}
                                    horizontal
                                />
                            ):(
                                <Banner
                                    visible={true}
                                    actions={[
                                        
                                    ]}
                                >
                                    <Text style={{ fontFamily:'Poppins-Light',fontSize:18 }}>
                                        Vous pouvez cibler d'autres agents dans  votre activité afin que ces derniers y prennent part
                                    </Text>
                                </Banner>
                            )
                        }
                    </View>
                    </Card>
                    <View style={{ alignItems:'center',padding:20 }}>
                        <Button
                            title="Valider"
                            onPress={onPress}
                            buttonStyle={{ padding:15,width:150,borderRadius:15,backgroundColor:R.color.colorPrimary }}
                            titleStyle={{ fontWeight:'normal',fontFamily:'Poppins-Light',fontSize:16 }}
                        />
                    </View>
                </ScrollView>
            </View>
        </Overlay>
    )
}