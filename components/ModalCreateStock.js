import React from 'react';
import { View } from 'react-native';
import { Avatar, Button, Icon, Input, Overlay, Text } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import { Appbar } from 'react-native-paper';
import R from '../resources/styles.json';
import RadioButton from './RadioButton';
import {Picker} from '@react-native-picker/picker';
import { capitalize } from '../Manager';



export default function App({visible,onChangeDeviseValue,devises=[],devise=0,type="product",mode="",maxLength=10,value,fournisseur,onChangeText,onChangeFournisseur,onClose,onPress,status=true}){
    App.defaultProps={
        visible:false,
        logo:null,
        status:true,
        statusSigne:true,
        fournisseur:''
    }
    let inputProvider=null;

    return(
        <Overlay
            isVisible={visible}
            fullScreen
            animationType='slide'
            overlayStyle={{padding:0}}
        >
            <View style={{flex:1}}>
            <Appbar.Header style={{backgroundColor:R.color.colorPrimary}}>
                <Appbar.BackAction onPress={onClose} />
                <Appbar.Content title="Approvisionnement" />
            </Appbar.Header>
            <View style={{flex:1,justifyContent:'center',padding:10}}>
                <View style={{flex:1,justifyContent:'center',padding:10}}>
                    <View>
                        <View style={{ flexDirection:'row',alignItems:'center' }}>
                            <View style={{ flex:1 }}>
                                <Input
                                    label={mode=="devise"?"Montant à approvisionner":"Quantité"}
                                    value={value}
                                    onChangeText={onChangeText}
                                    inputStyle={{ textAlign:mode=="devise"?'right':'left' }}
                                    maxLength={maxLength}
                                    keyboardType='number-pad'
                                    onSubmitEditing={()=>inputProvider.focus()}
                                    returnKeyType='next'
                                    errorMessage={status==true?mode=="devise"?"Ce montant n'est pas valide":"Cette quantité n'est pas valide":""}
                                />
                            </View>
                            {
                                (mode=="devise" && type=="service")?(
                                    <View style={{ width:125 }}>
                                        <Picker
                                            mode="dropdown"
                                            selectedValue={devise}
                                            onValueChange={onChangeDeviseValue}
                                        >
                                        {
                                            devises.map((item)=>(
                                                <Picker.Item
                                                    label={capitalize(item.signe).toUpperCase()}
                                                    value={item.id}
                                                    key={`index${item.id}`}
                                                />
                                            ))
                                        }
                                        </Picker>
                                    </View>
                                ):null
                            }
                        </View>
                        <Input
                            label="Fournisseur"
                            value={fournisseur}
                            onChangeText={onChangeFournisseur}
                            ref={x=>inputProvider=x}
                            maxLength={20}
                        />
                    </View>
                    <View style={{alignItems:'center'}}>
                        <Button
                            title="Ajouter"
                            onPress={onPress}
                            buttonStyle={{padding:15,paddingLeft:25,paddingRight:25,backgroundColor:R.color.colorPrimary}}
                        />
                    </View>
                </View>
            </View>
            </View>
        </Overlay>
    )
    
}