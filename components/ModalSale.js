import React from 'react';
import { Image } from 'react-native';
import { FlatList } from 'react-native';
import { View } from 'react-native';
import { Avatar, Button, Icon, Input, Overlay, Text } from 'react-native-elements';
import { FloatingMenu } from 'react-native-floating-action-menu';
import { ScrollView } from 'react-native-gesture-handler';
import R from '../resources/styles.json';
import RadioButton from './RadioButton';



export default function App({visible,onClient,onTotal,onRemise,list=[],renderItem,isOpen=false,onOpen,remise,onClose,onPress,total="",count="",client=""}){
    App.defaultProps={
        visible:false,
        logo:null,
        status:true,
        statusSigne:true
    }


    return(
        <Overlay
            isVisible={visible}
            fullScreen
            animationType='slide'
            overlayStyle={{padding:0}}
            key="modal-sale"
        >
            <View style={{flex:1}}>
            <View style={{position:'absolute',top:0,left:0,margin:10,zIndex:3000}}>
                <Avatar
                    icon={{name:'close',type:'font-awesome',color:'#fff'}}
                    size='medium'
                    containerStyle={{backgroundColor:'brown'}}
                    rounded
                    onPress={onClose}
                />
            </View>
            <View style={{position:'absolute',flexDirection:'row',alignItems:'center',top:110,right:0,margin:10,zIndex:3000}}>
                <Text style={{color:R.color.background,paddingRight:5}}>
                    Valider la vente
                </Text>
                <Avatar
                    icon={{name:'check',type:'font-awesome',color:'#fff'}}
                    size='medium'
                    containerStyle={{backgroundColor:R.color.colorPrimaryDark,elevation:10}}
                    rounded
                    onPress={onPress}
                />
            </View>
            <View style={{flex:1,justifyContent:'center'}}>
                <View style={{flex:1}}>
                    
                    <View style={{backgroundColor:R.color.colorPrimary,padding:10,paddingTop:18}}>
                        <Text style={{textAlign:'center',fontSize:18,opacity:0.8,color:R.color.background,padding:10}}>
                            {count}
                        </Text>
                        <Text numberOfLines={1} 
                            style={{textAlign:'center',fontWeight:'bold',color:R.color.background,fontSize:20,padding:10,paddingTop:0}}>
                            {total}
                        </Text>
                        <View style={{flexDirection:'row',alignItems:"center",alignSelf:'center'}}>
                            {
                                (client!=undefined && client!=null && client!="")?(
                                    <Icon
                                        name="user-circle-o" type='font-awesome'
                                        color={R.color.background}
                                    />
                                ):null
                            }
                           
                            <View style={{flex:1}}>
                                <Text style={{padding:10,fontSize:18,fontFamily:"Poppins-Light",opacity:0.8,color:R.color.background}}>
                                    {
                                        (client!=undefined && client!=null && client!="")?client:"Aucun client"
                                    }
                                </Text>
                            </View>
                        </View>
                    </View>
                    <FlatList
                        data={list}
                        renderItem={renderItem}
                        key='list-cart'
                        keyExtractor={item=>item.id}
                    />
                    <FloatingMenu
                        items={[
                            {
                                label:"Le client",
                                labelStyle:{color:'#000',fontFamily:'Poppins-Regular',fontSize:14},
                                onPress:onClient,
                                icon:{name:"add",type:"ionicon"},
                                key:"cl"
                            },
                            {
                                label:"Le total",
                                key:"tt",
                                //image:require('../assets/logo/ligal.png'),
                                icon:()=>(
                                    <Image source={require('../assets/logo/ligal.png')} />
                                ),
                                onPress:onTotal,
                                labelStyle:{color:'#000',fontFamily:'Poppins-Regular',fontSize:14},
                            },
                            {
                                label:"La remise",
                                key:"rm",
                                onPress:onRemise,
                                labelStyle:{color:'#000',fontFamily:'Poppins-Regular',fontSize:14},
                            },
                        ]}
                        isOpen={isOpen}
                        onMenuToggle={onOpen}
                        key="floating-menu"

                    />
                </View>
            </View>
            </View>
        </Overlay>
    )
    
}