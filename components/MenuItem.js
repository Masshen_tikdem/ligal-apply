import React from 'react';
import { View } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { Icon, Text } from 'react-native-elements';
import R from '../resources/styles.json';


export default function App({onPress,title="",icon="money",type="font-awesome",color="#444"}){

    return(
        <TouchableOpacity onPress={onPress}>
            <View style={{ flexDirection:'row',alignItems:'center',padding:2,borderBottomWidth:1,borderBottomColor:'#eee' }}>
                <View style={{ paddingRight:5 }}>
                    <Icon
                        name={icon}
                        type={type}
                        color={color}
                    />
                </View>
                <View style={{ paddingLeft:5 }}>
                    <Text numberOfLines={1} style={{ padding:5,paddingLeft:0,fontFamily:'Poppins-Medium',fontSize:15 }}>
                        {title}
                    </Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}