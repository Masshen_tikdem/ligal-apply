import React from 'react';
import { View } from 'react-native';
import { Avatar, Button, Icon, Input, Overlay, Text } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import R from '../resources/styles.json';
import RadioButton from './RadioButton';



export default function App({visible,update=true,logo,value,signe,onChangeText,onChangeSigne,onClose,onPress,status=true,statusSigne=true}){
    App.defaultProps={
        visible:false,
        logo:null,
        status:true,
        statusSigne:true,
        update:false
    }

    let inputSign=null;

    return(
        <Overlay
            isVisible={visible}
            fullScreen
            animationType='slide'
        >
            <View style={{ flex:1 }}>
            <View>
                <Avatar
                    icon={{name:'close',type:'font-awesome',color:'#fff'}}
                    size='medium'
                    containerStyle={{backgroundColor:'brown'}}
                    rounded
                    onPress={onClose}
                />
            </View>
            <View style={{flex:1,justifyContent:'center',padding:10}}>
                <View>
                    {/*<View style={{alignItems:'center',padding:20}}>
                        <Avatar
                            title='logo'
                            source={{uri:logo}}
                            rounded
                            size='xlarge'
                            titleStyle={{fontSize:50,fontWeight:'bold'}}
                        >
                            <Avatar.Accessory 
                                Component={()=>(
                                    <Icon name='camera-outline' type='ionicon' />
                                )}
                                size={50}
                                style={{backgroundColor:R.color.colorSecondary,elevation:12}}
                            />
                        </Avatar>
                    </View>*/}
                    <View>
                        <Input
                            label="Nom de la monnaie"
                            value={value}
                            onChangeText={onChangeText}
                            maxLength={15}
                            onSubmitEditing={()=>inputSign.focus()}
                            returnKeyType='next'
                            errorMessage={status==true?"Cette devise existe déjà":""}
                        />
                        <Input
                            label="Signe de la monnaie"
                            value={signe}
                            onChangeText={onChangeSigne}
                            maxLength={5}
                            ref={x=>inputSign=x}
                            errorMessage={statusSigne==true?"Ce signe est déjà utilisé":""}
                        />
                    </View>
                    <View style={{alignItems:'center'}}>
                        <Button
                            title={update==true?"Modifier":"Ajouter"}
                            onPress={onPress}
                            buttonStyle={{padding:15,paddingLeft:25,paddingRight:25,backgroundColor:R.color.colorPrimary}}
                        />
                    </View>
                </View>
            </View>
            </View>
        </Overlay>
    )
    
}