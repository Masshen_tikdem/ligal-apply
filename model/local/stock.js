import Logiciel from '../../controller/database/logiciel';
import React from "react";
import CommandValues from '../../controller/commandValues';
import S from '../../resources/settings.json';
import * as DeviseModel from './devise';
import { getTotalStock } from '../../Manager';

const db_name="stocks";

export async function store({fournisseur=null,id_devise=0,created_at="",id_agent=0,id_article=0,private_key=null,quantity=0}){
   let rep=null;
    try {
        const cmd=new CommandValues();
        //cmd.addValue('fournisseur',fournisseur);
        cmd.addValue('created_at',created_at);
        cmd.addValue('id_agent',id_agent);
        cmd.addValue('id_article',id_article);
        cmd.addValue('private_key',private_key);
        cmd.addValue('quantity',quantity);
        cmd.addValue('id_devise',id_devise);
        cmd.addValue('fournisseur',fournisseur);
        const query=cmd.getInsertCommand({list:cmd.getValues(),table:db_name});
        if(query!=null){
            const logiciel=new Logiciel();
            const table=await logiciel.ExecuteQuery(query);
            if(table.rowsAffected>0){
                rep=await showLast();
            }
        }
   } catch (error) {
       rep=null;
   }
   return rep;
}

export async function update({fournisseur=null,created_at=null,id_agent=null,id_article=null,private_key=null,quantity=null,id=0}){
    let rep=false;
     try {
         const cmd=new CommandValues();
         cmd.addValue('fournisseur',fournisseur);
         cmd.addValue('created_at',created_at);
         cmd.addValue('id_agent',id_agent);
         cmd.addValue('id_article',id_article);
         cmd.addValue('private_key',private_key);
         cmd.addValue('quantity',quantity);
         const query=cmd.getUpdateCommand({list:cmd.getValues(),table:db_name,whereClause:"id="+id});
         if(query!=null){
             const logiciel=new Logiciel();
             const table=await logiciel.ExecuteQuery(query);
             if(table.rowsAffected>0){
                 rep=true;
             }
         }
    } catch (error) {
        rep=false;
    }
    return rep;
 }

export async function get(){
    let rep=[];
        try {
            const logiciel=new Logiciel();
            let query="SELECT stocks.*,";
            query+="articles.name,articles.description,articles.logo,articles.private_key AS article_private_key,articles.type,articles.mode,";
            query+="familles.name AS famille,agents.private_key AS agent_private_key ";
            query+="FROM stocks INNER jOIN articles ON articles.id=stocks.id_article ";
            query+="INNER JOIN familles ON familles.id=articles.id_famille ";
            query+="INNER JOIN agents ON agents.id=stocks.id_agent ";
            query+="ORDER BY stocks.created_at DESC";
            const result=await logiciel.ExecuteQuery(query);
            const devises=await DeviseModel.get();
            if(result.rows.item.length>0){
                for (let index = 0; index < result.rows.length; index++) {
                    const element = result.rows.item(index);
                    if(element.type==S.ENUM.article.service && element.mode==S.ENUM.stockage.devise){
                        const list=[];
                        list.push(element);
                        const stock=await getTotalStock(list,devises);
                        element.quantity=stock;
                        rep.push(element);
                    }else{
                        rep.push(element)
                    }
                }
            }
        } catch (error) {
        }
        return rep;
}

/**
 * 
 * @param {*id of article} id 
 */
export async function show(id=0){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT stocks.*,";
        query+="articles.name,articles.description,articles.logo,articles.private_key AS article_private_key,articles.type,articles.mode,";
        query+="agents.first_name,agents.last_name,agents.photo,";
        query+="familles.name AS famille,agents.private_key AS agent_private_key ";
        query+="FROM stocks INNER jOIN articles ON articles.id=stocks.id_article ";
        query+="INNER JOIN familles ON familles.id=articles.id_famille ";
        query+="INNER JOIN agents ON agents.id=stocks.id_agent ";
        query+="WHERE articles.id="+id+" ";
        query+="ORDER BY stocks.created_at DESC";
        const result=await logiciel.ExecuteQuery(query);
        const devises=await DeviseModel.get();
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                if(element.type==S.ENUM.article.service && element.mode==S.ENUM.stockage.devise){
                    const list=[];
                    list.push(element);
                    const stock=await getTotalStock(list,devises);
                    element.quantity=stock;
                    rep.push(element);
                }else{
                    rep.push(element)
                }
                
            }
        }
    } catch (error) {
        console.log('show stock',error)
    }
    return rep;
}

export async function showLast(){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        let query="SELECT stocks.*,";
        query+="articles.name,articles.description,articles.logo,articles.private_key AS article_private_key,articles.type,articles.mode,";
        query+="agents.first_name,agents.last_name,agents.photo,";
        query+="familles.name AS famille,agents.private_key AS agent_private_key ";
        query+="FROM stocks INNER jOIN articles ON articles.id=stocks.id_article ";
        query+="INNER JOIN familles ON familles.id=articles.id_famille ";
        query+="INNER JOIN agents ON agents.id=stocks.id_agent ";
        query+="ORDER BY stocks.id DESC limit 1";
        const result=await logiciel.ExecuteQuery(query);
        const devises=await DeviseModel.get();
        if(result.rows.item.length>0){
            const element = result.rows.item(0);
            if(element.type==S.ENUM.article.service && element.mode==S.ENUM.stockage.devise){
                const list=[];
                list.push(element);
                const stock=await getTotalStock(list,devises);
                element.quantity=stock;
                rep=element;
            }else{
                rep=element;
            }
        }
    } catch (error) {
        console.log('show stock',error)
    }
    return rep;
}
/**
 * 
 * @param {id of stock} id 
 * @returns 
 */
export async function showStock(id=0){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        let query="SELECT stocks.*,";
        query+="articles.name,articles.description,articles.logo,articles.private_key AS article_private_key,articles.type,articles.mode,";
        query+="agents.first_name,agents.last_name,agents.photo,";
        query+="familles.name AS famille,agents.private_key AS agent_private_key ";
        query+="FROM stocks INNER jOIN articles ON articles.id=stocks.id_article ";
        query+="INNER JOIN familles ON familles.id=articles.id_famille ";
        query+="INNER JOIN agents ON agents.id=stocks.id_agent ";
        query+="WHERE stocks.id="+id+" ";
        query+="ORDER BY stocks.created_at DESC";
        const result=await logiciel.ExecuteQuery(query);
        const devises=await DeviseModel.get();
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                if(element.type==S.ENUM.article.service && element.mode==S.ENUM.stockage.devise){
                    const list=[];
                    list.push(element);
                    const stock=await getTotalStock(list,devises);
                    element.quantity=stock;
                    rep=element;
                }else{
                    rep=element;
                }
                
            }
        }
    } catch (error) {
        console.log('show stock',error)
    }
    return rep;
}

export async function getBetween(date1="",date2=""){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT stocks.*,";
        query+="articles.name,articles.description,articles.logo,articles.private_key AS article_private_key,articles.type,articles.mode,";
        query+="familles.name AS famille,agents.private_key AS agent_private_key ";
        query+="FROM stocks INNER jOIN articles ON articles.id=stocks.id_article ";
        query+="INNER JOIN familles ON familles.id=articles.id_famille ";
        query+="INNER JOIN agents ON agents.id=stocks.id_agent ";
        query+="WHERE created_at between '"+date1+"' AND '"+date2+"' ";
        query+="ORDER BY stocks.created_at DESC";
        const result=await logiciel.ExecuteQuery(query);
        const devises=await DeviseModel.get();
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                if(element.type==S.ENUM.article.service && element.mode==S.ENUM.stockage.devise){
                    const list=[];
                    list.push(element);
                    const stock=await getTotalStock(list,devises);
                    element.quantity=stock;
                    rep.push(element);
                }else{
                    rep.push(element)
                }
                
            }
        }
    } catch (error) {
    }
    return rep;
}



