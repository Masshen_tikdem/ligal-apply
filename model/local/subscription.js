import Logiciel from '../../controller/database/logiciel';
import React from "react";
import CommandValues from '../../controller/commandValues';

const db_name="subscriptions";

export async function store({type='',unity=null,price="",created_at="",validate_at="",transaction=null,status=1,user=null,private_key=null}){
   let rep=false;
    try {
        const cmd=new CommandValues();
        cmd.addValue('created_at',created_at);
        cmd.addValue('type',type);
        cmd.addValue('status',status);
        cmd.addValue('private_key',private_key);
        cmd.addValue('validate_at',validate_at);
        cmd.addValue('transaction',transaction);
        cmd.addValue('id_user',user);
        cmd.addValue('price',price);
        cmd.addValue('unity',unity);
        const query=cmd.getInsertCommand({list:cmd.getValues(),table:db_name});
        if(query!=null){
            const logiciel=new Logiciel();
            const table=await logiciel.ExecuteQuery(query);
            if(table.rowsAffected>0){
                rep=true;
            }
        }
   } catch (error) {
       rep=false;
       console.log('error',error);
   }
   return rep;
}

export async function update({type=null,unity,price=null,created_at=null,validate_at=null,transaction=null,status=null,user=null,private_key=null,id=0}){
    let rep=false;
     try {
         const cmd=new CommandValues();
         cmd.addValue('created_at',created_at);
         cmd.addValue('type',type);
         cmd.addValue('status',status);
         cmd.addValue('private_key',private_key);
         cmd.addValue('validate_at',validate_at);
         cmd.addValue('transaction',transaction);
         cmd.addValue('id_user',user);
         cmd.addValue('price',price);
         cmd.addValue('unity',unity);
         const query=cmd.getUpdateCommand({list:cmd.getValues(),table:db_name,whereClause:"id="+id});
         if(query!=null){
             const logiciel=new Logiciel();
             const table=await logiciel.ExecuteQuery(query);
             if(table.rowsAffected>0){
                 rep=true;
             }
         }
    } catch (error) {
        rep=false;
        console.log('error',error);
    }
    return rep;
 }

 export async function get(id){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT * ";
        query+="FROM subscriptions ";
        if(id>0){
            query+="WHERE id_user="+id+" ";
        }
        query+="ORDER BY created_at DESC";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
    }
    return rep;
}

export async function getByType(id,type){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT * ";
        query+="FROM subscriptions ";
        if(id>0){
            query+="WHERE id_user="+id+" and type='"+type+"' ";
        }
        query+="ORDER BY created_at DESC";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
    }
    return rep;
}

export async function getSubscription(id,type1,type2){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT * ";
        query+="FROM subscriptions ";
        if(id>0){
            query+="WHERE id_user="+id+" and (type='"+type1+"' or type='"+type2+"') ";
        }
        query+="ORDER BY created_at DESC";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
    }
    return rep;
}

 