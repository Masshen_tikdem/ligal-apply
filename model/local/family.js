import Logiciel from '../../controller/database/logiciel';
import React from "react";
import CommandValues from '../../controller/commandValues';

const db_name="familles";

export async function store({name='',logo=null,image_updated=null,type=null,status=0,private_key=null,is_updated=0}){
   let rep=null;
    try {
        const cmd=new CommandValues();
        cmd.addValue('name',name);
        cmd.addValue('logo',logo);
        cmd.addValue('type',type);
        cmd.addValue('status',status);
        cmd.addValue('private_key',private_key);
        cmd.addValue('is_updated',is_updated);
        cmd.addValue('image_updated',image_updated);
        const query=cmd.getInsertCommand({list:cmd.getValues(),table:db_name});
        if(query!=null){
            const logiciel=new Logiciel();
            const table=await logiciel.ExecuteQuery(query);
            if(table.rowsAffected>0){
                rep=await showLast();
            }
        }
   } catch (error) {
       rep=null;
   }
   return rep;
}
export async function storeLink({high_family=0,sub_family=null,status=0,private_key=null,is_updated=0}){
    let rep=null;
     try {
         const cmd=new CommandValues();
         cmd.addValue('high_family',high_family);
         cmd.addValue('sub_family',sub_family);
         cmd.addValue('status',status);
         cmd.addValue('private_key',private_key);
         cmd.addValue('is_updated',is_updated);
         const query=cmd.getInsertCommand({list:cmd.getValues(),table:"links"});
         if(query!=null){
             const logiciel=new Logiciel();
             const table=await logiciel.ExecuteQuery(query);
             if(table.rowsAffected>0){
                 rep=true;
             }
         }
    } catch (error) {
        rep=null;
    }
    return rep;
 }

 export async function updateLink({id=0,high_family=null,sub_family=null,status=null,private_key=null,is_updated=null}){
    let rep=false;
     try {
         const cmd=new CommandValues();
         cmd.addValue('high_family',high_family);
         cmd.addValue('sub_family',sub_family);
         cmd.addValue('status',status);
         cmd.addValue('private_key',private_key);
         cmd.addValue('is_updated',is_updated);
         const query=cmd.getUpdateCommand({list:cmd.getValues(),table:"links",whereClause:"id="+id});
         if(query!=null){
             const logiciel=new Logiciel();
             const table=await logiciel.ExecuteQuery(query);
             if(table.rowsAffected>0){
                 rep=true;
             }
         }
    } catch (error) {
        rep=false;
    }
    return rep;
 }

export async function update({id=0,name=null,logo=null,image_updated=null,type=null,status=null,private_key=null,is_updated=null}){
    let rep=null;
     try {
         const cmd=new CommandValues();
         cmd.addValue('name',name);
         cmd.addValue('logo',logo);
         cmd.addValue('type',type);
         cmd.addValue('status',status);
         cmd.addValue('private_key',private_key);
         cmd.addValue('is_updated',is_updated);
         cmd.addValue('image_updated',image_updated);
         const query=cmd.getUpdateCommand({list:cmd.getValues(),table:db_name,whereClause:"id="+id});
         if(query!=null){
             const logiciel=new Logiciel();
             const table=await logiciel.ExecuteQuery(query);
             if(table.rowsAffected>0){
                 rep=await show(id);
             }
         }
    } catch (error) {
        rep=null;
    }
    return rep;
 }

export async function get(){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT familles.*,";
        query+="category.name as category,category.id as category_id,category.private_key AS category_private_keys,";
        query+="links.private_key AS link_private_key,links.status AS link_status,links.is_updated AS link_is_updated,links.id AS id_link ";
        query+="FROM familles LEFT JOIN links on familles.id=links.sub_family ";
        query+="LEFT JOIN familles AS category ON links.high_family=category.id ";
        query+="ORDER BY familles.name ASC";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
    }
    return rep;
}

export async function getLinks(){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT * from links ";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
    }
    return rep;
}

export async function marks(id){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT familles.*,";
        query+="category.name as category,category.id as category_id,";
        query+="links.private_key AS link_private_key,links.status AS link_status,links.is_updated AS link_is_updated,links.id AS id_link ";
        query+="FROM familles LEFT JOIN links on familles.id=links.sub_family ";
        query+="LEFT JOIN familles AS category ON links.high_family=category.id ";
        query+="WHERE category.id="+id+" ";
        query+="ORDER BY familles.name ASC";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
    }
    return rep;
}


export async function show(id=0){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        let query="SELECT familles.*,";
        query+="category.name as category,category.id as category_id ";
        query+="FROM familles LEFT JOIN links on familles.id=links.sub_family ";
        query+="LEFT JOIN familles AS category ON links.high_family=category.id ";
        query+="WHERE familles.id="+id+" "
        query+="ORDER BY familles.name ASC";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            const element = result.rows.item(0);
            rep=element;
        }
    } catch (error) {
    }
    return rep;
}

export async function showLast(){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        let query="SELECT familles.*,";
        query+="category.name as category,category.id as category_id ";
        query+="FROM familles LEFT JOIN links on familles.id=links.sub_family ";
        query+="LEFT JOIN familles AS category ON links.high_family=category.id ";
        //query+="WHERE familles.id="+id+" "
        query+="ORDER BY familles.id DESC limit 1";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            const element = result.rows.item(0);
            rep=element;
        }
    } catch (error) {
    }
    return rep;
}

export async function last(){
    let rep={};
    try {
        const logiciel=new Logiciel();
        let query="SELECT familles.*,";
        query+="category.name as category,category.id as category_id ";
        query+="FROM familles LEFT JOIN links on familles.id=links.sub_family ";
        query+="LEFT JOIN familles AS category ON links.high_family=category.id ";
        query+="ORDER BY familles.id DESC limit 1";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            const element = result.rows.item(0);
            rep=element;
        }
    } catch (error) {
    }
    return rep;
}



