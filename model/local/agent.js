import Logiciel from '../../controller/database/logiciel';
import React from "react";
import CommandValues from '../../controller/commandValues';

const db_name="agents";

export async function store({id=null,code=null,last_name='',first_name='',address=null,photo=null,phone="",email=null,password=null,sexe="",status=0,private_key=null,is_updated=0,image_updated=0}){
   let rep=false;
    try {
        const cmd=new CommandValues();
        cmd.addValue('last_name',last_name);
        cmd.addValue('first_name',first_name);
        cmd.addValue('address',address);
        cmd.addValue('photo',photo);
        cmd.addValue('private_key',private_key);
        cmd.addValue('is_updated',is_updated);
        cmd.addValue('phone',phone);
        cmd.addValue('email',email);
        cmd.addValue('password',password);
        cmd.addValue('sex',sexe);
        cmd.addValue('status',status);
        cmd.addValue('image_updated',image_updated);
        cmd.addValue('code',code);
        const query=cmd.getInsertCommand({list:cmd.getValues(),table:db_name});
        if(query!=null){
            const logiciel=new Logiciel();
            const table=await logiciel.ExecuteQuery(query);
            if(table.rowsAffected>0){
                rep=true;
            }
        }
   } catch (error) {
       rep=false;
       console.log('henock error',error);
   }
   return rep;
}

export async function update({id=0,code=null,last_name=null,first_name=null,address=null,photo=null,phone=null,email=null,password=null,sexe=null,status=null,private_key=null,is_updated=null,image_updated=null}){
    let rep=null;
     try {
         const cmd=new CommandValues();
         cmd.addValue('last_name',last_name);
         cmd.addValue('first_name',first_name);
         cmd.addValue('address',address);
         cmd.addValue('photo',photo);
         cmd.addValue('private_key',private_key);
         cmd.addValue('is_updated',is_updated);
         cmd.addValue('phone',phone);
         cmd.addValue('email',email);
         cmd.addValue('password',password);
         cmd.addValue('sex',sexe);
         cmd.addValue('status',status);
         cmd.addValue('image_updated',image_updated);
         cmd.addValue('code',code);
         const query=cmd.getUpdateCommand({list:cmd.getValues(),table:db_name,whereClause:"id="+id});
         if(query!=null){
             const logiciel=new Logiciel();
             const table=await logiciel.ExecuteQuery(query);
             if(table.rowsAffected>0){
                 rep=await show(id);
             }
         }
    } catch (error) {
        rep=null;
    }
    return rep;
 }

export async function storeWork({created_at="",identity=null,updated_at='',finished_at=null,poste=null,id_agent="",id_shop=null,status=0,private_key=null,is_updated=0}){
    let rep=false;
     try {
         const cmd=new CommandValues();
         cmd.addValue('created_at',created_at);
         cmd.addValue('updated_at',updated_at);
         cmd.addValue('finished_at',finished_at);
         cmd.addValue('poste',poste);
         cmd.addValue('private_key',private_key);
         cmd.addValue('is_updated',is_updated);
         cmd.addValue('id_agent',id_agent);
         cmd.addValue('id_shop',id_shop);
         cmd.addValue('status',status);
         cmd.addValue('identity',identity);
         const query=cmd.getInsertCommand({list:cmd.getValues(),table:"works"});
         if(query!=null){
             const logiciel=new Logiciel();
             const table=await logiciel.ExecuteQuery(query);
             if(table.rowsAffected>0){
                 rep=true;
             }
         }
    } catch (error) {
        rep=false;
    }
    return rep;
 }

 export async function updateWork({id=0,identity=null,created_at=null,updated_at=null,finished_at=null,poste=null,status=null,private_key=null,is_updated=null}){
    let rep=false;
     try {
         const cmd=new CommandValues();
         cmd.addValue('created_at',created_at);
         cmd.addValue('updated_at',updated_at);
         cmd.addValue('finished_at',finished_at);
         cmd.addValue('poste',poste);
         cmd.addValue('private_key',private_key);
         cmd.addValue('is_updated',is_updated);
         cmd.addValue('status',status);
         cmd.addValue('identity',identity);
         const query=cmd.getUpdateCommand({list:cmd.getValues(),table:"works",whereClause:"id="+id});
         if(query!=null){
             const logiciel=new Logiciel();
             const table=await logiciel.ExecuteQuery(query);
             if(table.rowsAffected>0){
                 rep=true;
             }
         }
    } catch (error) {
        rep=false;
    }
    return rep;
 }


 export async function colleagues(id){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT agents.*,works.private_key as work,works.status AS work_status FROM agents ";
        query+="LEFT JOIN works on agents.id=works.id_agent ";
        if(id>0){
            query+="WHERE works.id_agent!="+id+" ";
        }
        query+="ORDER BY agents.first_name,agents.last_name ";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
        console.log('error coll',error)
    }
    return rep;
}

export async function get(){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT agents.*,";
        query+="works.created_at as work_created_at,works.updated_at as work_updated_at,";
        query+="works.finished_at as work_finished_at,works.is_updated as work_is_updated,";
        query+="works.status as work_status,works.poste as work_poste,works.id as work_id,";
        query+="works.private_key as work_private_key,works.id_shop as work_id_shop ";
        query+="FROM agents LEFT JOIN works on agents.id=works.id_agent ";
        query+="ORDER BY agents.first_name,agents.last_name";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
    }
    return rep;
}
export async function last(){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        let query="SELECT agents.*,";
        query+="works.created_at as work_created_at,works.updated_at as work_updated_at,";
        query+="works.finished_at as work_finished_at,works.is_updated as work_is_updated,";
        query+="works.status as work_status,works.poste as work_poste,works.id as work_id,";
        query+="works.private_key as work_private_key,works.id_shop as work_id_shop ";
        query+="FROM agents LEFT JOIN works on agents.id=works.id_agent ";
        query+="ORDER BY agents.id DESC limit 1";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.length>0){
            const element = result.rows.item(0);
            rep=element;
        }
    } catch (error) {
    }
    return rep;
}

export async function lastId(){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        let query="SELECT id ";
        query+="FROM agents ";
        query+="ORDER BY id DESC limit 1";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.length>0){
            const element = result.rows.item(0);
            rep=element;
        }
    } catch (error) {
    }
    return rep;
}

export async function getUserByKey(key=""){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        let query="SELECT * ";
        query+="FROM agents ";
        query+="WHERE private_key='"+key+"'";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.length>0){
            const element = result.rows.item(0);
            rep=element;
        }
    } catch (error) {
    }
    return rep;
}

export async function show(id=0){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        let query="SELECT agents.*,";
        query+="works.created_at as work_created_at,works.updated_at as work_updated_at,";
        query+="works.finished_at as work_finished_at,works.is_updated as work_is_updated,";
        query+="works.status as work_status,works.poste as work_poste,works.id as work_id,";
        query+="works.private_key as work_private_key,works.id_shop as work_id_shop ";
        query+="FROM agents LEFT JOIN works on agents.id=works.id_agent ";
        query+="WHERE agents.id="+id;
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.length>0){
            const element = result.rows.item(0);
            rep=element;
        }
    } catch (error) {
    }
    return rep;
}

export async function getStock(id=0){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        let query="SELECT sum(stocks.quantity) as sum_quantity ";
        query+="FROM agents INNER JOIN stocks ON agents.id=stocks.id_agent ";
        query+="WHERE agents.id="+id+" ";
        query+="GROUP BY agents.id ";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.length>0){
            const element = result.rows.item(0);
            rep=element;
        }
    } catch (error) {
    }
    return rep;
}
export async function getBuy(id=0){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        let query="SELECT count(buys.id_customer) as count_customer,sum(buys.quantity) as sum_quantity ";
        query+="FROM agents INNER JOIN buys ON agents.id=buys.id_agent ";
        query+="WHERE agents.id="+id+" ";
        query+="GROUP BY buys.id ";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.length>0){
            const element = result.rows.item(0);
            rep=element;
        }
    } catch (error) {
    }
    return rep;
}

export async function getCustomer(id=0){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        let query="SELECT count(buys.id_customer) as count_customer ";
        query+="FROM agents INNER JOIN buys ON agents.id=buys.id_agent ";
        query+="WHERE buys.id_customer>0 and agents.id="+id+" ";
        query+="GROUP BY buys.id ";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.length>0){
            const element = result.rows.item(0);
            rep=element;
        }
    } catch (error) {
        
    }
    return rep;
}



