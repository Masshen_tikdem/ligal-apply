import Logiciel from '../../controller/database/logiciel';
import React from "react";
import CommandValues from '../../controller/commandValues';
import * as TargetModel from './target';

const db_name="activities";

export async function store({description='',status="",type="",created_at="",recall_date=null,quantity=null,price=null,link=null,private_key=null,is_updated=0,devise=0,user=0}){
   let rep=false;
    try {
        const cmd=new CommandValues();
        cmd.addValue('description',description);
        cmd.addValue('created_at',created_at);
        cmd.addValue('type',type);
        cmd.addValue('status',status);
        cmd.addValue('private_key',private_key);
        cmd.addValue('is_updated',is_updated);
        cmd.addValue('recall_date',recall_date);
        cmd.addValue('quantity',quantity);
        cmd.addValue('id_agent',user);
        cmd.addValue('price',price);
        cmd.addValue('link',link);
        cmd.addValue('id_devise',devise);
        const query=cmd.getInsertCommand({list:cmd.getValues(),table:db_name});
        if(query!=null){
            const logiciel=new Logiciel();
            const table=await logiciel.ExecuteQuery(query);
            if(table.rowsAffected>0){
                rep=true;
            }
        }
   } catch (error) {
       rep=false;
       console.log('error',error);
   }
   return rep;
}

export async function update({description=null,status=null,type=null,created_at=null,recall_date=null,quantity=null,price=null,link=null,private_key=null,is_updated=null,devise=null,user=null,id=0}){
    let rep=false;
     try {
         const cmd=new CommandValues();
         cmd.addValue('description',description);
         cmd.addValue('created_at',created_at);
         cmd.addValue('type',type);
         cmd.addValue('status',status);
         cmd.addValue('private_key',private_key);
         cmd.addValue('is_updated',is_updated);
         cmd.addValue('recall_date',recall_date);
         cmd.addValue('quantity',quantity);
         cmd.addValue('price',price);
         cmd.addValue('link',link);
         cmd.addValue('id_devise',devise);
         const query=cmd.getUpdateCommand({list:cmd.getValues(),table:db_name,whereClause:'id='+id});
         if(query!=null){
             const logiciel=new Logiciel();
             const table=await logiciel.ExecuteQuery(query);
             if(table.rowsAffected>0){
                 rep=true;
             }
         }
    } catch (error) {
        rep=false;
    }
    return rep;
 }

 export async function get(id){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT activities.*,COUNT(targets.id) as size ";
        query+="FROM activities LEFT JOIN targets ON activities.id=targets.id_activity ";
        if(id>0){
            query+="WHERE activities.id_agent="+id+" ";
        }
        query+="GROUP BY activities.id ";
        query+="ORDER BY activities.created_at DESC";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                const target=await TargetModel.get(element.id);
                element.targets=target;
                rep.push(element)
            }
        }
    } catch (error) {
    }
    return rep;
}

export async function getByKey(key=""){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        let query="SELECT * ";
        query+="FROM activities WHERE private_key='"+key+"'";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.length>0){
            const element = result.rows.item(0);
            rep=element;
        }
    } catch (error) {
        console.log('error key',error);
    }
    return rep;
}

export async function getTypes(){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT activities.*,COUNT(targets.type) as size ";
        query+="GROUP BY activities.type ";
        query+="ORDER BY size DESC";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
    }
    return rep;
}

export async function show(id=0){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        let query="SELECT activities.*,COUNT(targets.id) as size ";
        query+="FROM activities LEFT JOIN targets ON activities.id=targets.id_activity ";
        query+="WHERE activities.id="+id+" ";
        query+="GROUP BY activities.id ";
        query+="ORDER BY activities.created_at DESC";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            rep=result.rows.item(0)
        }
    } catch (error) {
    }
    return rep;
}