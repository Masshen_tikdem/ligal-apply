import Logiciel from '../../controller/database/logiciel';
import React from "react";
import CommandValues from '../../controller/commandValues';
import { getLink } from '../../Manager';

const db_name="notifications";

export async function store({status=null,type='',title="",content="",link="",created_at="",table="",id_item=0}){
   let rep=false;
   const item=getLink(id_item,table);
    try {
        const cmd=new CommandValues();
        cmd.addValue('type',type);
        cmd.addValue('title',title);
        cmd.addValue('content',content);
        cmd.addValue('link',item);
        cmd.addValue('created_at',created_at);
        cmd.addValue('status',status);
        const query=cmd.getInsertCommand({list:cmd.getValues(),table:db_name});
        if(query!=null){
            const logiciel=new Logiciel();
            const table=await logiciel.ExecuteQuery(query);
            if(table.rowsAffected>0){
                rep=true;
            }
        }
   } catch (error) {
       rep=false;
   }
   return rep;
}

export async function update({id=0,id_item=null,status=null,type=null,title=null,content=null,link=null,created_at=null,table=null}){
    let rep=false;
    const item=getLink(id_item,table)
     try {
         const cmd=new CommandValues();
         cmd.addValue('type',type);
         cmd.addValue('title',title);
         cmd.addValue('content',content);
         cmd.addValue('link',item);
         cmd.addValue('created_at',created_at);
         cmd.addValue('status',status);
         const query=cmd.getUpdateCommand({list:cmd.getValues(),table:db_name,whereClause:"id="+id});
         console.log('update',query);
         if(query!=null){
             const logiciel=new Logiciel();
             const table=await logiciel.ExecuteQuery(query);
             if(table.rowsAffected>0){
                 rep=true;
             }
         }
    } catch (error) {
        rep=false;
        console.log('update error',error);
    }
    return rep;
 }



export async function get(){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT * FROM notifications ORDER BY created_at DESC";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
    }
    return rep;
}

export async function show(id=0,table=""){
    let rep=[];
    try {
        const link=getLink(id,table);
        const logiciel=new Logiciel();
        let query="SELECT * FROM notifications ";
        query+="WHERE link='"+link+"' ";
        query+="ORDER BY created_at DESC";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
        console.log('ee',error)
    }
    return rep;
}

export async function content(table=""){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT * FROM notifications ";
        query+="WHERE link like '%"+table+"%' ";
        query+="ORDER BY created_at DESC ";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
    }
    return rep;
}



