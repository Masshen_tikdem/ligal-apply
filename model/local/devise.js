import Logiciel from '../../controller/database/logiciel';
import React from "react";
import CommandValues from '../../controller/commandValues';

const db_name="devises";

export async function store({name='',logo=null,image_updated=null,type=0,status=0,private_key=null,is_updated=0,signe=''}){
   let rep=null;
    try {
        const cmd=new CommandValues();
        cmd.addValue('name',name);
        cmd.addValue('logo',logo);
        cmd.addValue('type',type);
        cmd.addValue('status',status);
        cmd.addValue('private_key',private_key);
        cmd.addValue('is_updated',is_updated);
        cmd.addValue('signe',signe);
        cmd.addValue('image_updated',image_updated);
        const query=cmd.getInsertCommand({list:cmd.getValues(),table:db_name});
        if(query!=null){
            const logiciel=new Logiciel();
            const table=await logiciel.ExecuteQuery(query);
            if(table.rowsAffected>0){
                rep=await showLast();
            }
        }
   } catch (error) {
       rep=null;
   }
   return rep;
}

export async function update({id=0,name=null,image_updated=null,logo=null,type=null,status=null,private_key=null,is_updated=null,signe=null}){
    let rep=null;
     try {
         const cmd=new CommandValues();
         cmd.addValue('name',name);
         cmd.addValue('logo',logo);
         cmd.addValue('type',type);
         cmd.addValue('status',status);
         cmd.addValue('private_key',private_key);
         cmd.addValue('is_updated',is_updated);
         cmd.addValue('signe',signe);
         cmd.addValue('image_updated',image_updated);
         const query=cmd.getUpdateCommand({list:cmd.getValues(),table:db_name,whereClause:"id="+id});
         if(query!=null){
             const logiciel=new Logiciel();
             const table=await logiciel.ExecuteQuery(query);
             if(table.rowsAffected>0){
                 rep=await showLast();
             }
         }
    } catch (error) {
        rep=null;
    }
    return rep;
 }

export async function get(){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT * FROM devises ORDER BY type DESC,name ASC";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
    }
    return rep;
}

export async function showLast(){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        let query="SELECT * FROM devises ORDER BY id DESC limit 1";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            const element = result.rows.item(0);
            rep=element;
        }
    } catch (error) {
    }
    return rep;
}



