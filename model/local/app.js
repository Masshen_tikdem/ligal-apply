import Logiciel from '../../controller/database/logiciel';
import React from "react";
import CommandValues from '../../controller/commandValues';

const db_name="app";

export async function store({version=0}){
   let rep=false;
    try {
        const cmd=new CommandValues();
        cmd.addValue('version',version);
        const query=cmd.getInsertCommand({list:cmd.getValues(),table:db_name});
        if(query!=null){
            const logiciel=new Logiciel();
            const table=await logiciel.ExecuteQuery(query);
            if(table.rowsAffected>0){
                rep=true;
            }
        }
   } catch (error) {
       rep=false;
   }
   return rep;
}

export async function update({id=0,version=0}){
    let rep=false;
     try {
         const cmd=new CommandValues();
         cmd.addValue('version',version);
         const query=cmd.getUpdateCommand({list:cmd.getValues(),table:db_name,whereClause:"id="+id});
         if(query!=null){
             const logiciel=new Logiciel();
             const table=await logiciel.ExecuteQuery(query);
             if(table.rowsAffected>0){
                 rep=true;
             }
         }
    } catch (error) {
        rep=false;
    }
    return rep;
 }

 export async function get(){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        const query="select * from app limit 1;"
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.length>0){
            rep=result.rows.item(0);
        }
    } catch (error) {
       
    }
    return rep;
}