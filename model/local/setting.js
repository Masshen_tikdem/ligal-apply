import Logiciel from '../../controller/database/logiciel';
import React from "react";
import CommandValues from '../../controller/commandValues';

const db_name="settings";

export async function store({code='',value=0,id_shop,is_updated=0,private_key=null}){
   let rep=false;
    try {
        const cmd=new CommandValues();
        cmd.addValue('code',code);
        cmd.addValue('value',value);
        cmd.addValue('id_shop',id_shop);
        cmd.addValue('is_updated',is_updated);
        cmd.addValue('private_key',private_key);
        const query=cmd.getInsertCommand({list:cmd.getValues(),table:db_name});
        if(query!=null){
            const logiciel=new Logiciel();
            const table=await logiciel.ExecuteQuery(query);
            if(table.rowsAffected>0){
                rep=true;
            }
        }
   } catch (error) {
       rep=false;
   }
   return rep;
}

export async function update({id=0,code=null,value=null,id_shop=null,is_updated=1,private_key=null}){
    let rep=false;
     try {
         const cmd=new CommandValues();
         cmd.addValue('code',code);
         cmd.addValue('value',value);
         cmd.addValue('id_shop',id_shop);
         cmd.addValue('is_updated',is_updated);
         cmd.addValue('private_key',private_key);
         const query=cmd.getUpdateCommand({list:cmd.getValues(),table:db_name,whereClause:'id='+id});
         console.log('update query',query);
         if(query!=null){
             const logiciel=new Logiciel();
             const table=await logiciel.ExecuteQuery(query);
             if(table.rowsAffected>0){
                 rep=true;
             }
         }
    } catch (error) {
        rep=false;
    }
    return rep;
 }

export async function get(){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT * ";
        query+="FROM settings ";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
    }
    return rep;
}

