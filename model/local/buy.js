import Logiciel from '../../controller/database/logiciel';
import CommandValues from '../../controller/commandValues';
import DateTime from 'date-and-time';

const db_name="buys";

export async function store({created_at='',description=null,facture=null,quantity=0,id_agent=0,id_client=0,price=0,id_article=0,id_devise=0,remise=null,status=0,private_key=null,reference=null,is_alter=false}){
   let rep=null;
    try {
        const cmd=new CommandValues();
        cmd.addValue('created_at',created_at);
        cmd.addValue('quantity',quantity);
        cmd.addValue('id_agent',id_agent);
        cmd.addValue('id_customer',id_client);
        cmd.addValue('private_key',private_key);
        cmd.addValue('price',price);
        cmd.addValue('id_article',id_article);
        cmd.addValue('id_devise',id_devise);
        cmd.addValue('remise',remise);
        cmd.addValue('status',status);
        cmd.addValue('reference',reference);
        cmd.addValue('description',description);
        cmd.addValue('bill',facture);
        cmd.addValue('is_alter',is_alter==true?1:0);
        const query=cmd.getInsertCommand({list:cmd.getValues(),table:db_name});
        if(query!=null){
            const logiciel=new Logiciel();
            const table=await logiciel.ExecuteQuery(query);
            if(table.rowsAffected>0){
                rep=await showLast();
            }
        }
   } catch (error) {
       rep=null;
   }
   return rep;
}

export async function update({id=0,created_at=null,description=null,facture=null,quantity=null,id_agent=null,id_client=null,price=null,id_article=null,id_devise=null,remise=null,status=null,private_key=null,reference=null,is_alter=null}){
    let rep=null;
     try {
         const cmd=new CommandValues();
         cmd.addValue('created_at',created_at);
         cmd.addValue('quantity',quantity);
         cmd.addValue('id_agent',id_agent);
         cmd.addValue('id_customer',id_client);
         cmd.addValue('private_key',private_key);
         cmd.addValue('price',price);
         cmd.addValue('id_article',id_article);
         cmd.addValue('id_devise',id_devise);
         cmd.addValue('remise',remise);
         cmd.addValue('status',status);
         cmd.addValue('reference',reference);
         cmd.addValue('is_alter',is_alter!=null?is_alter==true?1:0:null);
         const query=cmd.getUpdateCommand({list:cmd.getValues(),table:db_name,whereClause:"id="+id});
         if(query!=null){
             const logiciel=new Logiciel();
             const table=await logiciel.ExecuteQuery(query);
             if(table.rowsAffected>0){
                 rep=showVerify(id);
             }
         }
    } catch (error) {
        rep=null;
    }
    return rep;
 }

export async function get(){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT buys.*,";
        query+="articles.name,articles.type as article_type,articles.description,articles.private_key AS article_private_key,articles.type,articles.mode,";
        query+="devises.signe,devises.private_key  AS devise_private_key,";
        query+="agents.private_key  AS agent_private_key,customers.private_key AS customer_private_key ";
        query+="FROM buys INNER jOIN articles ON articles.id=buys.id_article ";
        query+="INNER JOIN devises ON devises.id=buys.id_devise ";
        query+="INNER JOIN agents ON agents.id=buys.id_agent ";
        query+="LEFT JOIN customers ON customers.id=buys.id_customer ";
        query+="ORDER BY buys.created_at DESC";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
    }
    return rep;
}

export async function getByBill(bill){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT buys.*,";
        query+="articles.name,articles.type as article_type,articles.description,articles.private_key AS article_private_key,articles.type,articles.mode,";
        query+="devises.signe,devises.private_key  AS devise_private_key,";
        query+="agents.private_key  AS agent_private_key,customers.private_key AS customer_private_key ";
        query+="FROM buys INNER jOIN articles ON articles.id=buys.id_article ";
        query+="INNER JOIN devises ON devises.id=buys.id_devise ";
        query+="INNER JOIN agents ON agents.id=buys.id_agent ";
        query+="LEFT JOIN customers ON customers.id=buys.id_customer ";
        query+="WHERE buys.bill='"+bill+"' ";
        query+="ORDER BY buys.created_at DESC";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
    }
    return rep;
}

/**
 * 
 * @param {*article to show} id 
 */
export async function show(id=0){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT buys.*,";
        query+="articles.name,articles.description,articles.logo,articles.type,articles.mode,";
        query+="familles.name AS famille,";
        query+="devises.signe ";
        query+="FROM buys INNER jOIN articles ON articles.id=buys.id_article ";
        query+="INNER JOIN devises ON devises.id=buys.id_devise ";
        query+="INNER JOIN familles ON familles.id=articles.id_famille ";
        query+="WHERE articles.id="+id+" ";
        query+="ORDER BY buys.created_at DESC";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
    }
    return rep;
}

export async function showLast(){
    let rep=null;
    try {
        const logiciel=new Logiciel();

        let query="SELECT buys.*,";
        query+="articles.name,articles.type as article_type,articles.description,articles.private_key AS article_private_key,articles.type,articles.mode,";
        query+="devises.signe,devises.private_key  AS devise_private_key,";
        query+="agents.private_key  AS agent_private_key,customers.private_key AS customer_private_key ";
        query+="FROM buys INNER jOIN articles ON articles.id=buys.id_article ";
        query+="INNER JOIN devises ON devises.id=buys.id_devise ";
        query+="INNER JOIN agents ON agents.id=buys.id_agent ";
        query+="LEFT JOIN customers ON customers.id=buys.id_customer ";
        query+="ORDER BY buys.id DESC limit 1";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            const element = result.rows.item(0);
            rep=element
        }
    } catch (error) {
    }
    return rep;
}

export async function showVerify(id=0){
    let rep=null;
    try {
        const logiciel=new Logiciel();

        let query="SELECT buys.*,";
        query+="articles.name,articles.type as article_type,articles.description,articles.private_key AS article_private_key,articles.type,articles.mode,";
        query+="devises.signe,devises.private_key  AS devise_private_key,";
        query+="agents.private_key  AS agent_private_key,customers.private_key AS customer_private_key ";
        query+="FROM buys INNER jOIN articles ON articles.id=buys.id_article ";
        query+="INNER JOIN devises ON devises.id=buys.id_devise ";
        query+="INNER JOIN agents ON agents.id=buys.id_agent ";
        query+="LEFT JOIN customers ON customers.id=buys.id_customer ";
        query+="WHERE buys.id="+id;
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            const element = result.rows.item(0);
            rep=element
        }
    } catch (error) {
    }
    return rep;
}
/**
 * 
 * @param {show a buy} id 
 */
export async function showBuy(id=0){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        let query="SELECT buys.*,";
        query+="articles.name,articles.description,articles.logo,articles.type,articles.mode,";
        query+="familles.name AS famille,";
        query+="agents.first_name,agents.last_name,agents.sex,agents.photo,";
        query+="customers.name AS customer_name,customers.phone AS customer_phone,customers.private_key AS customer_private_key,";
        query+="devises.signe ";
        query+="FROM buys INNER jOIN articles ON articles.id=buys.id_article ";
        query+="INNER JOIN devises ON devises.id=buys.id_devise ";
        query+="INNER JOIN familles ON familles.id=articles.id_famille ";
        query+="INNER JOIN agents ON agents.id=buys.id_agent ";
        query+="LEFT JOIN customers ON customers.id=buys.id_customer ";
        query+="WHERE buys.id="+id+" ";
        query+="ORDER BY buys.created_at DESC";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.length>0){
            const element = result.rows.item(0);
            rep=element;
        }
    } catch (error) {
    }
    return rep;
}

export async function getBetween(date1="",date2=""){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT buys.*,";
        query+="articles.name,articles.description,articles.logo,articles.type,articles.mode,";
        query+="devises.signe ";
        query+="FROM buys INNER jOIN articles ON articles.id=buys.id_article ";
        query+="INNER JOIN devises ON devises.id=buys.id_devise ";
        query+="WHERE created_at between '"+date1+"' AND '"+date2+"' ";
        query+="ORDER BY buys.created_at DESC ";
        
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
    }
    return rep;
}



