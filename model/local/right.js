import Logiciel from '../../controller/database/logiciel';
import React from "react";
import CommandValues from '../../controller/commandValues';

const db_name="rights";

export async function store({code='',value=0,id_agent=0,is_updated=0,private_key=null}){
   let rep=null;
    try {
        const cmd=new CommandValues();
        cmd.addValue('code',code);
        cmd.addValue('value',value);
        cmd.addValue('id_agent',id_agent);
        cmd.addValue('is_updated',is_updated);
        cmd.addValue('private_key',private_key);
        const query=cmd.getInsertCommand({list:cmd.getValues(),table:db_name});
        if(query!=null){
            const logiciel=new Logiciel();
            const table=await logiciel.ExecuteQuery(query);
            if(table.rowsAffected>0){
                rep=await showLast();
            }
        }
   } catch (error) {
       rep=null;
   }
   return rep;
}

export async function update({id=0,code=null,value=null,id_agent=null,is_updated=1,private_key=null}){
    let rep=false;
     try {
         const cmd=new CommandValues();
         cmd.addValue('code',code);
         cmd.addValue('value',value);
         cmd.addValue('id_agent',id_agent);
         cmd.addValue('is_updated',is_updated);
         cmd.addValue('private_key',private_key);
         const query=cmd.getUpdateCommand({list:cmd.getValues(),table:db_name,whereClause:'id='+id});
         console.log('update query',query);
         if(query!=null){
             const logiciel=new Logiciel();
             const table=await logiciel.ExecuteQuery(query);
             if(table.rowsAffected>0){
                 rep=true;
             }
         }
    } catch (error) {
        rep=false;
    }
    return rep;
 }

export async function get(id=0){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT rights.*,agents.private_key as agent_private_key ";
        query+="FROM rights INNER JOIN agents on rights.id_agent=agents.id ";
        if(id>0){
            query+="WHERE id_agent="+id;
        }
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
    }
    return rep;
}

export async function showLast(){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        let query="SELECT rights.*,agents.private_key as agent_private_key ";
        query+="FROM rights INNER JOIN agents on rights.id_agent=agents.id ";
        query+="ORDER BY rights.id DESC limit 1";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            const element = result.rows.item(0);
            rep=element;
        }
    } catch (error) {
    }
    return rep;
}

