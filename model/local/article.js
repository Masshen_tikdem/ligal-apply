import Logiciel from '../../controller/database/logiciel';
import React from "react";
import CommandValues from '../../controller/commandValues';
import * as DeviseModel from './devise';
import S from '../../resources/settings.json';
import { getTotalBuy, getTotalStock } from '../../Manager';

const db_name="articles";

export async function store({name='',mode=null,stockage=null,id_devise=null,logo=null,image_updated=null,type=null,status=0,private_key=null,is_updated=0,description=null,price=0,famille=0,devise=0,quantifiable=0,shop=0}){
   let rep=null;
    try {
        const cmd=new CommandValues();
        cmd.addValue('name',name);
        cmd.addValue('logo',logo);
        cmd.addValue('type',type);
        cmd.addValue('status',status);
        cmd.addValue('private_key',private_key);
        cmd.addValue('description',description);
        cmd.addValue('price',price);
        cmd.addValue('id_famille',famille);
        cmd.addValue('id_devise',devise);
        cmd.addValue('quantifiable',quantifiable);
        cmd.addValue('id_shop',shop);
        cmd.addValue('is_updated',is_updated);
        cmd.addValue('image_updated',image_updated);
        cmd.addValue('id_devise',id_devise);
        cmd.addValue('mode',mode);
        cmd.addValue('stockage',stockage);
        const query=cmd.getInsertCommand({list:cmd.getValues(),table:db_name});
        if(query!=null){
            const logiciel=new Logiciel();
            const table=await logiciel.ExecuteQuery(query);
            if(table.rowsAffected>0){
                rep=await showLast();
            }
        }
   } catch (error) {
       console.log('error article',error);
       rep=null;
   }
   return rep;
}

export async function update({id=0,name=null,mode=null,stockage=null,id_devise=null,image_updated=null,logo=null,type=null,status=null,private_key=null,is_updated=null,description=null,price=null,famille=null,devise=null,quantifiable=null,shop=null}){
    let rep=null;
     try {
         const cmd=new CommandValues();
         cmd.addValue('name',name);
         cmd.addValue('logo',logo);
         cmd.addValue('type',type);
         cmd.addValue('status',status);
         cmd.addValue('private_key',private_key);
         cmd.addValue('description',description);
         cmd.addValue('price',price);
         cmd.addValue('id_famille',famille);
         cmd.addValue('id_devise',devise);
         cmd.addValue('quantifiable',quantifiable);
         cmd.addValue('id_shop',shop);
         cmd.addValue('is_updated',is_updated);
         cmd.addValue('image_updated',image_updated);
         cmd.addValue('id_devise',id_devise);
        cmd.addValue('mode',mode);
        cmd.addValue('stockage',stockage);
         const query=cmd.getUpdateCommand({list:cmd.getValues(),table:db_name,whereClause:"id="+id});
         if(query!=null){
             const logiciel=new Logiciel();
             const table=await logiciel.ExecuteQuery(query);
             if(table.rowsAffected>0){
                 rep=await show(id);
             }
         }
    } catch (error) {
        rep=null;
    }
    return rep;
 }
/**
 * get the articles by famille
 * @param {*} id 
 */
export async function get(id){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT articles.*,";
        //query+="SUM(stocks.quantity) AS stock,";
        query+="familles.name AS famille,devises.signe AS devise,familles.private_key AS famille_private_key,devises.private_key AS devise_private_key ";
        query+="FROM articles INNER JOIN familles ON articles.id_famille=familles.id ";
        query+="INNER JOIN devises ON articles.id_devise=devises.id ";
        //query+="LEFT JOIN stocks ON articles.id=stocks.id_article ";
        if(id>0 && id!=undefined && id!=null){
            query+="WHERE articles.id_famille="+id+" ";
        }
        query+="GROUP BY articles.id ";
        query+="ORDER BY articles.name ASC ";
        const result=await logiciel.ExecuteQuery(query);
        const devises=await DeviseModel.get();
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                const id=element.id;
                if(element.type!=S.ENUM.article.service || (element.type==S.ENUM.article.service && element.mode!=S.ENUM.stockage.devise)){
                    const p=await logiciel.ExecuteQuery(`select sum(quantity) as buy from buys where id_article=${id} group by id_article`);
                    const q=await logiciel.ExecuteQuery(`select sum(quantity) as stock from stocks where id_article=${id} group by id_article`);
                    let row=p.rows.item(0);
                    const buy=(row!=undefined && row!=null)?(row.buy==null || row==undefined)?null:row.buy:null;
                    row=q.rows.item(0);
                    const stock=(row!=undefined && row!=null)?(row.stock==null || row==undefined)?null:row.stock:null;
                    element.buy=buy;
                    element.stock=stock;
                    rep.push(element)
                }else{
                    let list=[];
                    const p=await logiciel.ExecuteQuery(`select * from buys where id_article=${id}`);
                    if(p.rows.length>0){
                        for (let i = 0; i < p.rows.length; i++) {
                            list.push(p.rows.item(i));
                        }
                    }
                    const buy=await getTotalBuy(list,devises);
                    const q=await logiciel.ExecuteQuery(`select * from stocks where id_article=${id}`);
                    list=[];
                    if(q.rows.length>0){
                        for (let i = 0; i < q.rows.length; i++) {
                            list.push(q.rows.item(i));
                        }
                    }
                    const stock=await getTotalStock(list,devises);
                    
                    element.buy=buy;
                    element.stock=stock;
                    rep.push(element)

                }
                
            }
        }
    } catch (error) {
        console.log('error',error);
    }
    return rep;

}


export async function show(id){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        let query="SELECT articles.*,";
        query+="familles.name AS famille,devises.signe AS devise,familles.private_key AS famille_private_key,devises.private_key AS devise_private_key ";
        query+="FROM articles INNER JOIN familles ON articles.id_famille=familles.id ";
        query+="INNER JOIN devises ON articles.id_devise=devises.id ";
        query+="WHERE articles.id="+id+" ";
        query+="GROUP BY articles.id ";
        query+="ORDER BY articles.name ASC ";
        const result=await logiciel.ExecuteQuery(query);
        const devises=await DeviseModel.get();
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                const id=element.id;
                if(element.type!=S.ENUM.article.service || (element.type==S.ENUM.article.service && element.mode!=S.ENUM.stockage.devise)){
                    const p=await logiciel.ExecuteQuery(`select sum(quantity) as buy from buys where id_article=${id} group by id_article`);
                    const q=await logiciel.ExecuteQuery(`select sum(quantity) as stock from stocks where id_article=${id} group by id_article`);
                    let row=p.rows.item(0);
                    const buy=(row!=undefined && row!=null)?(row.buy==null || row==undefined)?null:row.buy:null;
                    row=q.rows.item(0);
                    const stock=(row!=undefined && row!=null)?(row.stock==null || row==undefined)?null:row.stock:null;
                    element.buy=buy;
                    element.stock=stock;
                    rep=element;
                }else{
                    let list=[];
                    const p=await logiciel.ExecuteQuery(`select * from buys where id_article=${id}`);
                    if(p.rows.length>0){
                        for (let i = 0; i < p.rows.length; i++) {
                            list.push(p.rows.item(i));
                        }
                    }
                    const buy=await getTotalBuy(list,devises);
                    const q=await logiciel.ExecuteQuery(`select * from stocks where id_article=${id}`);
                    list=[];
                    if(q.rows.length>0){
                        for (let i = 0; i < q.rows.length; i++) {
                            list.push(q.rows.item(i));
                        }
                    }
                    const stock=await getTotalStock(list,devises);
                    
                    element.buy=buy;
                    element.stock=stock;
                    rep=element;

                }
                
            }
        }
    } catch (error) {
    }
    return rep;

}

export async function showLast(){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        let query="SELECT articles.*,";
        query+="familles.name AS famille,devises.signe AS devise,familles.private_key AS famille_private_key,devises.private_key AS devise_private_key ";
        query+="FROM articles INNER JOIN familles ON articles.id_famille=familles.id ";
        query+="INNER JOIN devises ON articles.id_devise=devises.id ";
        //query+="WHERE articles.id="+id+" ";
        query+="GROUP BY articles.id ";
        query+="ORDER BY articles.id DESC limit 1 ";
        const result=await logiciel.ExecuteQuery(query);
        const devises=await DeviseModel.get();
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                const id=element.id;
                if(element.type!=S.ENUM.article.service || (element.type==S.ENUM.article.service && element.mode!=S.ENUM.stockage.devise)){
                    const p=await logiciel.ExecuteQuery(`select sum(quantity) as buy from buys where id_article=${id} group by id_article`);
                    const q=await logiciel.ExecuteQuery(`select sum(quantity) as stock from stocks where id_article=${id} group by id_article`);
                    let row=p.rows.item(0);
                    const buy=(row!=undefined && row!=null)?(row.buy==null || row==undefined)?null:row.buy:null;
                    row=q.rows.item(0);
                    const stock=(row!=undefined && row!=null)?(row.stock==null || row==undefined)?null:row.stock:null;
                    element.buy=buy;
                    element.stock=stock;
                    rep=element;
                }else{
                    let list=[];
                    const p=await logiciel.ExecuteQuery(`select * from buys where id_article=${id}`);
                    if(p.rows.length>0){
                        for (let i = 0; i < p.rows.length; i++) {
                            list.push(p.rows.item(i));
                        }
                    }
                    const buy=await getTotalBuy(list,devises);
                    const q=await logiciel.ExecuteQuery(`select * from stocks where id_article=${id}`);
                    list=[];
                    if(q.rows.length>0){
                        for (let i = 0; i < q.rows.length; i++) {
                            list.push(q.rows.item(i));
                        }
                    }
                    const stock=await getTotalStock(list,devises);
                    
                    element.buy=buy;
                    element.stock=stock;
                    rep=element;

                }
                
            }
        }
    } catch (error) {
    }
    return rep;

}



