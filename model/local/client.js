import Logiciel from '../../controller/database/logiciel';
import React from "react";
import CommandValues from '../../controller/commandValues';
import DateTime from 'date-and-time';
import { getTotalBuy } from '../../Manager';
import * as DeviseModel from "./devise";

const db_name="customers";

export async function store({name='',photo=null,phone="",identity=null,private_key=null,is_updated=0}){
   let rep=null;
    try {
        const cmd=new CommandValues();
        cmd.addValue('name',name);
        cmd.addValue('photo',photo);
        cmd.addValue('phone',phone);
        cmd.addValue('identity',identity);
        cmd.addValue('private_key',private_key);
        cmd.addValue('is_updated',is_updated);
        const query=cmd.getInsertCommand({list:cmd.getValues(),table:db_name});
        if(query!=null){
            const logiciel=new Logiciel();
            const table=await logiciel.ExecuteQuery(query);
            if(table.rowsAffected>0){
                rep=await showLast();
            }
        }
   } catch (error) {
       rep=null;
       console.log('erreur',error);
   }
   return rep;
}

export async function update({name=null,photo=null,phone=null,id=0,identity=null,private_key=null,is_updated=null}){
    let rep=null;
     try {
         const cmd=new CommandValues();
         cmd.addValue('name',name);
         cmd.addValue('photo',photo);
         cmd.addValue('phone',phone);
         cmd.addValue('identity',identity);
         cmd.addValue('private_key',private_key);
         cmd.addValue('is_updated',is_updated);
         const query=cmd.getUpdateCommand({list:cmd.getValues(),table:db_name,whereClause:"id="+id});
         if(query!=null){
             const logiciel=new Logiciel();
             const table=await logiciel.ExecuteQuery(query);
             if(table.rowsAffected>0){
                 rep=await show(id);
             }
         }
    } catch (error) {
        rep=false;
        console.log('error',error)
    }
    return rep;
 }


export async function get(){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT * FROM customers ORDER BY name ASC";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
    }
    return rep;
}

export async function showLast(){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        let query="SELECT * FROM customers ORDER BY id DESC limit 1";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            rep = result.rows.item(0);
        }
    } catch (error) {
    }
    return rep;
}

export async function show(id=0){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        let query="SELECT * FROM customers WHERE id="+id;
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            rep = result.rows.item(0);
        }
    } catch (error) {
    }
    return rep;
}


export async function getCustomers(){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT customers.* ";
        query+="FROM customers ";
        query+="ORDER BY customers.name ASC";
        const result=await logiciel.ExecuteQuery(query);
        const devises=await DeviseModel.get();
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                const p=await getCustomerBuy(element.id);
                let list=[];
                const q=await logiciel.ExecuteQuery(`select buys.*,articles.name,articles.description,articles.id_famille from buys inner join articles on articles.id=buys.id_article where id_customer=${element.id}`);
                if(q.rows.length>0){
                    for (let i = 0; i < q.rows.length; i++) {
                        list.push(q.rows.item(i));
                    }
                }
                const buy=await getTotalBuy(list,devises);
                element.buys=p;
                element.buy=buy;
                rep.push(element)
            }
        }
    } catch (error) {
        console.log('error',error);
    }
    return rep;
}

export async function getCustomerBuy(id=0){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT buys.*,articles.name,articles.description ";
        query+="FROM buys ";
        query+="INNER JOIN articles ON buys.id_article=articles.id ";
        query+="WHERE id_customer="+id+" ";
        query+="ORDER BY created_at DESC";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
        console.log('error',error);
    }
    return rep;
}





