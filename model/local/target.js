import Logiciel from '../../controller/database/logiciel';
import React from "react";
import CommandValues from '../../controller/commandValues';

const db_name="targets";

export async function store({description=null,activity=0,user=0,status="",private_key=null,is_updated=0}){
   let rep=false;
    try {
        const cmd=new CommandValues();
        cmd.addValue('description',description);
        cmd.addValue('id_activity',activity);
        cmd.addValue('id_agent',user);
        cmd.addValue('status',status);
        cmd.addValue('private_key',private_key);
        cmd.addValue('is_updated',is_updated);
        const query=cmd.getInsertCommand({list:cmd.getValues(),table:db_name});
        if(query!=null){
            const logiciel=new Logiciel();
            const table=await logiciel.ExecuteQuery(query);
            if(table.rowsAffected>0){
                rep=true;
            }
        }
   } catch (error) {
       rep=false;
   }
   return rep;
}


export async function update({id=0,description=null,activity=null,user=null,status=null,private_key=null,is_updated=null}){
    let rep=false;
     try {
         const cmd=new CommandValues();
         cmd.addValue('description',description);
         cmd.addValue('status',status);
         cmd.addValue('private_key',private_key);
         cmd.addValue('is_updated',is_updated);
         const query=cmd.getUpdateCommand({list:cmd.getValues(),table:db_name,whereClause:'id='+id});
         if(query!=null){
             const logiciel=new Logiciel();
             const table=await logiciel.ExecuteQuery(query);
             if(table.rowsAffected>0){
                 rep=true;
             }
         }
    } catch (error) {
        rep=false;
    }
    return rep;
 }

 /**
  * get all targets about an activity
  * @param {activity} id 
  * @returns 
  */
 export async function get(id=0){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT targets.*,agents.first_name,agents.last_name,agents.photo,agents.sex,";
        query+="activities.description as description,activities.created_at as created_at,"
        query+="activities.type as type,activities.recall_date,";
        query+="targets.description as tar_description ";
        query+="FROM targets INNER JOIN agents ON agents.id=targets.id_agent ";
        query+="INNER JOIN activities ON activities.id=targets.id_activity ";
        if(id>0){
            query+="WHERE targets.id_activity="+id+" ";
        }
        query+="ORDER BY agents.first_name ASC";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
        console.log('error',error);
    }
    return rep;
}

export async function getByUser(id=0){
    let rep=[];
    try {
        const logiciel=new Logiciel();
        let query="SELECT targets.*,agents.first_name,agents.last_name,agents.photo,agents.sex ";
        query+="FROM targets INNER JOIN activities ON activities.id=targets.id_activity ";
        query+="WHERE targets.id_agent="+id+" ";
        query+="ORDER BY activities.created_at ASC";
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.item.length>0){
            for (let index = 0; index < result.rows.length; index++) {
                const element = result.rows.item(index);
                rep.push(element)
            }
        }
    } catch (error) {
    }
    return rep;
}