import Logiciel from '../../controller/database/logiciel';
import React from "react";
import CommandValues from '../../controller/commandValues';


export async function store({name='',description=null,is_updated=0,image_updated=null,logo=null,country="",private_key=null,town='',longitude=null,latitude=null,number=null,phone=null,site=null,address=null}){
   let rep=false;
    try {
        const cmd=new CommandValues();
        cmd.addValue('name',name);
        cmd.addValue('logo',logo);
        cmd.addValue('country',country);
        cmd.addValue('town',town);
        cmd.addValue('longitude',longitude);
        cmd.addValue('latitude',latitude);
        cmd.addValue('number',number);
        cmd.addValue('phone',phone);
        cmd.addValue('site',site);
        cmd.addValue('address',address);
        cmd.addValue('description',description);
        cmd.addValue('is_updated',is_updated);
        cmd.addValue('private_key',private_key);
        cmd.addValue('image_updated',image_updated);
        const query=cmd.getInsertCommand({list:cmd.getValues(),table:'shops'});
        if(query!=null){
            const logiciel=new Logiciel();
            const table=await logiciel.ExecuteQuery(query);
            if(table.rowsAffected>0){
                rep=true;
            }
        }
   } catch (error) {
       rep=false;
   }
   return rep;
}

export async function update({id=0,description=null,name=null,image_updated=null,is_updated=null,private_key=null,logo=null,country=null,town=null,longitude=null,latitude=null,number=null,phone=null,site=null,address=null}){
    let rep=false;
     try {
         const cmd=new CommandValues();
         cmd.addValue('name',name);
         cmd.addValue('logo',logo);
         cmd.addValue('country',country);
         cmd.addValue('town',town);
         cmd.addValue('longitude',longitude);
         cmd.addValue('latitude',latitude);
         cmd.addValue('number',number);
         cmd.addValue('phone',phone);
         cmd.addValue('site',site);
         cmd.addValue('address',address);
         cmd.addValue('description',description);
         cmd.addValue('is_updated',is_updated);
         cmd.addValue('private_key',private_key);
         cmd.addValue('image_updated',image_updated);
         const query=cmd.getUpdateCommand({list:cmd.getValues(),table:'shops',whereClause:"id="+id});
         if(query!=null){
             const logiciel=new Logiciel();
             const table=await logiciel.ExecuteQuery(query);
             if(table.rowsAffected>0){
                 rep=true;
             }
         }
    } catch (error) {
        rep=false;
    }
    return rep;
 }

export async function get(){
    let rep=null;
    try {
        const logiciel=new Logiciel();
        const query="select * from shops limit 1;"
        const result=await logiciel.ExecuteQuery(query);
        if(result.rows.length>0){
            rep=result.rows.item(0);
        }
    } catch (error) {
       
    }
    return rep;
}

