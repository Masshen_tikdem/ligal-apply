import S from '../../resources/settings.json';

const network=S.NETWORK;

export async function store(request){
    let rep=null;
    let status=500;
    try {   
        const url=network+'api/users';
        
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body:request
        };
       
        const getter=await fetch(url,requestOptions);
        if(getter.status==200 || (getter.status>=400 && getter.status<500)){
            rep=await getter.json();
            status=getter.status;
        }
    } catch (error) {
        rep={error:error};
        status=1000;
    }
    return {response:rep,status};
}

export async function updatePhoto(id,request){
    let rep=null;
    let status=500;
    try {   
        const url=network+'api/users/photo/'+id;
        
        const requestOptions = {
            method: 'POST',
            body:request
        };
       
        const getter=await fetch(url,requestOptions);
        if(getter.status==200 || (getter.status>=400 && getter.status<500)){
            rep=await getter.json();
            status=getter.status;
        }
    } catch (error) {
        console.log('error UPDATE PHOTO',error);
        rep={error:error};
        status=1000;
    }
    return {response:rep,status};
}

export async function update(id,request){
    let rep=null;
    let status=500;
    try {   
        const url=network+'api/users/'+id;
        
        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body:request
        };
       
        const getter=await fetch(url,requestOptions);
        if(getter.status==200 || (getter.status>=400 && getter.status<500)){
            rep=await getter.json();
            status=getter.status;
        }
    } catch (error) {
        rep={error:error};
        status=1000;
    }
    return {response:rep,status};
}

export async function storeWork(request){
    let rep=null;
    let status=500;
    try {   
        const url=network+'api/users/work';
        
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body:request
        };
       
        const getter=await fetch(url,requestOptions);
        if(getter.status==200 || (getter.status>=400 && getter.status<500)){
            rep=await getter.json();
            status=getter.status;
        }
    } catch (error) {
        rep={error:error};
        status=1000;
    }
    return {response:rep,status};
}

export async function authentification(request){
    let rep=null;
    let status=500;
    try {   
        const url=network+'api/users/authentification';
        
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body:request
        };
       
        const getter=await fetch(url,requestOptions);
        if(getter.status==200 || (getter.status>=400 && getter.status<500)){
            rep=await getter.json();
            status=getter.status;
        }
    } catch (error) {
        rep={error:error};
        status=1000;
    }
    return {response:rep,status};
}

export async function search(request){
    let rep=null;
    let status=500;
    try {   
        const url=network+'api/users/search';
        
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body:request
        };
       
        const getter=await fetch(url,requestOptions);
        if(getter.status==200 || (getter.status>=400 && getter.status<500)){
            rep=await getter.json();
            status=getter.status;
        }
    } catch (error) {
        rep={error:error};
        status=1000;
    }
    return {response:rep,status};
}

export async function show(key){
    let rep=null;
    let status=500;
    try {   
        const url=network+'api/users/'+key;
        
        const requestOptions = {
            method: 'GET',
        };
       
        const getter=await fetch(url,requestOptions);
        if(getter.status==200 || (getter.status>=400 && getter.status<500)){
            rep=await getter.json();
            status=getter.status;
        }
    } catch (error) {
        rep={error:error};
        status=1000;
    }
    return {response:rep,status};
}