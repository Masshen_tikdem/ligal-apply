import S from '../../resources/settings.json';
const network=S.NETWORK;

export async function get(){
    let rep=null;
    let status=500;
    try {   
        const url=network+'api/info';
        
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
        };
       
        const getter=await fetch(url,requestOptions);
        if(getter.status==200 || (getter.status>=400 && getter.status<500)){
            rep=await getter.json();
            status=getter.status;
        }
    } catch (error) {
        rep={error:error};
        status=1000;
    }
    return {response:rep,status};
}