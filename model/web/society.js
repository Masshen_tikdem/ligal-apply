import S from '../../resources/settings.json';

const network=S.NETWORK;

export async function store(request){
    let rep=null;
    let status=500;
    if(typeof request === 'object'){
        request=JSON.stringify(request);
    }
    try {
        const url=network+'api/society';

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body:request
        };
        const getter=await fetch(url,requestOptions);
        if(getter.status==200 || (getter.status>=400 && getter.status<500)){
            rep=await getter.json();
            status=getter.status;
        }
    } catch (error) {
        rep={error:error};
        status=1000;
    }
    return {response:rep,status};
}

export async function update(id,request){
    let rep=null;
    let status=500;
    try {
        const url=network+'api/society/'+id;

        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body:request
        };

        const getter=await fetch(url,requestOptions);
        if(getter.status==200 || (getter.status>=400 && getter.status<500)){
            rep=await getter.json();
            status=getter.status;
        }
    } catch (error) {
        rep={error:error};
        status=1000;
    }
    return {response:rep,status};
}

export async function getUserShop(id){
    let rep=null;
    let status=500;
    try {
        const url=network+'api/society/user/'+id;

        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
        };

        const getter=await fetch(url,requestOptions);
        if(getter.status==200 || (getter.status>=400 && getter.status<500)){
            rep=await getter.json();
            status=getter.status;
        }
    } catch (error) {
        rep={error:error};
        status=1000;
    }
    return {response:rep,status};
}

export async function recover(id){
    let rep=null;
    let status=500;
    try {
        const url=network+'api/society/recover/'+id;

        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
        };

        const getter=await fetch(url,requestOptions);
        if(getter.status==200 || (getter.status>=400 && getter.status<500)){
            rep=await getter.json();
            status=getter.status;
        }
    } catch (error) {
        rep={error:error};
        status=1000;
    }
    return {response:rep,status};
}

export async function show(id){
    let rep=null;
    let status=500;
    try {
        const url=network+'api/society/'+id;

        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
        };

        const getter=await fetch(url,requestOptions);
        if(getter.status==200 || (getter.status>=400 && getter.status<500)){
            rep=await getter.json();
            status=getter.status;
        }
    } catch (error) {
        rep={error:error};
        status=1000;
    }
    return {response:rep,status};
}