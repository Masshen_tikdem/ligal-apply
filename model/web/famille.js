import S from '../../resources/settings.json';

const network=S.NETWORK;

export async function store(request){
    let rep=null;
    let status=500;
    try {   
        const url=network+'api/category';
        
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body:request
        };
       
        const getter=await fetch(url,requestOptions);
        if(getter.status==200 || (getter.status>=400 && getter.status<500)){
            rep=await getter.json();
            status=getter.status;
        }
    } catch (error) {
        rep={error:error};
        status=1000;
    }
    return {response:rep,status};
}

export async function show(key){
    let rep=null;
    let status=500;
    try {   
        const url=network+'api/category/'+key;
        
        const requestOptions = {
            method: 'GET',
        };
       
        const getter=await fetch(url,requestOptions);
        if(getter.status==200 || (getter.status>=400 && getter.status<500)){
            rep=await getter.json();
            status=getter.status;
        }
    } catch (error) {
        rep={error:error};
        status=1000;
    }
    return {response:rep,status};
}

export async function showLink(key){
    let rep=null;
    let status=500;
    try {   
        const url=network+'api/category/link/'+key;
        
        const requestOptions = {
            method: 'GET',
        };
       
        const getter=await fetch(url,requestOptions);
        if(getter.status==200 || (getter.status>=400 && getter.status<500)){
            rep=await getter.json();
            status=getter.status;
        }
    } catch (error) {
        rep={error:error};
        status=1000;
    }
    return {response:rep,status};
}

export async function updatePhoto(id,request){
    let rep=null;
    let status=500;
    try {   
        const url=network+'api/category/photo/'+id;
        
        const requestOptions = {
            method: 'POST',
            body:request
        };
       
        const getter=await fetch(url,requestOptions);
        if(getter.status==200 || (getter.status>=400 && getter.status<500)){
            rep=await getter.json();
            status=getter.status;
        }
    } catch (error) {
        rep={error:error};
        status=1000;
    }
    return {response:rep,status};
}

export async function storeLink(request){
    let rep=null;
    let status=500;
    try {   
        const url=network+'api/category/link';
        
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body:request
        };
       
        const getter=await fetch(url,requestOptions);
        if(getter.status==200 || (getter.status>=400 && getter.status<500)){
            rep=await getter.json();
            status=getter.status;
        }
    } catch (error) {
        rep={error:error};
        status=1000;
    }
    return {response:rep,status};
}

export async function update(id,request){
    let rep=null;
    let status=500;
    try {   
        const url=network+'api/category/'+id;
        
        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body:request
        };
       
        const getter=await fetch(url,requestOptions);
        if(getter.status==200 || (getter.status>=400 && getter.status<500)){
            rep=await getter.json();
            status=getter.status;
        }
    } catch (error) {
        rep={error:error};
        status=1000;
    }
    return {response:rep,status};
}

export async function updateLink(id,request){
    let rep=null;
    let status=500;
    try {   
        const url=network+'api/category/link/'+id;
        
        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body:request
        };
       
        const getter=await fetch(url,requestOptions);
        if(getter.status==200 || (getter.status>=400 && getter.status<500)){
            rep=await getter.json();
            status=getter.status;
        }
    } catch (error) {
        rep={error:error};
        status=1000;
    }
    return {response:rep,status};
}