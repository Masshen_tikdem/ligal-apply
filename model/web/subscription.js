import { getHeaders, http } from "../../config/http";

export async function store(request){
    let response=null;
    let status=500;
    const url='api/subscription';
    const rep=await http.post(url,request,{
        headers:getHeaders()
    });
    response=rep.data;
    status=rep.status;

    return {response,status};
}

export async function get(request){
    let response=null;
    let status=500;
    const url='api/subscription/user';
    const rep=await http.post(url,request,{
        headers:getHeaders()
    });
    response=rep.data;
    status=rep.status;
    return {response,status};
}

export async function show(id){
    let response=null;
    let status=500;
    const url='api/subscription/'+id;
    const rep=await http.get(url,{
        headers:getHeaders()
    });
    response=rep.data;
    status=rep.status;
    return {response,status};
}