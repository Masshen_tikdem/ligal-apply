import axios from "axios";
import S from '../resources/settings.json';

const http= axios.create({
    baseURL:S.NETWORK,
    timeout:5000
})

const getHeaders=()=>{
    return {
        'Content-type': 'application/json',
        //Authorization: 'Bearer ' + authServiceApi.getToken(),
    };
}

export {getHeaders,http};